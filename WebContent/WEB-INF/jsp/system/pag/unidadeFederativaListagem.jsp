<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:listagem>
	<t:janelaFiltro >
		<t:tabelaFiltro columns="1">
		<t:property name="pais" renderAs="doubleline"/>
		<t:property name="nome" renderAs="doubleline"/>
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados>
			<t:property name="nome"/>
			<t:property name="sigla"/>
			<t:property name="pais" label="Pa�s"/>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>

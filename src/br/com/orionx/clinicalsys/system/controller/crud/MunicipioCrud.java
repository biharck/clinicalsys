package br.com.orionx.clinicalsys.system.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;

import br.com.orionx.clinicalsys.bean.Municipio;
import br.com.orionx.clinicalsys.filtros.MunicipioFiltro;
import br.com.orionx.clinicalsys.util.generics.CrudController;

@Controller(path="/system/pag/Municipio",authorizationModule=CrudAuthorizationModule.class)
public class MunicipioCrud extends CrudController<MunicipioFiltro, Municipio, Municipio> {

	@Override
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Administração &raquo; <a href=\"/ClinicalSys/system/pag/Municipio\">Municipio</a></p>";
	}
	
	@Action(value = "buscamuni")
	protected void buscamuni(WebRequestContext request)throws Exception {
	}

	
	@Override
	protected void defaultSimpleAjaxRequest(WebRequestContext request) {
		super.defaultSimpleAjaxRequest(request);
	}
}

package br.com.orionx.clinicalsys.system.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;

import br.com.orionx.clinicalsys.bean.Feriado;
import br.com.orionx.clinicalsys.filtros.FeriadoFiltro;
import br.com.orionx.clinicalsys.util.generics.CrudController;

@Controller(path="/system/pag/Feriado",authorizationModule=CrudAuthorizationModule.class)
public class FeriadoCrud extends CrudController<FeriadoFiltro, Feriado, Feriado>{

	@Override
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Administração &raquo; <a href=\"/ClinicalSys/system/pag/Feriado\">Feriados</a></p>";
	}

}

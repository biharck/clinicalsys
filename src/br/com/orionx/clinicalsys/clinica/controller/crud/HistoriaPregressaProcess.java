package br.com.orionx.clinicalsys.clinica.controller.crud;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

import br.com.orionx.clinicalsys.bean.Alimentacao;
import br.com.orionx.clinicalsys.bean.CrescimentoPaciente;
import br.com.orionx.clinicalsys.bean.DemaisDoencas;
import br.com.orionx.clinicalsys.bean.DoencasDaInfancia;
import br.com.orionx.clinicalsys.bean.HistoriaPregressa;
import br.com.orionx.clinicalsys.bean.ImunizacoesInfancia;
import br.com.orionx.clinicalsys.bean.MedicamentosEmUsoPaciente;
import br.com.orionx.clinicalsys.bean.Paciente;
import br.com.orionx.clinicalsys.dao.AlimentacaoDAO;
import br.com.orionx.clinicalsys.dao.CrescimentoPacienteDAO;
import br.com.orionx.clinicalsys.dao.DemaisDoencasDAO;
import br.com.orionx.clinicalsys.dao.DoencasDaInfanciaDAO;
import br.com.orionx.clinicalsys.dao.ImunizacoesInfanciaDAO;
import br.com.orionx.clinicalsys.service.AlimentacaoService;
import br.com.orionx.clinicalsys.service.ClassificacaoMedicacaoService;
import br.com.orionx.clinicalsys.service.DemaisDoencasService;
import br.com.orionx.clinicalsys.service.DoencasDaInfanciaService;
import br.com.orionx.clinicalsys.service.HistoriaPregressaService;
import br.com.orionx.clinicalsys.service.ImunizacoesInfanciaService;
import br.com.orionx.clinicalsys.service.MedicamentosEmUsoPacienteService;
import br.com.orionx.clinicalsys.service.PacienteService;

@Controller(path="/clinic/process/historiaPregressa",authorizationModule=ProcessAuthorizationModule.class)
public class HistoriaPregressaProcess extends MultiActionController{

	private PacienteService pacienteService;
	private DoencasDaInfanciaService doencasDaInfanciaService;
	private DemaisDoencasService demaisDoencasService;
	private AlimentacaoService alimentacaoService;
	private HistoriaPregressaService historiaPregressaService;
	private AlimentacaoDAO alimentacaoDAO;
	private DoencasDaInfanciaDAO doencasDaInfanciaDAO;
	private DemaisDoencasDAO demaisDoencasDAO;
	private ClassificacaoMedicacaoService classificacaoMedicacaoService;
	private MedicamentosEmUsoPacienteService medicamentosEmUsoPacienteService;
	private ImunizacoesInfanciaService imunizacoesInfanciaService;
	private ImunizacoesInfanciaDAO imunizacoesInfanciaDAO;
	private CrescimentoPacienteDAO crescimentoPacienteDAO;
	
	public void setPacienteService(PacienteService pacienteService) {
		this.pacienteService = pacienteService;
	}
	public void setDoencasDaInfanciaService(DoencasDaInfanciaService doencasDaInfanciaService) {
		this.doencasDaInfanciaService = doencasDaInfanciaService;
	}
	public void setDemaisDoencasService(DemaisDoencasService demaisDoencasService) {
		this.demaisDoencasService = demaisDoencasService;
	}
	public void setAlimentacaoService(AlimentacaoService alimentacaoService) {
		this.alimentacaoService = alimentacaoService;
	}
	public void setHistoriaPregressaService(HistoriaPregressaService historiaPregressaService) {
		this.historiaPregressaService = historiaPregressaService;
	}
	public void setAlimentacaoDAO(AlimentacaoDAO alimentacaoDAO) {
		this.alimentacaoDAO = alimentacaoDAO;
	}
	public void setDoencasDaInfanciaDAO(DoencasDaInfanciaDAO doencasDaInfanciaDAO) {
		this.doencasDaInfanciaDAO = doencasDaInfanciaDAO;
	}
	public void setDemaisDoencasDAO(DemaisDoencasDAO demaisDoencasDAO) {
		this.demaisDoencasDAO = demaisDoencasDAO;
	}
	public void setClassificacaoMedicacaoService(ClassificacaoMedicacaoService classificacaoMedicacaoService) {
		this.classificacaoMedicacaoService = classificacaoMedicacaoService;
	}
	public void setMedicamentosEmUsoPacienteService(MedicamentosEmUsoPacienteService medicamentosEmUsoPacienteService) {
		this.medicamentosEmUsoPacienteService = medicamentosEmUsoPacienteService;
	}
	public void setImunizacoesInfanciaService(ImunizacoesInfanciaService imunizacoesInfanciaService) {
		this.imunizacoesInfanciaService = imunizacoesInfanciaService;
	}
	public void setImunizacoesInfanciaDAO(ImunizacoesInfanciaDAO imunizacoesInfanciaDAO) {
		this.imunizacoesInfanciaDAO = imunizacoesInfanciaDAO;
	}
	public void setCrescimentoPacienteDAO(CrescimentoPacienteDAO crescimentoPacienteDAO) {
		this.crescimentoPacienteDAO = crescimentoPacienteDAO;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request){
		Paciente p = new Paciente();
		p.setIdPaciente(Integer.parseInt(getParameter("idPaciente")));
		p = pacienteService.getPacienteProntuario(p);
		request.setAttribute("pathURL", getPathURL(p.getIdPaciente()));
		request.setAttribute("paciente", p);
		HistoriaPregressa hp = historiaPregressaService.getByPaciente(p);
		List<DoencasDaInfancia> de = doencasDaInfanciaService.findAll();
		List<DemaisDoencas> dd = demaisDoencasService.findAll();
		List<Alimentacao> alimentacoes = alimentacaoService.findAll();
		List<ImunizacoesInfancia> i0meses = imunizacoesInfanciaDAO.getByMeses(0);
		List<ImunizacoesInfancia> i1meses = imunizacoesInfanciaDAO.getByMeses(1);
		List<ImunizacoesInfancia> i2meses = imunizacoesInfanciaDAO.getByMeses(2);
		List<ImunizacoesInfancia> i3meses = imunizacoesInfanciaDAO.getByMeses(3);
		List<ImunizacoesInfancia> i4meses = imunizacoesInfanciaDAO.getByMeses(4);
		List<ImunizacoesInfancia> i5meses = imunizacoesInfanciaDAO.getByMeses(5);
		List<ImunizacoesInfancia> i6meses = imunizacoesInfanciaDAO.getByMeses(6);
		List<ImunizacoesInfancia> i9meses = imunizacoesInfanciaDAO.getByMeses(9);
		List<ImunizacoesInfancia> i12meses = imunizacoesInfanciaDAO.getByMeses(12);
		List<ImunizacoesInfancia> i15meses = imunizacoesInfanciaDAO.getByMeses(15);
		List<ImunizacoesInfancia> i48meses = imunizacoesInfanciaDAO.getByMeses(48);
		List<ImunizacoesInfancia> i120meses = imunizacoesInfanciaDAO.getByMeses(120);
		if(hp == null){
			hp = new HistoriaPregressa();
			request.setAttribute("listadoencas", de);
			request.setAttribute("listademaisdoencas", dd);
			request.setAttribute("listaalimentacao", alimentacoes);
			request.setAttribute("listaImunizacoesInfancia0Meses", i0meses);
			request.setAttribute("listaImunizacoesInfancia1Meses", i1meses);
			request.setAttribute("listaImunizacoesInfancia2Meses", i2meses);
			request.setAttribute("listaImunizacoesInfancia3Meses", i3meses);
			request.setAttribute("listaImunizacoesInfancia4Meses", i4meses);
			request.setAttribute("listaImunizacoesInfancia5Meses", i5meses);
			request.setAttribute("listaImunizacoesInfancia6Meses", i6meses);
			request.setAttribute("listaImunizacoesInfancia9Meses", i9meses);
			request.setAttribute("listaImunizacoesInfancia12Meses", i12meses);
			request.setAttribute("listaImunizacoesInfancia15Meses", i15meses);
			request.setAttribute("listaImunizacoesInfancia48Meses", i48meses);
			request.setAttribute("listaImunizacoesInfancia120Meses", i120meses);
			return new ModelAndView("direct:process/historiaPregressa","hp",hp);
		}
		
		hp.setAlimentacaosTransient(alimentacaoDAO.findByHistoriaPregressa(hp));
		hp.setDoencasDaInfanciasTransient(doencasDaInfanciaDAO.findByHistoriaPregressa(hp));
		hp.setDemaisDoencasTransient(demaisDoencasDAO.findByHistoriaPregressa(hp));
		hp.setImunizacoesTransient(imunizacoesInfanciaDAO.findByHistoriaPregressa(hp));
		
		for (DoencasDaInfancia doencasDaInfancia : de) {
			if(hp.getDoencasDaInfanciasTransient().contains(doencasDaInfancia))
				doencasDaInfancia.setChecked(true);
		}
		request.setAttribute("listadoencas", de);
		
		
		for (DemaisDoencas demaisDoencas : dd) {
			if(hp.getDemaisDoencasTransient().contains(demaisDoencas))
				demaisDoencas.setChecked(true);
		}
		request.setAttribute("listademaisdoencas", dd);
		
		for (Alimentacao alimentacao : alimentacoes) {
			if(hp.getAlimentacaosTransient().contains(alimentacao))
				alimentacao.setChecked(true);
		}
		request.setAttribute("listaalimentacao", alimentacoes);
		
		hp.setImunizacoesTransient(imunizacoesInfanciaDAO.findByHistoriaPregressa(hp));
		for (ImunizacoesInfancia imunizacoesInfancia : i0meses) {
			if(hp.getImunizacoesTransient().contains(imunizacoesInfancia))
				imunizacoesInfancia.setChecked(true);
		}
		request.setAttribute("listaImunizacoesInfancia0Meses", i0meses);
		
		for (ImunizacoesInfancia imunizacoesInfancia : i1meses) {
			if(hp.getImunizacoesTransient().contains(imunizacoesInfancia))
				imunizacoesInfancia.setChecked(true);
		}
		request.setAttribute("listaImunizacoesInfancia1Meses", i1meses);
		
		for (ImunizacoesInfancia imunizacoesInfancia : i2meses) {
			if(hp.getImunizacoesTransient().contains(imunizacoesInfancia))
				imunizacoesInfancia.setChecked(true);
		}
		request.setAttribute("listaImunizacoesInfancia2Meses", i2meses);
		
		for (ImunizacoesInfancia imunizacoesInfancia : i3meses) {
			if(hp.getImunizacoesTransient().contains(imunizacoesInfancia))
				imunizacoesInfancia.setChecked(true);
		}
		request.setAttribute("listaImunizacoesInfancia3Meses", i3meses);
		
		for (ImunizacoesInfancia imunizacoesInfancia : i4meses) {
			if(hp.getImunizacoesTransient().contains(imunizacoesInfancia))
				imunizacoesInfancia.setChecked(true);
		}
		request.setAttribute("listaImunizacoesInfancia4Meses", i4meses);
		
		for (ImunizacoesInfancia imunizacoesInfancia : i5meses) {
			if(hp.getImunizacoesTransient().contains(imunizacoesInfancia))
				imunizacoesInfancia.setChecked(true);
		}
		request.setAttribute("listaImunizacoesInfancia5Meses", i5meses);
		
		for (ImunizacoesInfancia imunizacoesInfancia : i6meses) {
			if(hp.getImunizacoesTransient().contains(imunizacoesInfancia))
				imunizacoesInfancia.setChecked(true);
		}
		request.setAttribute("listaImunizacoesInfancia6Meses", i6meses);
		
		for (ImunizacoesInfancia imunizacoesInfancia : i9meses) {
			if(hp.getImunizacoesTransient().contains(imunizacoesInfancia))
				imunizacoesInfancia.setChecked(true);
		}
		request.setAttribute("listaImunizacoesInfancia9Meses", i9meses);
		
		for (ImunizacoesInfancia imunizacoesInfancia : i12meses) {
			if(hp.getImunizacoesTransient().contains(imunizacoesInfancia))
				imunizacoesInfancia.setChecked(true);
		}
		request.setAttribute("listaImunizacoesInfancia12Meses", i12meses);
		
		for (ImunizacoesInfancia imunizacoesInfancia : i15meses) {
			if(hp.getImunizacoesTransient().contains(imunizacoesInfancia))
				imunizacoesInfancia.setChecked(true);
		}
		request.setAttribute("listaImunizacoesInfancia15Meses", i15meses);
		
		for (ImunizacoesInfancia imunizacoesInfancia : i48meses) {
			if(hp.getImunizacoesTransient().contains(imunizacoesInfancia))
				imunizacoesInfancia.setChecked(true);
		}
		request.setAttribute("listaImunizacoesInfancia48Meses", i48meses);
		
		for (ImunizacoesInfancia imunizacoesInfancia : i120meses) {
			if(hp.getImunizacoesTransient().contains(imunizacoesInfancia))
				imunizacoesInfancia.setChecked(true);
		}
		request.setAttribute("listaImunizacoesInfancia120Meses", i120meses);
		
		if(hp.getInteracoes()!=null){
			hp.setInteracoes(medicamentosEmUsoPacienteService.getByHistoriaPregressa(hp));
			for (MedicamentosEmUsoPaciente mup : hp.getInteracoes()) {
				mup.setClassificacaoMedicacao(classificacaoMedicacaoService.getClassificacaoByMedicamento(mup.getMedicamento()));
			}
		}
		
		hp.setListaCrescimentoPacienteTransient(crescimentoPacienteDAO.findByHistoriaPregressa(hp));
		hp.setListaCrescimentoPaciente(new ArrayList<CrescimentoPaciente>());
		/**
		 * calculando o IMC
		 */
		for (CrescimentoPaciente cp : hp.getListaCrescimentoPacienteTransient()) {
			Double imc = cp.getPeso() / (Math.pow(cp.getAltura(),2));
			imc = doubleParaMoeda(imc);
			imc = imc * 100;
			if(imc < 18.5){
				cp.setDescricao("Abaixo do peso ideal");
			}else if(imc >= 18.5 && imc <= 24.9){
				cp.setDescricao("Peso Ideal");
			}else if(imc >= 25 && imc <= 34.9){
				cp.setDescricao("Sobrepeso");
			}else if(imc >= 30.0 && imc <= 34.9){
				cp.setDescricao("Obesidade grau 1");
			}else if(imc >= 35.0 && imc <= 39.9){
				cp.setDescricao("Obesidade grau 2");
			}else if(imc >= 40.0){
				cp.setDescricao("Obesidade grau 3");
			}
			cp.setImc(imc.floatValue());
			
			Float peso = cp.getPeso()/1000;
			Float altura = cp.getAltura()/100;
			Float cir = cp.getCircAbdominal();
			if(cir!=null){
				cir = cir / 100;
				cp.setCircAbdominalString(new BigDecimal(cir).setScale(2,BigDecimal.ROUND_HALF_UP).toString());
			}
			
			cp.setPesoString(new BigDecimal(peso).setScale(3,BigDecimal.ROUND_HALF_UP).toString());
			cp.setAlturaString(new BigDecimal(altura).setScale(2,BigDecimal.ROUND_HALF_UP).toString());
			
		}
		
		
		return new ModelAndView("direct:process/historiaPregressa","hp",hp);
	}
	
	public double doubleParaMoeda(double value){  
        double currency = new BigDecimal(value).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue();  
        return currency;  
    }  
	
	public String getPathURL(Integer id) {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; <a href=\"/ClinicalSys/clinic/process/prontuario?idPaciente="+id+"\">Prontuário</a> &raquo; <a href=\"/ClinicalSys/clinic/process/historiaPregressa?idPaciente="+id+"\">História Pregressa</a></p>";
	}
	
	@Action("salvar")
	public ModelAndView salvar(WebRequestContext request, HistoriaPregressa hp){
		try {
			request.getAttribute("idPaciente");
			Paciente p = new Paciente();
			p.setIdPaciente(Integer.parseInt(getParameter("idPaciente")));
			hp.setPaciente(p);
			
			
			hp.setListaCrescimentoPacienteTransient(crescimentoPacienteDAO.findByHistoriaPregressa(hp));
			if(hp.getListaCrescimentoPaciente()!=null && hp.getListaCrescimentoPacienteTransient()!=null){
				hp.getListaCrescimentoPaciente().addAll(hp.getListaCrescimentoPacienteTransient());
			}
			
			historiaPregressaService.saveOrUpdate(hp);
			request.addMessage("Registro salvo com sucesso.");
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Ocorreu um erro ao salvar o registro :"+e.getMessage());
		}
		return index(request);
	}
}

package br.com.orionx.clinicalsys.service;

import java.util.List;

import br.com.orionx.clinicalsys.bean.Colaborador;
import br.com.orionx.clinicalsys.bean.ColaboradorAgenda;
import br.com.orionx.clinicalsys.bean.TipoAgenda;
import br.com.orionx.clinicalsys.dao.ColaboradorAgendaDAO;
import br.com.orionx.clinicalsys.util.generics.GenericService;

public class ColaboradorAgendaService extends GenericService<ColaboradorAgenda>{

	private ColaboradorAgendaDAO colaboradorAgendaDAO;
	
	public void setColaboradorAgendaDAO(ColaboradorAgendaDAO colaboradorAgendaDAO) {
		this.colaboradorAgendaDAO = colaboradorAgendaDAO;
	}
	
	/**
	 * <p>M�todo de refer�ncia ao DAO
	 * @return {@link List} {@link ColaboradorAgenda}
	 */
	public List<ColaboradorAgenda> getAgendaColaboradores(){
		return colaboradorAgendaDAO.getAgendaColaboradores();
	}
	
	public ColaboradorAgenda getByColaborador(Colaborador colaborador,TipoAgenda tipoAgenda){
		return colaboradorAgendaDAO.getByColaborador(colaborador, tipoAgenda);
	}
	public ColaboradorAgenda getByColaborador(Colaborador colaborador){
		return colaboradorAgendaDAO.getByColaborador(colaborador);
	}
	
	public ColaboradorAgenda getColaboradorAgendaAndColaborador(ColaboradorAgenda ca){
		return colaboradorAgendaDAO.getColaboradorAgendaAndColaborador(ca);
	}
	
}

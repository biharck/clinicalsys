package br.com.orionx.clinicalsys.dao;

import java.util.List;

import br.com.orionx.clinicalsys.bean.HistoriaPregressa;
import br.com.orionx.clinicalsys.bean.MedicamentosEmUsoPaciente;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class MedicamentosEmUsoPacienteDAO extends GenericDAO<MedicamentosEmUsoPaciente>{

	
	public List<MedicamentosEmUsoPaciente> getByHistoriaPregressa(HistoriaPregressa hp){
		return query()
			.select("medicamentosEmUsoPaciente.idMedicamentosEmUsoPaciente, medicamentosEmUsoPaciente.posologia," +
					"hp.idHistoriaPregressa, medicamento.idMedicamento, medicamento.principioAtivo")
			.leftOuterJoin("medicamentosEmUsoPaciente.historiaPregressa hp")
			.leftOuterJoin("medicamentosEmUsoPaciente.medicamento medicamento")
			.where("hp=?",hp)
			.list();
	}
}

package br.com.orionx.clinicalsys.service;

import java.util.List;

import br.com.orionx.clinicalsys.bean.CID10Grupo;
import br.com.orionx.clinicalsys.dao.CID10GrupoDAO;
import br.com.orionx.clinicalsys.util.generics.GenericService;

public class CID10GrupoService extends GenericService<CID10Grupo> {

	private CID10GrupoDAO grupoDAO;
	
	public void setGrupoDAO(CID10GrupoDAO grupoDAO) {
		this.grupoDAO = grupoDAO;
	}
	
	/**
	 * <p>M�todo que faz refer�ncia ao dao
	 * @param catInic categoria inicial {@link String}
	 * @param catFim categoria final {@link String}
	 * @see br.com.orionx.clinicalsys.dao.CID10GrupoDAO#getGruposByCapitulo
	 * @return {@link List} {@link CID10Grupo}
	 */
	public List<CID10Grupo> getGruposByCapitulo(String catInic, String catFim){
		return grupoDAO.getGruposByCapitulo(catInic, catFim);
	}
}

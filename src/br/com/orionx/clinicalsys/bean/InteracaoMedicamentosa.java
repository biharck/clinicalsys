package br.com.orionx.clinicalsys.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.Required;

import br.com.orionx.clinicalsys.util.log.Log;


@Entity
@Table(name = "interacaomedicamentosa")
public class InteracaoMedicamentosa implements Log{
	
	private Integer idInteracaoMedicamentosa;
	private ClassificacaoMedicacao classificacaoMedicacao;
	private Medicamento medicamento;
	private List<InteracaoEntreMedicamentos> interacoes;
	private List<InteracaoEntreClassificacoes> interacoesClass;
	private Boolean ativo;
	
	//Log
	private Integer idUsuarioAltera;
	private Timestamp dtAltera;
	
	//GET
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getIdInteracaoMedicamentosa() {
		return idInteracaoMedicamentosa;
	}
	@Transient
	@Required
	public ClassificacaoMedicacao getClassificacaoMedicacao() {
		return classificacaoMedicacao;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idMedicamento")
	@Required
	public Medicamento getMedicamento() {
		return medicamento;
	}
	@OneToMany(mappedBy = "interacaoMedicamentosa")
	@Required
	@DisplayName("Interações entre medicamentos")
	public List<InteracaoEntreMedicamentos> getInteracoes() {
		return interacoes;
	}
	@OneToMany(mappedBy = "interacaoMedicamentosa")
	@Required
	@DisplayName("Interações entre classificações")
	public List<InteracaoEntreClassificacoes> getInteracoesClass() {
		return interacoesClass;
	}
	public void setInteracoesClass(List<InteracaoEntreClassificacoes> interacoesClass) {
		this.interacoesClass = interacoesClass;
	}
	@Required
	public Boolean getAtivo() {
		return ativo;
	}
	
	//SET
	public void setIdInteracaoMedicamentosa(Integer idInteracaoMedicamentosa) {
		this.idInteracaoMedicamentosa = idInteracaoMedicamentosa;
	}
	public void setClassificacaoMedicacao(ClassificacaoMedicacao classificacaoMedicacao) {
		this.classificacaoMedicacao = classificacaoMedicacao;
	}
	public void setMedicamento(Medicamento medicamento) {
		this.medicamento = medicamento;
	}
	public void setInteracoes(List<InteracaoEntreMedicamentos> interacoes) {
		this.interacoes = interacoes;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	//LOG
	@Override
	public void setIdUsuarioAltera(Integer idUsuarioAltera) {
		this.idUsuarioAltera = idUsuarioAltera;
	}

	@Override
	public void setDtAltera(Timestamp dtAltera) {
		this.dtAltera = dtAltera;
	}

	@Override
	public Integer getIdUsuarioAltera() {
		return idUsuarioAltera;
	}

	@Override
	public Timestamp getDtAltera() {
		return dtAltera;
	}
	

}

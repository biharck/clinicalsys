package br.com.orionx.clinicalsys.service;

import br.com.orionx.clinicalsys.bean.ClassificacaoMedicacao;
import br.com.orionx.clinicalsys.bean.Medicamento;
import br.com.orionx.clinicalsys.dao.ClassificacaoMedicacaoDAO;
import br.com.orionx.clinicalsys.util.generics.GenericService;

public class ClassificacaoMedicacaoService extends GenericService<ClassificacaoMedicacao>{

	private ClassificacaoMedicacaoDAO classificacaoMedicacaoDAO;
	public void setClassificacaoMedicacaoDAO(ClassificacaoMedicacaoDAO classificacaoMedicacaoDAO) {
		this.classificacaoMedicacaoDAO = classificacaoMedicacaoDAO;
	}
	
	/**
	 * <p>M�todo de refer�ncia ao DAO
	 * @param m {@link Medicamento}
	 * @return {@link ClassificacaoMedicacao}
	 */
	public ClassificacaoMedicacao getClassificacaoByMedicamento(Medicamento m){
		return classificacaoMedicacaoDAO.getClassificacaoByMedicamento(m);
	}
	
}

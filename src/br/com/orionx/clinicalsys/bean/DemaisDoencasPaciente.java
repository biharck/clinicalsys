package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name= "demaisdoencaspaciente")
public class DemaisDoencasPaciente {
	
	private Integer idDemaisDoencasPaciente;
	private DemaisDoencas demaisDoencas;
	private HistoriaPregressa historiaPregressa;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getIdDemaisDoencasPaciente() {
		return idDemaisDoencasPaciente;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idDemaisDoencas")
	@ForeignKey(name = "fk_demaisdoencas_pct")
	public DemaisDoencas getDemaisDoencas() {
		return demaisDoencas;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idHistoriaPregressa")
	@ForeignKey(name = "fk_pct_demaisdoencas")
	public HistoriaPregressa getHistoriaPregressa() {
		return historiaPregressa;
	}
	public void setIdDemaisDoencasPaciente(Integer idDemaisDoencasPaciente) {
		this.idDemaisDoencasPaciente = idDemaisDoencasPaciente;
	}
	public void setDemaisDoencas(DemaisDoencas demaisDoencas) {
		this.demaisDoencas = demaisDoencas;
	}
	public void setHistoriaPregressa(HistoriaPregressa historiaPregressa) {
		this.historiaPregressa = historiaPregressa;
	}
	
	

}

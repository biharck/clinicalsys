<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"> 
<%@page import="br.com.orionx.clinicalsys.util.ClinicalSysUtil"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"> 
 
<!-- Website Title --> 
<title>ClinicalSys</title>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="cs" uri="clinicalsys"%>

<!-- Template stylesheet -->
<link id="screenColor" href="${application}/css/blue/screen.css" rel="stylesheet" type="text/css" media="all">
<link id="datepickerColor" href="${application}/css/blue/datepicker.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/tipsy.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/js/visualize/visualize.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/js/jwysiwyg/jquery.wysiwyg.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/js/fancybox/jquery.fancybox-1.3.0.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/tipsy.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/autocomplete.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/default.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/ui.datepicker.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/ui.theme.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/ui.dialog.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/dtree.css" rel="stylesheet" type="text/css" media="all">


<!--[if IE]>
	<link href="${application}/css/ie.css" rel="stylesheet" type="text/css" media="all">
	<script type="text/javascript" src="${application}/js/excanvas.js"></script>
<![endif]-->

<!-- Jquery and plugins -->
<script type="text/javascript" src="${application}/js/jquery.js"></script>
<script type="text/javascript" src="${application}/js/jquery-ui.js"></script>
<script type="text/javascript" src="${application}/js/jquery.img.preload.js"></script>
<script type="text/javascript" src="${application}/js/hint.js"></script>
<script type="text/javascript" src="${application}/js/visualize/jquery.visualize.js"></script>
<script type="text/javascript" src="${application}/js/jwysiwyg/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="${application}/js/fancybox/jquery.fancybox-1.3.0.js"></script>
<script type="text/javascript" src="${application}/js/jquery.tipsy.js"></script>
<script type="text/javascript" src="${application}/js/custom_blue.js"></script>
<script type="text/javascript" src="${application}/js/validate.js"></script>
<script type="text/javascript" src="${application}/js/functions.js"></script>
<script type="text/javascript" src="${application}/js/dtree.js"></script>
<script type="text/javascript" src="${application}/js/util.js"></script>
<script type="text/javascript" src="${application}/js/ajax.js"></script>
<script type="text/javascript" src="${application}/js/input.js"></script>
<script type="text/javascript" src="${application}/js/dynatable.js"></script>
<script type="text/javascript" src="${application}/js/clinicalSysUtil.js"></script>
<script type="text/javascript" src="${application}/js/autocomplete/jquery.bgiframe.min.js"></script>
<script type="text/javascript" src="${application}/js/autocomplete/jquery.autocomplete.js"></script>
<!-- lib do qtip -->
<script type="text/javascript" src="${application}/js/jquery-qtip-1.0.0-rc3140944/jquery.qtip-1.0.0-rc3.min.js"></script>


</head>
<body class="nobg">
<a name="topo"></a>
<div class="content_wrapper">

	<!-- Begin header -->
	<div id="header">
		<div id="logo">
			<img src="${application}/images/logo.png" alt="logo"/>
		</div>
		<c:if test="${param.fromInsertOne != 'true'}">
			<c:if test="${param.onePath != 'true'}">
				<div id="search">
					<form action="${application}/clinic/process/prontuario" id="search_form" name="search_form" method="get">
						<input type="text" id="paciente" onkeyUp="ajaxGetName(this.value)" onChange="verificaExistencia()" name="what" title="Procurar" class="search noshadow" />
						<input type="hidden" name="autocomplete_id" id="autocomplete_id"/>
					</form>
				</div>
			</c:if>
		</c:if>
		<div id="account_info">
			<img src="${application}/images/icon_online.png" alt="Online" class="mid_align"/>
			Ol� <a href="">Dr. <%= ClinicalSysUtil.getUsuarioLogado().getLogin() %></a>
			<c:if test="${param.fromInsertOne != 'true'}">
				<c:if test="${param.onePath != 'true'}">
			 		(<a href="">1 nova menssagem</a>) | <a href="${application}/system/Logout">Logout</a>
			 	</c:if>
			</c:if>
			&nbsp;&nbsp;&nbsp;&nbsp;Sua sess�o expira em: <span id="cronometro_tempo"></span>
		</div>
	</div>
	<!-- End header -->
	
	<!-- Begin content -->
	<div id="content" style="margin-left:30px;">
		<div class="inner">
			<c:if test="${param.ACAO != 'criar'}">
				<c:if test="${param.fromInsertOne != 'true'}">
					<c:if test="${param.onePath != 'true'}">
						<i id="pathURL">${pathURL}</i>
					</c:if>
				</c:if>
			</c:if>
			
			<br class="clear"/>
			<div class="messageOuterDiv">
				<n:messages/> <%-- Imprime as mensagens do sistema --%>
			</div>
			<!-- conte�do da aba -->
			<div class="onecolumn">
				<t:tela>
					<div class="header">
						<span>${paciente.nome}</span>
						<div class="switch" style="width:220px">
							
						<!-- 
							<table width="100px" cellpadding="0" cellspacing="0">
							<tbody>
								<tr>
									<td>
										<input type="button" id="tab1" name="tab1" class="left_switch active" value="Resumo" style="width:80px"/>
									</td>
									<td>
										<input type="button" id="tab2" name="tab2" class="middle_switch" value="Hist�rico" style="width:80px"/>
									</td>
									<td>
										<input type="button" id="tab3" name="tab3" class="right_switch" value="Tab3" style="width:50px"/>
									</td>
								</tr>
							</tbody>
							</table>
							 -->
						</div>
					</div>
					<br class="clear"/>
					<div class="content">
						<div id="tab1_content" class="tab_content">
							<ol class="style">
	            	  			<div class="headerDiv">
	            	  				<c:if test="${!empty paciente.fotoPaciente}">
										<img src="${pageContext.request.contextPath}/DOWNLOADFILE/${paciente.fotoPaciente.cdfile}" id="imagem" class="foto"/><br />
									</c:if>
									<c:if test="${empty paciente.fotoPaciente}">
										<img src="${pageContext.request.contextPath}/images/peopleNotFound.png" id="imagem" class="foto"/><br />
									</c:if>
									<div align="left">
										<table style='text-align:justify;'>
											<tr>
												<td>
													<b>Nome:</b>&nbsp;${paciente.nome}
												</td>
											</tr>
											<tr>
												<td>
													<b>Idade:</b>&nbsp;${cs:getidade(paciente.dtnascimento)} Anos					
												</td>
											</tr>
										</table>
									</div>					
								</div>
	            			</ol>
	            			<p style="color:#666;">
	            				<c:if test="${paciente.sexo.idSexo == 1}">
	            					<img src="/ClinicalSys/images/male.png">
	            				</c:if>
	            				<c:if test="${paciente.sexo.idSexo == 2}">
	            					<img src="/ClinicalSys/images/female.png">
	            				</c:if>
								<span class="reference">${paciente.nome}</span>, � <span class="reference">${paciente.estadoCivil.nome}</span>, tem <span class="reference">${cs:getidade(paciente.dtnascimento)}</span> anos de idade, filho de <span class="reference">${paciente.mae}</span>, sexo <span class="reference">${paciente.sexo.nome}</span>.<br />
								Sua profiss�o � <span class="reference">${paciente.profissao}</span>.<br />
								Natural de <span class="reference">${paciente.naturalidade}</span>, atualmente reside em <span class="reference">${paciente.tipoLogradouro.descricao}</span> <span class="reference">${paciente.logradouro}</span>, n�mero <span class="reference">${paciente.numero}</span>, bairro <span class="reference">${paciente.bairro}</span>,
								<span class="reference">${paciente.municipio.nome}</span> - <span class="reference">${paciente.municipio.unidadeFederativa.sigla}</span>.<br />
								<span class="reference">${paciente.nome}</span> possui os seguintes planos de sa�de:<br />
								<c:forEach items="${paciente.planos}" var="plano" >
									<span class="reference">${plano.plano.nome}</span> - <span class="reference">${plano.plano.operadora.razaoSocial}<br />
								</c:forEach>
							</p>
							<br class="clear"/>
						</div>
						<div id="tab2_content" class="tab_content hide">
							<h4>Headline of tab 1</h4>
							<ol class="style">
	            	  			<li>Cupidatat non</li>
	            	 			<li>Officia deserunt mollit</li>
	            	  			<li>Velit esse cillum</li>
	            			</ol>
	            			<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in porta lectus. Maecenas dignissim enim quis ipsum 
								mattis aliquet. Maecenas id velit et elit gravida bibendum. Duis nec rutrum lorem. Donec egestas metus a risus 
								euismod ultricies. Maecenas lacinia orci at neque commodo commodo. Donec egestas metus a risus 
								euismod ultricies. 
							</p>
		  					<br class="clear"/>
						</div>
						<div id="tab3_content" class="tab_content hide">
							<h4>Headline of tab 1</h4>
							<div style="text-align:center; width:720px; margin-left:auto; margin-right:auto;" id="div-human-body">
								<img id="Image-Maps_4201102081628057" src="/ClinicalSys/images/womanbody.png" usemap="#Image-Maps_4201102081628057" border="0" width="720" height="449" alt="" />
								<map id="_Image-Maps_4201102081628057" name="Image-Maps_4201102081628057">
								<area shape="poly" coords="138,64,138,57,137,53,134,48,133,41,136,38,136,34,137,29,138,24,143,21,146,18,152,17,157,15,163,16,170,20,176,27,179,35,182,44,180,50,177,54,174,58,172,63,171,66,165,70,158,71,152,72,149,71,145,70,145,68," href="javascript:alert('cabeca');" alt="cabeca" title="cabeca"   />
								<area shape="rect" coords="718,447,720,449" href="http://www.image-maps.com/index.php?aff=mapped_users_4201102081628057" alt="Image Map" title="Image Map" />
								</map>
								<!-- Image map text links - Start - If you do not wish to have text links under your image map, you can move or delete this DIV -->
								<div style="text-align:center; font-size:12px; font-family:verdana; margin-left:auto; margin-right:auto; width:720px;">
									<a style="text-decoration:none; color:black; font-size:12px; font-family:verdana;" href="javascript:alert('cabeca');" title="cabeca" alt="cabeca" tootip='Cabe�a'>cabeca</a>
								 | 	<a style="text-decoration:none; color:black; font-size:12px; font-family:verdana;" href="http://www.image-maps.com/index.php?aff=mapped_users_4201102081628057" title="Image Map">Image Map</a>
								</div>
								<!-- Image map text links - End - -->
							
							</div>
							<br class="clear"/>
						</div>
					</div>
				</t:tela>
			</div>
			<!-- End one column tab content window -->
			
			<!-- Begin footer -->
			<div id="footer">
				 Copyright&copy; by OrionX Technologies
			</div>
			<!-- End footer -->
			
		</div>
	</div><!-- End inner -->
</div>
<!-- End content -->

<div id="menuLateral">
	<ul id="shortcut">
		<li>
   		   <a rel="modalFrame" href="/ClinicalSys/clinic/pag/Paciente?ACAO=editar&onePath=true&idPaciente=${paciente.idPaciente}" title="Cadastro do Paciente">
		    <img src="${application}/images/shortcut/cadastro.png" alt="Cadastro do Paciente"/><br/>
		    <strong>Cadastro</strong>
		  </a>
		</li>
		<li>
   		   <a href="javascript:redirectHistoriaPregressa(${paciente.idPaciente})" title="Hist�ria Pregressa">
		    <img src="${application}/images/shortcut/historico.png" alt="Hist�ria Pregressa"/><br/>
		    <strong>Hist. Preg</strong>
		  </a>
		</li>
		<li>
   		   <a href="javascript:redirectAnamnese(${paciente.idPaciente})" title="Anamnese">
		    <img src="${application}/images/shortcut/anamnese.png" alt="Anamnese"/><br/>
		    <strong>Anamnese</strong>
		  </a>
		</li>
		<li>
   		    <a href="javascript:redirectPrescricao(${paciente.idPaciente})" title="Prescri��o">
		    <img src="${application}/images/shortcut/exameFisico.png" alt="Prescri��o"/><br/>
		    <strong>Prescri��o</strong>
		  </a>
		</li>
		<li>
   		   <a href="javascript:redirectExames(${paciente.idPaciente})" title="Exames">
		    <img src="${application}/images/shortcut/exames.png" alt="Exames"/><br/>
		    <strong>Exames</strong>
		  </a>
		</li>
		<li>
   		   <a href="javascript:redirectFormularios(${paciente.idPaciente})" title="Formul�rios">
		    <img src="${application}/images/shortcut/pdf.png" alt="Formul�rios"/><br/>
		    <strong>Formul�rios</strong>
		  </a>
		</li>
	</ul>
	<div id="headerMenuLateral">
		<div id="actionMenuLateral">&laquo;</div>
	</div>
</div>
<div id="actionMenuLateral-open">&raquo;</div>

<c:if test="${param.onePath != 'true'}">
	<div id="rodapeFixo">
	<div id="headerFooter"><div align="right"><div id="divButtonClose">Fechar</div></div></div>
		
		<!-- Begin shortcut menu -->
		<ul id="shortcut">
   			<li>
   			   <a href="#" onclick="openDialogAguarde();location.href='${application}/clinic/process/listaEspera'" title="Lista de Espera">
			    <img src="${application}/images/shortcut/atendimento.png" alt="Lista de Espera"/><br/>
			    <strong>Espera</strong>
			  </a>
			</li>
   			<li>
   			  <a href="#" onclick="openDialogAguarde();location.href='${application}/clinic/process/Calendar'" title="Agenda">
			    <img src="${application}/images/shortcut/agenda.png" alt="Agenda"/><br/>
			    <strong>Agenda</strong>
			  </a>
			</li>
			<li>
   			  <a href="#" onclick="openDialogAguarde();location.href='${application}/clinic/pag/Paciente'" id="shortcut_contacts" title="Cadastro de Pacientes">
			    <img src="${application}/images/shortcut/patient.png" alt="Cadastro de Pacientes"/><br/>
			    <strong>Paciente</strong>
			  </a>
			</li>
			<li>
   			  <a href="#" onclick="openDialogAguarde();location.href='${application}/clinic/process/Medicamentos'" id="shortcut_contacts" title="Medicamentos">
			    <img src="${application}/images/shortcut/bookmedicine.png" alt="Medicamentos"/><br/>
			    <strong>Drogas</strong>
			  </a>
			</li>
   			<li>
   			  <a href="modal_window.html" title="Relat�rios">
			    <img src="${application}/images/shortcut/report.png" alt="Relat�rios"/><br/>
			    <strong>Relat�rios</strong>
			  </a>
			</li>
   			<li>
   			  <a href="modal_window.html" title="Dashboard">
			    <img src="${application}/images/shortcut/stats.png" alt="Dashboard"/><br/>
			    <strong>Dashboard</strong>
			  </a>
			</li>
			<li>
   			  <a href="#" onclick="openMenuByModulo('estoque')" title="Administra��o de estoque e financeiro">
			    <img src="${application}/images/shortcut/stock.png" alt="Administra��o de estoque e financeiro"/><br/>
			    <strong>Adm</strong>
			  </a>
			</li>
			<li>
   			  <a href="#" onclick="openMenuByModulo('util')" title="Utilit�rios">
			    <img src="${application}/images/shortcut/util.png" alt="Utilit�rios"/><br/>
			    <strong>Utilit�rios</strong>
			  </a>
			</li>
			<li>
   			  <a href="#" onclick="openMenuByModulo('system')" title="Configura��es do sistema">
			    <img src="${application}/images/shortcut/setting.png" alt="Configura��es"/><br/>
			    <strong>Config</strong>
			  </a>
			</li>
 			</ul>
		<!-- End shortcut menu -->
	</div>
	<div id="openRodapeFixo">
		<div id="divButton">Abrir</div>
	</div>
</c:if>
<!-- ui-dialog -->
<!-- Include das menssagens de alerta -->
<jsp:include page="../../menssages/dialogInformation.jsp"></jsp:include>
<script type="text/javascript">
$(document).ready(function(){
	$('#show_menu').fadeIn();
	$('#wysiwyg').css('width', '97%');
	//setNotifications();
	startCountdown();
	$('#divButtonClose').click(function(){
		$('#rodapeFixo').slideUp();
		$('#openRodapeFixo').slideDown();
	});
	$('#openRodapeFixo').click(function(){
		$('#openRodapeFixo').slideUp();
		$('#rodapeFixo').slideDown();
	});
	$('#actionMenuLateral').click(function(){
		//$('#menuLateral').hide('slide', {direction: 'left'}, 600);
		$('#menuLateral').animate({width: 'hide'});
		$('#actionMenuLateral-open').animate({width: 'show'});
	});
	$('#actionMenuLateral-open').click(function(){
		$('#menuLateral').animate({width: 'show'});
		$('#actionMenuLateral-open').animate({width: 'hide'});
	});
	var dados = "";
	$("#paciente").autocomplete(dados.split(';'), {
		max: 10,
		scroll: true,
		scrollHeight: 300
		
	});
	
});
	$(function() {
		$('a[rel=modalFrame]').click(function(e) {
			e.preventDefault();
			var $this = $(this);
			var horizontalPadding = 30;
			var verticalPadding = 30;
	        $('<iframe id="cadPaciente" class="externalSite" src="' + this.href + '" />').dialog({
	            title: ($this.attr('title')) ? $this.attr('title') : '',
	            autoOpen: true,
	            width: 980,
	            height: 630,
	            modal: true,
	            resizable: true,
				autoResize: true,
	            overlay: {
	                opacity: 0.5,
	                background: "black"
	            }
	        }).width(1000 - horizontalPadding).height(630 - verticalPadding);	        
		});
	});
	function closeIframe(){
	   var iframe = document.getElementById('cadPaciente');
	   iframe.parentNode.removeChild(iframe);
	   openDialogAguarde();
	   location.href = location.href;
	}
	function redirectHistoriaPregressa(param){
		openDialogAguarde();
		location.href= '/ClinicalSys/clinic/process/historiaPregressa?idPaciente='+param;
	}
	function redirectAnamnese(param){
		openDialogAguarde();
		location.href= '/ClinicalSys/clinic/process/Anamnese?idPaciente='+param;
	}
	function redirectPrescricao(param){
		openDialogAguarde();
		location.href= '/ClinicalSys/clinic/process/prescricao?idPaciente='+param;
	}
	function redirectFormularios(param){
		openDialogAguarde();
		location.href= '/ClinicalSys/clinic/process/formularios?idPaciente='+param;
	}
	function redirectExames(param){
		openDialogAguarde();
		location.href= '/ClinicalSys/clinic/process/exames?idPaciente='+param;
	}
	
	function ajaxGetName(nome){
	 	if(nome!='')
	     	sendRequest('/ClinicalSys/clinic/pag/Paciente','ACAO=getPacienteAutoComplete&nomeParc='+nome,'POST', ajaxCallbackNome, erroCallbackNome);
	 }
	 
	 function ajaxCallbackNome(data){
		$("#paciente").setOptions({data: data.split(';')});
	 }
	 
	 function erroCallbackNome(request){
		openDialogInformation("Ups. ocorreu um erro",true);
	 }
</script>
</body>
</html>
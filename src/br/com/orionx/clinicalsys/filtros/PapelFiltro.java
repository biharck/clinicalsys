package br.com.orionx.clinicalsys.filtros;

import org.nextframework.validation.annotation.MaxLength;

import br.com.orionx.clinicalsys.util.generics.FiltroListagem;

public class PapelFiltro extends FiltroListagem{

	protected String nome;
	
	@MaxLength(200)
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
}

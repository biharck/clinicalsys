package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name = "versaoxmltiss")
public class VersaoXMLTISS {
	
	private Integer idVersaoXMLTISS;
	private String nome;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getIdVersaoXMLTISS() {
		return idVersaoXMLTISS;
	}
	@Required
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setIdVersaoXMLTISS(Integer idVersaoXMLTISS) {
		this.idVersaoXMLTISS = idVersaoXMLTISS;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

}

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="n" uri="next" %>
<%@ taglib prefix="t" uri="template" %>

<t:entrada>
	<span style="font-size: 14px; background-color: #DDE5FF;" id="nomeMed"></span><br/>
	<t:property name="idInteracaoMedicamentosa" type="hidden" label="" write="false"/>
	<t:janelaEntrada>
		<t:tabelaEntrada columns="1"  title="Princípio Ativo">
			<c:if test="${param.ACAO == 'criar'}">
				<n:comboReloadGroup useAjax="true">
					<t:property name="classificacaoMedicacao" renderAs="doubleline" onchange="openAguardeAjax()"/>
					<t:property name="medicamento" renderAs="doubleline" onchange="setName(this)"  itens="medicamentoDAO.getMedicamentosByClassif(classificacaoMedicacao)"/>
				</n:comboReloadGroup>
			</c:if>
			<c:if test="${param.ACAO != 'criar'}">
				<t:property name="medicamento" mode="output" style="color:red;"/>
			</c:if>
			<t:property name="ativo" renderAs="doubleline" trueFalseNullLabels="ativo, inativo," type="select-one"/>
		</t:tabelaEntrada>
		<t:detalhe name="interacoes"  onNewLine="clearCombo()">
			<n:column header="Indicações específicas e doses">
					<n:panelGrid columns="2" >
						<t:propertyConfig mode="input" renderAs="double" showLabel="true">
							<t:property name="classificacaoMedicacao" onchange="openAguardeAjax();buscaMedicamentos(this.value,${index})"/>
							<t:property name="medicamento" />
							<t:property name="consideracao" style="width:400px" rows="4"/>
						</t:propertyConfig>
					</n:panelGrid>
			</n:column>
		</t:detalhe>
		<t:detalhe name="interacoesClass">
			<n:column header="Indicações específicas e doses">
				<n:panelGrid columns="2" >
					<t:propertyConfig mode="input" renderAs="double" showLabel="true">
						<t:property name="classificacaoMedicacao"/>
						<t:property name="consideracao" style="width:400px" rows="4"/>
					</t:propertyConfig>
				</n:panelGrid>
			</n:column>
		</t:detalhe>
	</t:janelaEntrada>
</t:entrada>
<script type="text/javascript">
	function clearCombo(){
		$('select[name*=.medicamento]:last').empty();
	}
	function setName(param){
		obj = $(param);
		$('#nomeMed').html(param.options[param.selectedIndex].text);
	}
</script>
package br.com.orionx.clinicalsys.service;

import java.util.List;

import br.com.orionx.clinicalsys.bean.CIDOCategoria;
import br.com.orionx.clinicalsys.dao.CIDOCategoriaDAO;
import br.com.orionx.clinicalsys.util.generics.GenericService;

public class CIDOCategoriaService extends GenericService<CIDOCategoria> {

	
	private CIDOCategoriaDAO cidoCategoriaDAO;
	
	public void setCidoCategoriaDAO(CIDOCategoriaDAO cidoCategoriaDAO) {
		this.cidoCategoriaDAO = cidoCategoriaDAO;
	}

	/**
	 * <p>M�todo de refer�ncia ao DAO
	 * @see 
	 * @param catInic categoria inicial {@link String}
	 * @param catFim categoria final {@link String}
	 * @return {@link List} {@link CIDOCategoria}
	 */
	public List<CIDOCategoria> getCategoriasByGrupos(String catInic, String catFim){
		return cidoCategoriaDAO.getCategoriasByGrupos(catInic, catFim);
	}
	
}

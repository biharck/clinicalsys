package br.com.orionx.clinicalsys.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.QueryBuilder;

import br.com.orionx.clinicalsys.bean.ProdutoServico;
import br.com.orionx.clinicalsys.filtros.ProdutoServicoFiltro;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class ProdutoServicoDAO extends GenericDAO<ProdutoServico> {

	
	@Override
	public void updateListagemQuery(QueryBuilder<ProdutoServico> query,	FiltroListagem _filtro) {
		ProdutoServicoFiltro filtro = (ProdutoServicoFiltro) _filtro;
		
		if(filtro.getAtivoEnum()!=null)
			if(filtro.getAtivoEnum().getSituacao()!=null && (filtro.getAtivoEnum().getSituacao() || !filtro.getAtivoEnum().getSituacao()))
				query.where("produtoServico.ativo is "+filtro.getAtivoEnum().getSituacao());
		query.whereLikeIgnoreAll("produtoServico.codigo", filtro.getCodigo());
		query.whereLikeIgnoreAll("produtoServico.descricao", filtro.getDescricao());
		query.where("produtoServico.CFPO = ?", filtro.getCFPO());
		query.where("produtoServico.produtoOuServico=?",filtro.getProdutoOuServico());
		query.leftOuterJoin("produtoServico.categoriaProdutos categoriaProdutos").where("categoriaProdutos=?",filtro.getCategoriaProdutos());
		super.updateListagemQuery(query, filtro);
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<ProdutoServico> query) {
		query.select("produtoServico")
		.leftOuterJoin("produtoServico.categoriaProdutos categoriaProdutos");
	}
	
	@Override
	public List<ProdutoServico> findAll() {
		return query().where("produtoServico.ativo is true").list();
	}
	
	@Override
	public List<ProdutoServico> findForCombo(String... extraFields) {
		extraFields[0] += "ativo";
		return super.findForCombo(extraFields);
	}

}

package br.com.orionx.clinicalsys.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

import br.com.orionx.clinicalsys.util.log.Log;


@Entity
@Table(name = "feriado")
public class Feriado implements Log{
	
	private Integer idFeriado;
	private String nome;
	private Date data;
	private boolean sempre;
	private boolean facultativo;
	private Boolean tipoFeriado;//Nacional,Estadual, Municipal 
	
	
	//Log
	private Integer idUsuarioAltera;
	private Timestamp dtAltera;

	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdFeriado() {
		return idFeriado;
	}
	@MinLength(2)
	@MaxLength(50)
	@Required
	public String getNome() {
		return nome;
	}
	@Required
	public Date getData() {
		return data;
	}
	public boolean isSempre() {
		return sempre;
	}
	public boolean isFacultativo() {
		return facultativo;
	}
	public Boolean getTipoFeriado() {
		return tipoFeriado;
	}
	
	//SET
	public void setIdFeriado(Integer idFeriado) {
		this.idFeriado = idFeriado;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setSempre(boolean sempre) {
		this.sempre = sempre;
	}
	public void setFacultativo(boolean facultativo) {
		this.facultativo = facultativo;
	}
	public void setTipoFeriado(Boolean tipoFeriado) {
		this.tipoFeriado = tipoFeriado;
	}
	
	//LOG
	@Override
	public void setIdUsuarioAltera(Integer idUsuarioAltera) {
		this.idUsuarioAltera = idUsuarioAltera;
	}

	@Override
	public void setDtAltera(Timestamp dtAltera) {
		this.dtAltera = dtAltera;
	}

	@Override
	public Integer getIdUsuarioAltera() {
		return idUsuarioAltera;
	}

	@Override
	public Timestamp getDtAltera() {
		return dtAltera;
	}	

}

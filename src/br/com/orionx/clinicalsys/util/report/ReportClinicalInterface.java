package br.com.orionx.clinicalsys.util.report;

import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.Report;

import br.com.orionx.clinicalsys.filtros.ColaboradorFiltro;

public interface ReportClinicalInterface {

	Report createReport(WebRequestContext request,ColaboradorFiltro filtro);
}

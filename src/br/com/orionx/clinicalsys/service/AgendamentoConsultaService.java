package br.com.orionx.clinicalsys.service;

import java.sql.Date;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.orionx.clinicalsys.bean.AgendamentoConsulta;
import br.com.orionx.clinicalsys.bean.Colaborador;
import br.com.orionx.clinicalsys.bean.ColaboradorAgenda;
import br.com.orionx.clinicalsys.bean.ConfiguracaoAtendimento;
import br.com.orionx.clinicalsys.bean.IndispAgendaColaborador;
import br.com.orionx.clinicalsys.dao.AgendamentoConsultaDAO;
import br.com.orionx.clinicalsys.util.ClinicalSysUtil;
import br.com.orionx.clinicalsys.util.generics.GenericService;

public class AgendamentoConsultaService extends GenericService<AgendamentoConsulta>{
	
	private AgendamentoConsultaDAO agendamentoConsultaDAO;
	private IndispAgendaColaboradorService indispAgendaColaboradorService;
	private ColaboradorAgendaService colaboradorAgendaService;
	private ConfiguracaoAtendimentoService configuracaoAtendimentoService;
	
	public void setAgendamentoConsultaDAO(AgendamentoConsultaDAO agendamentoConsultaDAO) {
		this.agendamentoConsultaDAO = agendamentoConsultaDAO;
	}
	public void setIndispAgendaColaboradorService(IndispAgendaColaboradorService indispAgendaColaboradorService) {
		this.indispAgendaColaboradorService = indispAgendaColaboradorService;
	}
	public void setColaboradorAgendaService(ColaboradorAgendaService colaboradorAgendaService) {
		this.colaboradorAgendaService = colaboradorAgendaService;
	}
	public void setConfiguracaoAtendimentoService(ConfiguracaoAtendimentoService configuracaoAtendimentoService) {
		this.configuracaoAtendimentoService = configuracaoAtendimentoService;
	}
	
	/**
	 * <p>M�todo de refer�ncia ao DAO
	 * @return {@link List} {@link AgendamentoConsulta}
	 * @see br.com.orionx.clinicalsys.dao.AgendamentoConsultaDAO#getConsultasAgendadas
	 */
	public List<AgendamentoConsulta> getConsultasAgendadas(){
		return  agendamentoConsultaDAO.getConsultasAgendadas();
	}

	/**
	 * <p>M�todo de refer�ncia ao DAO
	 * @return {@link List} {@link AgendamentoConsulta}
	 * @see br.com.orionx.clinicalsys.dao.AgendamentoConsultaDAO#getConsultasAgendadasByColaborador
	 */
	public List<AgendamentoConsulta> getConsultasAgendadasByColaborador(Colaborador c){
		return  agendamentoConsultaDAO.getConsultasAgendadasByColaborador(c);
	}
	
	/**
	 * <p>M�todo de refer�ncia ao DAO
	 * @return {@link List} {@link AgendamentoConsulta}
	 * @see br.com.orionx.clinicalsys.dao.AgendamentoConsultaDAO#getConsultasAgendadasByColaborador
	 */
	public List<AgendamentoConsulta> getConsultasAgendadasByColaboradorAndDate(Colaborador c,Date date){
		return  agendamentoConsultaDAO.getConsultasAgendadasByColaboradorAndDate(c,date);
	}
	
	/**
	 * <p>M�todo de refer�ncia ao DAO
	 * @return {@link AgendamentoConsulta}
	 * @see br.com.orionx.clinicalsys.service.AgendamentoConsultaService#getConsultaAgendadaById
	 */
	public AgendamentoConsulta getConsultaAgendadaById(Integer id){
		return agendamentoConsultaDAO.getConsultaAgendadaById(id);
	}
	
	/**
	 * <p>M�todo que verifica se em um determinado dia e hora, existe alguma consulta para o colaborador
	 * @param date {@link Date} data da consulta
	 * @param ca {@link ColaboradorAgenda} colaborador respons�vel
	 * @param hora {@link Time} hora da consulta
	 * @return {@link Boolean} true se existir e false caso contr�rio.
	 */
	public boolean indisponivel(Date date, ColaboradorAgenda ca, Time hora,AgendamentoConsulta acExistente){
		AgendamentoConsulta ac =  agendamentoConsultaDAO.getAgendamentoByHorarioAndDia(date,ca,hora);
		if(ac==null || ac.getIdAgendamentoConsulta()==null)
			return false;
		else{
			if(ac.getIdAgendamentoConsulta().equals(acExistente.getIdAgendamentoConsulta()))
				return false;
		}
		return true;
	}
	
	
	public boolean colaboradorTrabalhaNesteHorario(ColaboradorAgenda ca, AgendamentoConsulta ac){
		
		Date dataDaHora = ac.getDia();
		Time horaMarcada = ac.getHora();
		
		ca = colaboradorAgendaService.loadForEntrada(ca);
		
		Calendar calendarioDataDaHora = Calendar.getInstance();
		calendarioDataDaHora.setTimeInMillis(dataDaHora.getTime());
		
		long ipp = ca.getInicioPrimeiroPeriodo()!=null? ca.getInicioPrimeiroPeriodo().getTime(): 0l;
		long tpp = ca.getTerminoPrimeiroPeriodo()!=null? ca.getTerminoPrimeiroPeriodo().getTime(): 0l;
		
		long isp = ca.getInicioSegundoPeriodo()!=null? ca.getInicioSegundoPeriodo().getTime(): 0l;
		long tsp = ca.getTerminoSegundoPeriodo()!=null? ca.getTerminoSegundoPeriodo().getTime(): 0l;
		
		long itp = ca.getInicioTerceiroPeriodo()!=null? ca.getInicioTerceiroPeriodo().getTime(): 0l;
		long ttp = ca.getTerminoTerceiroPeriodo()!=null? ca.getTerminoTerceiroPeriodo().getTime(): 0l;
		
		long iqp = ca.getInicioQuartoPeriodo()!=null? ca.getInicioQuartoPeriodo().getTime(): 0l;
		long tqp = ca.getTerminoQuartoPeriodo()!=null? ca.getTerminoQuartoPeriodo().getTime(): 0l;
		
		ConfiguracaoAtendimento configAtendimento = configuracaoAtendimentoService.getDadosConfiguracao();
		
		SimpleDateFormat formataHora = new SimpleDateFormat("HH:mm");
		
		//buscar do banco de dados
		float tempoConsulta = configAtendimento.getTempoConsulta();
		float resultado = 0F;
		//tempo em minutos
		
		/**
		 * se o resto do per�odo for superior a 80% do intervalo, � realizado um tipo de encaixe no hor�rio
		 */
		long first  =  (tpp - ipp) / 60000;
		resultado = new Float(first)/tempoConsulta;
		if(resultado % 2 >= 0.8)
			first += 60;
		
		long second =  (tsp - isp) / 60000;
		resultado = new Float(second)/tempoConsulta;
		if(resultado % 2 >= 0.8)
			second += 60;
		
		long third  =  (ttp - itp) / 60000;
		resultado = new Float(third)/tempoConsulta;
		if(resultado % 2 >= 0.8)
			third += 60;
		
		long fourth =  (tqp - iqp) / 60000;
		resultado = new Float(fourth)/tempoConsulta;
		if(resultado % 2 >= 0.8)
			fourth += 60;
		
		
		List<IndispAgendaColaborador> indisponibilidades = new ArrayList<IndispAgendaColaborador>();
		indisponibilidades = indispAgendaColaboradorService.getIndisponibilidades(ca);
		List<String> horariosSair = new ArrayList<String>();
		if(indisponibilidades!=null){
			for (IndispAgendaColaborador dia : indisponibilidades) {
				if(dia.getDiaSemana().equals(calendarioDataDaHora.get(calendarioDataDaHora.DAY_OF_WEEK)-1))
					horariosSair.add(dia.getHora());
			}
		}
		
		/**
		 * tirar tb horarios que j� tenham consultas agendadas
		 * 
		 */
		List<AgendamentoConsulta> agendamentos = getConsultasAgendadasByColaboradorAndDate(ca.getColaborador(),dataDaHora);
		for (AgendamentoConsulta agendamentoConsulta : agendamentos) {
			if(ac.getIdAgendamentoConsulta().equals(agendamentoConsulta.getIdAgendamentoConsulta()))
				continue;
			horariosSair.add(formataHora.format(agendamentoConsulta.getHora()));
		}
		return horariosSair.contains(formataHora.format(horaMarcada));
	}
	
	/**
	 * <p>M�todo de refer�ncia ado DAO
	 * @return {@link List} {@link AgendamentoConsulta}
	 */
	public List<AgendamentoConsulta> getConsultasAgendadasDiaCorrente(){
		return agendamentoConsultaDAO.getConsultasAgendadasDiaCorrente();
	}
	
	/**
	 * <p>M�todo de refer�ncia ado DAO
	 * @return {@link List} {@link AgendamentoConsulta}
	 */
	public List<AgendamentoConsulta> getConsultasAgendadasByColaboradorSuperiores(Colaborador c){
		return agendamentoConsultaDAO.getConsultasAgendadasByColaboradorSuperiores(c);
	}

}

package br.com.orionx.clinicalsys.bean;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.Required;

import br.com.orionx.clinicalsys.util.log.Log;

@Entity
@Table(name="atestado")
public class Atestado implements Log{
	
	private Integer idAtestado;
	private Colaborador colaborador;
	private Date dtInicio;
	private Date dtFim;
//	private CID10 cid10;
	
	//Log
	private Integer idUsuarioAltera;
	private Timestamp dtAltera;
	
	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdAtestado() {
		return idAtestado;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "idColaborador")
	@ForeignKey(name = "fk_atestado_colaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
	@Required
	@DisplayName("Data de In�cio")
	public Date getDtInicio() {
		return dtInicio;
	}
	@DisplayName("Data Final")
	public Date getDtFim() {
		return dtFim;
	}
//	@Required
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="idCid10")
//	@ForeignKey(name = "fk_atestado_cid10")
//	public CID10 getCid10() {
//		return cid10;
//	}
	
	//SET
	public void setIdAtestado(Integer idAtestado) {
		this.idAtestado = idAtestado;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}
	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}
//	public void setCid10(CID10 cid10) {
//		this.cid10 = cid10;
//	}

	//LOG
	@Override
	public void setIdUsuarioAltera(Integer idUsuarioAltera) {
		this.idUsuarioAltera = idUsuarioAltera;
	}

	@Override
	public void setDtAltera(Timestamp dtAltera) {
		this.dtAltera = dtAltera;
	}

	@Override
	public Integer getIdUsuarioAltera() {
		return idUsuarioAltera;
	}

	@Override
	public Timestamp getDtAltera() {
		return dtAltera;
	}
}

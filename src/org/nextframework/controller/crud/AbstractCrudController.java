/*
 * Next Framework http://www.nextframework.org
 * Copyright (C) 2009 the original author or authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * You may obtain a copy of the license at
 * 
 *     http://www.gnu.org/copyleft/lesser.html
 * 
 */
package org.nextframework.controller.crud;

import org.nextframework.controller.Action;
import org.nextframework.controller.Command;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.Input;
import org.nextframework.controller.MultiActionController;
import org.nextframework.controller.OnErrors;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author rogelgarcia
 * @since 01/02/2006
 * @version 1.1
 */
public abstract class AbstractCrudController<FILTRO extends FiltroListagem, FORMBEAN> extends MultiActionController {

	public static final String LISTAGEM = "listagem";

	public static final String ENTRADA = "entrada";

	public static final String CRIAR = "criar";

	public static final String EDITAR = "editar";
	
	public static final String CONSULTAR = "consultar";

	public static final String SALVAR = "salvar";

	public static final String EXCLUIR = "excluir";

	public static final String SAVEANDNEW = "saveandnew";

	@DefaultAction
	@Action(LISTAGEM)
	@Input(LISTAGEM)
	@Command(session = true, validate = true)
	public abstract ModelAndView doListagem(WebRequestContext request, FILTRO filtro) throws CrudException;

	@Action(ENTRADA)
	@Input(ENTRADA)
	public abstract ModelAndView doEntrada(WebRequestContext request, FORMBEAN form) throws CrudException;

	@Action(CRIAR)
	@OnErrors(LISTAGEM)
	public abstract ModelAndView doCriar(WebRequestContext request, FORMBEAN form) throws CrudException;

	@Action(CONSULTAR)
	public ModelAndView doConsultar(WebRequestContext request, FORMBEAN form) throws CrudException {
		request.setAttribute(CONSULTAR, true);
		return doEditar(request, form);
	}
	
	@Action(EDITAR)
	@OnErrors(LISTAGEM)
	public abstract ModelAndView doEditar(WebRequestContext request, FORMBEAN form) throws CrudException;

	@Action(SALVAR)
	@Command(validate = true)
	@Input(ENTRADA)
	public abstract ModelAndView doSalvar(WebRequestContext request, FORMBEAN form) throws CrudException;
	
	@Action(SAVEANDNEW)
	@Command(validate = true)
	@Input(ENTRADA)
	public abstract ModelAndView saveAndNew(WebRequestContext request, FORMBEAN form) throws Exception;

	@Action(EXCLUIR)
	@OnErrors(LISTAGEM)
	public abstract ModelAndView doExcluir(WebRequestContext request, FORMBEAN form) throws CrudException;

}

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="cs" uri="clinicalsys"%>

<t:entrada >
<c:if test="${param.ACAO != 'criar'}">
	<div class="ultimaalteracao">
		<n:panel>Registro criado por <font style="color:#E77272">${cs:getuserbyid(paciente.idPaciente)}</font> no dia <font style="color:#E77272">${cs:dateformat(paciente.dtInclusao)}</font> �s <font style="color:#E77272">${cs:hourformat(paciente.dtInclusao)}</font> </n:panel>
	</div>
</c:if>
	<t:janelaEntrada showSubmit="false">
		<t:tabelaEntrada columns="1" >
			<n:panel  >
				<n:panelGrid columns="2" columnStyles="width:400px,width:300px">
					<n:group legend="Dados Pessoais" colspan="1">
						<n:panelGrid columns="6" >
							<t:property name="idPaciente" colspan="1" id="idpaciente" type="hidden" renderAs="doubleline"/>
							<t:property name="nome" renderAs="doubleline" colspan="4" id="nome" style="width:400px;"/>
							<t:property name="sexo" renderAs="doubleline" colspan="1"/>
							
							<t:property name="rg" renderAs="doubleline" colspan="2" style="width:150px;" />
							<t:property name="orgaoExpedidor" renderAs="doubleline" colspan="2" style="width:50px;"/>
							<t:property name="cpf" renderAs="doubleline" style="width:150px;" colspan="2" onchange="validateCpfOver(this)"/>
							
							<t:property name="naturalidade" renderAs="doubleline" colspan="2" style="width:150px;"/>
							<t:property name="estadoCivil" renderAs="doubleline" colspan="2" />
							<t:property name="dtnascimento" type="text" OnKeyUp="mascara_data_press($(this))" maxlength="10" renderAs="doubleline" colspan="1" datepickerrangedate="true" style="width:100px;" onchange="calculaIdade(this.value);"/>
							<n:panel colspan="1" id="labelIdade"></n:panel>
							
							<t:property name="profissao" renderAs="doubleline" colspan="6" style="width:400px;"/>
							<t:property name="escolaridade" renderAs="doubleline" colspan="6"/>
							<t:property name="mae" renderAs="doubleline" colspan="6"  style="width:400px;"/>
							<t:property name="pai" renderAs="doubleline" colspan="6"  style="width:400px;"/>
							<t:property name="email" renderAs="doubleline" colspan="6"  style="width:400px;" onchange="validaEmailFormulario(this)" id="email"/>
							<c:if test="${empty paciente.idPaciente}">
								<t:property name="confirmaEmail" renderAs="doubleline"  colspan="6" style="width:400px;" onchange="validaEmailFormulario(this);equalsEmail(this,document.getElementById('email'));"/>
							</c:if>
							<t:property name="indicacao" renderAs="doubleline" colspan="6"  style="width:400px;" selectOnePath="/clinic/pag/Paciente?onePath=true" selectOneWindowSize="980,630"/>
							<t:property name="ativo" trueFalseNullLabels="Ativo,Inativo,"/>
						</n:panelGrid>
					</n:group>
					
					<n:panel id="picture" style="text-align:center;">
						<n:group legend="Foto">
							<c:if test="${showpicture}">
								<n:link url="/DOWNLOADFILE/${paciente.fotoPaciente.cdfile}">
									<center><img src="${pageContext.request.contextPath}/DOWNLOADFILE/${paciente.fotoPaciente.cdfile}" id="imagem" class="foto"/></center>	
								</n:link>
							</c:if>
							<c:if test="${!showpicture}">
								<div class="image">
									<center><img src="${pageContext.request.contextPath}/images/peopleNotFound.png" id="imagem" style="padding-top:13px;"/></center>
								</div>
							</c:if>
							<t:property name="fotoPaciente" id="foto" label="" renderAs="doubleline" onchange="verifyExt();" tabindex="6"/>
						</n:group>
						<n:group legend="Telefones">
							<t:detalhe name="listaTelefonePaciente">
								<t:property name="telefone"/>
								<t:property name="telefoneTipo" label="Tipo"/>
							</t:detalhe>
						</n:group>
					</n:panel>
					<n:panel colspan="2">
						<n:group legend="Endere�o" colspan="2">
							<n:panelGrid columns="6" colspan="2">
								<t:property name="cep" id="cep" renderAs="doubleline" colspan="1" onchange="ajaxBuscaCEP(this.value)" style="width:100px;"/>
								<n:panel colspan="1" id="modalLink">
									<label for="btn_busca_end">N�o sabe o CEP?</label><br>
									<a id="btn_busca_end"  href="javascript:openDialogBuscaCEP()"><img src="/ClinicalSys/images/shortcut/find-help.png"/> Clique aqui</a>
								</n:panel>
								<n:panel colspan="4"></n:panel>
								<t:property name="tipoLogradouro" renderAs="doubleline" colspan="2" id="tipoLogradouro"/>
								<t:property name="logradouro" renderAs="doubleline" colspan="3" style="width:400px;" id="logradouro"/>
								<t:property name="numero" renderAs="doubleline" colspan="2" id="numero"/>
								<t:property name="complemento" renderAs="doubleline" colspan="2" style="width:100px;"/>
								<t:property name="bairro" renderAs="doubleline" colspan="2" id="bairro" style="width:200px;"/>
								<n:panel colspan="2"></n:panel>
								<n:comboReloadGroup useAjax="true">
									<t:property name="pais" renderAs="doubleline" colspan="4" onchange="openAguardeAjax()" id="pais"/>
									<t:property name="unidadeFederativa" renderAs="doubleline" colspan="2" onchange="openAguardeAjax()" id="uf"/>
									<t:property name="municipio" renderAs="doubleline" colspan="2" id="cidade"/>
								</n:comboReloadGroup>
							</n:panelGrid>
						</n:group>	
					</n:panel>
					<n:panel colspan="2">
						<n:group legend="Rela��o de Planos do Paciente">
							<t:detalhe name="planos"  onNewLine="clearCombo()" nomeColunaAcao="Excluir?" >
								<t:property name="operadora" onchange="buscaPlanos(this.value,${index})"/>
								<t:property name="plano"/>
								<t:property name="numeroCarteira"/>
								<t:property name="validade" type="text" OnKeyUp="mascara_data_press($(this))" maxlength="10" datepickerrangedate="true" style="width:100px;" onclick="insereDatepicker($(this))" />
							</t:detalhe>
						</n:group>
					</n:panel>	
				</n:panelGrid>
			</n:panel>	
				
			
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>
	
<script type="text/javascript">

	function calculaIdade(param){
		if(param == '' || param == null)
			return false;
		
		var idade = calcular_idade(param);
		$('#labelIdade').html("<br/><font style='color:#E77272'><b>"+ idade + " anos de idade.</b></font>").fadeIn();
	}
	
	function insereDatepicker(param){
		param.datepicker();
		$('#email').click();
		$('#email').focus();
		param.focus();
		param.click();
	}

	function clearCombo(){
		$('select[name*=.plano]:last').empty();
	}
	
	$(document).ready(function(){
		$('#nome').focus();
		
		var dtnascimento = '${paciente.dtnascimento}';
		if(dtnascimento!=null && dtnascimento != ''){
			var dtFormatada = data_americana_brasileira(dtnascimento,'dd/MM/yyyy');
			calculaIdade(dtFormatada);
		}
		var parametro = "${param.onePath}";
		if(parametro == true || parametro == "true"){
			$('.linkBar').html('<button type="button" onclick="openDialogAguarde();form.ACAO.value =\'salvar\';form.action = \'/ClinicalSys/clinic/pag/Paciente?iframe=true\'; form.validate = \'true\'; submitForm()" title="" class="noboard"><img src="/ClinicalSys/images/shortcut/save.png" alt="Salvar" class="help" original-title="Salvar"></button>');
		}
	});
</script>
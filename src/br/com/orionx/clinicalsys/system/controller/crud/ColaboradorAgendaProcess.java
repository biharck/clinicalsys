package br.com.orionx.clinicalsys.system.controller.crud;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.view.ajax.View;
import org.springframework.web.servlet.ModelAndView;

import br.com.orionx.clinicalsys.bean.AgendamentoConsulta;
import br.com.orionx.clinicalsys.bean.ColaboradorAgenda;
import br.com.orionx.clinicalsys.bean.ConfiguracaoAtendimento;
import br.com.orionx.clinicalsys.bean.IndispAgendaColaborador;
import br.com.orionx.clinicalsys.bean.TipoAgenda;
import br.com.orionx.clinicalsys.service.AgendamentoConsultaService;
import br.com.orionx.clinicalsys.service.ColaboradorAgendaService;
import br.com.orionx.clinicalsys.service.ConfiguracaoAtendimentoService;
import br.com.orionx.clinicalsys.service.IndispAgendaColaboradorService;
import br.com.orionx.clinicalsys.service.TipoAgendaService;
import br.com.orionx.clinicalsys.util.ClinicalSysUtil;

@Controller(path="/system/process/ColaboradorAgenda",authorizationModule=ProcessAuthorizationModule.class)
public class ColaboradorAgendaProcess extends MultiActionController{
	
	private IndispAgendaColaboradorService indispAgendaColaboradorService;
	private ColaboradorAgendaService colaboradorAgendaService;
	private ConfiguracaoAtendimentoService configuracaoAtendimentoService;
	private AgendamentoConsultaService agendamentoConsultaService;
	private TipoAgendaService tipoAgendaService;
	
	public void setIndispAgendaColaboradorService(IndispAgendaColaboradorService indispAgendaColaboradorService) {
		this.indispAgendaColaboradorService = indispAgendaColaboradorService;
	}
	public void setColaboradorAgendaService(ColaboradorAgendaService colaboradorAgendaService) {
		this.colaboradorAgendaService = colaboradorAgendaService;
	}
	public void setConfiguracaoAtendimentoService(ConfiguracaoAtendimentoService configuracaoAtendimentoService) {
		this.configuracaoAtendimentoService = configuracaoAtendimentoService;
	}
	public void setAgendamentoConsultaService(AgendamentoConsultaService agendamentoConsultaService) {
		this.agendamentoConsultaService = agendamentoConsultaService;
	}
	public void setTipoAgendaService(TipoAgendaService tipoAgendaService) {
		this.tipoAgendaService = tipoAgendaService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request, ColaboradorAgenda ca){
		request.setAttribute("pathURL", getPathURL());
		List<ColaboradorAgenda> colaboradoresAgenda = colaboradorAgendaService.findAll();
		
		if(colaboradoresAgenda!=null && !colaboradoresAgenda.isEmpty() && getParameter("newCA")==null){
			return new ModelAndView("process/colaboradorAgendaResumo","lista",colaboradoresAgenda);
		}
		else
			return new ModelAndView("process/colaboradorAgenda","colaboradorAgenda",ca);
	}

	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Administra��o &raquo; <a href=\"/ClinicalSys/system/process/ColaboradorAgenda\">Agendas dos Colaboradores</a></p>";
	}
	
	public ModelAndView generateAgenda(WebRequestContext request, ColaboradorAgenda ca){
		request.setAttribute("tempoConsulta", configuracaoAtendimentoService.getDadosConfiguracao().getTempoConsulta());
		
		
		long ipp = ca.getInicioPrimeiroPeriodo()!=null? ca.getInicioPrimeiroPeriodo().getTime(): 0l;
		long tpp = ca.getTerminoPrimeiroPeriodo()!=null? ca.getTerminoPrimeiroPeriodo().getTime(): 0l;
		
		long isp = ca.getInicioSegundoPeriodo()!=null? ca.getInicioSegundoPeriodo().getTime(): 0l;
		long tsp = ca.getTerminoSegundoPeriodo()!=null? ca.getTerminoSegundoPeriodo().getTime(): 0l;
		
		long itp = ca.getInicioTerceiroPeriodo()!=null? ca.getInicioTerceiroPeriodo().getTime(): 0l;
		long ttp = ca.getTerminoTerceiroPeriodo()!=null? ca.getTerminoTerceiroPeriodo().getTime(): 0l;
		
		long iqp = ca.getInicioQuartoPeriodo()!=null? ca.getInicioQuartoPeriodo().getTime(): 0l;
		long tqp = ca.getTerminoQuartoPeriodo()!=null? ca.getTerminoQuartoPeriodo().getTime(): 0l;
		
		ConfiguracaoAtendimento configAtendimento = configuracaoAtendimentoService.getDadosConfiguracao();
		
		//buscar do banco de dados
		float tempoConsulta = configAtendimento.getTempoConsulta();
		float resultado = 0F;
		//tempo em minutos
		
		/**
		 * se o resto do per�odo for superior a 80% do intervalo, � realizado um tipo de encaixe no hor�rio
		 */
		long first = getTempoAtendimento(tpp, ipp, resultado, tempoConsulta);
		long second = getTempoAtendimento(tsp, isp, resultado, tempoConsulta);
		long third = getTempoAtendimento(ttp, itp, resultado, tempoConsulta);
		long fourth = getTempoAtendimento(tqp, iqp, resultado, tempoConsulta);
		
		long totalDisponivel = first + second + third + fourth;
		
		Long totalMinutos = totalDisponivel/ (long) tempoConsulta;
		int total = new Integer(new Long(first/60).intValue()) + new Integer(new Long(second/60).intValue())+new Integer(new Long(third/60).intValue())+new Integer(new Long(fourth/60).intValue());
		
		int[] arrayIntervalos = {new Integer(new Long(first/(long) tempoConsulta).intValue()),
								new Integer(new Long(second/(long) tempoConsulta).intValue()),
								new Integer(new Long(third/(long) tempoConsulta).intValue()),
								new Integer(new Long(fourth/(long) tempoConsulta).intValue())};
		Time[] arrayHoras = {ca.getInicioPrimeiroPeriodo(),ca.getInicioSegundoPeriodo(),ca.getInicioTerceiroPeriodo(),ca.getInicioQuartoPeriodo()};
		
		/**
		 * envio de infoma��es para o jsp
		 */
		request.setAttribute("iteracoes", total);
		request.setAttribute("difPrimeiroPeriodo", arrayIntervalos[0]);
		request.setAttribute("difSegundoPeriodo", arrayIntervalos[1]);
		request.setAttribute("difTerceiroPeriodo", arrayIntervalos[2]);
		request.setAttribute("difQuartoPeriodo", arrayIntervalos[3]);
		request.setAttribute("residuoMinutos", totalMinutos - total);
		
		
		
		if(request.getParameter("saveConfigAgenda")!=null && request.getParameter("saveConfigAgenda").equals("true")){
			List<IndispAgendaColaborador> indisponibilidades = new ArrayList<IndispAgendaColaborador>();
			if (getParameter("showResumen")!=null && getParameter("showResumen").equals("true")) {
				indisponibilidades = indispAgendaColaboradorService.getIndisponibilidades(ca);
				Set<Integer> diasIndisponiveisDaSemana = null;
				if(indisponibilidades!=null){
					diasIndisponiveisDaSemana = new HashSet<Integer>();
					for (IndispAgendaColaborador dia : indisponibilidades) {
						diasIndisponiveisDaSemana.add(dia.getDiaSemana());
					}
				}
				StringBuilder sb = new StringBuilder();
				sb.append("<table>");
				sb.append("<tr style='background-color:#DDE5FF;'>");
					sb.append("<td>");
					sb.append("</td>");
					sb.append("<td>");
						sb.append("<center>Domingo</center>");											
					sb.append("</td>");
					sb.append("<td>");
						sb.append("<center>Segunda</center>");
					sb.append("</td>");
					sb.append("<td>");
						sb.append("<center>Ter�a</center>");
					sb.append("</td>");
					sb.append("<td>");
						sb.append("<center>Quarta</center>");
					sb.append("</td>");
					sb.append("<td>");
						sb.append("<center>Quinta</center>");
					sb.append("</td>");
					sb.append("<td>");
						sb.append("<center>Sexta</center>");
					sb.append("</td>");
					sb.append("<td>");
						sb.append("<center>S�bado</center>");
					sb.append("</td>");
				sb.append("</tr>");
				
				Time timeTmp = null;
				boolean lineColor = true;
				for (int j = 0; j < arrayIntervalos.length; j++) {
					boolean firstTime = true;
					for (int i = 0; i < arrayIntervalos[j]; i++) {
						
						if(firstTime){
							timeTmp = arrayHoras[j];
							firstTime = false;
						}else{
							if(timeTmp==null)continue;
							timeTmp = ClinicalSysUtil.addTime(timeTmp, (float) tempoConsulta);
						}
						
						sb.append(lineColor?"<tr>":"<tr style='background-color:#EDF0F9;'>");
						lineColor = !lineColor;
							sb.append("<td>");
								sb.append(timeTmp);
							sb.append("</td>");
						
						forExterno:	
						for (int k = 0; k < 7; k++) {
							if(diasIndisponiveisDaSemana!=null && diasIndisponiveisDaSemana.contains(k)){
								SimpleDateFormat format = new SimpleDateFormat("HH:mm");
								for (IndispAgendaColaborador indisp : indisponibilidades) {
									if(indisp.getDiaSemana() == k && indisp.getHora().equals(format.format(timeTmp))){
										sb.append("<td style='background-color:#E3909E;'></td>");
										continue forExterno;
									}
										
								}
								sb.append("<td style='background-color:#FFFF80;'></td>");
							}else{
								sb.append("<td style='background-color:#FFFF80;'></td>");
							}
						}
					}
					sb.append("</tr>");
				}
				sb.append("</table>");
				
				sb.append("<br/>");
				
				sb.append("<table style='width:50%; margin-left:0px;'>");
					sb.append("<tr style='background-color:#DDE5FF;'>");
						sb.append("<td colspan='2'>");
							sb.append("<center> Legenda </center>");
						sb.append("</td>");
					sb.append("</tr>");
					sb.append("<tr>");
						sb.append("<td style='background-color:#FFFF80;'>");
							sb.append("<center> Dispon�vel </center>");
						sb.append("</td>");
						sb.append("<td style='background-color:#E3909E;'>");
							sb.append("<center> Indispon�vel </center>");
						sb.append("</td>");
					sb.append("</tr>");
				sb.append("</table>");
				
				
				View.getCurrent().println(sb.toString());
			}
			else{
				for (int j = 0; j < total; j++) {
					for (int i = 0; i < 7; i++) {
						if(request.getParameter(i+"_"+j)==null)
							indisponibilidades.add(new IndispAgendaColaborador(i, request.getParameter(("hora_"+j)), ca));
					}
				}
				ColaboradorAgenda catmp = colaboradorAgendaService.getByColaborador(ca.getColaborador(), ca.getTipoAgenda());
				if(catmp==null)
					colaboradorAgendaService.saveOrUpdate(ca);
				else{
					TipoAgenda ta = tipoAgendaService.loadForEntrada(ca.getTipoAgenda());
					request.addError("J� existe uma agenda de "+ ta.getDescricao()+ " ativa para este colaborador.");
					return index(request, ca);
				}
				for (IndispAgendaColaborador indispAgendaColaborador : indisponibilidades) {
					indispAgendaColaboradorService.saveOrUpdate(indispAgendaColaborador);
				}
				List<ColaboradorAgenda> colaboradoresAgenda = colaboradorAgendaService.findAll();
				return new ModelAndView("process/colaboradorAgendaResumo","lista",colaboradoresAgenda);
			}
		}
		else return new ModelAndView("process/colaboradorAgenda","colaboradorAgenda",ca);
		
		return index(request, ca);
	}
	
	
	/**
	 * <p>Fun�ao via Ajax que retorna a agenda do colaborador formatado em tabela
	 */
	public void getAgendaColaborador(WebRequestContext request, ColaboradorAgenda ca){
		int id = Integer.parseInt(getParameter("idColaboradorAgenda"));
		ca.setIdColaboradorAgenda(id);
		ca = colaboradorAgendaService.load(ca);
		generateAgenda(request, ca);
	}
	
	/**
	 * Criar on delete cascade no banco de dados
	 * @param request
	 * @param ca
	 */
	public void desativarAgenda(WebRequestContext request, ColaboradorAgenda ca){
		int id = Integer.parseInt(getParameter("idColaboradorAgenda"));
		ca.setIdColaboradorAgenda(id);
		
		/**
		 * Procura para ver se existe agendamento para este colaborador, se tiver sua agenda n�o pode ser alterada...
		 */
		List<AgendamentoConsulta> consultas = agendamentoConsultaService.getConsultasAgendadasByColaboradorSuperiores(ca.getColaborador());
		if(consultas!=null && !consultas.isEmpty()){
			View.getCurrent().println("msg = 'N�o foi poss�vel desativar esta agenda, pois existem agendamentos para este colaborador.';");
		}else{
			colaboradorAgendaService.delete(ca);
			View.getCurrent().println("var ok = true;");
			View.getCurrent().println("msg = 'Registro Exclu�do com sucesso!';");
		}
			
		
		
	}
	
	/**
	 * Retorna o tempo de atendimento em minutos
	 */
	public static long getTempoAtendimento (long inicio, long fim, float resultado, float tempoConsulta ){
		long posicao  =  (inicio - fim) / 60000;
		resultado = new Float(posicao)/tempoConsulta;
		if((resultado % 2 >= 0.8) && (resultado*tempoConsulta < posicao))
			posicao += tempoConsulta;
		
		return posicao;
		
	}
	
}

package br.com.orionx.clinicalsys.bean;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

import br.com.orionx.clinicalsys.util.log.Inclusao;
import br.com.orionx.clinicalsys.util.log.Log;

@Entity
@Table(name = "agendamentoconsulta")
public class AgendamentoConsulta implements Log, Inclusao{
	
	private Integer idAgendamentoConsulta;
	private Paciente paciente;
	private Colaborador colaborador;
	private Plano plano;
	private Date dia;
	private Time hora;
	private Time horaFim;
	private String observacoes;
	
	private Integer situacao;  //EnumSituacoesAgendamento
	private boolean allDay;
	private Time horaChegada;
	private Time horaAtendimento;
	private boolean retorno;
	private boolean remarcacao;
	
	private String telefones;
	private String nascimento;
	
	//Log
	private Integer idUsuarioAltera;
	private Timestamp dtAltera;
	
	//Inclusao
	private Integer idUsuarioInclusao;
	private Timestamp dtInclusao;
	
	//Transient
	private String situacaoURL;
	private String situacaoDESC;
	
	
	public AgendamentoConsulta(){}
	
	public AgendamentoConsulta(Integer idAgendamentoConsulta){
		this.idAgendamentoConsulta = idAgendamentoConsulta;
	}
	
	//GET
	@Transient
	public String getTelefones() {
		return telefones;
	}
	@Transient
	public String getNascimento() {
		return nascimento;
	}
	@Transient
	public String getSituacaoDESC() {
		return situacaoDESC;
	}
	@Transient
	public String getSituacaoURL() {
		return situacaoURL;
	}
	public void setTelefones(String telefones) {
		this.telefones = telefones;
	}
	public void setNascimento(String nascimento) {
		this.nascimento = nascimento;
	}
	public void setSituacaoDESC(String situacaoDESC) {
		this.situacaoDESC = situacaoDESC;
	}
	public void setSituacaoURL(String situacaoURL) {
		this.situacaoURL = situacaoURL;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getIdAgendamentoConsulta() {
		return idAgendamentoConsulta;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idPaciente")
	@Required
	public Paciente getPaciente() {
		return paciente;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idColaborador")
	@Required
	public Colaborador getColaborador() {
		return colaborador;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idPlano")
	@Required
	public Plano getPlano() {
		return plano;
	}
	@Required
	public Date getDia() {
		return dia;
	}
	@Required
	public Time getHora() {
		return hora;
	}
	@Required
	public Time getHoraFim() {
		return horaFim;
	}
	
	@MaxLength(990)
	@Column(length=1000)
	public String getObservacoes() {
		return observacoes;
	}
	@Required
	public Integer getSituacao() {
		return situacao;
	}
	public boolean isAllDay() {
		return allDay;
	}
	public Time getHoraChegada() {
		return horaChegada;
	}
	public Time getHoraAtendimento() {
		return horaAtendimento;
	}
	public boolean isRetorno() {
		return retorno;
	}
	public boolean isRemarcacao() {
		return remarcacao;
	}
	
	//SET
	public void setIdAgendamentoConsulta(Integer idAgendamentoConsulta) {
		this.idAgendamentoConsulta = idAgendamentoConsulta;
	}
	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setPlano(Plano plano) {
		this.plano = plano;
	}
	public void setDia(Date dia) {
		this.dia = dia;
	}
	public void setHora(Time hora) {
		this.hora = hora;
	}
	public void setHoraFim(Time horaFim) {
		this.horaFim = horaFim;
	}
	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}
	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}
	public void setAllDay(boolean allDay) {
		this.allDay = allDay;
	}
	public void setHoraChegada(Time horaChegada) {
		this.horaChegada = horaChegada;
	}
	public void setHoraAtendimento(Time horaAtendimento) {
		this.horaAtendimento = horaAtendimento;
	}
	public void setRetorno(boolean retorno) {
		this.retorno = retorno;
	}
	public void setRemarcacao(boolean remarcacao) {
		this.remarcacao = remarcacao;
	}

	//LOG
	@Override
	public void setIdUsuarioAltera(Integer idUsuarioAltera) {
		this.idUsuarioAltera = idUsuarioAltera;
	}

	@Override
	public void setDtAltera(Timestamp dtAltera) {
		this.dtAltera = dtAltera;
	}

	@Override
	public Integer getIdUsuarioAltera() {
		return idUsuarioAltera;
	}

	@Override
	public Timestamp getDtAltera() {
		return dtAltera;
	}
	
	//INCLUSAO
	@Override
	public void setIdUsuarioInclusao(Integer idUsuarioInclusao) {
		this.idUsuarioInclusao = idUsuarioInclusao;
	}

	@Override
	public void setDtInclusao(Timestamp dtInclusao) {
		this.dtInclusao = dtInclusao;
	}

	@Override
	public Integer getIdUsuarioInclusao() {
		return idUsuarioInclusao;
	}

	@Override
	public Timestamp getDtInclusao() {
		return dtInclusao;
	}
}

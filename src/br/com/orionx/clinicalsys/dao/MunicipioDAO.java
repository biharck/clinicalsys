package br.com.orionx.clinicalsys.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;

import br.com.orionx.clinicalsys.bean.Municipio;
import br.com.orionx.clinicalsys.bean.Pais;
import br.com.orionx.clinicalsys.bean.UnidadeFederativa;
import br.com.orionx.clinicalsys.filtros.MunicipioFiltro;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

@DefaultOrderBy("municipio.nome")
public class MunicipioDAO extends GenericDAO<Municipio> {

	
	@Override
	public void updateListagemQuery(QueryBuilder<Municipio> query,	FiltroListagem _filtro) {
		MunicipioFiltro filtro = (MunicipioFiltro) _filtro;
		query.where("municipio.unidadeFederativa=?",filtro.getUf()).whereLikeIgnoreAll("municipio.nome", filtro.getNome());
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Municipio> query) {
		query.leftOuterJoinFetch("municipio.unidadeFederativa uf");
		query.leftOuterJoinFetch("uf.pais pais");
	}
	
	/**
	 * <p>M�todo que recebe o c�digo do munic�pio e recebe o munic�pio,pa�s e uf
	 * @param wsmunicipio c�digo do munic�pio
	 * @return {@link Municipio} {@link UnidadeFederativa} {@link Pais}
	 */
	public Municipio getMunicipioByWebService(String wsmunicipio){
		return query()
					.select("municipio.nome, municipio.idMunicipio,unidadeFederativa.nome,unidadeFederativa.idUF,pais.nome, pais.idPais")
					.join("municipio.unidadeFederativa unidadeFederativa")
					.join("unidadeFederativa.pais pais")
					.where("municipio.idMunicipio=?",Integer.parseInt(wsmunicipio))
					.unique();
	}
	
	/**
	 * <p>Atrav�s do munic�pio � retornado a UF e o pais.
	 * @return {@link Municipio}
	 */
	public Municipio getMunicipioCompleto(Municipio m){
		return query()
				.select("municipio.nome, municipio.idMunicipio,unidadeFederativa.nome,unidadeFederativa.idUF,pais.nome, pais.idPais")
				.join("municipio.unidadeFederativa unidadeFederativa")
				.join("unidadeFederativa.pais pais")
				.where("municipio=?",m)
				.unique();
	}
	
	/**
	 * <p>Atrav�s do munic�pio � retornado a UF e o pais.
	 * @return {@link List} {@link Municipio}
	 */
	public List<Municipio> getMunicipiosCompletos(Integer id){
		return query()
				.select("municipio.nome, municipio.idMunicipio,unidadeFederativa.nome,unidadeFederativa.idUF,pais.nome, pais.idPais")
				.join("municipio.unidadeFederativa unidadeFederativa")
				.join("unidadeFederativa.pais pais")
				.where("unidadeFederativa.idUF=?",id)
				.list();
	}
}

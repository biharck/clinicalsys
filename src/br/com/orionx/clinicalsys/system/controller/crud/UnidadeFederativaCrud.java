package br.com.orionx.clinicalsys.system.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;

import br.com.orionx.clinicalsys.bean.UnidadeFederativa;
import br.com.orionx.clinicalsys.filtros.UnidadeFederativaFiltro;
import br.com.orionx.clinicalsys.util.generics.CrudController;

@Controller(path="/system/pag/UF",authorizationModule=CrudAuthorizationModule.class)
public class UnidadeFederativaCrud extends CrudController<UnidadeFederativaFiltro, UnidadeFederativa, UnidadeFederativa> {

	@Override
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Administração &raquo; <a href=\"/ClinicalSys/system/pag/UF\">Unidade Federativa</a></p>";
	}
	
}

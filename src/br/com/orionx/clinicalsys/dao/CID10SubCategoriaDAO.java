package br.com.orionx.clinicalsys.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.support.rowset.SqlRowSet;

import br.com.orionx.clinicalsys.bean.CID10SubCategoria;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class CID10SubCategoriaDAO extends GenericDAO<CID10SubCategoria> {

	
	/**
	 * <p>Método responsável em buscar sub-categorias de um cid 10 a partir de seu id
	 * @return {@link List} {@link CID10SubCategoria}
	 */
	public List<CID10SubCategoria> getCid10SubCategoria(String categoria){
		
		List<CID10SubCategoria> subCategorias = new ArrayList<CID10SubCategoria>();
		
		
		SqlRowSet srs = getJdbcTemplate().queryForRowSet(
				"select * "+
				"from `cid10subcategoria` sc "+
				"where SUBSTRING(sc.subCat from 1 for 3) = '"+categoria+"'");
		
		while (srs.next()) {
			subCategorias.add(new CID10SubCategoria(srs.getInt("idCid10SubCategoria"),
									  srs.getString("subCat"),
									  srs.getString("classif"),
									  srs.getString("restrSexo"),
									  srs.getString("causaObito"),
									  srs.getString("descricao"),
									  srs.getString("descrabrev"),
									  srs.getString("refer"),
									  srs.getString("excluidos")));
	    }
		return subCategorias;
	}
}


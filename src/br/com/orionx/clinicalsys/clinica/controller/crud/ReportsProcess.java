package br.com.orionx.clinicalsys.clinica.controller.crud;

import java.util.ArrayList;
import java.util.List;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

import br.com.orionx.clinicalsys.bean.Papel;
import br.com.orionx.clinicalsys.bean.Permissao;
import br.com.orionx.clinicalsys.bean.Usuario;
import br.com.orionx.clinicalsys.service.PapelService;
import br.com.orionx.clinicalsys.service.PermissaoService;
import br.com.orionx.clinicalsys.util.ClinicalSysUtil;

@Controller(path="/clinic/process/Reports",authorizationModule=ProcessAuthorizationModule.class)
public class ReportsProcess extends MultiActionController{

	private PermissaoService permissaoService;
	private PapelService papelService;
	
	public void setPermissaoService(PermissaoService permissaoService) {
		this.permissaoService = permissaoService;
	}
	public void setPapelService(PapelService papelService) {
		this.papelService = papelService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request){
		
		List<Permissao> permissoes = new ArrayList<Permissao>();
	
		Usuario u = ClinicalSysUtil.getUsuarioLogado();
		
		
		for (Papel p : papelService.findByUsuario(u)) {
			permissoes.addAll(permissaoService.getPermissoesUsuario(p));
		}
		request.setAttribute("pathURL", getPathURL());
		return new ModelAndView("direct:process/reports","permissoes",permissoes);
	}
	
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; <a href=\"/ClinicalSys/clinic/process/Reports\">Relatórios</a></p>";
	}
	
}

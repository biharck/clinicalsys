package br.com.orionx.clinicalsys.service;

import java.util.List;

import br.com.orionx.clinicalsys.bean.Paciente;
import br.com.orionx.clinicalsys.dao.PacienteDAO;
import br.com.orionx.clinicalsys.filtros.PacienteFiltro;
import br.com.orionx.clinicalsys.util.generics.GenericService;

public class PacienteService extends GenericService<Paciente> {

	private PacienteDAO pacienteDAO;
	
	public void setPacienteDAO(PacienteDAO pacienteDAO) {
		this.pacienteDAO = pacienteDAO;
	}
	
	public String getPacienteAutoComplete(String nome){
		StringBuilder sb = new StringBuilder();
		List<Paciente> pacientes = pacienteDAO.getPacienteAutoComplete(nome);
		for (Paciente paciente : pacientes) {
			sb.append(paciente.getNome() + " - " + paciente.getCpf() + " [" + paciente.getIdPaciente() + "];");
		}
		return sb.toString();
	}
	
	public Paciente getPacienteByCPF(String cpf){
		return pacienteDAO.getPacienteByCPF(cpf);
	}
	
	public Paciente getPacienteProntuario(Paciente paciente){
		return pacienteDAO.getPacienteProntuario(paciente);
	}
	public List<Paciente> getPacientesReport(PacienteFiltro filtro){
		return pacienteDAO.getPacientesReport(filtro);
	}
}

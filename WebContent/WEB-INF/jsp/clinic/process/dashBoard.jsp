<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>


<script type="text/javascript" src="${application}/js/jscharts.js"></script>

<!-- Begin three column window -->
<div class="onecolumn" style="padding-bottom:20px">
	<div class="header">
		<span>Dashboard</span>
	</div>
	<br class="clear"/>
	<div class="content">
		<!-- Begin left column window -->
		<div class="twocolumn">
			<div class="column_left">
				<div class="header">
					<span>Form elements</span>
				</div>
				<br class="clear"/>
				<div class="content">
					<div id="chart_container2">Loading chart...</div>
					<script type="text/javascript">
						var myChart = new JSChart('chart_container2', 'pie', '');
						myChart.setDataArray([['Sector 1', 2],['Sector 2', 1],['Sector 3', 3],['Sector 4', 6],['Sector 5', 9],['Sector 6', 10]]);
						myChart.colorize(['#F8C600','#F8A300','#F88000','#F85D00','#F84600','#F7F200']);
						myChart.setSize(420, 420);
						myChart.setPieRadius(130);
						myChart.setTitleColor('#000');
						myChart.setPieUnitsColor('#000');
						myChart.setPieUnitsFontSize(11);
						myChart.setPieValuesFontSize(11);
						myChart.setPieValuesColor('#000');
						myChart.set3D(true);
						myChart.setPieAngle(60);
						myChart.setPieDepth(30);
						myChart.draw();
					</script>
				</div>
			</div>
			<!-- End left column window -->
			
			<!-- Begin right column window -->
			<div class="column_right">
				<div class="header">
					<span>Text style and photos</span>
				</div>
				<br class="clear"/>
				<div class="content">
					<div id="chart_container">Loading chart...</div>
					<script type="text/javascript">
						var myChart = new JSChart('chart_container', 'bar', '');
						myChart.setDataArray([['Jan', 47.5],['Feb', 42],['Mar', 32.5],['Apr', 29],['May', 25],['Jun', 22.5],['Jul', 27],['Aug', 33.5],['Sep', 38],['Oct', 34],['Nov', 30],['Dec', 22.5]]);
						myChart.colorize(['#A7D0C8','#C8E1DB','#8BCEC0','#66B8A7','#66B8A7','#80CACB','#9ACAC1','#B4D9D1','#C5DCD7','#D1E4E0','#C7DFDA','#B2D7CF']);
						myChart.setSize(420, 420);
						myChart.setBarValues(false);
						myChart.setBarOpacity(0.7);
						myChart.setBarSpacingRatio(35);
						myChart.setBarBorderWidth(0);
						myChart.setTitle('Home broadband penetration');
						myChart.setTitleFontSize(10);
						myChart.setTitleColor('#408F7F');
						myChart.setAxisValuesColor('#408F7F');
						myChart.setAxisNameX('');
						myChart.setAxisNameY('%');
						myChart.setAxisNameColor('#408F7F');
						myChart.setAxisColor('#5DB0A0');
						myChart.setGridOpacity(0.8);
						myChart.setGridColor('#B9D7C9');
						myChart.draw();
					</script>
				</div>
			</div>
			<!-- End right column window -->
		</div>
		<!-- End two column window -->
		<br class="clear"/>
		<!-- Begin left column window -->
		<div class="twocolumn">
			<div class="column_left">
				<div class="header">
					<span>Form elements</span>
				</div>
				<br class="clear"/>
				<div class="content">
					<div id="chart_container3">Loading chart...</div>
					<script type="text/javascript">
						var myChart = new JSChart('chart_container3', 'bar', '');
						myChart.setDataArray([['Mar02', 24],['Mar03', 57],['Mar04', 33],['Mar05', 72],['Mar06', 22]]);
						myChart.colorize(['#C7D217','#B4BE14','#949E07','#6B720C','#E3E982']);
						myChart.setSize(600, 300);
						myChart.setIntervalEndY(80);
						myChart.setTitle('Year-to-year growth rates in home broadband penetration');
						myChart.setTitleFontSize(10);
						myChart.setGridOpacity(0.8);
						myChart.setBarSpacingRatio(55);
						myChart.setBarValuesColor('#7B7D77');
						myChart.setBarBorderWidth(0);
						myChart.setBarOpacity(1);
						myChart.setAxisWidth(1);
						myChart.setAxisNameX('');
						myChart.setAxisNameY('');
						myChart.draw();
					</script>
				</div>
			</div>
			<!-- End left column window -->
			
			<!-- Begin right column window -->
			<div class="column_right">
				<div class="header">
					<span>Text style and photos</span>
				</div>
				<br class="clear"/>
				<div class="content">
					<div id="chart_container4">Loading chart...</div>
					<script type="text/javascript">
						var myChart = new JSChart('chart_container4', 'line', '');
						myChart.setDataArray([[1, 20],[10, 60],[20, 79],[30, 90],[40, 100],[50, 113],[60, 125],[70, 135],[80, 148],[90, 170],[99, 215]], 'blue line');
						myChart.setDataArray([[0, 28],[8, 85],[20, 90],[30, 100],[40, 118],[50, 145],[60, 180],[70, 155],[80, 160],[90, 190],[100, 205]], 'red line');
						myChart.colorize(['#3E90C9','#3E90C9','#3E90C9','#3E90C9','#3E90C9','#3E90C9','#3E90C9','#3E90C9','#3E90C9','#3E90C9','#3E90C9']);
						myChart.setSize(550, 300);
						myChart.setTitle('Interactive line chart showing inverse of the normal cumulative distribution');
						myChart.setTitleColor('#37379E');
						myChart.setTitleFontSize(10);
						myChart.setLineColor('#37379E');
						myChart.setLineOpacity(1);
						myChart.setLineWidth(2);
						myChart.setAxisValuesColor('#37379E');
						myChart.setAxisNameX('%');
						myChart.setAxisNameY('');
						myChart.setAxisColor('#B7C611');
						myChart.setGridColor('#B7C611');
						myChart.setTooltip([1,'20','blue line']);
						myChart.setTooltip([10,'60','blue line']);
						myChart.setTooltip([20,'79','blue line']);
						myChart.setTooltip([30,'90','blue line']);
						myChart.setTooltip([40,'100','blue line']);
						myChart.setTooltip([50,'113','blue line']);
						myChart.setTooltip([60,'125','blue line']);
						myChart.setTooltip([70,'135','blue line']);
						myChart.setTooltip([80,'148','blue line']);
						myChart.setTooltip([90,'170','blue line']);
						myChart.setTooltip([99,'215','blue line']);
						myChart.setFlagColor('#37379F');
						myChart.setFlagRadius(4);
						myChart.setAxisValuesNumberY(10);
						myChart.setIntervalEndY(225);
						myChart.setIntervalStartY(0);
						myChart.setLineColor('#C83636', 'red line');
						myChart.setTooltip([0,'28','red line']);
						myChart.setTooltip([8,'85','red line']);
						myChart.setTooltip([20,'95','red line']);
						myChart.setTooltip([30,'100','red line']);
						myChart.setTooltip([40,'118','red line']);
						myChart.setTooltip([50,'145','red line']);
						myChart.setTooltip([60,'180','red line']);
						myChart.setTooltip([70,'155','red line']);
						myChart.setTooltip([80,'160','red line']);
						myChart.setTooltip([90,'190','red line']);
						myChart.setTooltip([100,'205']);
						myChart.setLegendShow(true);
						myChart.draw();
					</script>
				</div>
			</div>
			<!-- End right column window -->
		</div>
		<!-- End two column window -->
		<br class="clear"/>
</div>
<!-- End three column window -->

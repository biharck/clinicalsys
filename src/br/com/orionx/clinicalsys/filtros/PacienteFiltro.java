package br.com.orionx.clinicalsys.filtros;

import org.nextframework.types.Cpf;

import br.com.orionx.clinicalsys.bean.Operadora;
import br.com.orionx.clinicalsys.bean.Plano;
import br.com.orionx.clinicalsys.bean.Sexo;
import br.com.orionx.clinicalsys.util.generics.FiltroListagem;

public class PacienteFiltro extends FiltroListagem {

	private String nome;
	private Sexo sexo;
	private Cpf cpf;
	private Integer registro;
	//Planos...
	private Operadora operadora;
	private Plano plano;
	private AtivoEnum ativoEnum;
	
	
	public Integer getRegistro() {
		return registro;
	}
	public void setRegistro(Integer registro) {
		this.registro = registro;
	}
	public String getNome() {
		return nome;
	}
	public Sexo getSexo() {
		return sexo;
	}
	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}
	public Cpf getCpf() {
		return cpf;
	}
	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	public Operadora getOperadora() {
		return operadora;
	}
	public void setOperadora(Operadora operadora) {
		this.operadora = operadora;
	}
	public Plano getPlano() {
		return plano;
	}
	public AtivoEnum getAtivoEnum() {
		return ativoEnum;
	}
	public void setPlano(Plano plano) {
		this.plano = plano;
	}
	public void setAtivoEnum(AtivoEnum ativoEnum) {
		this.ativoEnum = ativoEnum;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}

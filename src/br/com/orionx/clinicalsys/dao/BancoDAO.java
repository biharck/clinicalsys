package br.com.orionx.clinicalsys.dao;

import java.util.List;

import br.com.orionx.clinicalsys.bean.Banco;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class BancoDAO extends GenericDAO<Banco>{

	@Override
	public List<Banco> findAll() {
		return query().where("banco.ativo is true").list();
	}
	
	@Override
	public List<Banco> findForCombo(String... extraFields) {
		extraFields[0] += "ativo";
		return super.findForCombo(extraFields);
	}

}

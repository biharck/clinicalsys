package br.com.orionx.clinicalsys.sistema.authorization.dao;

import java.util.List;
import java.util.Map;

import org.nextframework.authorization.AuthorizationDAO;
import org.nextframework.authorization.Permission;
import org.nextframework.authorization.Role;
import org.nextframework.authorization.User;
import org.nextframework.persistence.QueryBuilder;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate3.HibernateTemplate;

import br.com.orionx.clinicalsys.bean.Papel;
import br.com.orionx.clinicalsys.bean.Permissao;
import br.com.orionx.clinicalsys.bean.Usuario;
import br.com.orionx.clinicalsys.bean.UsuarioPapel;


/**
 * <p>DAO de autoriza��o, detectado automaticamente.<br>
 * Fornece informa��es sobre autoriza��o para o framework</p>
 * 
 *  @author biharck
 */
public class NextAuthorizationDAO implements AuthorizationDAO {
    
    private HibernateTemplate hibernateTemplate;
    private JdbcTemplate jdbcTemplate;
    
    public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
        this.hibernateTemplate = hibernateTemplate;
    }
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
    
    //API
    /**
     * @return Retorna uma {@link list} {@link Papel} do sistema
     */
    public Role[] findAllRoles() {
        List<Role> roles = new QueryBuilder<Role>(hibernateTemplate)
                            .from(Papel.class)
                            .list();
        return roles.toArray(new Role[roles.size()]);
    }
 
    //API
    /**
     * <p>Acha a permiss�o para determinado n�vel em determinada tela</p>
     * 
     * @param role papel que desejamos a permiss�o
     * @param controlName path que representa a tela desejada
     * @return {@link Permission}
     * 
     * @author biharck
     */
    public Permission findPermission(Role role, String controlName) {
        return new QueryBuilder<Permission>(hibernateTemplate)
                    .from(Permissao.class)
                    .where("permissao.role = ?", role)
                    .where("permissao.path = ?", controlName)
                    .unique();
    }
 
    //API
    /**
     * <p>Acha o usu�rio pelo login</p>
     * 
     * @return {@link Usuario}
     */
    public User findUserByLogin(String login) {
        return new QueryBuilder<User>(hibernateTemplate)
                    .from(Usuario.class)
                    .where("usuario.login = ?", login)
                    .unique();
    }
 
    //API
    /**
     * <p>Acha os papeis (n�veis) de determinado usu�rio</p>
     * @param user Usu�rio
     * @return {@link Role}[]
     * 
     * @author biharck
     */
    public Role[] findUserRoles(User user) {
        List<Role> list = new QueryBuilder<Role>(hibernateTemplate)
                            .select("papel")
                            .from(UsuarioPapel.class)
                            .leftOuterJoin("usuarioPapel.papel papel")
                            .leftOuterJoin("usuarioPapel.usuario usuario")
                            .where("usuario = ?", user)
                            .list();
        return list.toArray(new Role[list.size()]);
    }
    
    //API
    /**
     * <p>Salva ou atualiza uma permiss�o no banco de dados.</p>
     * 
     * @return {@link Permission}
     */
    @SuppressWarnings("unchecked")
    public Permission savePermission(String controlName, Role role, Map permissionMap) {
        Permissao permissao = (Permissao)findPermission(role, controlName);
        if(permissao == null){
            permissao = new Permissao();
            permissao.setPath(controlName);
            permissao.setRole((Papel) role);
        }
        permissao.setPermissionMap(permissionMap);
        hibernateTemplate.saveOrUpdate(permissao);
        return permissao;
    }
    
}

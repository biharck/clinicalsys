package br.com.orionx.clinicalsys.system.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.orionx.clinicalsys.bean.ViaAdministracao;
import br.com.orionx.clinicalsys.filtros.ViaAdministracaoFiltro;
import br.com.orionx.clinicalsys.util.DatabaseError;
import br.com.orionx.clinicalsys.util.exception.ClinicalSysException;
import br.com.orionx.clinicalsys.util.generics.CrudController;

@Controller(path="/system/pag/ViaAdministracao",authorizationModule=CrudAuthorizationModule.class)
public class ViaAdministracaoCrud extends CrudController<ViaAdministracaoFiltro, ViaAdministracao, ViaAdministracao>{

	@Override
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Administração &raquo; <a href=\"/ClinicalSys/system/pag/ViaAdministracao\">Vias de Administração</a></p>";
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, ViaAdministracao form)throws CrudException {
		return saveUnique(request, form,true);
	}
	
	@Override
	public ModelAndView saveAndNew(WebRequestContext request, ViaAdministracao form)throws Exception {
		return saveUnique(request, form,false);
	}
	
	public ModelAndView saveUnique(WebRequestContext request, ViaAdministracao form, boolean retornaListagem){
		try {
			salvar(request, form);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_nome_viaadm")) throw new ClinicalSysException("Já existe uma via de administração com este nome cadastrado no sistema.");
			return sendRedirectToAction("entrada");
		}
		catch (Exception e) {
			return sendRedirectToAction("entrada");
		}
		if(retornaListagem)return sendRedirectToAction("listagem");
		else return sendRedirectToAction("entrada");
	}

}

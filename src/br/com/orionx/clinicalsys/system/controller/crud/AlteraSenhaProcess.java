package br.com.orionx.clinicalsys.system.controller.crud;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

import br.com.orionx.clinicalsys.bean.Usuario;
import br.com.orionx.clinicalsys.service.UsuarioService;
import br.com.orionx.clinicalsys.util.ClinicalSysUtil;

@Controller(path="/system/process/AlteraSenha",authorizationModule=ProcessAuthorizationModule.class)
public class AlteraSenhaProcess extends MultiActionController{
	
	private UsuarioService usuarioService;
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request) {
		Usuario user = ClinicalSysUtil.getUsuarioLogado();
		user.setPassword("");
		return new ModelAndView("direct:process/alterasenha","usuario", user);
	}
	
	public ModelAndView updateSenha(WebRequestContext request, Usuario usuario){
		StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
		Usuario user = ClinicalSysUtil.getUsuarioLogado();
		user = usuarioService.getUsuarioById(user.getIdUsuario());
		user.setPassword(encryptor.encryptPassword(usuario.getPassword()));
		try {
			usuarioService.updateSenha(user);
			request.addMessage("Senha Alterada com sucesso!");
		} catch (Exception e) {
			request.addError("Ups! Houve uma falha ao tentar alterar sua senha! Entre em contato com a OrionX o que aconteceu.");
		}
		return index(request);
		
	}

}

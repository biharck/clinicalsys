package br.com.orionx.clinicalsys.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;

import br.com.orionx.clinicalsys.bean.IndustriaFarmaceutica;
import br.com.orionx.clinicalsys.filtros.IndustriaFarmaceuticaFiltro;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

@DefaultOrderBy("industriaFarmaceutica.nome")
public class IndustriaFarmaceuticaDAO extends GenericDAO<IndustriaFarmaceutica>{

	
	@Override
	public void updateListagemQuery(QueryBuilder<IndustriaFarmaceutica> query, FiltroListagem _filtro) {
		IndustriaFarmaceuticaFiltro filtro = (IndustriaFarmaceuticaFiltro) _filtro;
		if(filtro.getAtivoEnum()!=null)
			if(filtro.getAtivoEnum().getSituacao()!=null && (filtro.getAtivoEnum().getSituacao() || !filtro.getAtivoEnum().getSituacao()))
				query.where("industriaFarmaceutica.ativo is "+filtro.getAtivoEnum().getSituacao());
		query.whereLikeIgnoreAll("industriaFarmaceutica.nome", filtro.getNome());
		query.orderBy("industriaFarmaceutica.nome");
		super.updateListagemQuery(query, filtro);
	}
	
	@Override
	public List<IndustriaFarmaceutica> findAll() {
		return query().where("industriaFarmaceutica.ativo is true").orderBy("industriaFarmaceutica.nome").list();
	}
	
	@Override
	public List<IndustriaFarmaceutica> findForCombo(String... extraFields) {
		extraFields[0] += "ativo";
		return super.findForCombo(extraFields);
	}

}

package br.com.orionx.clinicalsys.bean;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name = "colaboradorcargo")
public class ColaboradorCargo {
	
	private Integer idColaboradorCargo;
	private Colaborador colaborador;
	private Cargo cargo;
	private Date dataAdmissao;
	private Date dataDesligamento;
	private String regimeTrabalho;
	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdColaboradorCargo() {
		return idColaboradorCargo;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="idColaborador")
	@ForeignKey(name = "fk_colcargo_colaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="idCargo")
	@ForeignKey(name = "fk_colcargo_cargo")
	@Required
	public Cargo getCargo() {
		return cargo;
	}
	@Required
	public Date getDataAdmissao() {
		return dataAdmissao;
	}
	public Date getDataDesligamento() {
		return dataDesligamento;
	}
	@Required
	@MaxLength(100)
	@MinLength(1)
	@Column(length=200)
	public String getRegimeTrabalho() {
		return regimeTrabalho;
	}
	
	//SET
	public void setIdColaboradorCargo(Integer idColaboradorCargo) {
		this.idColaboradorCargo = idColaboradorCargo;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	public void setDataAdmissao(Date dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}
	public void setDataDesligamento(Date dataDesligamento) {
		this.dataDesligamento = dataDesligamento;
	}
	public void setRegimeTrabalho(String regimeTrabalho) {
		this.regimeTrabalho = regimeTrabalho;
	}
	

}

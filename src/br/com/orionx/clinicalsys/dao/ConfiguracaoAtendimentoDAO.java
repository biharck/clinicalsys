package br.com.orionx.clinicalsys.dao;

import org.nextframework.persistence.SaveOrUpdateStrategy;

import br.com.orionx.clinicalsys.bean.ConfiguracaoAtendimento;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class ConfiguracaoAtendimentoDAO extends GenericDAO<ConfiguracaoAtendimento>{

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("precosConsultas");
	}
	
	public ConfiguracaoAtendimento getDadosConfiguracao() {
		return query()
					.select("configuracaoAtendimento.idConfiguracaoAtendimento,configuracaoAtendimento.tempoConsulta, configuracaoAtendimento.idUsuarioAltera," +
							"configuracaoAtendimento.dtAltera,configuracaoAtendimento.valorConsulta," +
							"pc.idPrecoConsultaEspecialidade, pc.valorConsulta, pc.tempoConsulta, pc.ativo," +
							"es.idEspecializacao, es.nome")
					.where("configuracaoAtendimento.idConfiguracaoAtendimento = ?",getMaxId())
					.leftOuterJoin("configuracaoAtendimento.precosConsultas pc")
					.leftOuterJoin("pc.especializacao es")
					.unique()
					
					;
	}
	
	public Integer getMaxId(){
		return getJdbcTemplate().queryForInt("select coalesce(max(idConfiguracaoAtendimento),1) from configuracaoatendimento");
	}
	
}

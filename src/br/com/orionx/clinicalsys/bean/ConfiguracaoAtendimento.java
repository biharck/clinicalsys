package br.com.orionx.clinicalsys.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.nextframework.types.Money;
import org.nextframework.validation.annotation.MinValue;
import org.nextframework.validation.annotation.Required;

import br.com.orionx.clinicalsys.util.log.Log;

@Entity
@Table(name = "configuracaoatendimento")
/**
 * a cada aletar��o gera um novo registro, assim tendo seu hist�rico,
 * e na tela sempre carrega o mais recente
 */
public class ConfiguracaoAtendimento implements Log{
	
	private Integer idConfiguracaoAtendimento;
	private Money valorConsulta;
	private Integer tempoConsulta;//em minutos tempo de atendimento

	/**
	 *vicular custo da consulta por especialidade, cada vez que mudar o preco da consulta um novo,
	 *registro no BD � gerado, e o anterior � desativado 
	 */
	private List<PrecoConsultaEspecialidade> precosConsultas;
	
	//Log
	private Integer idUsuarioAltera;
	private Timestamp dtAltera;
	
	public ConfiguracaoAtendimento(){}
	public ConfiguracaoAtendimento(Integer idConfiguracaoAtendimento){
		this.idConfiguracaoAtendimento = idConfiguracaoAtendimento;
	}
	
	//GET
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getIdConfiguracaoAtendimento() {
		return idConfiguracaoAtendimento;
	}
	@Required
	@MinValue(1)
	public Money getValorConsulta() {
		return valorConsulta;
	}
	@Required
	@MinValue(1)
	/**
	 * Em minutos
	 */
	public Integer getTempoConsulta() {
		return tempoConsulta;
	}
	@OneToMany(mappedBy = "configuracaoAtendimento")
	public List<PrecoConsultaEspecialidade> getPrecosConsultas() {
		return precosConsultas;
	}
	
	
	//SET
	public void setIdConfiguracaoAtendimento(Integer idConfiguracaoAtendimento) {
		this.idConfiguracaoAtendimento = idConfiguracaoAtendimento;
	}
	public void setValorConsulta(Money valorConsulta) {
		this.valorConsulta = valorConsulta;
	}
	public void setTempoConsulta(Integer tempoConsulta) {
		this.tempoConsulta = tempoConsulta;
	}
	public void setPrecosConsultas(List<PrecoConsultaEspecialidade> precosConsultas) {
		this.precosConsultas = precosConsultas;
	}
	
	//LOG
	@Override
	public void setIdUsuarioAltera(Integer idUsuarioAltera) {
		this.idUsuarioAltera = idUsuarioAltera;
	}

	@Override
	public void setDtAltera(Timestamp dtAltera) {
		this.dtAltera = dtAltera;
	}

	@Override
	public Integer getIdUsuarioAltera() {
		return idUsuarioAltera;
	}

	@Override
	public Timestamp getDtAltera() {
		return dtAltera;
	}


}

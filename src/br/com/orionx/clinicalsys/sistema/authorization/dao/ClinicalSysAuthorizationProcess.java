package br.com.orionx.clinicalsys.sistema.authorization.dao;

import org.nextframework.authorization.AuthorizationProcessFilter;
import org.nextframework.authorization.DefaultAuthorizationProcess;
import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

@Controller(path="/system/process/authorization", authorizationModule=ProcessAuthorizationModule.class)
public class ClinicalSysAuthorizationProcess extends DefaultAuthorizationProcess {

	@Override
	public ModelAndView list(WebRequestContext request,	AuthorizationProcessFilter authorizationFilter) {
		request.getSession().setAttribute("pathURL", getPathURL());
		return super.list(request, authorizationFilter);
	}
	
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Administração &raquo; <a href=\"/system/process/authorization\">Autorizações do Sistema</a></p>";
	}

}

package br.com.orionx.clinicalsys.dao;

import java.util.List;

import br.com.orionx.clinicalsys.bean.ConfiguracaoAtendimento;
import br.com.orionx.clinicalsys.bean.PrecoConsultaEspecialidade;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class PrecoConsultaEspecialidadeDAO extends GenericDAO<PrecoConsultaEspecialidade> {

	
	/**
	 * <p>M�todo que retornar so pre�os das configura��es de atendimento a partir de uma configura��o de atendimento
	 * @return {@link List} {@link PrecoConsultaEspecialidade}
	 * @author biharck
	 * 
	 */
	public List<PrecoConsultaEspecialidade> getPrecosByConfiguracao(ConfiguracaoAtendimento ca){
		return query().select("precoConsultaEspecialidade").where("precoConsultaEspecialidade.configuracaoAtendimento=?",ca).where("precoConsultaEspecialidade.ativo is true").list();
	}
	
	@Override
	public List<PrecoConsultaEspecialidade> findAll() {
		return query().where("precoConsultaEspecialidade.ativo is true").list();
	}
	
	@Override
	public List<PrecoConsultaEspecialidade> findForCombo(String... extraFields) {
		extraFields[0] += "ativo";
		return super.findForCombo(extraFields);
	}

}

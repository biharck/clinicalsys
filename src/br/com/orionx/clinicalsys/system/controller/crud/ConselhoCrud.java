package br.com.orionx.clinicalsys.system.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;

import br.com.orionx.clinicalsys.bean.Conselho;
import br.com.orionx.clinicalsys.filtros.ConselhoFiltro;
import br.com.orionx.clinicalsys.util.generics.CrudController;

@Controller(path="/system/pag/Conselho",authorizationModule=CrudAuthorizationModule.class)
public class ConselhoCrud extends CrudController<ConselhoFiltro, Conselho, Conselho> {

	@Override
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Administração &raquo; <a href=\"/ClinicalSys/system/pag/Conselho\">Conselhos</a></p>";
	}

}

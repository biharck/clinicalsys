package br.com.orionx.clinicalsys.system.controller.crud;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

import br.com.orionx.clinicalsys.bean.ConfiguracaoAtendimento;
import br.com.orionx.clinicalsys.dao.ConfiguracaoAtendimentoDAO;
import br.com.orionx.clinicalsys.service.ConfiguracaoAtendimentoService;
import br.com.orionx.clinicalsys.service.PrecoConsultaEspecialidadeService;

@Controller(path="/system/process/ConfiguracaoAtendimento",authorizationModule=ProcessAuthorizationModule.class)
public class ConfiguracaoAtendimentoProcess extends MultiActionController{

	ConfiguracaoAtendimentoDAO configuracaoAtendimentoDAO;
	ConfiguracaoAtendimentoService configuracaoAtendimentoService;
	PrecoConsultaEspecialidadeService precoConsultaEspecialidadeService;
	
	public void setConfiguracaoAtendimentoDAO(ConfiguracaoAtendimentoDAO configuracaoAtendimentoDAO) {
		this.configuracaoAtendimentoDAO = configuracaoAtendimentoDAO;
	}
	public void setPrecoConsultaEspecialidadeService(PrecoConsultaEspecialidadeService precoConsultaEspecialidadeService) {
		this.precoConsultaEspecialidadeService = precoConsultaEspecialidadeService;
	}
	public void setConfiguracaoAtendimentoService(ConfiguracaoAtendimentoService configuracaoAtendimentoService) {
		this.configuracaoAtendimentoService = configuracaoAtendimentoService;
	}

	@DefaultAction
	public ModelAndView index(WebRequestContext request,ConfiguracaoAtendimento ca){
		request.setAttribute("pathURL", getPathURL());
		ca = configuracaoAtendimentoDAO.getDadosConfiguracao();
		if(ca == null)
			ca = new ConfiguracaoAtendimento();
		else{
			ca.setPrecosConsultas(precoConsultaEspecialidadeService.getPrecosByConfiguracao(ca));
			request.setAttribute("dtAltera", ca.getDtAltera() );
			request.setAttribute("idUsuarioAltera", ca.getIdUsuarioAltera());
		}
		return new ModelAndView("process/configuracaoAtendimento","configuracaoAtendimento",ca);
	}
	
	
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Administração &raquo; <a href=\"/ClinicalSys/system/process/ConfiguracaoAtendimento\">Configurações de Atendimento</a></p>";
	}
	
	
	@Action("salvar")
	public ModelAndView salvar(WebRequestContext request, ConfiguracaoAtendimento ca){
		try {
			configuracaoAtendimentoService.saveOrUpdate(ca);
			request.addMessage("Registro salvo com sucesso");
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Falha ao salvar o registro, entre em contato com o administrador do sistema");
		}
		return index(request, ca);
	}

}

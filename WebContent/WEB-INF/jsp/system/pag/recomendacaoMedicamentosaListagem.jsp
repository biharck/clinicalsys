<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="cs" uri="clinicalsys"%>

<t:listagem >
	<t:janelaFiltro >
		<t:tabelaFiltro columns="1">
		<t:property name="nome" renderAs="doubleline" style="width:300px" id="nome"/>
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados >
		<t:tabelaResultados >
			<n:column header="Nome" order="recomendacaoMedicamentosa.nome">
				${cs:trunc(recomendacaoMedicamentosa.nome,100)}
		  	</n:column>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
<script type="text/javascript">
	$(document).ready(function(){
		$('#nome').focus();
	});
</script>
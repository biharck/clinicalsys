package br.com.orionx.clinicalsys.system.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;

import br.com.orionx.clinicalsys.bean.InteracaoEntreMedicamentos;
import br.com.orionx.clinicalsys.bean.InteracaoMedicamentosa;
import br.com.orionx.clinicalsys.filtros.InteracaoMedicamentosaFiltro;
import br.com.orionx.clinicalsys.service.ClassificacaoMedicacaoService;
import br.com.orionx.clinicalsys.util.generics.CrudController;


@Controller(path="/system/pag/InteracaoMedicamentosa",authorizationModule=CrudAuthorizationModule.class)
public class InteracaoMedicamentosaCrud extends CrudController<InteracaoMedicamentosaFiltro, InteracaoMedicamentosa, InteracaoMedicamentosa>{

	private ClassificacaoMedicacaoService classificacaoMedicacaoService;
	public void setClassificacaoMedicacaoService(ClassificacaoMedicacaoService classificacaoMedicacaoService) {
		this.classificacaoMedicacaoService = classificacaoMedicacaoService;
	}
	
	@Override
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Administração &raquo; <a href=\"/ClinicalSys/system/pag/InteracaoMedicamentosa\">Interações Medicamentosas</a></p>";
	}
	
	@Override
	protected void entrada(WebRequestContext request,InteracaoMedicamentosa form) throws Exception {
		if(form.getInteracoes()!=null)
			for (InteracaoEntreMedicamentos iem : form.getInteracoes()) {
				iem.setClassificacaoMedicacao(classificacaoMedicacaoService.getClassificacaoByMedicamento(iem.getMedicamento()));
			}
		super.entrada(request, form);
	}
	

}

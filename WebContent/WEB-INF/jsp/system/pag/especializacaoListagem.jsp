<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro columns="1">
			<t:property name="nome" renderAs="doubleline" id="nome"/>		
			<t:property name="descricao" renderAs="doubleline" />		
			<t:property name="ativoEnum" renderAs="doubleline"/>		
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados>
			<t:property name="nome"/>
			<t:property name="descricao"/>
			<t:property name="ativo"/>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
<script type="text/javascript">
$(document).ready(function(){
	$('#nome').focus();
});
</script>
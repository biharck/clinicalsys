/*
 * Next Framework http://www.nextframework.org
 * Copyright (C) 2009 the original author or authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * You may obtain a copy of the license at
 * 
 *     http://www.gnu.org/copyleft/lesser.html
 * 
 */
package org.nextframework.view.menu;

import java.util.Iterator;

public class MenuBuilder {
	
	private int identation;
	private String path;
	
	public String build(Menu menu,String path){
		this.path = path.replace("/WEB-INF/menu/", "").replace(".xml", "");
		StringBuilder stringBuilder = new StringBuilder();
//		stringBuilder.append("<ul id='main_menu' class=''>").append('\n');
		identation = 1;
		for (Iterator<Menu> iter = menu.getSubmenus().iterator(); iter.hasNext();) {
			Menu submenu = iter.next();
			build(submenu, stringBuilder,iter.hasNext());
		}
//		stringBuilder.append("</ul>");
		return stringBuilder.toString().replace("align=\"absmiddle\"", "").replace("&nbsp;", "");
	}

	private void build(Menu menu, StringBuilder stringBuilder, boolean hasNext) {
		ident(stringBuilder);
		if(menu.getTitle()!= null && menu.getTitle().matches("--(-)+")){
			stringBuilder.append("_cmSplit");
			return;
		}
		openMenu(stringBuilder);
		if(menu.getUrl()!=null && !menu.getUrl().equals(""))
			stringBuilder.append("<a  onclick='openDialogAguarde()' href='").append(menu.getUrl()).append("'>");
		else
			stringBuilder.append("<a  href='").append(menu.getUrl()).append("'>");
		printItem(menu.getIcon(), stringBuilder);
		printItem(menu.getTitle(), stringBuilder);		
		stringBuilder.append("</a>");
		
		if (menu.containSubMenus()) {
			stringBuilder.append('\n');
			identation++;
			printItem("<ul>", stringBuilder);
			for (Iterator<Menu> iter = menu.getSubmenus().iterator(); iter.hasNext();) {
				Menu submenu = iter.next();
				build(submenu, stringBuilder,iter.hasNext());
			}
			printItem("</ul>", stringBuilder);
			identation--;
			ident(stringBuilder);
			closeMenu(stringBuilder, hasNext);
		} else {
			closeMenu(stringBuilder, hasNext);
		}
	}

	private void closeMenu(StringBuilder stringBuilder, boolean hasNext) {
		stringBuilder.append("</li>");
		stringBuilder.append('\n');
	}

	private void printItem(String texto, StringBuilder stringBuilder) {
		stringBuilder.append(texto);
	}

	private void openMenu(StringBuilder stringBuilder) {
		stringBuilder.append("<li path='").append(path).append("'>");
	}

	private void ident(StringBuilder stringBuilder) {
		for (int i = 0; i < identation; i++) {
			stringBuilder.append("    ");
		}
	}
}

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="cs" uri="clinicalsys"%>

<!-- Begin two column window -->
<!-- Begin left column window -->
<div class="twocolumn">
	<div class="column_left">
		<div class="header">
			<span>Rela��o do CID-O</span>
		</div>
		<br class="clear"/>
		<div class="content">
			<t:tela>
			<div class="dtree">
				<p>
					<a href="javascript: d.openAll();">&nbsp;<img src="/ClinicalSys/images/shortcut/arrow_down.png">&nbsp;Expandir todos</a> 
					|
				 	<a href="javascript: d.closeAll();">&nbsp;<img src="/ClinicalSys/images/shortcut/arrow_up.png">&nbsp;Fechar todos</a>
				</p>
				<script type="text/javascript">
					${tree}
				</script>
				
			</div>
		</t:tela>
		</div>
	</div>
	<!-- End left column window -->
	
	<!-- Begin right column window -->
	<div class="column_right">
		<div class="header">
			<span id="titleCid">...</span>
		</div>
		<br class="clear"/>
		<div class="content" id="textoCID10">
			
			
		</div>
		<a href="#topo">&nbsp;<img src="/ClinicalSys/images/shortcut/arrow_up.png">&nbsp;Topo da P�gina</a>
	</div>
	<!-- End right column window -->
</div>
<!-- End two column window -->

<script type="text/javascript">
	function getText(id,classname){
		var texto = $('#sd'+d.getSelected()).html();
		$('#titleCid').html(texto);
		if(classname == 3)
			ajaxBuscaCid10(id,classname);
		else
			$('#textoCID10').html('');		
	}

</script>
<style type="text/css">
.column_right{
	position: fixed;
	margin-left: 600px;
}
.quote{
	padding: 4px 15px;
}
</style>

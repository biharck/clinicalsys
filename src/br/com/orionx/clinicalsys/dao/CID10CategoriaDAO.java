package br.com.orionx.clinicalsys.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.support.rowset.SqlRowSet;

import br.com.orionx.clinicalsys.bean.CID10Categoria;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class CID10CategoriaDAO extends GenericDAO<CID10Categoria>{
	
	/**
	 * <p>M�todo que retorna as categorias correspondentes ao grupo do cid10
	 * @param catInic categoria inicial {@link String}
	 * @param catFim categoria final {@link String}
	 * @return {@link List} {@link CID10Categoria}
	 */
	public List<CID10Categoria> getCategoriasByGrupos(String catInic, String catFim){
		
		List<CID10Categoria> categorias = new ArrayList<CID10Categoria>();
		
		
		SqlRowSet srs = getJdbcTemplate().queryForRowSet(
				"select * "+
				"from `cid10categoria` c "+
				"where c.`cat`  >= '"+catInic+"' and c.`cat` <= '"+catFim+"' ");
		
		while (srs.next()) {
			categorias.add(new CID10Categoria(srs.getInt("idCid10Categoria"),
									  srs.getString("cat"),
									  srs.getString("classif"),
									  srs.getString("descricao"),
									  srs.getString("descrabrev"),
									  srs.getString("refer"),
									  srs.getString("excluidos")));
	    }
		return categorias;
	}

}

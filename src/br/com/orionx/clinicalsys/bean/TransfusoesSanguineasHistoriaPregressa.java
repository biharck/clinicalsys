package br.com.orionx.clinicalsys.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.validation.annotation.Required;

@Table(name="transfusoessanguineashistoriapregressa")
@Entity
public class TransfusoesSanguineasHistoriaPregressa {

	private Integer idTransfusoesSanguineasHistoriaPregressa;
	private Integer numeroTransfusoes;
	private Date mesAnoQuando;
	private String porque;
	private HistoriaPregressa historiaPregressa;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdTransfusoesSanguineasHistoriaPregressa() {
		return idTransfusoesSanguineasHistoriaPregressa;
	}
	@Required
	public Integer getNumeroTransfusoes() {
		return numeroTransfusoes;
	}
	@Required
	public Date getMesAnoQuando() {
		return mesAnoQuando;
	}
	@Required
	public String getPorque() {
		return porque;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idHistoriaPregressa")
	@ForeignKey(name = "fk_hp_transfusoes")
	public HistoriaPregressa getHistoriaPregressa() {
		return historiaPregressa;
	}
	public void setIdTransfusoesSanguineasHistoriaPregressa(Integer idTransfusoesSanguineasHistoriaPregressa) {
		this.idTransfusoesSanguineasHistoriaPregressa = idTransfusoesSanguineasHistoriaPregressa;
	}
	public void setNumeroTransfusoes(Integer numeroTransfusoes) {
		this.numeroTransfusoes = numeroTransfusoes;
	}
	public void setMesAnoQuando(Date mesAnoQuando) {
		this.mesAnoQuando = mesAnoQuando;
	}
	public void setPorque(String porque) {
		this.porque = porque;
	}
	public void setHistoriaPregressa(HistoriaPregressa historiaPregressa) {
		this.historiaPregressa = historiaPregressa;
	}
	
	
}

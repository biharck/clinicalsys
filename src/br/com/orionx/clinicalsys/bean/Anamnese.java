package br.com.orionx.clinicalsys.bean;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="anamnese")
public class Anamnese {
	
	private Integer idAnamnese;
	private Paciente paciente;
	private String queixaPrincipal;
	
	//historia da doenca atual
	private String inicio;
	private String duracao;
	private String caracteristicaSintomas;
	private String evolucao;
	private String fatoresPioraMelhora;
	private String relacaoOutrasQueixas;
	private String situacaoSintomaMomento;
	private Date dataAnamnese;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdAnamnese() {
		return idAnamnese;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idPaciente")
	@ForeignKey(name="fk_paciente_anamnese")
	public Paciente getPaciente() {
		return paciente;
	}
	@Required
	@MaxLength(5000)
	@Column(length=5000)
	public String getQueixaPrincipal() {
		return queixaPrincipal;
	}
	@Required
	@MaxLength(400)
	public String getInicio() {
		return inicio;
	}
	@MaxLength(400)
	public String getDuracao() {
		return duracao;
	}
	@MaxLength(5000)
	@Column(length=5000)
	@Required
	public String getCaracteristicaSintomas() {
		return caracteristicaSintomas;
	}
	@MaxLength(5000)
	@Column(length=5000)
	public String getEvolucao() {
		return evolucao;
	}
	@MaxLength(5000)
	@Column(length=5000)
	public String getFatoresPioraMelhora() {
		return fatoresPioraMelhora;
	}
	@MaxLength(5000)
	@Column(length=5000)
	public String getRelacaoOutrasQueixas() {
		return relacaoOutrasQueixas;
	}
	@MaxLength(5000)
	@Column(length=5000)
	public String getSituacaoSintomaMomento() {
		return situacaoSintomaMomento;
	}
	@Required
	public Date getDataAnamnese() {
		return dataAnamnese;
	}
	public void setIdAnamnese(Integer idAnamnese) {
		this.idAnamnese = idAnamnese;
	}
	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}
	public void setQueixaPrincipal(String queixaPrincipal) {
		this.queixaPrincipal = queixaPrincipal;
	}
	public void setInicio(String inicio) {
		this.inicio = inicio;
	}
	public void setDuracao(String duracao) {
		this.duracao = duracao;
	}
	public void setCaracteristicaSintomas(String caracteristicaSintomas) {
		this.caracteristicaSintomas = caracteristicaSintomas;
	}
	public void setEvolucao(String evolucao) {
		this.evolucao = evolucao;
	}
	public void setFatoresPioraMelhora(String fatoresPioraMelhora) {
		this.fatoresPioraMelhora = fatoresPioraMelhora;
	}
	public void setRelacaoOutrasQueixas(String relacaoOutrasQueixas) {
		this.relacaoOutrasQueixas = relacaoOutrasQueixas;
	}
	public void setSituacaoSintomaMomento(String situacaoSintomaMomento) {
		this.situacaoSintomaMomento = situacaoSintomaMomento;
	}
	public void setDataAnamnese(Date dataAnamnese) {
		this.dataAnamnese = dataAnamnese;
	}
}

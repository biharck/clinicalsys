package br.com.orionx.clinicalsys.service;

import br.com.orionx.clinicalsys.bean.Municipio;
import br.com.orionx.clinicalsys.dao.MunicipioDAO;
import br.com.orionx.clinicalsys.util.generics.GenericService;

public class MunicipioService extends GenericService<Municipio> {

	private MunicipioDAO municipioDAO;
	
	public void setMunicipioDAO(MunicipioDAO municipioDAO) {
		this.municipioDAO = municipioDAO;
	}
	
	/**
	 * <p>M�todo de refer�ncia ao DAO
	 * @see br.com.orionx.clinicalsys.dao.MunicipioDAO#getMunicipioCompleto
	 * @param m {@link Municipio}
	 * @return {@link Municipio}
	 */
	public Municipio getMunicipioCompleto(Municipio m){
		return municipioDAO.getMunicipioCompleto(m);
	}
	
}

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="cs" uri="clinicalsys"%>

<div class="onecolumn">
	<div class="header">
		<span>Agenda dos Colaboradores</span>
	</div>
	<br class="clear"/>
	<div class="content">
		<t:tela>
			<c:forEach items="${lista}" var="colaboradorAgenda">
				<div class="headerDiv" onclick='openCalendar(${colaboradorAgenda.idColaboradorAgenda})'>
					<c:if test="${!empty colaboradorAgenda.colaborador.foto.cdfile}">
						<img src="${pageContext.request.contextPath}/DOWNLOADFILE/${colaboradorAgenda.colaborador.foto.cdfile}" id="imagem" class="foto"/><br />
					</c:if>
					<c:if test="${empty colaboradorAgenda.colaborador.foto.cdfile}">
						<img src="${pageContext.request.contextPath}/images/peopleNotFound.png" id="imagem" class="foto"/>
					</c:if>
					<div align="left">
						<table style='text-align:justify;'>
							<tr>
								<td>
									<b>Nome:</b>&nbsp;${colaboradorAgenda.colaborador.nome}
								</td>
							</tr>
							<tr>
								<td>
									<b>Agenda de:</b>&nbsp;${colaboradorAgenda.tipoAgenda.descricao}						
								</td>
							</tr>
							<tr>
								<td>
									<b>Especializações:</b>&nbsp;${colaboradorAgenda.colaborador.especializacoesConcat}
								</td>
							</tr>
						</table>
					</div>					
				</div>
			</c:forEach>
					<br class="clear"/>
		</t:tela>
	</div>
</div>
<script type="text/javascript">
$('.headerDiv').mouseover(function(){
	$(this).css('border','2px solid #FFFF80');
});

$('.headerDiv').mouseout(function(){
	$(this).css('border','2px solid #D5E5F1');
});
</script>
package br.com.orionx.clinicalsys.dao;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.orionx.clinicalsys.bean.Colaborador;
import br.com.orionx.clinicalsys.bean.Papel;
import br.com.orionx.clinicalsys.bean.Usuario;
import br.com.orionx.clinicalsys.bean.UsuarioPapel;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class UsuarioDAO extends GenericDAO<Usuario>{

	PapelDAO papelDAO;
	UsuarioPapelDAO usuarioPapelDAO;
	
	public void setPapelDAO(PapelDAO papelDAO) {
		this.papelDAO = papelDAO;
	}
	public void setUsuarioPapelDAO(UsuarioPapelDAO usuarioPapelDAO) {
		this.usuarioPapelDAO = usuarioPapelDAO;
	}
	
	/**
	 * <p> Retorna um usu�rio pelo seu ID
	 * @param code id do usu�rio
	 * @return usuario
	 */
	public Usuario getUsuarioById(Integer code){
		return query().select("usuario").where("usuario.idUsuario=?",code).unique();
	}
	
	@Override
	public Usuario loadForEntrada(Usuario bean) {
		bean = super.loadForEntrada(bean);
		bean.setPapeis(papelDAO.findByUsuario(bean));
		return bean;
	}
	
	/**
	 * <p> Retorna um usu�rio pelo seu ID do colaborador
	 * @param code id do usu�rio
	 * @return usuario
	 */
	public Usuario getUsuarioByColaborador(Colaborador c){
		return query()
				.select("usuario")
				.leftOuterJoin("usuario.colaboradores c")
				.where("c.idColaborador=?",c.getIdColaborador()).unique();
	}
	
	/**
	 * <p>M�todo que retorna um usu�rio atrav�s de seu login
	 * @param login {@link String} 
	 * @return {@link Usuario}
	 */
	public Usuario findByLogin(String login) {
		return query()
			.where("UPPER(usuario.login) = ?", login.toUpperCase())
			.unique();
	}

	@Override
	public void saveOrUpdate(final Usuario bean) {
		transactionTemplate.execute(new TransactionCallback<Object>(){
			public Object doInTransaction(TransactionStatus arg0) {
				Usuario usuarioSalvo = findByLogin(bean.getLogin());
				if(usuarioSalvo!=null){
					boolean ehEdicao = bean.getIdUsuario()!=null;
					boolean ehMesmoObjeto = usuarioSalvo.getIdUsuario().equals(bean.getIdUsuario());
					if (!ehEdicao || ehEdicao && !ehMesmoObjeto){
						throw new RuntimeException("J� existe um usu�rio com este Login, por favor digite outro login.");
					}
				}
				
				UsuarioDAO.super.saveOrUpdate(bean);
				papelDAO.deleteByUsuario(bean);
				if(bean.getPapeis() != null){
					for (Papel papel : bean.getPapeis()) {
						UsuarioPapel usuarioPapel = new UsuarioPapel();
						usuarioPapel.setPapel(papel);
						usuarioPapel.setUsuario(bean);
						usuarioPapelDAO.saveOrUpdate(usuarioPapel);
					}
				}
				return null;
			}
			
		});
	}
	
	public void updateSenha(Usuario user){
		getJdbcTemplate().update("update usuario set password = '"+user.getPassword() + "' where idUsuario = "+user.getIdUsuario());
	}
	
}

package br.com.orionx.clinicalsys.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="planopaciente")
public class PlanoPaciente {
	
	private Integer idPlanoPaciente;
	private Paciente paciente;
	private Plano plano;
	private String numeroCarteira;
	private Date validade;
	private Operadora operadora;
	
	//GET
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getIdPlanoPaciente() {
		return idPlanoPaciente;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idPaciente")
	public Paciente getPaciente() {
		return paciente;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idPlano")
	@DescriptionProperty
	@Required
	public Plano getPlano() {
		return plano;
	}
	@Required
	@MaxLength(30)
	public String getNumeroCarteira() {
		return numeroCarteira;
	}
	@Required
	public Date getValidade() {
		return validade;
	}
	@Transient
	@Required
	public Operadora getOperadora() {
		return operadora;
	}
	
	//SET
	public void setIdPlanoPaciente(Integer idPlanoPaciente) {
		this.idPlanoPaciente = idPlanoPaciente;
	}
	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}
	public void setPlano(Plano plano) {
		this.plano = plano;
	}
	public void setNumeroCarteira(String numeroCarteira) {
		this.numeroCarteira = numeroCarteira;
	}
	public void setValidade(Date validade) {
		this.validade = validade;
	}
	public void setOperadora(Operadora operadora) {
		this.operadora = operadora;
	}
}

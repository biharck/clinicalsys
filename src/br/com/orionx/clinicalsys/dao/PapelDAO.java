package br.com.orionx.clinicalsys.dao;

import java.util.ArrayList;
import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.QueryBuilder;

import br.com.orionx.clinicalsys.bean.Cargo;
import br.com.orionx.clinicalsys.bean.Papel;
import br.com.orionx.clinicalsys.bean.Usuario;
import br.com.orionx.clinicalsys.bean.UsuarioPapel;
import br.com.orionx.clinicalsys.filtros.PapelFiltro;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class PapelDAO extends GenericDAO<Papel>{

	/**
	 * <p>M�todo que retorna uma lista de papeis a partir de um cargo
	 * @return {@link List} {@link Papel}
	 * @author biharck
	 */
	public List<Papel> getListaPapelByCargo(Cargo cargo){
		return query()
				.select("papel")
				.join("papel.listaCargoPapel lcp")
				.join("lcp.cargo c")
				.where("c.idCargo=?",cargo.getIdCargo())
				.list();
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Papel> query,FiltroListagem _filtro) {
		PapelFiltro filtro = (PapelFiltro) _filtro;
		query.whereLikeIgnoreAll("papel.name", filtro.getNome());
		super.updateListagemQuery(query, filtro);
	}
	
	/**
	 * <p> Apaga os papeis de um determinado usu�rio
	 * @param usuario {@link Usuario}
	 */
	public void deleteByUsuario(Usuario usuario){
		getHibernateTemplate().bulkUpdate("delete from UsuarioPapel where usuario = ?", usuario);
	}
	
	/**
	 * <p>M�todo que retorna os papeis de um usu�rio
	 * @param usuario {@link Usuario}
	 * @return {@link List} {@link Papel}
	 */
	public List<Papel> findByUsuario(Usuario usuario){
		if(usuario.getIdUsuario() == null){
			return new ArrayList<Papel>();
		}
		return query()
				.select("papel")
				.from(UsuarioPapel.class)
				.leftOuterJoin("usuarioPapel.papel papel")
				.where("usuarioPapel.usuario = ?", usuario)
				.list();
	}
}

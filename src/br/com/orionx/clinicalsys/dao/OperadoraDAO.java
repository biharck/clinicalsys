package br.com.orionx.clinicalsys.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.QueryBuilder;
import org.nextframework.persistence.SaveOrUpdateStrategy;

import br.com.orionx.clinicalsys.bean.Operadora;
import br.com.orionx.clinicalsys.bean.Plano;
import br.com.orionx.clinicalsys.filtros.OperadoraFiltro;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class OperadoraDAO extends GenericDAO<Operadora>{
	
	@Override
	public Operadora loadForEntrada(Operadora bean) {
		return query().select("operadora.idUsuarioAltera,operadora.dtAltera,operadora.idOperadora,operadora.cnpj,operadora.razaoSocial,operadora.nomeFantasia,operadora.registroANS,operadora.suaIdentificacao," +
				"operadora.telefone,operadora.telefoneAutorizacao,operadora.email,operadora.website,operadora.ativo," +
				"operadora.cep,operadora.logradouro,operadora.numero,operadora.complemento,operadora.bairro,operadora.idUsuarioAltera,operadora.dtAltera," +
				"vxt.idVersaoXMLTISS, vxt.nome, tt.idTabelaTISS, tt.codigo, tt.descricao, planos.idPlano, planos.ativo, planos.nome,tl.idTipoLogradouro, tl.codigo," +
				"tl.descricao, m.idMunicipio, m.nome, uf.idUF, uf.nome, uf.sigla, pais.idPais, pais.nome,"+
				"logomarca.cdfile, logomarca.contenttype, logomarca.name, logomarca.size, logomarca.content")
		.leftOuterJoin("operadora.versaoXMLTISS vxt")
		.leftOuterJoin("operadora.tabelaTISS tt")
		.leftOuterJoin("operadora.planos planos")
		.leftOuterJoin("operadora.tipoLogradouro tl")
		.leftOuterJoin("operadora.municipio m")
		.leftOuterJoin("m.unidadeFederativa uf")
		.leftOuterJoin("uf.pais pais")
		.leftOuterJoin("operadora.logomarca logomarca")
		.where("operadora.idOperadora=?",bean.getIdOperadora())
		.unique();
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("planos");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Operadora> query,FiltroListagem _filtro) {
		
		OperadoraFiltro filtro = (OperadoraFiltro) _filtro;
		if(filtro.getAtivoEnum()!=null)
			if(filtro.getAtivoEnum().getSituacao()!=null && (filtro.getAtivoEnum().getSituacao() || !filtro.getAtivoEnum().getSituacao()))
				query.where("operadora.ativo is "+filtro.getAtivoEnum().getSituacao());

		query.leftOuterJoin("operadora.versaoXMLTISS vxt")
				.leftOuterJoin("operadora.tabelaTISS tt")
				.leftOuterJoin("operadora.planos planos")
				.where("vxt=?",filtro.getVersaoXMLTISS())
				.where("tt=?",filtro.getTabelaTISS())
				.whereLikeIgnoreAll("operadora.nomeFantasia", filtro.getNome())
				.whereLikeIgnoreAll("planos.nome", filtro.getNomePlano())
				.where("operadora.suaIdentificacao=?",filtro.getSuaIdentificacao());
				
		super.updateListagemQuery(query, _filtro);
	}
	
	/**
	 * <p>retornar a operadora a partir de um plano
	 * @param p {@link Plano}
	 * @return {@link Operadora}
	 */
	public Operadora getOperadoraByPlano(Plano p){
		return query()
				.select("operadora")
				.leftOuterJoin("operadora.planos planos")
				.where("planos=?",p)
				.unique();
	}
	
	@Override
	public List<Operadora> findAll() {
		return query().where("operadora.ativo is true").list();
	}
	
	@Override
	public List<Operadora> findForCombo(String... extraFields) {
		extraFields[0] += "ativo";
		return super.findForCombo(extraFields);
	}

		

}

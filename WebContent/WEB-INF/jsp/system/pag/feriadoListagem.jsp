<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro>
			<t:property name="nome" renderAs="doubleline" colspan="2" style="width:300px" id="nome"/>		
			<t:property name="dataInicio" type="text" renderAs="doubleline"  datepicker="true" style="width:100px"/>		
			<t:property name="dataFim" type="text" renderAs="doubleline"  datepicker="true" style="width:100px"/>		
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados>
			<t:property name="nome"/>
			<t:property name="data"/>
			<t:property name="tipoFeriado" trueFalseNullLabels="Nacional,Estadual,Municipal" />
			<t:property name="sempre"/>
			<t:property name="facultativo" />
			
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
<script type="text/javascript">
	$(document).ready(function(){
		$('#nome').focus();
	});
</script>
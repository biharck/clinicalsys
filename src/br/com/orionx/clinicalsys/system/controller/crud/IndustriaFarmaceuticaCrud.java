package br.com.orionx.clinicalsys.system.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.orionx.clinicalsys.bean.IndustriaFarmaceutica;
import br.com.orionx.clinicalsys.filtros.IndustriaFarmaceuticaFiltro;
import br.com.orionx.clinicalsys.util.DatabaseError;
import br.com.orionx.clinicalsys.util.exception.ClinicalSysException;
import br.com.orionx.clinicalsys.util.generics.CrudController;


@Controller(path="/system/pag/IndustriaFarmaceutica",authorizationModule=CrudAuthorizationModule.class)
public class IndustriaFarmaceuticaCrud extends CrudController<IndustriaFarmaceuticaFiltro, IndustriaFarmaceutica, IndustriaFarmaceutica>{

	@Override
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Administra��o &raquo; <a href=\"/ClinicalSys/system/pag/IndustriaFarmaceutica\">Ind�stria Farmac�utica</a></p>";
	}
	
	@Override
	protected void salvar(WebRequestContext request, IndustriaFarmaceutica bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_nome_industria")) throw new ClinicalSysException("J� existe uma Industria Farmac�utica com este princ�pio ativo e classifica��o cadastrado no sistema.");
		}
		catch (Exception e) {
			throw new ClinicalSysException("Ocorreu um erro ao salvar o registro, por favor, tente novamente mais tarde, e caso este erro persista entre em contao com o administrador do sistema.");
		}
	}
}

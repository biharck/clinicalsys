package br.com.orionx.clinicalsys.util.log;

import java.sql.Timestamp;

public interface Inclusao {
	
	void setIdUsuarioInclusao(Integer idUsuarioInclusao);
	void setDtInclusao(Timestamp dtInclusao);
	public Integer getIdUsuarioInclusao();
	public Timestamp getDtInclusao();
	
}

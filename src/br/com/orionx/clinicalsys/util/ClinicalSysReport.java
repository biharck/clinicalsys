package br.com.orionx.clinicalsys.util;

import java.util.Date;

import org.nextframework.controller.resource.ReportController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;

public abstract class ClinicalSysReport<FILTRO> extends	ReportController<FILTRO> {

	@Override
	public IReport createReport(WebRequestContext request, FILTRO command)throws Exception {

		Report report = (Report) createReportClinicalSys(request, command);

		configurarParametrosClinicalSys(request, report);
		return report;
	}

	protected void configurarParametrosClinicalSys(WebRequestContext request,Report report) {
		report.addParameter("LOGO", ClinicalSysUtil.getLogoEmpresa());
		report.addParameter("TITULO", getTitulo());
		report.addParameter("DATA", new Date(System.currentTimeMillis()));
		report.addParameter("HORA", System.currentTimeMillis());
		report.addParameter("USUARIO", ClinicalSysUtil.getUsuarioLogado()
				.getLogin());
	}

	public abstract IReport createReportClinicalSys(WebRequestContext request,
			FILTRO filtro) throws Exception;

	public abstract String getTitulo();
//http://tutorials.jenkov.com/java-reflection/getters-setters.html
}

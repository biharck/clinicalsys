package br.com.orionx.clinicalsys.system.controller.crud;

import java.io.IOException;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.view.DownloadFileServlet;
import org.springframework.web.servlet.ModelAndView;

import br.com.orionx.clinicalsys.bean.IndicacaoEspecificaDose;
import br.com.orionx.clinicalsys.bean.Medicamento;
import br.com.orionx.clinicalsys.bean.NomeComercialDroga;
import br.com.orionx.clinicalsys.service.MedicamentoService;

@Controller(path="/clinic/process/Medicamentos",authorizationModule=ProcessAuthorizationModule.class)
public class ApresentacaoMedicacaoProcess extends MultiActionController{

	private MedicamentoService medicamentoService;
	public void setMedicamentoService(MedicamentoService medicamentoService) {
		this.medicamentoService = medicamentoService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request){
		request.setAttribute("pathURL", getPathURL());
		return new ModelAndView("process/medicamentos");
	}

	/**
	 * <p>M�todo que recebe uma requisi��o ajax e retorna os medicamentos da letra selecionada pelo usu�rio
	 * @param request
	 * @throws JSONException
	 * @throws IOException
	 */
	public void findMedicineByLetter(WebRequestContext request) throws JSONException, IOException{
		
		JSONArray jsonArray = new JSONArray();
		
		JSONObject jsonObj = new JSONObject();
		
		List<Medicamento> medicamentosByLetra = medicamentoService.getMedicamentosByLetra(getParameter("letter"));
		if(medicamentosByLetra != null)
			for (Medicamento m : medicamentosByLetra) {
				jsonObj = new JSONObject();
				jsonObj.put("nomeMed", m.getPrincipioAtivo());
				jsonObj.put("idMed", m.getIdMedicamento());
				jsonArray.put(jsonObj);
			}
		request.getServletResponse().setContentType("application/json");
		request.getServletResponse().setCharacterEncoding("ISO-8859-1");
		request.getServletResponse().getWriter().println( jsonArray );
	}
	
	/**
	 * <p>M�todo respons�vel em receber uma requisi��o ajax e retornar os dados de um determinado medicamento
	 * @param request
	 * @throws JSONException
	 * @throws IOException
	 */
	public void findDescription(WebRequestContext request) throws JSONException, IOException{
		Medicamento m = new Medicamento();
		m.setIdMedicamento(Integer.parseInt(getParameter("idMedicamento")));
		m = medicamentoService.loadForEntrada(m);
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("principioAtivo", m.getPrincipioAtivo());
		jsonObj.put("classificacao", m.getClassificacaoMedicacao().getNome());
		jsonObj.put("descricao", m.getDescricaoDroga());
		jsonObj.put("nomeComercial", getNomesComerciais(m.getNomesComerciaisDroga()));
		jsonObj.put("ied", getIndicacoesEspecificas(m.getIndicacoesEspecificasEDoses()));
		jsonObj.put("efeitosColateraisCom", "<b>Efeitos Colaterais Mais Comuns: </b>" + m.getEfeitosColateraisComuns());
		jsonObj.put("efeitosColateraisMF", "<b>Efeitos Colaterais Menos Frequente: </b>" + m.getEfeitosColateraisMenosFrequente());
		if(m.getBula()!=null){
			jsonObj.put("bula", m.getBula().getCdfile());
			DownloadFileServlet.addCdfile(request.getSession(), m.getBula().getCdfile());
		}
		request.getServletResponse().setContentType("application/json");
		request.getServletResponse().setCharacterEncoding("ISO-8859-1");
		request.getServletResponse().getWriter().println( jsonObj );
	}
	
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Cl�nica &raquo; <a href=\"/ClinicalSys/clinic/process/Medicamentos\">Medicamentos</a></p>";
	}
	
	public String getNomesComerciais(List<NomeComercialDroga> nomesComerciaisDroga){
		StringBuilder sb = new StringBuilder();
		for (NomeComercialDroga nomeComercialDroga : nomesComerciaisDroga) {
			sb.append(nomeComercialDroga.getNomeComercial() )
			.append( " ,<b> ")
			.append( nomeComercialDroga.getIndustriaFarmaceutica().getNome() )
			.append( "</b> , dispon�vel em " )
			.append( nomeComercialDroga.getApresentacao().getNome() )
			.append( " , " )
			.append( nomeComercialDroga.getQtdDisponivelEmbalagem() );
			if(nomeComercialDroga.isGenerico())
				sb.append("<br/>").append("<center><img src='/ClinicalSys/images/medicamento_generico.gif'></center>");
			sb.append( "<HR WIDTH='50%'><br/>");
		}
		return sb.toString();
	}
	
	public String getIndicacoesEspecificas(List<IndicacaoEspecificaDose> indicacaoEspecificaDoses){
		StringBuilder sb = new StringBuilder();
		for (IndicacaoEspecificaDose ied : indicacaoEspecificaDoses) {
			sb.append("<b>Indica��o: </b>").append(ied.getIndicacao());
			sb.append("<br/>");
			if(ied.getDoseAdultos()!=null && !ied.getDoseAdultos().isEmpty())
				sb.append("<b>Doses em adultos: </b>").append(ied.getDoseAdultos()).append("<br/>");
			if(ied.getDoseCriancas()!=null && !ied.getDoseCriancas().isEmpty())
				sb.append("<b>Doses em Crian�as: </b>").append(ied.getDoseCriancas()).append("<br/>");
			if(ied.getNivelSericoDisfRenalHepa()!=null && !ied.getNivelSericoDisfRenalHepa().isEmpty())
				sb.append("<b>N�vel S�rico, Disfun��o Rena e Hep�tica: </b>").append(ied.getNivelSericoDisfRenalHepa()).append("<br/>");
			if(ied.getConsideracoesRecomendacoes()!=null && !ied.getConsideracoesRecomendacoes().isEmpty())
				sb.append("<b>Considera��es e Recomenda��es: </b>").append(ied.getConsideracoesRecomendacoes()).append("<br/>");
			
			sb.append( "<HR WIDTH='50%'><br/>");
		}
		return sb.toString();
	}
	
	public void findMedicamentos(WebRequestContext request){
		System.out.println(getParameter("pa"));
	}
}

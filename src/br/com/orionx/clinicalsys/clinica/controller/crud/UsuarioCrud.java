package br.com.orionx.clinicalsys.clinica.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.orionx.clinicalsys.bean.Usuario;
import br.com.orionx.clinicalsys.util.DatabaseError;
import br.com.orionx.clinicalsys.util.exception.ClinicalSysException;
import br.com.orionx.clinicalsys.util.generics.CrudController;
import br.com.orionx.clinicalsys.util.generics.FiltroListagem;

@Controller(path="/system/pag/Usuario",authorizationModule=CrudAuthorizationModule.class)
public class UsuarioCrud extends CrudController<FiltroListagem, Usuario, Usuario>{
	
	@Override
	protected void salvar(WebRequestContext request, Usuario bean)throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_login")) 
				throw new ClinicalSysException("J� existe um usu�rio com este login cadastrado no sistema.");
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public String getPathURL() {
		// TODO Auto-generated method stub
		return null;
	}

}

package br.com.orionx.clinicalsys.service;

import br.com.orionx.clinicalsys.bean.Operadora;
import br.com.orionx.clinicalsys.bean.Plano;
import br.com.orionx.clinicalsys.dao.OperadoraDAO;
import br.com.orionx.clinicalsys.util.generics.GenericService;

public class OperadoraService extends GenericService<Operadora>{

	private OperadoraDAO operadoraDAO;
	
	public void setOperadoraDAO(OperadoraDAO operadoraDAO) {
		this.operadoraDAO = operadoraDAO;
	}
	
	/**
	 * <p>M�todo de refer�ncia ao DAO
	 * @param p {@link Plano}
	 * @return {@link Operadora}
	 */
	public Operadora getOperadoraByPlano(Plano p){
		return operadoraDAO.getOperadoraByPlano(p);
	}
	
	
}

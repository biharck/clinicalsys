package br.com.orionx.clinicalsys.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.QueryBuilder;

import br.com.orionx.clinicalsys.bean.CategoriaProdutos;
import br.com.orionx.clinicalsys.filtros.CategoriaProdutosFiltro;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class CategoriaProdutosDAO extends GenericDAO<CategoriaProdutos>{

	
	@Override
	public void updateListagemQuery(QueryBuilder<CategoriaProdutos> query,FiltroListagem _filtro) {
		CategoriaProdutosFiltro filtro = (CategoriaProdutosFiltro) _filtro;
		if(filtro.getAtivoEnum()!=null)
			if(filtro.getAtivoEnum().getSituacao()!=null && (filtro.getAtivoEnum().getSituacao() || !filtro.getAtivoEnum().getSituacao()))
				query.where("categoriaProdutos.ativo is "+filtro.getAtivoEnum().getSituacao());

		query.whereLikeIgnoreAll("categoriaProdutos.nome",filtro.getNome());
		super.updateListagemQuery(query, filtro);
	}
	
	@Override
	public List<CategoriaProdutos> findAll() {
		return query().where("categoriaProdutos.ativo is true").list();
	}
	
	@Override
	public List<CategoriaProdutos> findForCombo(String... extraFields) {
		extraFields[0] += "ativo";
		return super.findForCombo(extraFields);
	}

}

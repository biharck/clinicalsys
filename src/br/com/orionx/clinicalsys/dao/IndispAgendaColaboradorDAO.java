package br.com.orionx.clinicalsys.dao;

import java.util.List;

import br.com.orionx.clinicalsys.bean.ColaboradorAgenda;
import br.com.orionx.clinicalsys.bean.IndispAgendaColaborador;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class IndispAgendaColaboradorDAO extends GenericDAO<IndispAgendaColaborador>{

	
	/**
	 * <p>M�todo que retorna as indisponibilidades da agenda do colaborador
	 * @param ca {@link ColaboradorAgenda}
	 * @return {@link List} {@link IndispAgendaColaborador}
	 */
	public List<IndispAgendaColaborador> getIndisponibilidades(ColaboradorAgenda ca){
		return query()
				.select("indispAgendaColaborador")
				.leftOuterJoin("indispAgendaColaborador.colaboradorAgenda ca")
				.where("ca=?",ca)
				.list()
				;
	}
}

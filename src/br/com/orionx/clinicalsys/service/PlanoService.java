package br.com.orionx.clinicalsys.service;

import java.util.List;

import br.com.orionx.clinicalsys.bean.Operadora;
import br.com.orionx.clinicalsys.bean.Plano;
import br.com.orionx.clinicalsys.dao.PlanoDAO;
import br.com.orionx.clinicalsys.util.generics.GenericService;

public class PlanoService extends GenericService<Plano>{

	private PlanoDAO planoDAO;
	public void setPlanoDAO(PlanoDAO planoDAO) {
		this.planoDAO = planoDAO;
	}
	
	/**
	 * <p>M�todo de refer�ncia ao DAO
	 * @param operadora {@link Operadora} 
	 * @see br.com.orionx.clinicalsys.dao.PlanoDAO#getPlanosByOperadora
	 * @return {@link List} {@link Plano}
	 */
	public List<Plano> getPlanosByOperadora(Operadora operadora){
		return planoDAO.getPlanosByOperadora(operadora);
	}
}

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="cs" uri="clinicalsys"%>

<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro colspan="2">
			<t:property name="nome" id="nome" renderAs="doubleline"/>
			<t:property name="codigoCBO" renderAs="doubleline" colspan="2"/>		
			<t:property name="descricao" renderAs="doubleline"/>		
			<t:property name="ativoEnum" renderAs="doubleline"/>		
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados showExcluirLink="false">
			<t:property name="codigoCBO"/>
			<t:property name="nome"/>
			<n:column header="Descri��o" order="cargo.descricao">
				${cs:trunc(cargo.descricao,50)}
		  	</n:column>
			<t:property name="ativo"/>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
<script type="text/javascript">
$(document).ready(function(){
	$('#nome').focus();
});
</script>

package br.com.orionx.clinicalsys.service;

import br.com.orionx.clinicalsys.bean.Anamnese;
import br.com.orionx.clinicalsys.bean.Paciente;
import br.com.orionx.clinicalsys.dao.AnamneseDAO;
import br.com.orionx.clinicalsys.util.generics.GenericService;

public class AnamneseService extends GenericService<Anamnese>{

	private AnamneseDAO anamneseDAO;
	public void setAnamneseDAO(AnamneseDAO anamneseDAO) {
		this.anamneseDAO = anamneseDAO;
	}
	
	public Anamnese getAnamneseByPaciente(Paciente p){
		return anamneseDAO.getAnamneseByPaciente(p);
	}
}

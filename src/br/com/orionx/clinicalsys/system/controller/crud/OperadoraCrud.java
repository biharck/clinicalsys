package br.com.orionx.clinicalsys.system.controller.crud;


import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.view.DownloadFileServlet;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.orionx.clinicalsys.bean.Operadora;
import br.com.orionx.clinicalsys.filtros.OperadoraFiltro;
import br.com.orionx.clinicalsys.service.ArquivoService;
import br.com.orionx.clinicalsys.util.DatabaseError;
import br.com.orionx.clinicalsys.util.exception.ClinicalSysException;
import br.com.orionx.clinicalsys.util.generics.CrudController;

@Controller(path="/system/pag/Operadora",authorizationModule=CrudAuthorizationModule.class)
public class OperadoraCrud extends CrudController<OperadoraFiltro, Operadora, Operadora>{

	private ArquivoService arquivoService;
	public void setArquivoService(ArquivoService arquivoService) {
		this.arquivoService = arquivoService;
	}
	
	@Override
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Administra��o &raquo; <a href=\"/ClinicalSys/system/pag/Operadora\">Operadora</a></p>"; 
	}
	@Override
	protected void entrada(WebRequestContext request, Operadora form)throws Exception {
		if(form!=null && form.getIdOperadora()!=null){
			try {
				form.setPais(form.getMunicipio().getUnidadeFederativa().getPais());
				form.setUnidadeFederativa(form.getMunicipio().getUnidadeFederativa());
				if(form.getLogomarca()!=null && form.getLogomarca().getCdfile()!=null){
					arquivoService.loadAsImage(form.getLogomarca());
					DownloadFileServlet.addCdfile(request.getSession(), form.getLogomarca().getCdfile());
					request.getSession().setAttribute("showpicture", true);
				}else
					request.getSession().setAttribute("showpicture", false);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			request.getSession().setAttribute("showpicture", false);
		}
		request.setAttribute("exibefoto",false);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Operadora bean) throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_cnpj_operadora")) 
				throw new ClinicalSysException("J� existe uma operadora com este CNPJ cadastrado no sistema.");
			
		}
		catch (Exception e) {
			throw new ClinicalSysException("O sistema encontrou dificuldades ao salvar este registro, entre em contato com o administrador do sistema.");
		}
	}

}

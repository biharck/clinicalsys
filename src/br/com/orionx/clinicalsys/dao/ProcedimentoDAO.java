package br.com.orionx.clinicalsys.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.QueryBuilder;

import br.com.orionx.clinicalsys.bean.Procedimento;
import br.com.orionx.clinicalsys.filtros.ProcedimentoFiltro;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class ProcedimentoDAO extends GenericDAO<Procedimento> {

	
	@Override
	public void updateListagemQuery(QueryBuilder<Procedimento> query,FiltroListagem _filtro) {
		ProcedimentoFiltro filtro = (ProcedimentoFiltro) _filtro;
		if(filtro.getAtivoEnum()!=null)
			if(filtro.getAtivoEnum().getSituacao()!=null && (filtro.getAtivoEnum().getSituacao() || !filtro.getAtivoEnum().getSituacao()))
				query.where("procedimento.ativo is "+filtro.getAtivoEnum().getSituacao());
		query.whereLikeIgnoreAll("procedimento.nome",filtro.getNome());
		
		super.updateListagemQuery(query, filtro);
	}
	
	@Override
	public List<Procedimento> findAll() {
		return query().where("procedimento.ativo is true").list();
	}
	
	@Override
	public List<Procedimento> findForCombo(String... extraFields) {
		extraFields[0] += "ativo";
		return super.findForCombo(extraFields);
	}

}

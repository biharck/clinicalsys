package br.com.orionx.clinicalsys.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.nextframework.bean.annotation.DescriptionProperty;

@Entity
@Table(name= "demaisdoencas")
public class DemaisDoencas {
	
	private Integer idDemaisDoencas;
	private String nome;
	private List<DemaisDoencasPaciente> demaisDoencasPacientes;
	
	private boolean checked;
	
	public DemaisDoencas(){}

	public DemaisDoencas(Integer idDemaisDoencas){
		this.idDemaisDoencas = idDemaisDoencas;
	}
	
	@Transient
	public boolean isChecked() {
		return checked;
	}
	
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdDemaisDoencas() {
		return idDemaisDoencas;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	@OneToMany(mappedBy = "demaisDoencas")
	public List<DemaisDoencasPaciente> getDemaisDoencasPacientes() {
		return demaisDoencasPacientes;
	}
	public void setIdDemaisDoencas(Integer idDemaisDoencas) {
		this.idDemaisDoencas = idDemaisDoencas;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setDemaisDoencasPacientes(List<DemaisDoencasPaciente> demaisDoencasPacientes) {
		this.demaisDoencasPacientes = demaisDoencasPacientes;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o instanceof DemaisDoencas){
			DemaisDoencas dd = (DemaisDoencas) o;
			return this.getIdDemaisDoencas().equals(dd.getIdDemaisDoencas());
		}
		return false;
	}
}

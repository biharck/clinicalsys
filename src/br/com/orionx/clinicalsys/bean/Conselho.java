package br.com.orionx.clinicalsys.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

import br.com.orionx.clinicalsys.util.log.Log;

@Entity
@Table(name = "conselho")
public class Conselho implements Log{
	
	private Integer idConselho;
	private String nome;
	private String codigoConselho;
	private boolean ativo;

	//Log
	private Integer idUsuarioAltera;
	private Timestamp dtAltera;
	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdConselho() {
		return idConselho;
	}
	@Required
	@MaxLength(50)
	@MinLength(6)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	@Required
	@MaxLength(100)
	@MinLength(1)
	public String getCodigoConselho() {
		return codigoConselho;
	}
	public boolean isAtivo() {
		return ativo;
	}
	
	//SET
	public void setIdConselho(Integer idConselho) {
		this.idConselho = idConselho;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCodigoConselho(String codigoConselho) {
		this.codigoConselho = codigoConselho;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	//LOG
	@Override
	public void setIdUsuarioAltera(Integer idUsuarioAltera) {
		this.idUsuarioAltera = idUsuarioAltera;
	}

	@Override
	public void setDtAltera(Timestamp dtAltera) {
		this.dtAltera = dtAltera;
	}

	@Override
	public Integer getIdUsuarioAltera() {
		return idUsuarioAltera;
	}

	@Override
	public Timestamp getDtAltera() {
		return dtAltera;
	}

	
}

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:listagem>
	<div align="center" class="alert_info" style="display:none;position:absolute;width:400px; margin: 0px 30%; text-align: center;" id="carregandoAjax">Carregando...</div>
	<t:janelaFiltro>
		<t:tabelaFiltro>
			<n:comboReloadGroup>
				<t:property name="classificacaoMedicacao" renderAs="doubleline" onchange="openAguardeAjax()"/>		
				<t:property name="medicamento" renderAs="doubleline" />
			</n:comboReloadGroup>		
			<t:property name="ativoEnum" renderAs="doubleline" />
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados>
			<t:property name="medicamento"/>
			<t:property name="ativo" trueFalseNullLabels="ativo, inativo,"/>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>

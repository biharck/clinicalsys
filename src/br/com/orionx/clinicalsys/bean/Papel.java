package br.com.orionx.clinicalsys.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.nextframework.authorization.Role;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

import br.com.orionx.clinicalsys.util.log.Log;

/**
 * <p>Representa um papel no sistema, um n�vel de permiss�o.<br>
 * Ex.: Administrador, Usu�rio, Financeiro</p>
 * 
 * @author biharck
 */
@Entity
@Table(name="papel")
public class Papel implements Role , Log{
    
    Integer idPapel;
    String description;
    String name;
    
    private List<CargoPapel> listaCargoPapel;
    
  //Log
	private Integer idUsuarioAltera;
	private Timestamp dtAltera;
    
    /*
     * GET
     */
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    public Integer getIdPapel() {
		return idPapel;
	}

    //API
    @DescriptionProperty
    @Required
    @MaxLength(200)
    @MinLength(5)
    public String getName() {
        return name;
    }
    
    @Required
    @MaxLength(200)
    @MinLength(5)
    public String getDescription() {
        return description;
    }
    
    @OneToMany(mappedBy = "papel")
    public List<CargoPapel> getListaCargoPapel() {
		return listaCargoPapel;
	}
    
    /*
     * SET
     */
    public void setDescription(String description) {
        this.description = description;
    }
    public void setIdPapel(Integer idPapel) {
		this.idPapel = idPapel;
	}
    public void setName(String name) {
        this.name = name;
    }
    public void setListaCargoPapel(List<CargoPapel> listaCargoPapel) {
		this.listaCargoPapel = listaCargoPapel;
	}
    
	//LOG
	@Override
	public void setIdUsuarioAltera(Integer idUsuarioAltera) {
		this.idUsuarioAltera = idUsuarioAltera;
	}

	@Override
	public void setDtAltera(Timestamp dtAltera) {
		this.dtAltera = dtAltera;
	}

	@Override
	public Integer getIdUsuarioAltera() {
		return idUsuarioAltera;
	}

	@Override
	public Timestamp getDtAltera() {
		return dtAltera;
	}


}

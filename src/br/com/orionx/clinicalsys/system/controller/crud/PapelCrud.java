package br.com.orionx.clinicalsys.system.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;

import br.com.orionx.clinicalsys.bean.Papel;
import br.com.orionx.clinicalsys.filtros.PapelFiltro;
import br.com.orionx.clinicalsys.util.generics.CrudController;

@Controller(path="/system/pag/Papel",authorizationModule=CrudAuthorizationModule.class)
public class PapelCrud extends CrudController<PapelFiltro, Papel, Papel>{

	@Override
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Administração &raquo; <a href=\"/ClinicalSys/system/pag/Papel\">Papel</a></p>"; 
	}

}

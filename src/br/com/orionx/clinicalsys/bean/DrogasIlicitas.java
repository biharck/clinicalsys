package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;

@Entity
@Table(name = "drogasIlicitas")
public class DrogasIlicitas {
	
	private Integer idDrogasIlicitas;
	private String nome;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getIdDrogasIlicitas() {
		return idDrogasIlicitas;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public void setIdDrogasIlicitas(Integer idDrogasIlicitas) {
		this.idDrogasIlicitas = idDrogasIlicitas;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}

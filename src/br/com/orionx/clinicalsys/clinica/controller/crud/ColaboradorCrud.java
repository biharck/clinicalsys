package br.com.orionx.clinicalsys.clinica.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.view.DownloadFileServlet;
import org.nextframework.view.ajax.View;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.orionx.clinicalsys.bean.Colaborador;
import br.com.orionx.clinicalsys.bean.ConfiguracaoAtendimento;
import br.com.orionx.clinicalsys.bean.PrecoConsultaEspecialidade;
import br.com.orionx.clinicalsys.dao.ArquivoDAO;
import br.com.orionx.clinicalsys.filtros.ColaboradorFiltro;
import br.com.orionx.clinicalsys.service.ColaboradorService;
import br.com.orionx.clinicalsys.service.ConfiguracaoAtendimentoService;
import br.com.orionx.clinicalsys.service.PapelService;
import br.com.orionx.clinicalsys.service.UsuarioService;
import br.com.orionx.clinicalsys.util.ClinicalSysUtil;
import br.com.orionx.clinicalsys.util.DatabaseError;
import br.com.orionx.clinicalsys.util.exception.ClinicalSysException;
import br.com.orionx.clinicalsys.util.generics.CrudController;

@Controller(path="/clinic/pag/Colaborador",authorizationModule=CrudAuthorizationModule.class)
public class ColaboradorCrud extends CrudController<ColaboradorFiltro, Colaborador, Colaborador>{
	
	
	private ColaboradorService colaboradorService;
	private ArquivoDAO arquivoDAO;
	private PapelService papelService;
	private UsuarioService usuarioService;
	private ConfiguracaoAtendimentoService configuracaoAtendimentoService;
	
	public void setPapelService(PapelService papelService) {
		this.papelService = papelService;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	public void setConfiguracaoAtendimentoService(ConfiguracaoAtendimentoService configuracaoAtendimentoService) {
		this.configuracaoAtendimentoService = configuracaoAtendimentoService;
	}
	
	@Override
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Administra��o &raquo; <a href=\"/ClinicalSys/clinic/pag/Colaborador\">Colaboradores</a></p>";
	}
	
	@Override
	protected void entrada(WebRequestContext request, Colaborador form)throws Exception {
		Colaborador colTmp = null;
		if(form!=null && form.getIdColaborador()!=null){
			colTmp = colaboradorService.getColaboradorEspecifico(form.getIdColaborador()); 
			form.setPais(colTmp.getMunicipio().getUnidadeFederativa().getPais());
			form.setUnidadeFederativa(colTmp.getMunicipio().getUnidadeFederativa());
			if(form.getUsuario()==null)
				form.setUsuario(colTmp.getUsuario());
			if(form.getCargos()==null)
				form.setCargos(colTmp.getCargos());
			if(form.getUsuario()!=null)
				form.setUsuario(usuarioService.loadForEntrada(form.getUsuario()));
			try {
				if(form.getFoto()!=null && form.getFoto().getCdfile()!=null){
					if(form.getFoto().getContent()==null)
						form.setFoto(colTmp.getFoto());
						arquivoDAO.loadAsImage(form.getFoto());
					DownloadFileServlet.addCdfile(request.getSession(), form.getFoto().getCdfile());
					request.getSession().setAttribute("showpicture", true);
				}else
					request.getSession().setAttribute("showpicture", false);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			request.getSession().setAttribute("showpicture", false);
		}
		request.setAttribute("listaPapel", papelService.findAll());
		super.entrada(request, form);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Colaborador bean)	throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_cpf_colaborador")) 
				throw new ClinicalSysException("J� existe um colaborador com este CPF cadastrado no sistema.");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,FiltroListagem filtro) throws CrudException {
		request.setAttribute("onePath", request.getParameter("onePath"));
		return super.doListagem(request, filtro);
	}
	@Override
	protected void listagem(WebRequestContext request, FiltroListagem filtro) throws Exception {
		request.setAttribute("onePath", request.getParameter("onePath"));
		super.listagem(request, filtro);
	}
	
	/**
	 * <p>Ajax usado para buscar o tempo m�ximo de dura��o de uma consulta a partir do cargo do colaborador
	 */
	public void getTempoConsulta(WebRequestContext request){
		
		Integer idColaborador = ClinicalSysUtil.returnIdWithParameters(request.getParameter("idColaborador"));
		Colaborador c = colaboradorService.getColaboradorEspecifico(idColaborador);
		
		ConfiguracaoAtendimento ca = configuracaoAtendimentoService.getDadosConfiguracao();
		boolean containsId = false;
		Integer tempo = 0;
		
		for (PrecoConsultaEspecialidade pce : ca.getPrecosConsultas()) {
			if(c.getListaEspecializacaoColaboradors().contains(pce.getEspecializacao())){
				containsId = true;
				tempo = pce.getTempoConsulta();
				break;
			}
		}
		/**
		 * siginifica que existe uma configura��o espec�fca para aquela especializa��o em quest�o.
		 */
		if(containsId)
			View.getCurrent().println("var tempo = "+ tempo);
		else
			View.getCurrent().println("var tempo = "+ ca.getTempoConsulta());
		
	}
}

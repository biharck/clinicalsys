package br.com.orionx.clinicalsys.filtros;

import br.com.orionx.clinicalsys.util.generics.FiltroListagem;

public class MedicamentoFiltro extends FiltroListagem {
	
	private String nomeComercial;
	private String principioAtivo;
	private String descricaoDroga;
	protected AtivoEnum ativoEnum;
	
	//GET
	public String getNomeComercial() {
		return nomeComercial;
	}
	public String getPrincipioAtivo() {
		return principioAtivo;
	}
	public String getDescricaoDroga() {
		return descricaoDroga;
	}
	public AtivoEnum getAtivoEnum() {
		return ativoEnum;
	}
	
	//SET
	public void setNomeComercial(String nomeComercial) {
		this.nomeComercial = nomeComercial;
	}
	public void setPrincipioAtivo(String principioAtivo) {
		this.principioAtivo = principioAtivo;
	}
	public void setDescricaoDroga(String descricaoDroga) {
		this.descricaoDroga = descricaoDroga;
	}
	public void setAtivoEnum(AtivoEnum ativoEnum) {
		this.ativoEnum = ativoEnum;
	}
	

}

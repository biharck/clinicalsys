package br.com.orionx.clinicalsys.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.nextframework.types.Money;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

import br.com.orionx.clinicalsys.util.log.Log;

@Entity
@Table(name = "produtoservico")
public class ProdutoServico implements Log{
	
	private Integer idProdutoServico;
	private String codigo;//60
	private String descricao;//120
	private String EAN;//14
	private String NCM;//8
	private String exTPI;//3
	private Integer CFPO;//4
	private Integer genero;//2
	private String unidadeComercial;//6
	private Integer quantidadeComercial;//15
	private Money valorUnitarioComercial;//20
	private String unidadeTributada;//6
	private Integer quantidadeTributada;//15
	private Money valorUnitarioTributado;//20
	private String eANTributado;//14
	private Money valorTotalFrete;//20
	private Money valorTotalSeguro;//20
	private Money valorDesconto;//20
	private Money valorTotalBruto;//20
	private boolean ativo;
	private Boolean produtoOuServico;//true = produto - false = servico
	private CategoriaProdutos categoriaProdutos;
	
	//Log
	private Integer idUsuarioAltera;
	private Timestamp dtAltera;

	//GET
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getIdProdutoServico() {
		return idProdutoServico;
	}
	@Required
	@MaxLength(60)
	public String getCodigo() {
		return codigo;
	}
	@Required
	@MaxLength(120)
	public String getDescricao() {
		return descricao;
	}
	@MaxLength(14)
	public String getEAN() {
		return EAN;
	}
	@MaxLength(8)
	public String getNCM() {
		return NCM;
	}
	@MaxLength(3)
	public String getExTPI() {
		return exTPI;
	}
	@Required
	@MaxLength(4)
	public Integer getCFPO() {
		return CFPO;
	}
	@MaxLength(2)
	public Integer getGenero() {
		return genero;
	}
	@Required
	@MaxLength(6)
	public String getUnidadeComercial() {
		return unidadeComercial;
	}
	@Required
	@MaxLength(15)
	public Integer getQuantidadeComercial() {
		return quantidadeComercial;
	}
	@Required
	@MaxLength(20)
	public Money getValorUnitarioComercial() {
		return valorUnitarioComercial;
	}
	@Required
	@MaxLength(6)
	public String getUnidadeTributada() {
		return unidadeTributada;
	}
	@Required
	@MaxLength(15)
	public Integer getQuantidadeTributada() {
		return quantidadeTributada;
	}
	
	public Money getValorUnitarioTributado() {
		return valorUnitarioTributado;
	}
	@MaxLength(14)
	public String geteANTributado() {
		return eANTributado;
	}
	@MaxLength(20)
	public Money getValorTotalFrete() {
		return valorTotalFrete;
	}
	@MaxLength(20)
	public Money getValorTotalSeguro() {
		return valorTotalSeguro;
	}
	@MaxLength(20)
	public Money getValorDesconto() {
		return valorDesconto;
	}
	@Required
	@MaxLength(20)
	public Money getValorTotalBruto() {
		return valorTotalBruto;
	}
	public boolean isAtivo() {
		return ativo;
	}
	@Required
	public Boolean getProdutoOuServico() {
		return produtoOuServico;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idCategoriaProdutos")
	@Required
	public CategoriaProdutos getCategoriaProdutos() {
		return categoriaProdutos;
	}
	
	//SET
	public void setIdProdutoServico(Integer idProdutoServico) {
		this.idProdutoServico = idProdutoServico;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setEAN(String eAN) {
		EAN = eAN;
	}
	public void setNCM(String nCM) {
		NCM = nCM;
	}
	public void setExTPI(String exTPI) {
		this.exTPI = exTPI;
	}
	public void setCFPO(Integer cFPO) {
		CFPO = cFPO;
	}
	public void setGenero(Integer genero) {
		this.genero = genero;
	}
	public void setUnidadeComercial(String unidadeComercial) {
		this.unidadeComercial = unidadeComercial;
	}
	public void setQuantidadeComercial(Integer quantidadeComercial) {
		this.quantidadeComercial = quantidadeComercial;
	}
	public void setValorUnitarioComercial(Money valorUnitarioComercial) {
		this.valorUnitarioComercial = valorUnitarioComercial;
	}
	public void setUnidadeTributada(String unidadeTributada) {
		this.unidadeTributada = unidadeTributada;
	}
	public void setQuantidadeTributada(Integer quantidadeTributada) {
		this.quantidadeTributada = quantidadeTributada;
	}
	public void setValorUnitarioTributado(Money valorUnitarioTributado) {
		this.valorUnitarioTributado = valorUnitarioTributado;
	}
	public void seteANTributado(String eANTributado) {
		this.eANTributado = eANTributado;
	}
	public void setValorTotalFrete(Money valorTotalFrete) {
		this.valorTotalFrete = valorTotalFrete;
	}
	public void setValorTotalSeguro(Money valorTotalSeguro) {
		this.valorTotalSeguro = valorTotalSeguro;
	}
	public void setValorDesconto(Money valorDesconto) {
		this.valorDesconto = valorDesconto;
	}
	public void setValorTotalBruto(Money valorTotalBruto) {
		this.valorTotalBruto = valorTotalBruto;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	public void setProdutoOuServico(Boolean produtoOuServico) {
		this.produtoOuServico = produtoOuServico;
	}
	public void setCategoriaProdutos(CategoriaProdutos categoriaProdutos) {
		this.categoriaProdutos = categoriaProdutos;
	}
	
	//LOG
	@Override
	public void setIdUsuarioAltera(Integer idUsuarioAltera) {
		this.idUsuarioAltera = idUsuarioAltera;
	}

	@Override
	public void setDtAltera(Timestamp dtAltera) {
		this.dtAltera = dtAltera;
	}

	@Override
	public Integer getIdUsuarioAltera() {
		return idUsuarioAltera;
	}

	@Override
	public Timestamp getDtAltera() {
		return dtAltera;
	}
	

}

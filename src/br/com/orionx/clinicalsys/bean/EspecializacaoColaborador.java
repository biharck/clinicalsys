package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;


@Entity
@Table(name = "especializacaocolaborador")
public class EspecializacaoColaborador {

	private Integer idEspecializacaoColaborador;
	private Especializacao especializacao;
	private Colaborador colaborador;
	private Conselho conselho;
	private String numeroConselho;
	private UnidadeFederativa ufConselho;
	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdEspecializacaoColaborador() {
		return idEspecializacaoColaborador;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "idEspecializacao")
	@ForeignKey(name = "fk_espcola_espe")
	public Especializacao getEspecializacao() {
		return especializacao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "idColaborador")
	@ForeignKey(name = "fk_espcola_colaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
	@MaxLength(30)
	@DisplayName("N�mero do Conselho (Caso Houver)")
	public String getNumeroConselho() {
		return numeroConselho;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idConselho")
	public Conselho getConselho() {
		return conselho;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idUFConselho")
	public UnidadeFederativa getUfConselho() {
		return ufConselho;
	}
	
	
	//SET
	public void setIdEspecializacaoColaborador(Integer idEspecializacaoColaborador) {
		this.idEspecializacaoColaborador = idEspecializacaoColaborador;
	}
	public void setEspecializacao(Especializacao especializacao) {
		this.especializacao = especializacao;
	}
	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}
	public void setNumeroConselho(String numeroConselho) {
		this.numeroConselho = numeroConselho;
	}
	public void setConselho(Conselho conselho) {
		this.conselho = conselho;
	}
	public void setUfConselho(UnidadeFederativa ufConselho) {
		this.ufConselho = ufConselho;
	}
}

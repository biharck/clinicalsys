package br.com.orionx.clinicalsys.service;

import java.util.List;

import br.com.orionx.clinicalsys.bean.ConfiguracaoAtendimento;
import br.com.orionx.clinicalsys.bean.PrecoConsultaEspecialidade;
import br.com.orionx.clinicalsys.dao.PrecoConsultaEspecialidadeDAO;
import br.com.orionx.clinicalsys.util.generics.GenericService;

public class PrecoConsultaEspecialidadeService extends GenericService<PrecoConsultaEspecialidade>{

	private PrecoConsultaEspecialidadeDAO precoConsultaEspecialidadeDAO;
	
	public void setPrecoConsultaEspecialidadeDAO(PrecoConsultaEspecialidadeDAO precoConsultaEspecialidadeDAO) {
		this.precoConsultaEspecialidadeDAO = precoConsultaEspecialidadeDAO;
	}
	
	/**
	 * <p>M�todo que retornar so pre�os das configura��es de atendimento a partir de uma configura��o de atendimento
	 * @return {@link List} {@link PrecoConsultaEspecialidade}
	 * @author biharck
	 * 
	 */
	public List<PrecoConsultaEspecialidade> getPrecosByConfiguracao(ConfiguracaoAtendimento ca){
		return precoConsultaEspecialidadeDAO.getPrecosByConfiguracao(ca);
	}
}

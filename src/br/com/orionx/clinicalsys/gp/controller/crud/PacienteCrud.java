package br.com.orionx.clinicalsys.gp.controller.crud;

import java.io.IOException;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.view.DownloadFileServlet;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.orionx.clinicalsys.bean.Municipio;
import br.com.orionx.clinicalsys.bean.Operadora;
import br.com.orionx.clinicalsys.bean.Paciente;
import br.com.orionx.clinicalsys.bean.Plano;
import br.com.orionx.clinicalsys.bean.PlanoPaciente;
import br.com.orionx.clinicalsys.dao.ArquivoDAO;
import br.com.orionx.clinicalsys.filtros.PacienteFiltro;
import br.com.orionx.clinicalsys.service.MunicipioService;
import br.com.orionx.clinicalsys.service.OperadoraService;
import br.com.orionx.clinicalsys.service.PacienteService;
import br.com.orionx.clinicalsys.service.PlanoService;
import br.com.orionx.clinicalsys.util.ClinicalSysUtil;
import br.com.orionx.clinicalsys.util.DatabaseError;
import br.com.orionx.clinicalsys.util.exception.ClinicalSysException;
import br.com.orionx.clinicalsys.util.generics.CrudController;

@Controller(path="/clinic/pag/Paciente",authorizationModule=CrudAuthorizationModule.class)
public class PacienteCrud extends CrudController<PacienteFiltro, Paciente, Paciente> {

	private ArquivoDAO arquivoDAO;
	private PlanoService planoService;
	private MunicipioService municipioService;
	private OperadoraService operadoraService;
	private PacienteService pacienteService;
	
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setPlanoService(PlanoService planoService) {
		this.planoService = planoService;
	}
	public void setMunicipioService(MunicipioService municipioService) {
		this.municipioService = municipioService;
	}
	public void setOperadoraService(OperadoraService operadoraService) {
		this.operadoraService = operadoraService;
	}
	public void setPacienteService(PacienteService pacienteService) {
		this.pacienteService = pacienteService;
	}
	
	@Override
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Gest�o &raquo; <a href=\"/ClinicalSys/clinic/pag/Paciente\">Pacientes</a></p>";
	}
	
	@Override
	protected void entrada(WebRequestContext request, Paciente form)throws Exception {
		if(form!=null && form.getIdPaciente()!=null){
			Municipio m = municipioService.getMunicipioCompleto(form.getMunicipio());
			form.setPais(m.getUnidadeFederativa().getPais());
			form.setUnidadeFederativa(m.getUnidadeFederativa());
			
			if(form.getPlanos()!=null)
				for (PlanoPaciente pp : form.getPlanos()) {
					pp.setOperadora(operadoraService.getOperadoraByPlano(pp.getPlano()));
				}
			
			try {
				if(form.getFotoPaciente()!=null && form.getFotoPaciente().getCdfile()!=null){
					if(form.getFotoPaciente().getContent()==null)
						arquivoDAO.loadAsImage(form.getFotoPaciente());
					DownloadFileServlet.addCdfile(request.getSession(), form.getFotoPaciente().getCdfile());
					request.getSession().setAttribute("showpicture", true);
				}else
					request.getSession().setAttribute("showpicture", false);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			request.getSession().setAttribute("showpicture", false);
		}
		super.entrada(request, form);
	}
	
	@Action("buscaPlanos")
	public void atualizaPlanosByOperadora(WebRequestContext context)throws IOException, JSONException{
		try{

			context.getServletResponse().setContentType("application/json");
			context.getServletResponse().setCharacterEncoding("ISO-8859-1");
			
			if(getParameter("operadora") == null || getParameter("operadora").equals("<null>")){
				JSONArray jsonArray = new JSONArray();
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("jscript", "$('select[name=planos["+getParameter("index")+"].plano]').empty();");
				jsonArray.put(jsonObject);
				context.getServletResponse().getWriter().println( jsonArray );
				return;
			}
			
			
			//enviando para o jsp
			context.getServletResponse().getWriter().println( formatCombo(planoService.getPlanosByOperadora(new Operadora(ClinicalSysUtil.returnIdWithParameters(getParameter("operadora")))),getParameter("index")) );
			
			
		}catch (Exception e) {
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("erro", true);
			context.getServletResponse().setContentType("application/json");
			context.getServletResponse().setCharacterEncoding("ISO-8859-1");
			context.getServletResponse().getWriter().println( jsonObj );
			e.printStackTrace();
		}
	}
	
	@Action("buscaPlanosDialog")
	public void atualizaPlanosByOperadoraDialog(WebRequestContext context)throws IOException, JSONException{
		try{
			
			context.getServletResponse().setContentType("application/json");
			context.getServletResponse().setCharacterEncoding("ISO-8859-1");
			
			if(getParameter("operadora") == null || getParameter("operadora").equals("<null>")){
				JSONArray jsonArray = new JSONArray();
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("jscript", "$('select[name=planosAjax]').empty();");
				jsonArray.put(jsonObject);
				context.getServletResponse().getWriter().println( jsonArray );
				return;
			}
			
			
			//enviando para o jsp
			context.getServletResponse().getWriter().println( formatCombo(planoService.getPlanosByOperadora(new Operadora(ClinicalSysUtil.returnIdWithParameters(getParameter("operadora")))),null) );
			
			
		}catch (Exception e) {
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("erro", true);
			context.getServletResponse().setContentType("application/json");
			context.getServletResponse().setCharacterEncoding("ISO-8859-1");
			context.getServletResponse().getWriter().println( jsonObj );
			e.printStackTrace();
		}
	}
	
	@Override
	protected void salvar(WebRequestContext request, Paciente bean)	throws Exception {
		try {
			
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_paciente_cpf")) throw new ClinicalSysException("J� existe um paciente com este CPF cadastrado no sistema.");
		}
		catch (Exception e) {
			throw new ClinicalSysException("Ocorreu um erro ao salvar o registro, por favor, tente novamente mais tarde, e caso este erro persista entre em contao com o administrador do sistema.");
		}
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, Paciente form) throws CrudException {
		if(request.getParameter("iframe")!=null && request.getParameter("iframe").equals("true")){
			pacienteService.saveOrUpdate(form);
			return redirectClosePage(request);
		}
		return super.doSalvar(request, form);
	}
	
	public ModelAndView redirectClosePage(WebRequestContext request){
		return new ModelAndView("pag/pacienteSaveSuccessProcess");
	}
	
	@Override
	public ModelAndView saveAndNew(WebRequestContext request, Paciente form)throws Exception {
		try {
			super.salvar(request, form);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_paciente_cpf")) throw new ClinicalSysException("J� existe um paciente com este CPF cadastrado no sistema.");
		}
		catch (Exception e) {
			throw new ClinicalSysException("Ocorreu um erro ao salvar o registro, por favor, tente novamente mais tarde, e caso este erro persista entre em contao com o administrador do sistema.");
		}
		request.addMessage("Registro salvo com sucesso. Voc� j� pode criar um novo registro." );
		return sendRedirectToAction("criar");
	}
	
	/**
	 * <p>Funn��o respons�vel em montar toda estrutura de um combo para povoar o html
	 * @param planos lista contendo todos os planos de um determinada operadora
	 * @return {@link JSONArray} formatada para enviar via JSON
	 * @throws JSONException 
	 */
	public JSONArray formatCombo(List<Plano> planos, String index) throws JSONException{
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = new JSONObject();
		
		if(index == null){
			jsonObject.put("jscript", "$('select[name=planosAjax]').empty();");
			jsonArray.put(jsonObject);
			jsonObject = new JSONObject();
			jsonObject.put("jscript", "$('select[name=planosAjax]').append('<option value=\"<null>\"></option>');");
			jsonArray.put(jsonObject);
			for (Plano plano : planos) {
				jsonObject = new JSONObject();
				jsonObject.put("jscript", "$('select[name=planosAjax]').append('<option value=\""+ClinicalSysUtil.getComboFormat(plano, "idPlano", plano.getIdPlano())+"\">"+plano.getNome()+"</option>');");
				jsonArray.put(jsonObject);
			}
		}else{
			jsonObject.put("jscript", "$('select[name=planos["+index+"].plano]').empty();");
			jsonArray.put(jsonObject);
			jsonObject = new JSONObject();
			jsonObject.put("jscript", "$('select[name=planos["+index+"].plano]').append('<option value=\"<null>\"></option>');");
			jsonArray.put(jsonObject);
			for (Plano plano : planos) {
				jsonObject = new JSONObject();
				jsonObject.put("jscript", "$('select[name=planos["+index+"].plano]').append('<option value=\""+ClinicalSysUtil.getComboFormat(plano, "idPlano", plano.getIdPlano())+"\">"+plano.getNome()+"</option>');");
				jsonArray.put(jsonObject);
			}
		}
		return jsonArray;
		
	}

	
	public void getPacienteAutoComplete(WebRequestContext request){
		try {
			request.getServletResponse().getWriter().println(pacienteService.getPacienteAutoComplete(getParameter("nomeParc")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void existeCPF(WebRequestContext request){
		Paciente p = pacienteService.getPacienteByCPF(getParameter("cpf"));
		try {
			JSONObject json = new JSONObject();
			if(p == null)
				json.put("existe", false);
			else
				json.put("existe", true);
			
			request.getServletResponse().setContentType("application/json");
			request.getServletResponse().setCharacterEncoding("ISO-8859-1");
			request.getServletResponse().getWriter().println( json );
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}

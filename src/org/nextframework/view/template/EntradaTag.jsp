<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="cs" uri="clinicalsys"%>

<c:set var="ultimoid"   value="${TEMPLATE_beanName}.idUsuarioAltera"/>
<c:set var="ultimadata" value="${TEMPLATE_beanName}.dtAltera"/> 
<div class="onecolumn">
	<div class="header">
		<span>Cadastro de ${entradaTag.titulo}</span>
	</div>
	<br class="clear"/>
	<div class="content">
		<div id="ErrosSubmissao"></div>
		<t:tela titulo="">
				<div align="center" class="alert_info" style="display:none;position:absolute;width:400px; margin: 0px 30%; text-align: center;" id="carregandoAjax">Carregando...</div>
				<c:if test="${consultar}">
					<input type="hidden" name="forcarConsulta" value="true"/>
				</c:if>
				<c:if test="${param.fromInsertOne == 'true'}">
					<input type="hidden" name="fromInsertOne" value="true"/>
				</c:if>
		
				<c:if test="${param.fromInsertOne != 'true'}">
					<c:if test="${consultar}">
						<div class="linkBar" align="right">
							<c:if test="${entradaTag.showListagemLink || !empty entradaTag.linkArea}">
								${entradaTag.invokeLinkArea}			
								<c:if test="${entradaTag.showListagemLink}">
									<n:link action="listagem" class="outterTableHeaderLink" ><img src="${application}/images/shortcut/return.png" id="voltarlistagem" alt="Voltar" class="help" title="Voltar"/></n:link>
								</c:if>				
							</c:if>	
						</div>
					</c:if>
				</c:if>
				
				<c:if test="${param.fromInsertOne != 'true'}">
					<c:if test="${!consultar}">
						<div class="linkBar" align="right">
							<c:if test="${entradaTag.showListagemLink || !empty entradaTag.linkArea}">
								${entradaTag.invokeLinkArea}			
								<c:if test="${entradaTag.showListagemLink}">
									<a onclick="openDialogConfirm()" href="#"  class="" ><img src="${application}/images/shortcut/return.png" alt="Voltar" class="help" title="Voltar"/></a>
								</c:if>
								<c:if test="${param.fromInsertOne != 'true'}">				
									<n:submit action="saveandnew" validate="true" class="noboardnew" confirmationScript="${janelaEntradaTag.submitConfirmationScript}"><img src="${application}/images/shortcut/saveandnew.png" alt="Salvar e Adicionar Novo" class="help" title="Salvar e Criar Novo"/></n:submit>
								</c:if>
								<n:submit action="salvar" validate="true" class="noboard" confirmationScript="${janelaEntradaTag.submitConfirmationScript}"><img src="${application}/images/shortcut/save.png" alt="Salvar" class="help" title="Salvar"/></n:submit>
							</c:if>	
						</div>
					</c:if>
				</c:if>
				<c:if test="${param.fromInsertOne == 'true'}">
					<div class="linkBar" align="right">
						<n:submit action="salvar" validate="true" class="noboard" confirmationScript="${janelaEntradaTag.submitConfirmationScript}"><img src="${application}/images/shortcut/save.png" alt="Salvar" class="help" title="Salvar"/></n:submit>
					</div>
				</c:if>
				<div>
					<c:if test="${consultar}">
						<c:set var="modeConsultar" value="output" scope="request"/>
					</c:if>
					<n:bean name="${TEMPLATE_beanName}">
						<t:propertyConfig mode="${n:default('input', modeConsultar)}">
							<n:doBody />
						</t:propertyConfig>
					</n:bean>
				</div>
				<!-- Serve pra quando tem muitos registro na tela, pra evitar que o usu�rio retorne a tela pra cima pra salvar -->
				<c:if test="${empty showDownOptions}">
					<c:if test="${!consultar}">
						<div class="linkBar" align="right">
							<c:if test="${entradaTag.showListagemLink || !empty entradaTag.linkArea}">
								<c:if test="${param.fromInsertOne != 'true'}">
									${entradaTag.invokeLinkArea}			
									<c:if test="${entradaTag.showListagemLink}">
										<a onclick="openDialogConfirm()" href="#"  class="" ><img src="${application}/images/shortcut/return.png" alt="Voltar" class="help" title="Voltar"/></a>
									</c:if>				
									<n:submit action="saveandnew" validate="true" class="noboard" confirmationScript="${janelaEntradaTag.submitConfirmationScript}"><img src="${application}/images/shortcut/saveandnew.png" alt="Salvar e Criar Novo" class="help" title="Salvar e Adicionar Novo"/></n:submit>
								</c:if>
								<n:submit action="salvar" validate="true" class="noboard" confirmationScript="${janelaEntradaTag.submitConfirmationScript}"><img src="${application}/images/shortcut/save.png" alt="Salvar" class="help" title="Salvar"/></n:submit>
							</c:if>	
						</div>
					</c:if>
				</c:if>
				<c:if test="${param.ACAO != 'criar'}">
					
						<div class="ultimaalteracao">
							<c:if test="${n:reevaluate(ultimoid,pageContext) != null }">
								<n:panel>�ltima altera��o neste registro foi feita por <font style="color:#E77272">${cs:getuserbyid(n:reevaluate(ultimoid,pageContext))}</font> no dia <font style="color:#E77272">${cs:dateformat(n:reevaluate(ultimadata,pageContext))}</font> �s <font style="color:#E77272">${cs:hourformat(n:reevaluate(ultimadata,pageContext))}</font> </n:panel>
							</c:if>
						</div>
					
				</c:if>
		</t:tela>
	</div>
</div>

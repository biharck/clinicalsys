<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:listagem showDeleteLink="false">
	<t:janelaFiltro>
		<t:tabelaFiltro colspan="2" showReport="true" urlreport="colaboradores">
			<t:property name="nome" renderAs="doubleline"  style="width:400px" id="nome"/>
			<t:property name="sexo" renderAs="doubleline" />
			<t:property name="usuario" renderAs="doubleline" />
			<t:property name="ativoEnum" renderAs="doubleline" style="width:400px"/>
			<t:property name="cargo" renderAs="doubleline" />
			<t:property name="especializacao" renderAs="doubleline" />
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados>
			<n:column header="Foto" width="40">  
                <c:if test="${!empty colaborador.foto}">  
                    <img src="${application}/DOWNLOADFILE/${colaborador.foto.cdfile}" width="32" id="imagem_listagem" class="foto"/><!-- <t:property name="foto"/> -->  
                </c:if>  
            </n:column>
			<t:property name="nome" />
			<t:property name="cpf" />
			<t:property name="email" />
			<t:property name="usuario.login" />			
				<t:property name="ativo" trueFalseNullLabels="Ativo, Inativo,"/>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
<script type="text/javascript">
	$(document).ready(function(){
		$('#nome').focus();
	});
</script>
package br.com.orionx.clinicalsys.util;

import java.awt.Image;
import java.lang.reflect.Method;
import java.sql.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.nextframework.authorization.User;
import org.nextframework.core.standard.Next;

import br.com.orionx.clinicalsys.bean.AgendamentoConsulta;
import br.com.orionx.clinicalsys.bean.Usuario;
import br.com.orionx.clinicalsys.dao.ClinicaDAO;
import br.com.orionx.clinicalsys.service.ArquivoService;
import br.com.orionx.clinicalsys.util.exception.ClinicalSysException;


public class ClinicalSysUtil {


	/**
	 * M�todo para pegar o usu�rio logado no sistema 
	 * Alterado para retornar somente o nome do usuario
	 * 
	 * @return user
	 * @author Biharck
	 */
	public static Usuario getUsuarioLogado(){
		User user = Next.getRequestContext().getUser();
		if(user instanceof Usuario){
			Usuario pessoa=(Usuario) user;
			return pessoa;
		} else {
			return null;
		}
	}

	/**
	 * 
	 *<p>M�todo respons�vel em receber uma string contendo o caminho de um dado e retornar<br>
	 *o id.
	 *
	 *
	 * @param values valores das requisies
	 * @return {@link Integer}id
	 * 
	 * @author Biharck Ara�jo
	 * @since Sep 17, 2008
	 */
	public static Integer returnIdWithParameters(String ...values) {
		Pattern pattern = Pattern.compile("\\d");
		StringBuilder result = new StringBuilder();
		Matcher matcher = pattern.matcher(values[0]);
		while(matcher.find())
			result.append(matcher.group());
		return Integer.parseInt(result.toString());
	}


	/**
	 * <b>M�todo utilizado para valida��o de cpf e cnpj</b>
	 * @author Biharck
	 * 
	 * @param campo
	 * @return
	 */
	public static boolean validaCpfCnpj(String campo){


		// Rotina para CPF        
		if (campo.length() == 11 )
		{
			int d1, d2, digito1, digito2, resto, digitoCPF;
			String  nDigResult;
			d1 = d2 = 0;
			digito1 = digito2 = resto = 0;

			for (int n_Count = 1; n_Count < campo.length() -1; n_Count++){
				digitoCPF = Integer.valueOf (campo.substring(n_Count -1, n_Count)).intValue();
				// Multiplique a ultima casa por 2 a seguinte por 3 a seguinte por 4 e assim por diante.
				d1 = d1 + ( 11 - n_Count ) * digitoCPF;
				// Para o segundo digito repita o procedimento incluindo o primeiro digito calculado 
				//no passo anterior.
				d2 = d2 + ( 12 - n_Count ) * digitoCPF;
			}

			// Primeiro resto da divis�o por 11.
			resto = (d1 % 11);
			// Se o resultado for 0 ou 1 o digito � 0 caso contr�rio o digito � 11 menos o 
			//resultado anterior.
			if (resto < 2) digito1 = 0;
			else digito1 = 11 - resto;

			d2 += 2 * digito1;
			// Segundo resto da divis�o por 11.
			resto = (d2 % 11);
			// Se o resultado for 0 ou 1 o digito � 0 caso contr�rio o digito � 11 menos o resultado anterior.
			if (resto < 2) digito2 = 0;
			else digito2 = 11 - resto;

			// Digito verificador do CPF que est� sendo validado.
			String nDigVerific = campo.substring (campo.length()-2, campo.length());
			// Concatenando o primeiro resto com o segundo.
			nDigResult = String.valueOf(digito1) + String.valueOf(digito2);
			// Comparar o digito verificador do cpf com o primeiro resto + o segundo resto.
			if(nDigVerific.equals(nDigResult)==false) throw new ClinicalSysException ("CPF Incorreto");
			return nDigVerific.equals(nDigResult);
		}
		// Rotina para CNPJ        
		else if (campo.length() == 14){
			int soma = 0,dig;
			String cnpj_calc = campo.substring(0,12);
			char[] chr_cnpj = campo.toCharArray();

			// Primeira parte
			for( int i = 0; i < 4; i++ ){
				if ( chr_cnpj[i]-48 >=0 && chr_cnpj[i]-48 <=9 )
					soma += (chr_cnpj[i] - 48) * (6 - (i + 1));
			}
			for( int i = 0; i < 8; i++ ){
				if ( chr_cnpj[i+4]-48 >=0 && chr_cnpj[i+4]-48 <=9 )
					soma += (chr_cnpj[i+4] - 48) * (10 - (i + 1));
			}

			dig = 11 - (soma % 11);
			cnpj_calc += ( dig == 10 || dig == 11 ) ? "0" : Integer.toString(dig);

			//Segunda parte
			soma = 0;
			for ( int i = 0; i < 5; i++ ){
				if ( chr_cnpj[i]-48 >=0 && chr_cnpj[i]-48 <=9 )
					soma += (chr_cnpj[i] - 48) * (7 - (i + 1));
			}
			for ( int i = 0; i < 8; i++ ){
				if ( chr_cnpj[i+5]-48 >=0 && chr_cnpj[i+5]-48 <=9 )
					soma += (chr_cnpj[i+5] - 48) * (10 - (i + 1));
			}

			dig = 11 - (soma % 11);
			cnpj_calc += ( dig == 10 || dig == 11 ) ? "0" : Integer.toString(dig);
			if(campo.equals(cnpj_calc)==false) throw new ClinicalSysException ("CNPJ Incorreto");
			else
				return campo.equals(cnpj_calc);
		}
		else
			throw new ClinicalSysException("CPF ou CNPJ incorretos");
	}


	/**
	 * <b>M�todo respons�vel em passar uma data para String no formato dd/MM/yyyy.</b>
	 * @param form
	 * @author Biharck 
	 */
	public static String dateToString(Date date) {
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		return  format.format(date);
	}
	
	/**
	 * <b>M�todo respons�vel em incrementar datas</b>
	 * 
	 * @param data - Data a ser incrementada.
	 * @param num - N�mero de vezes que um campo ser� incrementado.
	 * @param field - Campo a ser incrementado. <B>Exemplo:</b> <code>Calendar.DAY_OF_MONTH</code>
	 * @return java.sql.Date
	 * 
	 * @author Biharck
	 */
	public static java.sql.Date incrementDate(Date data, int num, int field){
		Calendar dt = Calendar.getInstance();
		dt.setTimeInMillis(data.getTime());
		dt.add(field, num);
		return new Date(dt.getTimeInMillis());
	}

	/**
	 * Abrevia um nome com base no tamanho m�ximo fornecido como par�metro
	 * 
	 * @author biharck
	 * 
	 */
	public static String abreviaNome(String nome,int maxLength) throws ClinicalSysException{
		if(nome == null){
			throw new ClinicalSysException("O par�metro nome n�o deve ser nulo.");
		}
		nome = nome.trim();
		if(maxLength < 0){
			throw new ClinicalSysException("O par�metro maxLenght n�o deve ser menor que zero.");
		}
		if(nome.length() <= maxLength){
			return nome;
		}
		// Se o nome possui espa�os
		if(nome.indexOf(" ") != -1){
			nome = nome.replaceAll("( ) +"," ");
			List<String> sobrenomes = new ArrayList<String>();
			int indexOf = nome.indexOf(" ");
			int indexOf2;
			String aux;
			// Adiciona os sobrenomes em uma lista
			while(indexOf != -1){
				indexOf2 = nome.indexOf(" ",indexOf+1) == -1 ? nome.length() : nome.indexOf(" ",indexOf+1);
				aux = nome.substring(indexOf,indexOf2);
				if(ContainsDe(aux) || indexOf2 == nome.length()){
					indexOf = nome.indexOf(" ",indexOf2);
					continue;
				}
				sobrenomes.add(nome.substring(indexOf,indexOf2));
				indexOf = nome.indexOf(" ",indexOf2);

			}
			// Oredena a lista de sobrenomes em ordem decrescente
			Collections.sort(sobrenomes,new Comparator<String>(){
				public int compare(String string , String string2) {
					if(string.length() > string2.length())
						return -1;
					else if(string.length() == string2.length())
						return 0;
					else return 1;
				}
			});
			/*
			 * Adiciona o �ltimo sobrenome no final da lista para que ele so seja
			 * abreviado em �ltimo caso
			 */            
			String ultimoNome = nome.substring(nome.lastIndexOf(" "), nome.length());
			sobrenomes.add(ultimoNome);
			/*
			 * Abrevia os sobrenomes e verifica se o tamanho do nome e menor ou igual
			 * ao tamanho passado como par�metro
			 */
			for (String sobrenome : sobrenomes) {
				String inicioNome = nome.substring(0,nome.indexOf(sobrenome));
				String fimNome = nome.substring(inicioNome.length() + sobrenome.length(),nome.length());
				nome = inicioNome+sobrenome.substring(0,2).toUpperCase()+"."+fimNome;
				if(nome.length() <= maxLength)
					return nome;
			}
			/*
			 * Caso depois de abreviar todos os sobrenomes o nome ainda for maior que o necess�rio
			 * ser�o retirados os sobrenomes abreviados at� que o nome tenha o tamanho almejado
			 */
			while(nome.length() > maxLength){
				if(nome.indexOf(".") == -1){
					nome = nome.substring(0,maxLength-3)+"...";
					break;
				}
				nome = nome.substring(0,nome.lastIndexOf(".")-2);
			}
			if(ContainsDe(nome.substring(nome.lastIndexOf(" "), nome.length()))){
				nome = nome.substring(0,nome.lastIndexOf(" "));
			}
			return nome;            
		}else //Se o nome n�o possui nenhum espa�o
			return nome.substring(0,maxLength-3)+"...";
	}

	/**
	 * Verifica se o nome � igual a
	 * DA, DAS, DE, DO ,DOS e E
	 * 
	 * @author biharck
	 * @param nome
	 * @return
	 */
	public static Boolean ContainsDe(String nome){
		if(nome!= null && !nome.equals("") && nome.length() < 4){
			List<String> listaDe = new ArrayList<String>();
			listaDe.add("DA");
			listaDe.add("DAS");
			listaDe.add("DE");
			listaDe.add("DO");
			listaDe.add("DOS");
			listaDe.add("E");
			for (String string : listaDe) {
				if(nome.toUpperCase().endsWith(string))
					return Boolean.TRUE;
			}

		}
		return Boolean.FALSE;
	}

	public static String ReturnHour(){
		return null;
	}

	/**
	  * Transforma uma string do tipo 'dd/MM/yyyy' em um objeto java.util.Date.
	  * 
	  * @param string
	  * @return
	  * 
	  * @author biharck
	  */
	 public static Date stringToDate(String string){
	  
	  int dia = Integer.parseInt(string.substring(0, 2));
	  int mes = Integer.parseInt(string.substring(3, 5));
	  int ano = Integer.parseInt(string.substring(6, 10));
	  
	  Calendar calendar = Calendar.getInstance();
	  calendar.set(Calendar.DAY_OF_MONTH, dia);
	  calendar.set(Calendar.MONTH, mes-1);
	  calendar.set(Calendar.YEAR, ano);
	  
	  return new Date(calendar.getTimeInMillis());
	 }
	 

	/**
	  * Retorna um string com separ��o de ponto a cada tr�s caracteres
	  * Ex: 1203810 -> 1.203.810
	  * 
	  * - Usado nas listagens onde aparece campo do tipo Long ou Int
	  * 
	  * @param texto
	  * @return String
	  * 
	  * @author Biharck 
	  */
	 public static String mascaraMilhar(String texto){			
		String novo = "";
		String pt = "";						
		for(int t = texto.length(); t > 0; t-=3){						
			int i=t;
			int f= (t-3>=0) ? t-3 : 0;
			String a = texto.substring(f,i);				
			novo = a+pt+novo;
			pt = ".";				
		}
		return novo;		
	 }
	 
	 /**
	  * <p>M�todo que recebe uma classe, o nome do @id da classe e o valor do id, e retorna no formato de um value
	  * do combo. ex br.com.orionx.clinicalsys.bean.Municipio[idMunicipio=3106200]
	  * @param o {@link Object}
	  * @param nomeId nome do campo com a annotation @id
	  * @param id
	  * @return formato do value do combo
	  * @author biharck
	  */
	 public static String getComboFormat(Object o,String nomeId, Integer id){
		 return o.getClass().getCanonicalName().toString()+"["+nomeId+"="+id+"]";
	 }
	 
	/**
	 * <p>verifica se na string s� existem n�meros
	 * 
	 * @param string {@link String} contendo poss�veis n�meros
	 * @return {@link Boolean}.<code>true</code> caso ok
	 * @author biharck
	 */		
	public static Boolean onlyNumbersPresent(String string){
		if (!string.matches("(([0-9])+)")) {
			return false;
		}
		return true;
	}

	/**
	 * <p>Fun��o respons�vel em remover acentua��o
	 * @param input {@link String} a ser removida
	 * @return {@link String} sem acentua��o
	 */
	public static String removeAcentuacao(String input){
		input = Normalizer.normalize(input, Normalizer.Form.NFD);  
		input = input.replaceAll("[^\\p{ASCII}]", "");  
		return input;
	}
	
	
	/**
	 * <p>Fun��o respons�vel em retornar a soma entre duas horas
	 * @param time1
	 * @param time2
	 * @return soma entre as horas
	 */
	public static Time addTime(Time time1, float minutos){
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(time1.getTime());
		c.add(Calendar.MINUTE, (int) minutos);
		return new Time(c.getTimeInMillis());
	}
	
	/**
	 * <p>M�todo que carrega a logomarca da empresa
	 * @return {@link Image}
	 */
	public static Image getLogoEmpresa(){
		return ArquivoService.getInstance().loadAsImage(ClinicaDAO.getInstance().getClinica().getLogomarca());
	}
	
	/**
	 * <p>M�todo respons�vel em saber se o m�todo � um GET
	 * @param method
	 * @return
	 */
	public static boolean isGetter(Method method){
	  if(!method.getName().startsWith("get"))      return false;
	  if(method.getParameterTypes().length != 0)   return false;  
		  if(void.class.equals(method.getReturnType())) return false;
		  return true;
	}
	
	/**
	 * <p>M�todo respons�vel em retornar o dia da semana
	 * @param dayOfWeek dia da semana a partir de 0
	 * @return {@link String} contendo o nome do dia da semana
	 */
	public String getDay(int dayOfWeek){
		switch(dayOfWeek){
			case 0: return "sun";
			case 1: return "mon";
			case 2: return "tue";
			case 3: return "wed";
			case 4: return "thu";
			case 5: return "fir";
			case 6: return "sat";
			default: return null;
		}
	}
	
	/**
	 * <p> retornar uma Strin caoncatenando os valores de um array
	 * @param arg array
	 * @param delimiter delimitador
	 * @return {@link String}
	 */
	public static String returnArrayConcat(Object[] arg, char delimiter){
		if(arg.length == 0)
			return "";
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < arg.length; i++) {
			sb.append(arg[i]).append(delimiter);
		}
		return sb.substring(0, sb.length()-1).toString();
	}
	
	public static String formataAgendaConsulta(List<AgendamentoConsulta> agendamentos){
		if(agendamentos==null || agendamentos.size()<1)
			return "";
		Integer id = null;
		StringBuilder calendar = new StringBuilder("");
		Calendar start = Calendar.getInstance();
		Calendar end = Calendar.getInstance();
		SimpleDateFormat formatoHora = new SimpleDateFormat("HH:mm");
		String endHourString = null;
		boolean hasEndDate = false;
		
		for (AgendamentoConsulta ac : agendamentos) {
			start.setTime(ac.getDia());
			String startHourString = formatoHora.format(ac.getHora());
			start.set(start.get(start.YEAR), start.get(start.MONTH), start.get(start.DAY_OF_MONTH), Integer.parseInt(startHourString.split(":")[0]), Integer.parseInt(startHourString.split(":")[1]),0);
			
			if(ac.getHoraFim()!=null){
				endHourString = formatoHora.format(ac.getHoraFim());
				end.set(start.get(start.YEAR), start.get(start.MONTH), start.get(start.DAY_OF_MONTH), Integer.parseInt(endHourString.split(":")[0]), Integer.parseInt(endHourString.split(":")[1]),0);
				hasEndDate = true;
			}
			
			id = ac.getIdAgendamentoConsulta();
			calendar
				.append("{")
				.append("   id:"+id+",")
				.append("   title:'"+ac.getPaciente().getNome()+"',")
				.append("   situacaoURL:'"+EnumSituacoesAgendamento.getEnumById(ac.getSituacao()).getImg()+"',")
				.append("   situacaoDESC:'"+EnumSituacoesAgendamento.getEnumById(ac.getSituacao()).getDescricao()+"',");
				if(ac.getObservacoes()!=null)
					calendar.append("   observacoes: '"+"Queixa: "+ac.getObservacoes()+"',");
				
				calendar.append("   allDay:"+ac.isAllDay()).append(",");
				if(ac.getPaciente().getEmail()!=null)
					calendar.append("   email:'"+ac.getPaciente().getEmail()).append("',");
				else
					calendar.append("   email:'E-mail: n�o informado!',");
				
				calendar.append("   startHour:'"+ac.getHora()+"',")
				.append("   endHour:'"+ac.getHoraFim()+"',")
				.append("   start: new Date("+start.get(start.YEAR)+","+
											start.get(start.MONTH)+","+
											start.get(start.DAY_OF_MONTH)+","+
											start.get(start.HOUR_OF_DAY)+","+
											start.get(start.MINUTE)+","+
											start.get(start.SECOND)+
										")");
				if(hasEndDate){
					hasEndDate = false;
					calendar.append(",   end: new Date("+start.get(start.YEAR)+","+
							start.get(start.MONTH)+","+
							start.get(start.DAY_OF_MONTH)+","+
							end.get(end.HOUR_OF_DAY)+","+
							end.get(end.MINUTE)+","+
							end.get(end.SECOND)+
						")");
				}
				calendar.append("},");
		}
		String removeComma = calendar.toString().substring(0, calendar.length()-1);
		
		return removeComma;
	}

	 
}

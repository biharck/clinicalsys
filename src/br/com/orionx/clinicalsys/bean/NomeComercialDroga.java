package br.com.orionx.clinicalsys.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name = "nomecomercialdroga")
public class NomeComercialDroga {
	
	private Integer idNomeComercialDroga;
	private String nomeComercial;
	private IndustriaFarmaceutica industriaFarmaceutica;
	private ApresentacaoMedicacao apresentacao;
	private String dosagem;
	private String qtdDisponivelEmbalagem;
	private boolean generico;
	private Medicamento medicamento;

	//GET
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getIdNomeComercialDroga() {
		return idNomeComercialDroga;
	}
	@Required
	@MinLength(2)
	@MaxLength(50)
	@Column(length = 100)
	public String getNomeComercial() {
		return nomeComercial;
	}
	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idIndustriaFarmaceutica")
	public IndustriaFarmaceutica getIndustriaFarmaceutica() {
		return industriaFarmaceutica;
	}
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "idApresentacaoMedicacao")
	@ForeignKey(name = "fk_med_aprese_med")
	public ApresentacaoMedicacao getApresentacao() {
		return apresentacao;
	}
	
	@MaxLength(2000)
	@Column(length = 2000)
	public String getDosagem() {
		return dosagem;
	}
	
	@MinLength(2)
	@MaxLength(220)
	@Column(length = 255)
	public String getQtdDisponivelEmbalagem() {
		return qtdDisponivelEmbalagem;
	}
	
	public boolean isGenerico() {
		return generico;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idMedicamento")
	public Medicamento getMedicamento() {
		return medicamento;
	}
	
	//SET
	public void setIdNomeComercialDroga(Integer idNomeComercialDroga) {
		this.idNomeComercialDroga = idNomeComercialDroga;
	}
	public void setNomeComercial(String nomeComercial) {
		this.nomeComercial = nomeComercial;
	}
	public void setIndustriaFarmaceutica(IndustriaFarmaceutica industriaFarmaceutica) {
		this.industriaFarmaceutica = industriaFarmaceutica;
	}
	public void setApresentacao(ApresentacaoMedicacao apresentacao) {
		this.apresentacao = apresentacao;
	}
	public void setDosagem(String dosagem) {
		this.dosagem = dosagem;
	}
	public void setQtdDisponivelEmbalagem(String qtdDisponivelEmbalagem) {
		this.qtdDisponivelEmbalagem = qtdDisponivelEmbalagem;
	}
	public void setGenerico(boolean generico) {
		this.generico = generico;
	}
	public void setMedicamento(Medicamento medicamento) {
		this.medicamento = medicamento;
	}
}

package br.com.orionx.clinicalsys.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "drogasilicitashistoriapregressa")
public class DrogasIlicitasHistoriaPregressa {
	
	private Integer idDrogasIlicitasHistoriaPregressa;
	private DrogasIlicitas drogasIlicitas;
	private Integer quantidade;
	private EnumFrequencia frequencia;
	private Date inicioVicio;
	private HistoriaPregressa historiaPregressa;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getIdDrogasIlicitasHistoriaPregressa() {
		return idDrogasIlicitasHistoriaPregressa;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idDrogasIlicitas")
	@ForeignKey(name = "fk_drogasilicitasHP")
	public DrogasIlicitas getDrogasIlicitas() {
		return drogasIlicitas;
	}
	public Integer getQuantidade() {
		return quantidade;
	}
	public EnumFrequencia getFrequencia() {
		return frequencia;
	}
	public Date getInicioVicio() {
		return inicioVicio;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idHistoriaPregressa")
	@ForeignKey(name = "fk_hp_drogasilicitas")
	public HistoriaPregressa getHistoriaPregressa() {
		return historiaPregressa;
	}
	public void setIdDrogasIlicitasHistoriaPregressa(
			Integer idDrogasIlicitasHistoriaPregressa) {
		this.idDrogasIlicitasHistoriaPregressa = idDrogasIlicitasHistoriaPregressa;
	}
	public void setDrogasIlicitas(DrogasIlicitas drogasIlicitas) {
		this.drogasIlicitas = drogasIlicitas;
	}
	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	public void setFrequencia(EnumFrequencia frequencia) {
		this.frequencia = frequencia;
	}
	public void setInicioVicio(Date inicioVicio) {
		this.inicioVicio = inicioVicio;
	}
	public void setHistoriaPregressa(HistoriaPregressa historiaPregressa) {
		this.historiaPregressa = historiaPregressa;
	}
}

package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "alimentacaopaciente")
public class AlimentacaoPaciente {
	
	private Integer idAlimentacaoPaciente;
	private Alimentacao alimentacao;
	private HistoriaPregressa historiaPregressa;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getIdAlimentacaoPaciente() {
		return idAlimentacaoPaciente;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idHistoriaPregressa")
	@ForeignKey(name = "fk_pct_alimentacao")
	public HistoriaPregressa getHistoriaPregressa() {
		return historiaPregressa;
	}
	
	@JoinColumn(name = "idPaciente")
	@ForeignKey(name = "fk_alimentacao_pct")
	@ManyToOne(fetch = FetchType.LAZY)
	public Alimentacao getAlimentacao() {
		return alimentacao;
	}
	
	
	public void setIdAlimentacaoPaciente(Integer idAlimentacaoPaciente) {
		this.idAlimentacaoPaciente = idAlimentacaoPaciente;
	}
	public void setHistoriaPregressa(HistoriaPregressa historiaPregressa) {
		this.historiaPregressa = historiaPregressa;
	}
	public void setAlimentacao(Alimentacao alimentacao) {
		this.alimentacao = alimentacao;
	}
}

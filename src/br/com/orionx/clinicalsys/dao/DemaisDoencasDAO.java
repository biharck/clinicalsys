package br.com.orionx.clinicalsys.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.orionx.clinicalsys.bean.DemaisDoencas;
import br.com.orionx.clinicalsys.bean.HistoriaPregressa;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class DemaisDoencasDAO extends GenericDAO<DemaisDoencas> {

	public List<DemaisDoencas> findByHistoriaPregressa(HistoriaPregressa hp){
		if(hp.getIdHistoriaPregressa() == null){
			return new ArrayList<DemaisDoencas>();
		}
		return query()
				.select("demaisDoencas")
				.from(DemaisDoencas.class)
				.leftOuterJoin("demaisDoencas.demaisDoencasPacientes ddp")
				.where("ddp.historiaPregressa = ?", hp)
				.list();
	}
	
}

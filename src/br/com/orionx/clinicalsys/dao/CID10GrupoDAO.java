package br.com.orionx.clinicalsys.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.support.rowset.SqlRowSet;

import br.com.orionx.clinicalsys.bean.CID10Grupo;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class CID10GrupoDAO extends GenericDAO<CID10Grupo> {

	
	/**
	 * <p>M�todo que retorna os grupos correspondente ao cap�tulo do cid10
	 * @param catInic categoria inicial {@link String}
	 * @param catFim categoria final {@link String}
	 * @return {@link List} {@link CID10Grupo}
	 */
	public List<CID10Grupo> getGruposByCapitulo(String catInic, String catFim){
		
		List<CID10Grupo> grupos = new ArrayList<CID10Grupo>();
		
		
		SqlRowSet srs = getJdbcTemplate().queryForRowSet(
				"select * "+
				"from `cid10grupo` g "+
				"where g.`catInic`  >= '"+catInic+"' and g.`catFim` <= '"+catFim+"' ");
		
		while (srs.next()) {
			grupos.add(new CID10Grupo(srs.getInt("idCid10Grupo"),
									  srs.getString("catInic"),
									  srs.getString("catFim"),
									  srs.getString("descricao"),
									  srs.getString("descrabrev")));
	    }
		return grupos;
	}
}

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="cs" uri="clinicalsys"%>


<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro columns="3" showReport="true" urlreport="pacientes">
			<t:property name="nome" renderAs="doubleline" style="width:400px;"/>		
			<t:property name="registro" renderAs="doubleline"/>		
			<t:property name="sexo" renderAs="doubleline"/>		
			<t:property name="cpf" renderAs="doubleline"/>		
			<n:comboReloadGroup>
				<t:property name="operadora" renderAs="doubleline"/>		
				<t:property name="plano" renderAs="doubleline"/>		
			</n:comboReloadGroup>
			<t:property name="ativoEnum" renderAs="doubleline"/>
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados >
		<t:tabelaResultados showExcluirLink="false" >
			<n:column header="Prontuário">
				<a  href="javascript:redirectProntuario(${paciente.idPaciente})" ><img border="0" src="/ClinicalSys/images/bula.png"  alt="Prontuário" class="help" title="Prontuário"></a>
			</n:column>
			<n:column header="Foto" width="40">  
                <c:if test="${!empty paciente.fotoPaciente}">  
                    <img  src="${application}/DOWNLOADFILE/${paciente.fotoPaciente.cdfile}" width="32" id="imagem_listagem" class="foto"/><!-- <t:property name="fotoPaciente"/> -->  
                </c:if>  
            </n:column>  
			<t:property name="idPaciente" label="Registro" />
			<t:property name="nome"/>
			<n:column header="Idade">
				${cs:getidade(paciente.dtnascimento)} Anos
			</n:column>			
			<t:property name="sexo"/>
			<t:property name="cpf"/>
			<t:property name="email"/>
			<t:property name="ativo" trueFalseNullLabels="Ativo,Inativo,"/>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
<script type="text/javascript">
	function redirectProntuario(param){
		openDialogAguarde();
		location.href= '/ClinicalSys/clinic/process/prontuario?idPaciente='+param;
	}
</script>	

package br.com.orionx.clinicalsys.system.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.orionx.clinicalsys.bean.CategoriaProdutos;
import br.com.orionx.clinicalsys.filtros.CategoriaProdutosFiltro;
import br.com.orionx.clinicalsys.util.DatabaseError;
import br.com.orionx.clinicalsys.util.exception.ClinicalSysException;
import br.com.orionx.clinicalsys.util.generics.CrudController;

@Controller(path="/system/pag/CategoriaProdutos",authorizationModule=CrudAuthorizationModule.class)
public class CategoriaProdutosCrud extends CrudController<CategoriaProdutosFiltro, CategoriaProdutos, CategoriaProdutos>{

	@Override
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Administra��o &raquo; <a href=\"/ClinicalSys/system/pag/CategoriaProdutos\">Categorias de produtos e servi�os</a></p>";
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, CategoriaProdutos form)throws CrudException {
		return saveUnique(request, form,true);
	}
	
	@Override
	public ModelAndView saveAndNew(WebRequestContext request, CategoriaProdutos form)throws Exception {
		return saveUnique(request, form,false);
	}
	
	public ModelAndView saveUnique(WebRequestContext request, CategoriaProdutos form, boolean retornaListagem){
		try {
			salvar(request, form);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_nome_catprodutos")) throw new ClinicalSysException("J� existe uma categoria de produtos com este nome cadastrado no sistema.");
			return sendRedirectToAction("entrada");
		}
		catch (Exception e) {
			return sendRedirectToAction("entrada");
		}
		if(retornaListagem)return sendRedirectToAction("listagem");
		else return sendRedirectToAction("entrada");
	}

}

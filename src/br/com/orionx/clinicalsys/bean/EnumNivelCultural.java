package br.com.orionx.clinicalsys.bean;

public enum EnumNivelCultural {

	ELEVADO(1,"Elevado"),
	MEDIO(2,"M�dio"),
	BAIXO(3,"Baixo");
	
	private Integer id;
	private String descricao;

	private EnumNivelCultural(Integer id, String descricao){
		this.id = id;
		this.descricao = descricao;
	}
	
	public Integer getId() {
		return id;
	}
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}

}

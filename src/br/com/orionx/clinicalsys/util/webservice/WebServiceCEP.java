package br.com.orionx.clinicalsys.util.webservice;


public class WebServiceCEP {

	private String quantidade;
	private Retorno retorno;
	
	
	public String getQuantidade() {
		return quantidade;
	}
	public Retorno getRetorno() {
		return retorno;
	}
	public void setQuantidade(String quantidade) {
		this.quantidade = quantidade;
	}
	public void setRetorno(Retorno retorno) {
		this.retorno = retorno;
	}
	
	
}

package br.com.orionx.clinicalsys.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Timestamp;

import br.com.orionx.clinicalsys.bean.Cargo;

public class CargaCSV {

	public static void main(String[] args) {
		salvaSGBD(null);
	}

	public static void salvaTISS(FileReader reader) {
		String linha = null;

		try {
			BufferedReader leitor = new BufferedReader(reader);
			String vetor[] = null;

			while ((linha = leitor.readLine()) != null) {
				vetor = linha.split(";");
				montaCargo(vetor[0],vetor[1]);
			}
			leitor.close();
			reader.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static Cargo montaCargo(String... args){
		Cargo c = new Cargo();
		c.setAtivo(true);
		c.setCodigoCBO(args[0]);
		c.setDescricao(args[1]);
		c.setNome(args[1]);
		c.setDtAltera(new Timestamp(System.currentTimeMillis()));
		c.setIdCargo(null);
		return c;
//		salvaSGBD(c);
	}
	
	public static void salvaSGBD(Cargo c){
		String linha = null;

		try {
			BufferedReader leitor = new BufferedReader(new FileReader("/Users/biharck/carga/cbocarga.csv"));

			while ((linha = leitor.readLine()) != null) {
				System.out.println(linha);
			}
			leitor.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

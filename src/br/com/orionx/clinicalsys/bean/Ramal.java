package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name = "ramal")
public class Ramal {
	
	private Integer idRamal;
	private String localizacao;
	private Integer numero;
	private Clinica clinica;
	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdRamal() {
		return idRamal;
	}
	@Required
	@DisplayName("Localiza��o")
	@MinLength(2)
	@MaxLength(20)
	public String getLocalizacao() {
		return localizacao;
	}
	@Required
	@DisplayName("N�mero")
	@MinLength(1)
	@MaxLength(10)
	public Integer getNumero() {
		return numero;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "idClinica")
	@ForeignKey(name = "fk_ramal_clinica")
	public Clinica getClinica() {
		return clinica;
	}
	
	//SET
	public void setIdRamal(Integer idRamal) {
		this.idRamal = idRamal;
	}
	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	
	public void setClinica(Clinica clinica) {
		this.clinica = clinica;
	}

}

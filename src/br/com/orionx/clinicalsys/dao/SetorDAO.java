package br.com.orionx.clinicalsys.dao;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.QueryBuilder;

import br.com.orionx.clinicalsys.bean.Setor;
import br.com.orionx.clinicalsys.filtros.SetorFiltro;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class SetorDAO extends GenericDAO<Setor>{

	
	@Override
	public void updateListagemQuery(QueryBuilder<Setor> query,FiltroListagem _filtro) {
		SetorFiltro filtro = (SetorFiltro) _filtro;		
		query.whereLikeIgnoreAll("setor.nome",filtro.getNome());
		super.updateListagemQuery(query, filtro);
	}
}

package br.com.orionx.clinicalsys.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * http://pt.wikipedia.org/wiki/Anexo:Lista_de_c�digos_da_CID-10
 * sub n�veis em formato de �rvore
 * 
 * @author biharck
 * representam os grupos do cid10
 *
 */
@Entity
@Table(name = "cid10grupo")
public class CID10Grupo{
	
	private Integer idCid10Grupo;
	private String catInic;
	private String catFim;
	private String descricao;
	private String descrabrev;
	
	private List<CID10Categoria> categorias;
	
	public CID10Grupo() {}
	
	public CID10Grupo(Integer idCid10Grupo, String catInic, String catFim, String descricao, String descrabrev) {
		this.idCid10Grupo   = idCid10Grupo;
		this.catInic 		= catInic;
		this.catFim 		= catFim;
		this.descricao 		= descricao;
		this.descrabrev 	= descrabrev;
	}
	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdCid10Grupo() {
		return idCid10Grupo;
	}

	public String getCatInic() {
		return catInic;
	}

	public String getCatFim() {
		return catFim;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getDescrabrev() {
		return descrabrev;
	}
	
	@Transient
	public List<CID10Categoria> getCategorias() {
		return categorias;
	}

	//SET
	public void setIdCid10Grupo(Integer idCid10Grupo) {
		this.idCid10Grupo = idCid10Grupo;
	}

	public void setCatInic(String catInic) {
		this.catInic = catInic;
	}

	public void setCatFim(String catFim) {
		this.catFim = catFim;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setDescrabrev(String descrabrev) {
		this.descrabrev = descrabrev;
	}
	public void setCategorias(List<CID10Categoria> categorias) {
		this.categorias = categorias;
	}

}

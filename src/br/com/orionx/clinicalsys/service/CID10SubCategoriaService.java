package br.com.orionx.clinicalsys.service;

import java.util.List;

import br.com.orionx.clinicalsys.bean.CID10SubCategoria;
import br.com.orionx.clinicalsys.dao.CID10SubCategoriaDAO;
import br.com.orionx.clinicalsys.util.generics.GenericService;

public class CID10SubCategoriaService extends GenericService<CID10SubCategoriaService> {

	private CID10SubCategoriaDAO cid10SubCategoriaDAO;
	
	public void setCid10SubCategoriaDAO(CID10SubCategoriaDAO cid10SubCategoriaDAO) {
		this.cid10SubCategoriaDAO = cid10SubCategoriaDAO;
	}
	
	/**
	 * <p>M�todo de refer�ncia ao DAO
	 * @return {@link CID10SubCategoria}
	 */
	public List<CID10SubCategoria> getCid10SubCategoria(String categoria){
		return cid10SubCategoriaDAO.getCid10SubCategoria(categoria);
	}
}

package br.com.orionx.clinicalsys.dao;

import br.com.orionx.clinicalsys.bean.HistoriaPregressa;
import br.com.orionx.clinicalsys.bean.ImunizacoesInfanciaPaciente;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class ImunizacoesInfanciaPacienteDAO extends GenericDAO<ImunizacoesInfanciaPaciente>{

	public void deleteByHistoriaPregressa(HistoriaPregressa hp){
		getHibernateTemplate().bulkUpdate("delete from ImunizacoesInfanciaPaciente where historiaPregressa = ?", hp);
	}
	
}

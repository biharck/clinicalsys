package br.com.orionx.clinicalsys.filtros;

import org.nextframework.types.Cpf;

import br.com.orionx.clinicalsys.bean.Cargo;
import br.com.orionx.clinicalsys.bean.Especializacao;
import br.com.orionx.clinicalsys.bean.Sexo;
import br.com.orionx.clinicalsys.util.generics.FiltroListagem;

public class ColaboradorFiltro extends FiltroListagem {
	
	private String nome;
	private Sexo sexo;
	private Cpf cpf;
	private String usuario;
	private AtivoEnum ativoEnum;
	private Cargo cargo;
	private Especializacao especializacao;
	
	//GET
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Sexo getSexo() {
		return sexo;
	}
	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}
	public Cpf getCpf() {
		return cpf;
	}
	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public AtivoEnum getAtivoEnum() {
		return ativoEnum;
	}
	public void setAtivoEnum(AtivoEnum ativoEnum) {
		this.ativoEnum = ativoEnum;
	}
	public Cargo getCargo() {
		return cargo;
	}
	
	//SET
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	public Especializacao getEspecializacao() {
		return especializacao;
	}
	public void setEspecializacao(Especializacao especializacao) {
		this.especializacao = especializacao;
	}
	
}

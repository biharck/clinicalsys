package br.com.orionx.clinicalsys.dao;

import org.nextframework.persistence.SaveOrUpdateStrategy;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.orionx.clinicalsys.bean.Alimentacao;
import br.com.orionx.clinicalsys.bean.AlimentacaoPaciente;
import br.com.orionx.clinicalsys.bean.DemaisDoencas;
import br.com.orionx.clinicalsys.bean.DemaisDoencasPaciente;
import br.com.orionx.clinicalsys.bean.DoencasDaInfancia;
import br.com.orionx.clinicalsys.bean.DoencasDaInfanciaPaciente;
import br.com.orionx.clinicalsys.bean.HistoriaPregressa;
import br.com.orionx.clinicalsys.bean.ImunizacoesInfancia;
import br.com.orionx.clinicalsys.bean.ImunizacoesInfanciaPaciente;
import br.com.orionx.clinicalsys.bean.Paciente;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class HistoriaPregressaDAO extends GenericDAO<HistoriaPregressa>{

	private DoencasDaInfanciaPacienteDAO doencasDaInfanciaPacienteDAO;
	private DemaisDoencasPacienteDAO demaisDoencasPacienteDAO;
	private AlimentacaoPacienteDAO alimentacaoPacienteDAO;
	private AlimentacaoDAO alimentacaoDAO;
	private DoencasDaInfanciaDAO doencasDaInfanciaDAO;
	private DemaisDoencasDAO demaisDoencasDAO;
	private ImunizacoesAdolescenciaPacienteDAO  imunizacoesAdolescenciaPacienteDAO;
	private ImunizacoesAdultoPacienteDAO imunizacoesAdultoPacienteDAO;
	private ImunizacoesIdosoPacienteDAO imunizacoesIdosoPacienteDAO;
	private ImunizacoesInfanciaDAO imunizacoesInfanciaDAO;
	private ImunizacoesInfanciaPacienteDAO imunizacoesInfanciaPacienteDAO;
	
	public void setDoencasDaInfanciaPacienteDAO(DoencasDaInfanciaPacienteDAO doencasDaInfanciaPacienteDAO) {
		this.doencasDaInfanciaPacienteDAO = doencasDaInfanciaPacienteDAO;
	}
	public void setDemaisDoencasPacienteDAO(DemaisDoencasPacienteDAO demaisDoencasPacienteDAO) {
		this.demaisDoencasPacienteDAO = demaisDoencasPacienteDAO;
	}
	public void setAlimentacaoPacienteDAO(AlimentacaoPacienteDAO alimentacaoPacienteDAO) {
		this.alimentacaoPacienteDAO = alimentacaoPacienteDAO;
	}
	public void setAlimentacaoDAO(AlimentacaoDAO alimentacaoDAO) {
		this.alimentacaoDAO = alimentacaoDAO;
	}
	public void setDoencasDaInfanciaDAO(DoencasDaInfanciaDAO doencasDaInfanciaDAO) {
		this.doencasDaInfanciaDAO = doencasDaInfanciaDAO;
	}
	public void setDemaisDoencasDAO(DemaisDoencasDAO demaisDoencasDAO) {
		this.demaisDoencasDAO = demaisDoencasDAO;
	}
	public void setImunizacoesAdolescenciaPacienteDAO(ImunizacoesAdolescenciaPacienteDAO imunizacoesAdolescenciaPacienteDAO) {
		this.imunizacoesAdolescenciaPacienteDAO = imunizacoesAdolescenciaPacienteDAO;
	}
	public void setImunizacoesAdultoPacienteDAO(ImunizacoesAdultoPacienteDAO imunizacoesAdultoPacienteDAO) {
		this.imunizacoesAdultoPacienteDAO = imunizacoesAdultoPacienteDAO;
	}
	public void setImunizacoesIdosoPacienteDAO(ImunizacoesIdosoPacienteDAO imunizacoesIdosoPacienteDAO) {
		this.imunizacoesIdosoPacienteDAO = imunizacoesIdosoPacienteDAO;
	}
	public void setImunizacoesInfanciaDAO(ImunizacoesInfanciaDAO imunizacoesInfanciaDAO) {
		this.imunizacoesInfanciaDAO = imunizacoesInfanciaDAO;
	}
	public void setImunizacoesInfanciaPacienteDAO(ImunizacoesInfanciaPacienteDAO imunizacoesInfanciaPacienteDAO) {
		this.imunizacoesInfanciaPacienteDAO = imunizacoesInfanciaPacienteDAO;
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("doencasDaInfancia");
		save.saveOrUpdateManaged("demaisDoencas");
		save.saveOrUpdateManaged("interacoes");//medicamentos em uso MD
		save.saveOrUpdateManaged("alimentacao");
		save.saveOrUpdateManaged("ocupacoes");//OK MD
		save.saveOrUpdateManaged("drogasIlicitas");//OK MD
		save.saveOrUpdateManaged("cirurgias");//OK MD
		save.saveOrUpdateManaged("transfusoes");//OK MD
		save.saveOrUpdateManaged("imunizacoes");//OK MD
		save.saveOrUpdateManaged("listaCrescimentoPaciente");
	}
	
	
	@Override
	public void saveOrUpdate(final HistoriaPregressa bean) {
		transactionTemplate.execute(new TransactionCallback<Object>(){
			public Object doInTransaction(TransactionStatus arg0) {
				
				//salvando as imunizações
				if(bean.getImunizacoesAdolescenciaPaciente()!=null)
					imunizacoesAdolescenciaPacienteDAO.saveOrUpdate(bean.getImunizacoesAdolescenciaPaciente());
				
				if(bean.getImunizacoesAdultoPaciente()!=null)
					imunizacoesAdultoPacienteDAO.saveOrUpdate(bean.getImunizacoesAdultoPaciente());
				
				if(bean.getImunizacoesIdosoPaciente()!=null)
					imunizacoesIdosoPacienteDAO.saveOrUpdate(bean.getImunizacoesIdosoPaciente());
				//salva a historia pregressa
				
				HistoriaPregressaDAO.super.saveOrUpdate(bean);
				
				//apagar todas as referencias em outros objetos
				doencasDaInfanciaPacienteDAO.deleteByHistoriaPregressa(bean);
				if(bean.getDoencasDaInfanciasTransient()!=null)
					for (Object di : bean.getDoencasDaInfanciasTransient()) {
						DoencasDaInfanciaPaciente dip = new DoencasDaInfanciaPaciente();
						dip.setDoencasDaInfancia(new DoencasDaInfancia(Integer.parseInt(di.toString())));
						dip.setHistoriaPregressa(bean);
						doencasDaInfanciaPacienteDAO.saveOrUpdate(dip);
					}
				
				demaisDoencasPacienteDAO.deleteByHistoriaPregressa(bean);
				if(bean.getDemaisDoencasTransient()!=null)
					for (Object dd : bean.getDemaisDoencasTransient()) {
						DemaisDoencasPaciente ddp = new DemaisDoencasPaciente();
						ddp.setDemaisDoencas(new DemaisDoencas(Integer.parseInt(dd.toString())));
						ddp.setHistoriaPregressa(bean);
						demaisDoencasPacienteDAO.saveOrUpdate(ddp);
					}
				
				alimentacaoPacienteDAO.deleteByHistoriaPregressa(bean);
				if(bean.getAlimentacaosTransient()!=null)
					for (Object a : bean.getAlimentacaosTransient()) {
						AlimentacaoPaciente ap = new AlimentacaoPaciente();
						ap.setAlimentacao(new Alimentacao(Integer.parseInt(a.toString())));
						ap.setHistoriaPregressa(bean);
						alimentacaoPacienteDAO.saveOrUpdate(ap);
					}
				
				imunizacoesInfanciaPacienteDAO.deleteByHistoriaPregressa(bean);
				if(bean.getImunizacoesTransient() != null)
					for (Object i : bean.getImunizacoesTransient()) {
						ImunizacoesInfanciaPaciente iip = new ImunizacoesInfanciaPaciente();
						iip.setImunizacaoPaciente(new ImunizacoesInfancia(Integer.parseInt(i.toString())));
						iip.setHistoriaPregressa(bean);
						imunizacoesInfanciaPacienteDAO.saveOrUpdate(iip);
					}
				
				
				return null;
			}
		});
	}
	
	public HistoriaPregressa getByPaciente(Paciente p){
		return query()
					.select("historiaPregressa")
					.leftOuterJoin("historiaPregressa.paciente p")
					.where("p=?",p)
					.unique();
	}
	
	
	@Override
	public HistoriaPregressa loadForEntrada(HistoriaPregressa bean) {
		bean = super.loadForEntrada(bean);
		bean.setAlimentacaosTransient(alimentacaoDAO.findByHistoriaPregressa(bean));
		bean.setDoencasDaInfanciasTransient(doencasDaInfanciaDAO.findByHistoriaPregressa(bean));
		bean.setDemaisDoencasTransient(demaisDoencasDAO.findByHistoriaPregressa(bean));
		bean.setImunizacoesTransient(imunizacoesInfanciaDAO.findByHistoriaPregressa(bean));
		return bean;
	}
}

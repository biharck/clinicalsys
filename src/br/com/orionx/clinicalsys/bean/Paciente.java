package br.com.orionx.clinicalsys.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.types.Cep;
import org.nextframework.types.Cpf;
import org.nextframework.validation.annotation.Email;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

import br.com.orionx.clinicalsys.util.log.Inclusao;
import br.com.orionx.clinicalsys.util.log.Log;

@Entity
@Table(name = "paciente")
public class Paciente implements Log, Inclusao{
	
	private Integer idPaciente;
	private String naturalidade;
	private EstadoCivil estadoCivil;
	private Escolaridade escolaridade;
	private Sexo sexo;
	private UnidadeFederativa unidadeFederativa;
	private Pais pais;
	private Municipio municipio;
	private String nome;
	private Cpf cpf;
	private TipoLogradouro tipoLogradouro;
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairro;
	private Cep cep;
	private String rg;
	private String orgaoExpedidor;
	private Date dtnascimento;
	private List<TelefonePaciente> listaTelefonePaciente;
	
	private Arquivo fotoPaciente;
	private String email;
	private String confirmaEmail;
	private String mae;
	private String pai;
	private String profissao;
	private Paciente indicacao;
	
	//Planos...
	private Operadora operadora;
	private Plano plano;
	
	private List<PlanoPaciente> planos;
	private Boolean ativo;
	
	//Log
	private Integer idUsuarioAltera;
	private Timestamp dtAltera;
	
	//Inclusao
	private Integer idUsuarioInclusao;
	private Timestamp dtInclusao;
	
	public Paciente() {
		// TODO Auto-generated constructor stub
	}
	public Paciente(Integer idPaciente) {
		// TODO Auto-generated constructor stub
	}
	
	//Transient
	@Transient
	public Operadora getOperadora() {
		return operadora;
	}
	@Transient
	public Plano getPlano() {
		return plano;
	}
	@Transient
	@Required
	public UnidadeFederativa getUnidadeFederativa() {
		return unidadeFederativa;
	}
	@Transient
	@Required
	public Pais getPais() {
		return pais;
	}
	public void setOperadora(Operadora operadora) {
		this.operadora = operadora;
	}
	public void setPlano(Plano plano) {
		this.plano = plano;
	}
	public void setUnidadeFederativa(UnidadeFederativa unidadeFederativa) {
		this.unidadeFederativa = unidadeFederativa;
	}
	public void setPais(Pais pais) {
		this.pais = pais;
	}
	
	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdPaciente() {
		return idPaciente;
	}

	@MaxLength(100)
	@Required
	public String getNaturalidade() {
		return naturalidade;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idEstadoCivil")
	@ForeignKey(name = "fk_paciente_estadocivil")
	@Required
	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idEscolaridade")
	@ForeignKey(name = "fk_paciente_escolaridade")
	@Required
	public Escolaridade getEscolaridade() {
		return escolaridade;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idSexo")
	@ForeignKey(name = "fk_paciente_sexo")
	@Required
	public Sexo getSexo() {
		return sexo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idMunicipio")
	@ForeignKey(name = "fk_paciente_municipio")
	@Required
	public Municipio getMunicipio() {
		return municipio;
	}
	@Required
	@MaxLength(200)
	@MinLength(5)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Required
	public Cpf getCpf() {
		return cpf;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idTipoLogradouro")
	public TipoLogradouro getTipoLogradouro() {
		return tipoLogradouro;
	}
	
	@Required
	@MaxLength(200)
	@MinLength(2)
	public String getLogradouro() {
		return logradouro;
	}
	public String getNumero() {
		return numero;
	}
	
	public String getComplemento() {
		return complemento;
	}
	public String getBairro() {
		return bairro;
	}
	
	public Cep getCep() {
		return cep;
	}
	@MinLength(4)
	@MaxLength(50)
	@Required
	public String getRg() {
		return rg;
	}
	@MinLength(3)
	@MaxLength(50)
	@Required
	public String getOrgaoExpedidor() {
		return orgaoExpedidor;
	}
	@Required
	public Date getDtnascimento() {
		return dtnascimento;
	}
	
	@Required
	@OneToMany(mappedBy="paciente")
	public List<TelefonePaciente> getListaTelefonePaciente() {
		return listaTelefonePaciente;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "id")
	@ForeignKey(name = "fk_paciente_arquivo")
	public Arquivo getFotoPaciente() {
		return fotoPaciente;
	}
	
	@Email
	public String getEmail() {
		return email;
	}
	
	@Email
	public String getConfirmaEmail() {
		return confirmaEmail;
	}
	@Required
	@MaxLength(200)
	public String getMae() {
		return mae;
	}
	@MaxLength(150)
	public String getPai() {
		return pai;
	}
	@Required
	public String getProfissao() {
		return profissao;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idPacienteIndicacao")
	public Paciente getIndicacao() {
		return indicacao;
	}
	@OneToMany(mappedBy = "paciente")
	public List<PlanoPaciente> getPlanos() {
		return planos;
	}
	@Required
	public Boolean getAtivo() {
		return ativo;
	}
	//SET
	public void setIdPaciente(Integer idPaciente) {
		this.idPaciente = idPaciente;
	}
	public void setNaturalidade(String naturalidade) {
		this.naturalidade = naturalidade;
	}
	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public void setEscolaridade(Escolaridade escolaridade) {
		this.escolaridade = escolaridade;
	}
	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setTipoLogradouro(TipoLogradouro tipoLogradouro) {
		this.tipoLogradouro = tipoLogradouro;
	}
	
	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setCep(Cep cep) {
		this.cep = cep;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public void setOrgaoExpedidor(String orgaoExpedidor) {
		this.orgaoExpedidor = orgaoExpedidor;
	}
	public void setDtnascimento(Date dtnascimento) {
		this.dtnascimento = dtnascimento;
	}
	public void setListaTelefonePaciente(List<TelefonePaciente> listaTelefonePaciente) {
		this.listaTelefonePaciente = listaTelefonePaciente;
	}
	public void setPlanos(List<PlanoPaciente> planos) {
		this.planos = planos;
	}
	public void setFotoPaciente(Arquivo fotoPaciente) {
		this.fotoPaciente = fotoPaciente;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setConfirmaEmail(String confirmaEmail) {
		this.confirmaEmail = confirmaEmail;
	}
	public void setMae(String mae) {
		this.mae = mae;
	}
	public void setPai(String pai) {
		this.pai = pai;
	}
	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}
	
	public void setIndicacao(Paciente indicacao) {
		this.indicacao = indicacao;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	//LOG
	@Override
	public void setIdUsuarioAltera(Integer idUsuarioAltera) {
		this.idUsuarioAltera = idUsuarioAltera;
	}

	@Override
	public void setDtAltera(Timestamp dtAltera) {
		this.dtAltera = dtAltera;
	}

	@Override
	public Integer getIdUsuarioAltera() {
		return idUsuarioAltera;
	}

	@Override
	public Timestamp getDtAltera() {
		return dtAltera;
	}
	
	//INCLUSAO
	@Override
	public void setIdUsuarioInclusao(Integer idUsuarioInclusao) {
		this.idUsuarioInclusao = idUsuarioInclusao;
	}

	@Override
	public void setDtInclusao(Timestamp dtInclusao) {
		this.dtInclusao = dtInclusao;
	}

	@Override
	public Integer getIdUsuarioInclusao() {
		return idUsuarioInclusao;
	}

	@Override
	public Timestamp getDtInclusao() {
		return dtInclusao;
	}


}

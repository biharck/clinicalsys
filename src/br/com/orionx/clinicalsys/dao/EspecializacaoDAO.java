package br.com.orionx.clinicalsys.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.QueryBuilder;

import br.com.orionx.clinicalsys.bean.Especializacao;
import br.com.orionx.clinicalsys.filtros.EspecializacaoFiltro;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class EspecializacaoDAO extends GenericDAO<Especializacao> {
	
	@Override
	public void updateListagemQuery(QueryBuilder<Especializacao> query,FiltroListagem _filtro) {
		EspecializacaoFiltro filtro = (EspecializacaoFiltro) _filtro;
		if(filtro.getAtivoEnum()!=null)
			if(filtro.getAtivoEnum().getSituacao()!=null && (filtro.getAtivoEnum().getSituacao() || !filtro.getAtivoEnum().getSituacao()))
				query.where("especializacao.ativo is "+filtro.getAtivoEnum().getSituacao());
		query.whereLike("especializacao.descricao",filtro.getDescricao() )
		.whereLike("especializacao.nome", filtro.getNome());
		super.updateListagemQuery(query, _filtro);
	}
	
	@Override
	public List<Especializacao> findAll() {
		return query().where("especializacao.ativo is true").list();
	}
	
	@Override
	public List<Especializacao> findForCombo(String... extraFields) {
		extraFields[0] += "ativo";
		return super.findForCombo(extraFields);
	}


}

package br.com.orionx.clinicalsys.clinica.controller.crud;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

import br.com.orionx.clinicalsys.bean.ItemPrescricao;
import br.com.orionx.clinicalsys.bean.Paciente;
import br.com.orionx.clinicalsys.bean.Prescricao;
import br.com.orionx.clinicalsys.service.ClassificacaoMedicacaoService;
import br.com.orionx.clinicalsys.service.PacienteService;
import br.com.orionx.clinicalsys.service.PrescricaoService;


@Controller(path="/clinic/process/prescricao",authorizationModule=ProcessAuthorizationModule.class)
public class PrescricaoProcess extends MultiActionController{
	
	private PacienteService pacienteService;
	private PrescricaoService prescricaoService;
	private ClassificacaoMedicacaoService classificacaoMedicacaoService;
	
	public void setPacienteService(PacienteService pacienteService) {
		this.pacienteService = pacienteService;
	}
	public void setPrescricaoService(PrescricaoService prescricaoService) {
		this.prescricaoService = prescricaoService;
	}
	public void setClassificacaoMedicacaoService(ClassificacaoMedicacaoService classificacaoMedicacaoService) {
		this.classificacaoMedicacaoService = classificacaoMedicacaoService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request){
		Paciente p = new Paciente();
		p.setIdPaciente(Integer.parseInt(getParameter("idPaciente")));
		p = pacienteService.getPacienteProntuario(p);
		List<Prescricao> prescricoes = prescricaoService.getPrescricoesByPaciente(p);
		request.setAttribute("prescricoes", prescricoes);
		return new ModelAndView("direct:process/listaPrescricao","paciente",p);
	}
	
	public ModelAndView novo(WebRequestContext context, Prescricao prescricao){
		Paciente p = new Paciente();
		p.setIdPaciente(Integer.parseInt(getParameter("idPaciente")));
		p = pacienteService.getPacienteProntuario(p);
		if(prescricao!=null && prescricao.getInteracoes()!=null){
			prescricao.setPaciente(p);
			salvar(context, prescricao);
			context.setAttribute("idPaciente",p);
			return index(context);
		}
		Prescricao pre = new Prescricao();
		pre.setPaciente(p);
		context.setAttribute("prescricao", pre);
		return new ModelAndView("direct:process/prescricao","paciente",p);
	}
	
	public ModelAndView salvar(WebRequestContext request, Prescricao prescricao){
		if(prescricao.getInteracoes()!=null && prescricao.getInteracoes().size() > 0){
			Paciente p = new Paciente();
			p.setIdPaciente(Integer.parseInt(getParameter("idPaciente")));
			prescricao.setPaciente(p);
			prescricao.setDataPrescricao(new Date(System.currentTimeMillis()));
			prescricao.setHoraPrescricao(new Time(System.currentTimeMillis()));
			prescricaoService.saveOrUpdate(prescricao);
			request.setAttribute("idPaciente", p);
			request.addMessage("Registro Salvo com Sucesso." );	
		}
		return index(request);
	}
	
	public ModelAndView editar(WebRequestContext request, Prescricao prescricao){
		int idPrescricao = Integer.parseInt(request.getParameter("idPrescricao"));
		prescricao = prescricaoService.getPrescricaoById(idPrescricao);
		for (ItemPrescricao ip : prescricao.getInteracoes()) {
			ip.setClassificacaoMedicacao(classificacaoMedicacaoService.getClassificacaoByMedicamento(ip.getMedicamento()));
		}
		request.setAttribute("type", "edit");
		request.setAttribute("paciente", prescricao.getPaciente());
		return new ModelAndView("direct:process/prescricao","prescricao",prescricao);
	}
	
	public ModelAndView atualizar(WebRequestContext request, Prescricao prescricao){
		
		Prescricao temp = prescricaoService.loadForEntrada(prescricao);
		Paciente p = new Paciente();
		p.setIdPaciente(Integer.parseInt(getParameter("idPaciente")));
		prescricao.setPaciente(p);
		prescricao.setDataPrescricao(temp.getDataPrescricao());
		prescricao.setHoraPrescricao(temp.getHoraPrescricao());
		prescricaoService.saveOrUpdate(prescricao);
		request.addMessage("Registro atualizado com sucesso." );
		request.setAttribute("idPaciente", p);
		return index(request);
	}
}

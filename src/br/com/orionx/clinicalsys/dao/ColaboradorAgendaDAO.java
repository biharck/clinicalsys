package br.com.orionx.clinicalsys.dao;

import java.util.List;

import br.com.orionx.clinicalsys.bean.Colaborador;
import br.com.orionx.clinicalsys.bean.ColaboradorAgenda;
import br.com.orionx.clinicalsys.bean.TipoAgenda;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class ColaboradorAgendaDAO extends GenericDAO<ColaboradorAgenda>{
	
	/**
	 * <p> Método responsável em retornar todos as agendas dos colaboradores
	 * @return {@link ColaboradorAgenda}
	 */
	public List<ColaboradorAgenda> getAgendaColaboradores(){
		return query()
				.select("colaboradorAgenda.inicioPrimeiroPeriodo, colaboradorAgenda.terminoPrimeiroPeriodo, colaboradorAgenda.inicioSegundoPeriodo, colaboradorAgenda.terminoSegundoPeriodo," +
						"colaboradorAgenda.inicioTerceiroPeriodo, colaboradorAgenda.terminoTerceiroPeriodo,colaboradorAgenda.inicioQuartoPeriodo,colaboradorAgenda.terminoQuartoPeriodo,colaboradorAgenda.ativo," +
						"foto.cdfile, foto.contenttype, foto.name, foto.size, foto.content," +
						"colaborador.nome, tipoAgenda.descricao,colaboradorAgenda.idColaboradorAgenda,colaborador.idColaborador,tipoAgenda.idTipoAgenda,especializacao.nome")
				.leftOuterJoin("colaboradorAgenda.tipoAgenda tipoAgenda")
				.leftOuterJoin("colaboradorAgenda.colaborador colaborador")
				.leftOuterJoin("colaborador.foto foto")
				.leftOuterJoin("colaborador.listaEspecializacaoColaboradors listaEspecializacaoColaboradors")
				.leftOuterJoin("listaEspecializacaoColaboradors.especializacao especializacao")
				.list()
				;
	}
	
	
	public ColaboradorAgenda getByColaborador(Colaborador colaborador, TipoAgenda tipoAgenda){
		return query()
				.select("colaboradorAgenda")
				.where("colaboradorAgenda.colaborador=?",colaborador)
				.where("colaboradorAgenda.tipoAgenda=?",tipoAgenda)
				.unique();
	}
	
	public ColaboradorAgenda getByColaborador(Colaborador colaborador){
		return query()
				.select("colaboradorAgenda")
				.where("colaboradorAgenda.colaborador=?",colaborador)
				.unique();
	}
	
	public ColaboradorAgenda getColaboradorAgendaAndColaborador(ColaboradorAgenda ca){
		
		return query()
		.select("colaboradorAgenda.inicioPrimeiroPeriodo, colaboradorAgenda.terminoPrimeiroPeriodo, colaboradorAgenda.inicioSegundoPeriodo, colaboradorAgenda.terminoSegundoPeriodo," +
				"colaboradorAgenda.inicioTerceiroPeriodo, colaboradorAgenda.terminoTerceiroPeriodo,colaboradorAgenda.inicioQuartoPeriodo,colaboradorAgenda.terminoQuartoPeriodo,colaboradorAgenda.ativo," +
				"foto.cdfile, foto.contenttype, foto.name, foto.size, foto.content," +
				"colaborador.nome, tipoAgenda.descricao,colaboradorAgenda.idColaboradorAgenda,colaborador.idColaborador,tipoAgenda.idTipoAgenda,especializacao.nome")
		.leftOuterJoin("colaboradorAgenda.tipoAgenda tipoAgenda")
		.leftOuterJoin("colaboradorAgenda.colaborador colaborador")
		.leftOuterJoin("colaborador.foto foto")
		.leftOuterJoin("colaborador.listaEspecializacaoColaboradors listaEspecializacaoColaboradors")
		.leftOuterJoin("listaEspecializacaoColaboradors.especializacao especializacao")
		.where("colaboradorAgenda=?",ca)
		.unique();
	}
}

package br.com.orionx.clinicalsys.dao;

import br.com.orionx.clinicalsys.bean.DemaisDoencasPaciente;
import br.com.orionx.clinicalsys.bean.HistoriaPregressa;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class DemaisDoencasPacienteDAO extends GenericDAO<DemaisDoencasPaciente>{

	public void deleteByHistoriaPregressa(HistoriaPregressa hp){
		getHibernateTemplate().bulkUpdate("delete from DemaisDoencasPaciente where historiaPregressa = ?", hp);
	}
	
}

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro columns="1">
			<t:property name="uf" renderAs="doubleline" id="uf"/>
			<t:property name="nome" renderAs="doubleline"/>
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados>
			<t:property name="nome"/>
			<t:property name="unidadeFederativa"/>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>

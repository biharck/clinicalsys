package br.com.orionx.clinicalsys.clinica.controller.crud;

import java.io.IOException;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.orionx.clinicalsys.bean.ClassificacaoMedicacao;
import br.com.orionx.clinicalsys.bean.Medicamento;
import br.com.orionx.clinicalsys.filtros.MedicamentoFiltro;
import br.com.orionx.clinicalsys.service.MedicamentoService;
import br.com.orionx.clinicalsys.util.ClinicalSysUtil;
import br.com.orionx.clinicalsys.util.DatabaseError;
import br.com.orionx.clinicalsys.util.exception.ClinicalSysException;
import br.com.orionx.clinicalsys.util.generics.CrudController;

@Controller(path="/clinic/pag/Medicamento",authorizationModule=CrudAuthorizationModule.class)
public class MedicamentoCrud extends CrudController<MedicamentoFiltro, Medicamento, Medicamento>{

	private MedicamentoService medicamentoService;
	
	public void setMedicamentoService(MedicamentoService medicamentoService) {
		this.medicamentoService = medicamentoService;
	}
	
	@Override
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Administra��o &raquo; <a href=\"/ClinicalSys/clinic/pag/Medicamentos\">Medicamentos</a></p>";
	}
	
	
	@Override
	protected void salvar(WebRequestContext request, Medicamento bean) throws Exception {
		
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_principio_ativo")) throw new ClinicalSysException("J� existe um medicamento com este princ�pio ativo e classifica��o cadastrado no sistema.");
		}
		catch (Exception e) {
			throw new ClinicalSysException("Ocorreu um erro ao salvar o registro, por favor, tente novamente mais tarde, e caso este erro persista entre em contao com o administrador do sistema.");
		}
	}
	
	public void buscaMedicamentos(WebRequestContext request) throws JSONException, IOException{
		try{

			request.getServletResponse().setContentType("application/json");
			request.getServletResponse().setCharacterEncoding("ISO-8859-1");
			
			if(getParameter("classificacao") == null || getParameter("classificacao").equals("<null>")){
				JSONArray jsonArray = new JSONArray();
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("jscript", "$('select[name=interacoes["+getParameter("index")+"].medicamento]').empty();");
				jsonArray.put(jsonObject);
				request.getServletResponse().getWriter().println( jsonArray );
				return;
			}
			
			//enviando para o jsp
			request.getServletResponse().getWriter().println( formatCombo(medicamentoService.getMedicamentosByClassif(new ClassificacaoMedicacao(ClinicalSysUtil.returnIdWithParameters(getParameter("classificacao")))),getParameter("index")) );
			
			
		}catch (Exception e) {
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("erro", true);
			request.getServletResponse().setContentType("application/json");
			request.getServletResponse().setCharacterEncoding("ISO-8859-1");
			request.getServletResponse().getWriter().println( jsonObj );
			e.printStackTrace();
		}
	}
	
	/**
	 * <p>Fun��o respons�vel em montar toda estrutura de um combo para povoar o html
	 * @param planos lista contendo todos os medicamentos de uma determinada classifica��o
	 * @return {@link JSONArray} formatada para enviar via JSON
	 * @throws JSONException 
	 */
	public JSONArray formatCombo(List<Medicamento> medicamentos, String index) throws JSONException{
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("jscript", "$('select[name=interacoes["+index+"].medicamento]').empty();");
		jsonArray.put(jsonObject);
		jsonObject = new JSONObject();
		jsonObject.put("jscript", "$('select[name=interacoes["+index+"].medicamento]').append('<option value=\"<null>\"></option>');");
		jsonArray.put(jsonObject);
		for (Medicamento m : medicamentos) {
			jsonObject = new JSONObject();
			jsonObject.put("jscript", "$('select[name=interacoes["+index+"].medicamento]').append('<option value=\""+ClinicalSysUtil.getComboFormat(m, "idMedicamento", m.getIdMedicamento())+"\">"+m.getPrincipioAtivo()+"</option>');");
			jsonArray.put(jsonObject);
		}
		return jsonArray;
		
	}
}

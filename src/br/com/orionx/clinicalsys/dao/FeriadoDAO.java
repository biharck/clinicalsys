package br.com.orionx.clinicalsys.dao;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.QueryBuilder;

import br.com.orionx.clinicalsys.bean.Feriado;
import br.com.orionx.clinicalsys.filtros.FeriadoFiltro;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class FeriadoDAO extends GenericDAO<Feriado>{
	
	@Override
	public void updateListagemQuery(QueryBuilder<Feriado> query,FiltroListagem _filtro) {

		FeriadoFiltro filtro = (FeriadoFiltro) _filtro;
		query.whereLikeIgnoreAll("feriado.nome",filtro.getNome());
		if(filtro.getDataInicio()!=null && filtro.getDataFim()!=null)
			query.where("feriado.data between '"+ filtro.getDataInicio() + "' and '" + filtro.getDataFim() + "'");
		else if(filtro.getDataInicio()!=null && filtro.getDataFim()==null)
			query.where("feriado.data >= ?",filtro.getDataInicio());
		else if(filtro.getDataInicio()==null && filtro.getDataFim()!=null)
			query.where("feriado.data <= ?",filtro.getDataFim());
		
		super.updateListagemQuery(query, filtro);
	}

}

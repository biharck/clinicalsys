<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="cs" uri="clinicalsys"%>

<c:set var="ultimoid"   value="${idUsuarioAltera}"/>
<c:set var="dataAlt"   value="${dtAltera}"/>
<div class="onecolumn">
	<div class="header">
		<span>Configura��es de Atendimento</span>
	</div>
	<br class="clear"/>
	<div class="content">
		<t:tela>
			<div class="linkBar" align="right">
				<n:submit action="salvar" validate="true" class="noboard" ><img src="${application}/images/shortcut/save.png" alt="Salvar" class="help" title="Salvar"/></n:submit>
			</div>
			<div id="ErrosSubmissao"></div>
			<div align="center" class="alert_info" style="display:none;position:absolute;width:400px; margin: 0px 30%; text-align: center;" id="carregandoAjax">Carregando...</div>
			<n:bean name="configuracaoAtendimento">
				<t:property name="idConfiguracaoAtendimento" type="hidden" showLabel="false" write="false"/>
				<n:group legend="Configura��o padr�o" columns="6" colspan="2">
					<t:property name="valorConsulta" renderAs="doubleline" id="valorConsulta"/>
					<t:property name="tempoConsulta" renderAs="doubleline"/>
				</n:group>
				<br>
				<fieldset>
					<legend>Configura��es Espec�ficas</legend><br>
					<t:detalhe name="precosConsultas" nomeColunaAcao="Excluir" >
						<t:property name="especializacao" onchange="verificaEspecializacoesIguais(this,${index})"/>
						<t:property name="valorConsulta"/>
						<t:property name="tempoConsulta"/>
						<n:column header=" ">
							<t:property name="idPrecoConsultaEspecialidade" type="hidden"  showLabel="false" write="false"/>
							<t:property name="ativo" type="hidden"   showLabel="false" write="false" />	
						</n:column>
					</t:detalhe>
				</fieldset>
			</n:bean>
			<div class="linkBar" align="right">
				<n:submit action="salvar" validate="true" url="/system/process/ConfiguracaoAtendimento" class="noboard" ><img src="${application}/images/shortcut/save.png" alt="Salvar" class="help" title="Salvar"/></n:submit>
			</div>
			<div class="ultimaalteracao">
				<n:panel>�ltima altera��o neste registro foi feita por <font style="color:#E77272">${cs:getuserbyid(ultimoid)}</font> no dia <font style="color:#E77272">${cs:dateformat(dataAlt)}</font> �s <font style="color:#E77272">${cs:hourformat(dataAlt)}</font> </n:panel>
			</div>
		</t:tela>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#valorConsulta').focus();
	});
	
	function verificaEspecializacoesIguais(param,idx){
		
		var i = 0;
		$('[name*=.especializacao]').each(function(){
			
			if(this.value == param.value && i != idx){
				openDialogInformation("J� existe uma configura��o ativa para esta especialidade, caso queira trocar os valores e tempos, apenas altere os campos da respectiva " +
					"especialidade, ou caso queira apagar a configura��o, clique em Exluir (�cone vermelho com formato de um 'X' do lado direito de cada configura��o ");
				$('[name=precosConsultas['+idx+'].especializacao]').val('');	
				return;
			}
			i++;
		});
	}
</script>
package br.com.orionx.clinicalsys.util.connection;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import org.nextframework.core.web.NextWeb;
import org.nextframework.exception.NotInNextContextException;
import org.springframework.jdbc.datasource.DriverManagerDataSource;


public class ClinicalSysDataSource extends DriverManagerDataSource  {
	
	@Override
	protected Connection getConnectionFromDriverManager(String url, Properties props) throws SQLException {
		String urltmp = "";
		String username = "root";
		String password = "rails";

		Properties p = new Properties();
		p.setProperty("username", username);
		p.setProperty("password", password);
		
		setUsername(username);
		setPassword(password);
		
		try {
			NextWeb.getRequestContext();
			if(NextWeb.getRequestContext().getServletRequest().getRequestURL().toString().contains("www")){
				urltmp = "jdbc:mysql://localhost/clinicalsys";
			}else{
				urltmp = "jdbc:mysql://localhost/clinicalsys";	
			}
			setUrl(urltmp);
		} catch (NotInNextContextException e) {
			urltmp = "jdbc:mysql://localhost/connect";
			setUrl(urltmp);
		}
		return super.getConnectionFromDriverManager(urltmp, props);
	}

}

package br.com.orionx.clinicalsys.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.QueryBuilder;

import br.com.orionx.clinicalsys.bean.Conselho;
import br.com.orionx.clinicalsys.filtros.ConselhoFiltro;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class ConselhoDAO extends GenericDAO<Conselho> {

	
	@Override
	public void updateListagemQuery(QueryBuilder<Conselho> query,FiltroListagem _filtro) {
		ConselhoFiltro filtro = (ConselhoFiltro) _filtro;
		if(filtro.getAtivoEnum()!=null)
			if(filtro.getAtivoEnum().getSituacao()!=null && (filtro.getAtivoEnum().getSituacao() || !filtro.getAtivoEnum().getSituacao()))
				query.where("conselho.ativo is "+filtro.getAtivoEnum().getSituacao());
		query.whereLikeIgnoreAll("conselho.nome", filtro.getNome())
		.whereLikeIgnoreAll("conselho.codigoConselho",filtro.getCodigoConselho());
		super.updateListagemQuery(query, filtro);
	}
	
	@Override
	public List<Conselho> findAll() {
		return query().where("conselho.ativo is true").list();
	}
	
	@Override
	public List<Conselho> findForCombo(String... extraFields) {
		extraFields[0] += "ativo";
		return super.findForCombo(extraFields);
	}

}

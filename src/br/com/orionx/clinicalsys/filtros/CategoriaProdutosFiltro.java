package br.com.orionx.clinicalsys.filtros;

import br.com.orionx.clinicalsys.util.generics.FiltroListagem;

public class CategoriaProdutosFiltro extends FiltroListagem{

	private String nome;
	private AtivoEnum ativoEnum;
	
	public String getNome() {
		return nome;
	}
	public AtivoEnum getAtivoEnum() {
		return ativoEnum;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setAtivoEnum(AtivoEnum ativoEnum) {
		this.ativoEnum = ativoEnum;
	}
}

package br.com.orionx.clinicalsys.system.controller.crud;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

@Controller(path="/clinic/process/DashBoard",authorizationModule=ProcessAuthorizationModule.class)
public class DashBoardProcess extends MultiActionController{
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request){
		request.setAttribute("pathURL", getPathURL());
		return new ModelAndView("process/dashBoard");
	}

	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Cl�nica &raquo; <a href=\"/ClinicalSys/clinic/process/DashBoard\">Dashboard</a></p>";
	}
	
}

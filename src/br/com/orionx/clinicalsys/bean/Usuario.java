package br.com.orionx.clinicalsys.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.nextframework.authorization.User;
import org.nextframework.types.Password;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

import br.com.orionx.clinicalsys.util.log.Log;


/*
 * 
 * update pessoa set senha = 'dBqQUb5OG/vN+ytmfmZVorLcHZrTuIywdAuzBmcBDjnOgic8PoA964yPJ9HTKSLj';
 */
@Entity
@Table(name = "usuario")
public class Usuario implements User, Log{

	private Integer idUsuario;
	private String login;
	private String password;
	
	private List<Colaborador> colaboradores;
	private List<Papel> papeis;
	
	//Log
	private Integer idUsuarioAltera;
	private Timestamp dtAltera;

	private String confirmacaoSenha;
	
	@Transient
	@MaxLength(30)
	@MinLength(6)
	@Required
	public String getConfirmacaoSenha() {
		return confirmacaoSenha;
	}
	public void setConfirmacaoSenha(String confirmacaoSenha) {
		this.confirmacaoSenha = confirmacaoSenha;
	}
	/*
	 * GET
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)	
	public Integer getIdUsuario() {
		return idUsuario;
	}
	
	@OneToMany(mappedBy="usuario")
	public List<Colaborador> getColaboradores() {
		return colaboradores;
	}

	// API
	@MaxLength(30)
	@MinLength(5)
	@Required
	public String getLogin() {
		return login;
	}

	// API
	@Password
	@Required
	@MaxLength(30)
	@MinLength(6)
	public String getPassword() {
		return password;
	}
	@Transient
	@Required
	public List<Papel> getPapeis() {
		return papeis;
	}
	
	/*
	 * SET
	 */
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public void setColaboradores(List<Colaborador> colaboradores) {
		this.colaboradores = colaboradores;
	}

	public void setPapeis(List<Papel> papeis) {
		this.papeis = papeis;
	}
	
	//LOG
	@Override
	public void setIdUsuarioAltera(Integer idUsuarioAltera) {
		this.idUsuarioAltera = idUsuarioAltera;
	}

	@Override
	public void setDtAltera(Timestamp dtAltera) {
		this.dtAltera = dtAltera;
	}

	@Override
	public Integer getIdUsuarioAltera() {
		return idUsuarioAltera;
	}

	@Override
	public Timestamp getDtAltera() {
		return dtAltera;
	}
}

package br.com.orionx.clinicalsys.clinica.controller.crud;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.view.DownloadFileServlet;
import org.springframework.web.servlet.ModelAndView;

import br.com.orionx.clinicalsys.bean.Paciente;
import br.com.orionx.clinicalsys.dao.ArquivoDAO;
import br.com.orionx.clinicalsys.service.PacienteService;


@Controller(path="/clinic/process/prontuario",authorizationModule=ProcessAuthorizationModule.class)
public class ProntuarioProcess extends MultiActionController{
	
	private PacienteService pacienteService;
	private ArquivoDAO arquivoDAO;
	
	public void setPacienteService(PacienteService pacienteService) {
		this.pacienteService = pacienteService;
	}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request){
		Paciente p = new Paciente();
		if(getParameter("autocomplete_id")!=null)
			p.setIdPaciente(Integer.parseInt(getParameter("autocomplete_id")));
		else
			p.setIdPaciente(Integer.parseInt(getParameter("idPaciente")));
		p = pacienteService.getPacienteProntuario(p);
		try {
			if(p.getFotoPaciente()!=null && p.getFotoPaciente().getCdfile()!=null){
				if(p.getFotoPaciente().getContent()==null)
					arquivoDAO.loadAsImage(p.getFotoPaciente());
				DownloadFileServlet.addCdfile(request.getSession(), p.getFotoPaciente().getCdfile());
				request.getSession().setAttribute("showpicture", true);
			}else
				request.getSession().setAttribute("showpicture", false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ModelAndView("direct:process/prontuario","paciente",p);
	}
}

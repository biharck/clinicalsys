package br.com.orionx.clinicalsys.sistema.authorization.dao;

import java.util.List;

import br.com.orionx.clinicalsys.bean.AnamnesePaciente;
import br.com.orionx.clinicalsys.bean.Paciente;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class AnamnesePacienteDAO extends GenericDAO<AnamnesePaciente>{

	
	public List<AnamnesePaciente> getAnamnesesByPaciente(Paciente p){
		return query().select("anamnesePaciente.idAnamnesePaciente, p.idPaciente, p.nome, a.idAnamnese, " +
				"a.dataAnamnese,a.queixaPrincipal")
		.join("anamnesePaciente.paciente p")
		.join("anamnesePaciente.anamnese a")
		.where("p=?",p)
		.list();
	}
}

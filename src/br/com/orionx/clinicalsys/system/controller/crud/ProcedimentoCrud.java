package br.com.orionx.clinicalsys.system.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.orionx.clinicalsys.bean.Procedimento;
import br.com.orionx.clinicalsys.filtros.ProcedimentoFiltro;
import br.com.orionx.clinicalsys.util.DatabaseError;
import br.com.orionx.clinicalsys.util.exception.ClinicalSysException;
import br.com.orionx.clinicalsys.util.generics.CrudController;

@Controller(path="/system/pag/Procedimento",authorizationModule=CrudAuthorizationModule.class)
public class ProcedimentoCrud extends CrudController<ProcedimentoFiltro, Procedimento, Procedimento>{
	
	
	@Override
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Administra��o &raquo; <a href=\"/ClinicalSys/system/pag/Procedimento\">Procedimentos</a></p>";
	}
	
	@Override
	protected void salvar(WebRequestContext request, Procedimento bean)	throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_nome_procedimento")) throw new ClinicalSysException("J� existe um procedimento com este nome cadastrado no sistema.");
		}
	}

}

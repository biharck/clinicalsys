package br.com.orionx.clinicalsys.dao;

import java.util.List;

import br.com.orionx.clinicalsys.bean.Papel;
import br.com.orionx.clinicalsys.bean.Permissao;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class PermissaoDAO extends GenericDAO<Permissao> {

	
	public List<Permissao> getPermissoesUsuario(Papel papel){
		return query()
					.select("permissao")
					.leftOuterJoin("permissao.role papel")
					.where("papel=?",papel)
					.list();
	}
}

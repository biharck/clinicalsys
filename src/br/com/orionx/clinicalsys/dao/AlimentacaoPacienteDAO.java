package br.com.orionx.clinicalsys.dao;

import br.com.orionx.clinicalsys.bean.AlimentacaoPaciente;
import br.com.orionx.clinicalsys.bean.HistoriaPregressa;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class AlimentacaoPacienteDAO extends GenericDAO<AlimentacaoPaciente>{

	public void deleteByHistoriaPregressa(HistoriaPregressa hp){
		getHibernateTemplate().bulkUpdate("delete from AlimentacaoPaciente where historiaPregressa = ?", hp);
	}
	
}

package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.types.Telefone;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name = "telefonepaciente")
public class TelefonePaciente {
	
	private Integer idTelefonePaciente;
	private Telefone telefone;
	private TelefoneTipo telefoneTipo;
	private Paciente paciente;
	
	
	public TelefonePaciente() {}
	public TelefonePaciente(Paciente paciente,Telefone telefone,TelefoneTipo telefoneTipo) {
		this.paciente = paciente;
		this.telefone = telefone;
		this.telefoneTipo = telefoneTipo;
	}
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdTelefonePaciente() {
		return idTelefonePaciente;
	}
	@Required
	public Telefone getTelefone() {
		return telefone;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "idTelefoneTipo")
	public TelefoneTipo getTelefoneTipo() {
		return telefoneTipo;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "idPaciente")
	@ForeignKey(name = "fk_telpaciente_paciente")
	public Paciente getPaciente() {
		return paciente;
	}
	
	//SET
	public void setIdTelefonePaciente(Integer idTelefonePaciente) {
		this.idTelefonePaciente = idTelefonePaciente;
	}
	public void setTelefone(Telefone telefone) {
		this.telefone = telefone;
	}
	public void setTelefoneTipo(TelefoneTipo telefoneTipo) {
		this.telefoneTipo = telefoneTipo;
	}
	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

}

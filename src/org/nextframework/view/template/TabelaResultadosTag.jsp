<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>

	<n:dataGrid headerStyleClass="" footerStyleClass="" styleClass="data" bodyStyleClasses="" itens="${TtabelaResultados.itens}" var="${TtabelaResultados.name}" cellspacing="0"
		rowonclick="javascript:$csu.coloreLinha('tabelaResultados',this)"		 
		rowonmouseover="javascript:$csu.mouseonOverTabela('tabelaResultados',this)" 
		rowonmouseout="javascript:$csu.mouseonOutTabela('tabelaResultados',this)" 
		id="tabelaResultados" varIndex="index">
		<n:bean name="${TtabelaResultados.name}" valueType="${TtabelaResultados.valueType}">
			<n:getContent tagName="acaoTag" vars="acoes">
			<c:if test="${(!empty acoes) || (TtabelaResultados.showEditarLink) || (TtabelaResultados.showExcluirLink) || (TtabelaResultados.showConsultarLink)}">
					<n:column header="" style="width: 4%; white-space: nowrap; padding-right: 3px;">
						${acoes}
							<c:if test="${!empty exSelecionar}">
								<span style="font-size: 10px; color: red; white-space: pre; display:block;">Erro ao imprimir bot�o selecionar: ${exSelecionar.message} <c:catch>${exSelecionar.rootCause.message}</c:catch></span>
							</c:if>
							<c:if test="${TtabelaResultados.showConsultarLink}">
								<n:link action="consultar" parameters="${n:idProperty(n:reevaluate(TtabelaResultados.name,pageContext))}=${n:id(n:reevaluate(TtabelaResultados.name,pageContext))}" class="consult">consultar</n:link>
							</c:if>						
							<c:if test="${TtabelaResultados.showEditarLink}">
								<n:link action="editar" class="selectTR" onclick="openDialogAguarde()" parameters="${n:idProperty(n:reevaluate(TtabelaResultados.name,pageContext))}=${n:id(n:reevaluate(TtabelaResultados.name,pageContext))}"><img src="${application}/images/icon_edit.png" alt="Editar" class="help" title="Editar"/></n:link>
							</c:if>
								
					</n:column>
				</c:if>
				<t:propertyConfig mode="output" renderAs="column">
				
				<n:doBody />
				</t:propertyConfig>
					<n:column>
						<script language="javascript">
							<c:catch var="exSelecionar">
								imprimirSelecionar(new Array(${n:hierarchy(TtabelaResultados.valueType)}), 
									"<a href=\"javascript:selecionar('${n:escape(n:valueToString(n:reevaluate(TtabelaResultados.name, pageContext)))}','${n:escape(n:descriptionToString(n:reevaluate(TtabelaResultados.name, pageContext)))}')\"><img src=\"${application}/images/select.png\" alt=\"Selecionar\" class=\"help\" title=\"Selecionar\"/></a>&nbsp;");
							</c:catch>
						</script>
					</n:column>
					<n:column header="<input type='checkbox' name='allInputs' id='allInputs'  onclick='javascript:$csu.getChecksSelecionados();' >" style="width: 1%; white-space: nowrap; padding-right: 3px;">
						<c:if test="${TtabelaResultados.showExcluirLink}">
							<n:body><input type="checkbox" name="radioselecionado" value="${n:id(n:reevaluate(TtabelaResultados.name,pageContext))}"></n:body>
						</c:if>
					</n:column>
										
			</n:getContent>
		</n:bean>
	</n:dataGrid>
	<!-- Begin pagination -->
	<div class="pagination" >
		<n:pagging currentPage="${currentPage}" totalNumberOfPages="${numberOfPages}" selectedClass="active" unselectedClass="" />
	</div>
	<!-- End pagination -->
</div>
<script>
	<c:if test="${TtabelaResultados.showExcluirLink}">
		function deleteSelectedItens(){
			if($csu.getSelectedValues() == "")$csu.validarChecksSelecionados();
			else openDialogDelete();
		}
	</c:if>

	$('.pagination a').click(function(){
		openDialogAguarde();
	});
	$('.order').click(function(){
		openDialogAguarde();
	});
	

</script>
<style type="text/css">
table.data tr th
{
	font-weight: bold;
	background:url("/ClinicalSys/images/headerdeg.png") repeat-x scroll 0 0 #CDCDCD;
	border-bottom:1px solid #CCCCCC;
	border-top:1px solid #CCCCCC; 
}
</style>

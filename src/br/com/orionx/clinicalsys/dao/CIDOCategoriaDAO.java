package br.com.orionx.clinicalsys.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.support.rowset.SqlRowSet;

import br.com.orionx.clinicalsys.bean.CIDOCategoria;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class CIDOCategoriaDAO extends GenericDAO<CIDOCategoria> {

	

	/**
	 * <p>M�todo que retorna as categorias correspondentes ao grupo do cid-o
	 * @param catInic categoria inicial {@link String}
	 * @param catFim categoria final {@link String}
	 * @return {@link List} {@link CIDOCategoria}
	 */
	public List<CIDOCategoria> getCategoriasByGrupos(String catInic, String catFim){
		
		List<CIDOCategoria> categorias = new ArrayList<CIDOCategoria>();
		
		
		SqlRowSet srs = getJdbcTemplate().queryForRowSet(
				"select * "+
				"from `cidocategoria` c "+
				"where SUBSTRING(c.cat from 1 for 4)  >= '"+catInic+"' and SUBSTRING(c.cat from 1 for 4) <= '"+catFim+"' ");
		
		while (srs.next()) {
			categorias.add(new CIDOCategoria(srs.getInt("idCIDOCategoria"),
									  srs.getString("cat"),
									  srs.getString("descricao"),
									  srs.getString("refer")));
	    }
		return categorias;
	}
	
}

package br.com.orionx.clinicalsys.system.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;

import br.com.orionx.clinicalsys.bean.Especializacao;
import br.com.orionx.clinicalsys.filtros.EspecializacaoFiltro;
import br.com.orionx.clinicalsys.util.generics.CrudController;

@Controller(path="/system/pag/Especializacao",authorizationModule=CrudAuthorizationModule.class)
public class EspecializacaoCrud extends CrudController<EspecializacaoFiltro, Especializacao, Especializacao> {

	@Override
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Administração &raquo; <a href=\"/ClinicalSys/system/pag/Especializaco\">Especializações</a></p>";
	}

}

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<t:property name="idUF" type="hidden" label="" write="false" colspan="2"/>
	<t:janelaEntrada>
		<t:tabelaEntrada columns="2">
			<t:property name="pais" renderAs="doubleline" colspan="2"/>
			<t:property name="nome" renderAs="doubleline"/>
			<t:property name="sigla" renderAs="doubleline"/>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>

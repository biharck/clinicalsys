package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name= "ocupacaohistoriapregressa")
public class OcupacaoHistoriaPregressa {
	
	private Integer idOcupacaoHistoriaPregressa;
	private String ocupacao;
	private Integer inicio;
	private Integer fim;
	private HistoriaPregressa historiaPregressa;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdOcupacaoHistoriaPregressa() {
		return idOcupacaoHistoriaPregressa;
	}
	@Required
	@MaxLength(60)
	public String getOcupacao() {
		return ocupacao;
	}
	@MaxLength(4)
	@Required
	public Integer getInicio() {
		return inicio;
	}
	@MaxLength(4)
	public Integer getFim() {
		return fim;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idHistoriaPregressa")
	@ForeignKey(name = "fk_ocupacoes_HP")
	public HistoriaPregressa getHistoriaPregressa() {
		return historiaPregressa;
	}
	public void setIdOcupacaoHistoriaPregressa(Integer idOcupacaoHistoriaPregressa) {
		this.idOcupacaoHistoriaPregressa = idOcupacaoHistoriaPregressa;
	}
	public void setOcupacao(String ocupacao) {
		this.ocupacao = ocupacao;
	}
	public void setInicio(Integer inicio) {
		this.inicio = inicio;
	}
	public void setFim(Integer fim) {
		this.fim = fim;
	}
	public void setHistoriaPregressa(HistoriaPregressa historiaPregressa) {
		this.historiaPregressa = historiaPregressa;
	}
}

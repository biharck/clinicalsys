package br.com.orionx.clinicalsys.system.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;

import br.com.orionx.clinicalsys.bean.Cargo;
import br.com.orionx.clinicalsys.filtros.CargoFiltro;
import br.com.orionx.clinicalsys.service.PapelService;
import br.com.orionx.clinicalsys.util.DatabaseError;
import br.com.orionx.clinicalsys.util.exception.ClinicalSysException;
import br.com.orionx.clinicalsys.util.generics.CrudController;

@Controller(path="/system/pag/Cargo",authorizationModule=CrudAuthorizationModule.class)
public class CargoCrud extends CrudController<CargoFiltro, Cargo, Cargo> {

	private PapelService papelService;
	
	public void setPapelService(PapelService papelService) {
		this.papelService = papelService;
	}

	@Override
	protected void entrada(WebRequestContext request, Cargo form)throws Exception {
		request.setAttribute("listaPapel", papelService.findAll());
		if(form!=null && form.getIdCargo()!=null)
			form.setPapeis(papelService.getListaPapelByCargo(form));
		super.entrada(request, form);
	}
	
	@Override
	protected void salvar(WebRequestContext request, Cargo bean)throws Exception {
		try {
			super.salvar(request, bean);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_nome_cargo")) throw new ClinicalSysException("J� existe um cargo com este nome cadastrado no sistema.");
		}
	}
	@Override
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Administra��o &raquo; <a href=\"/ClinicalSys/system/pag/Cargo\">Cargos</a></p>";
	}
}

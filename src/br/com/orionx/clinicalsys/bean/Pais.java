package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name = "pais")
public class Pais {
	
	private Integer idPais;
	private String nome;
	private String sigla;

	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdPais() {
		return idPais;
	}
	
	@Required
	@MinLength(2)
	@MaxLength(100)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@Required
	@MinLength(2)
	@MaxLength(2)
	public String getSigla() {
		return sigla;
	}
	
	
	//SET
	public void setIdPais(Integer idPais) {
		this.idPais = idPais;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla.toUpperCase();
	}

}

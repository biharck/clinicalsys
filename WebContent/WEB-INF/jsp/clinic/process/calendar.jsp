<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page import="br.com.orionx.clinicalsys.util.ClinicalSysUtil"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"> 
 
<!-- Website Title --> 
<title>ClinicalSys</title>

<!-- Meta data for SEO -->
<meta name="description" content="">
<meta name="keywords" content="">


<!-- Template stylesheet -->
<link id="screenColor" href="${application}/css/blue/screen.css" rel="stylesheet" type="text/css" media="all">
<link id="datepickerColor" href="${application}/css/blue/datepicker.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/tipsy.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/js/visualize/visualize.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/js/jwysiwyg/jquery.wysiwyg.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/js/fancybox/jquery.fancybox-1.3.0.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/tipsy.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/default.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/ui.datepicker.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/ui.theme.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/ui.dialog.css" rel="stylesheet" type="text/css" media="all">
<link rel='stylesheet' type='text/css' href='${application}/css/fullcalendar/fullcalendar.css' />
<link href="${application}/css/ui.dialog.css" rel="stylesheet" type="text/css" media="all">
<link rel='stylesheet' type='text/css' href='${application}/css/fullcalendar/fullcalendar.css' />

<script type='text/javascript' src='${application}/js/ajax.js'></script>

<script type='text/javascript' src='${application}/js/fullcalendar/jquery-1.4.4.min.js'></script>
<script type='text/javascript' src='${application}/js/fullcalendar/jquery-ui-1.8.7.custom.min.js'></script>
<script type='text/javascript' src='${application}/js/fullcalendar/fullcalendar.min.js'></script>

 <link rel="stylesheet" href="/ClinicalSys/css/autocomplete/main.css" type="text/css" />
 <link rel="stylesheet" href="/ClinicalSys/css/autocomplete/jquery.autocomplete.css" type="text/css" />
 <script type="text/javascript" src="/ClinicalSys/js/autocomplete/jquery.bgiframe.min.js"></script>
 <script type="text/javascript" src="/ClinicalSys/js/autocomplete/jquery.autocomplete.js"></script>
 <script type="text/javascript" src="/ClinicalSys/js/functions.js"></script>

<!-- lib do qtip -->
<script type="text/javascript" src="${application}/js/jquery-qtip-1.0.0-rc3140944/jquery.qtip-1.0.0-rc3.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	startCountdown();
	$('#ui-datepicker-div').hide();	
	var dados = "";
	$("#paciente").autocomplete(dados.split(';'), {
		max: 10,
		scroll: true,
		scrollHeight: 300
		
	});
 });
</script>
<script>
 function ajaxGetName(nome){
 	if(nome!='')
     	sendRequest('/ClinicalSys/clinic/pag/Paciente','ACAO=getPacienteAutoComplete&nomeParc='+nome,'POST', ajaxCallbackNome, erroCallbackNome);
 }
 
 function ajaxCallbackNome(data){
	$("#paciente").setOptions({data: data.split(';')});
 }
 
 function erroCallbackNome(request){
	openDialogInformation("Ups. ocorreu um erro",true);
 }
</script>
</head>
<body class="nobg">
	<input type="hidden" name="idColaboradorAgenda" value="${colaboradorAgenda.idColaboradorAgenda}" id="idColaboradorAgenda"/>
	<input type="hidden" name="autocomplete_name" id="autocomplete_id"/>
	<input type="hidden" name="allDayValue" id="allDayValue"/>
	<input type="hidden" name="slotMinutes" id="slotMinutes" value="${slotMinutes}"/>
	<script type='text/javascript'><!--
		
	
	$(document).ready(function() {
		var noDrag = false;
		var date = new Date();
		var $datas = "${datas}";
		
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		
		var fullDays;
		var partialDays;
		
		var calendar = $('#calendar').fullCalendar({
			theme:true,
			dayNames:['Domingo', 'Segunda', 'Ter�a', 'Quarta', 'Quinta', 'Sexta', 'S�bado'],
			dayNamesShort:['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'S�b'],
			columnFormat:{
				month: 'ddd',    // Mon
    			week: 'ddd d/M', // Mon 9/7
    			day: 'dddd d/M' 
			},
			dontDisplay:'08:00,09:00',
			minTime: '${minHourDay}',
			maxTime:'${maxHourDay}',
			droppable: true,
			monthNames:['Janeiro', 'Fevereiro', 'Mar�o', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
			monthNamesShort:['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
			buttonText:{
			    prev:     '&nbsp;&#9668;&nbsp;',  // left triangle
			    next:     '&nbsp;&#9658;&nbsp;',  // right triangle
			    prevYear: '&nbsp;&lt;&lt;&nbsp;', // <<
			    nextYear: '&nbsp;&gt;&gt;&nbsp;', // >>
			    today:    'Hoje',
			    month:    'M�s',
			    week:     'Semana',
			    day:      'dia'
			},
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			titleFormat:{
			    month: 'MMMM yyyy', 
			    week: "'Semana ' d[ yyyy]{ '�'[ MMM] d ',' MMM 'de' yyyy}  ",
			    day: 'dddd, d MMM yyyy'            
			},
			timeFormat:{
			    // for all other views
			    '': 'H:mm{ - H:mm}'            // 7p
			},
			eventDragStart : function( event, jsEvent, ui, view ) {
				
				var situacao = event.situacaoDESC;
				if(situacao=='Cancelado'){
					openDialogInformation('Este agendamento de consulta n�o pode ser alterado pois foi cancelado.',true);
					noDrag = true;
					return false;
				}
				if(situacao=='Atendido'){
					openDialogInformation('Este agendamento de consulta n�o pode ser alterado pois o paciente j� foi atendido.',true);
					noDrag = true;
					return false;
				}
				if(situacao=='N�o Compareceu'){
					openDialogInformation('Este agendamento de consulta n�o pode ser alterado pois o paciente n�o compareceu no dia e hora marcados para a consulta.',true);
					noDrag = true;
					return false;
				}
				if(situacao=='Aguardando Atendimento'){
					openDialogInformation('Este agendamento de consulta n�o pode ser alterado pois o paciente est� aqui na cl�nica aguardando para ser atendido.',true);
					noDrag = true;
					return false;
				}
				
				var now = new Date();
				var notNow = new Date(event.start);
				notNow.setHours(0,0,0,0);
				now.setHours(0,0,0,0);
				
				if(notNow < now){
					openDialogInformation('N�o � poss�vel mover o compromisso para este dia, pois a data j� passou.',true);
					noDrag = true;
					return false;
				}
			},
			eventDrop : function( event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view ) { 
				var startDate = new Date(event.start);
				
				if(noDrag){
					revertFunc();
					noDrag = false;
					return false;
				}
				//verificar se o hor�rio pode ser agendan
				
				var now = new Date();
				var diff = startDate.getTime() - now.getTime();
				var num_days = (((diff / 1000) / 60) / 60) / 24;
				console.log(num_days)
				if(num_days < -0.1 || noDrag){
					openDialogInformation('N�o � poss�vel mover o compromisso para este dia, pois a data j� passou.',true);
					revertFunc();
					noDrag = false;
					return false;
				}
				var dayOfWeekStringStart = ''+event.start;
				dayOfWeekStringStart = dayOfWeekStringStart.substring(0,3);
				dayOfWeekStringStart = dayOfWeekStringStart.toLowerCase();
				
				var dayOfWeekStringEnd = ''+event.end;
				dayOfWeekStringEnd = dayOfWeekStringEnd.substring(0,3);
				dayOfWeekStringEnd = dayOfWeekStringEnd.toLowerCase();
				
				if($('#calendar td[thisday='+dayOfWeekStringStart+']').attr('dayoff')){
					openDialogInformation('N�o � poss�vel mover a data desta atividade, pois o colaborador n�o estar� presente na cl�nica.',true)
					revertFunc();
					return false;
				}
				if($('#calendar td[thisday='+dayOfWeekStringEnd+']').attr('dayoff')){
					openDialogInformation('N�o � poss�vel mover a data desta atividade, pois o colaborador n�o estar� presente na cl�nica no dia de t�rmino da atividade.',true)
					revertFunc();
					return false;
				}
				var dataFormatada = formatDate(event.start,'dd/mm/yyyy HH:nn:ss');
				
				$('#startDate').val(dataFormatada);
				$('#observacoes').val(event.observacoes);
				$('#allDayValue').val(allDay);
				saveAgenda(event.id);
			},
			selectable: true,
			selectHelper: true,
			disableResizing:true,
			allDayText: 'Dia Todo',
			select: function(start, end, allDay) {
				
				var startFormatado = formatDate(start,'HH:nn');
				
				$('#paciente').removeAttr('disabled');
				$('#paciente').css('border','1px solid');
				$('#paciente').css('border-color','silver');
				$('#paciente').css('background-color','#fff');
				
				$('#newAgendaConvenio').show();
				$('#eidtAgendaConvenio').hide();
				
				$('#startDate').css('border','none');
				$('#startDate').css('background-color','#FFF5D3');

				clearForm();
				var startDate = new Date(start);
				var now = new Date();
				var diff = startDate.getTime() - now.getTime();
				var num_days = (((diff / 1000) / 60) / 60) / 24;
				if(num_days < -1){
					openDialogInformation('N�o � poss�vel agendar neste dia, pois a data j� passou.',true);
					return false;
				}
				var dayOfWeekStringStart = ''+start;
				$('#startDate').val(formatDate(startDate,'dd/mm/yyyy'));
				dayOfWeekStringStart = dayOfWeekStringStart.substring(0,3);
				dayOfWeekStringStart = dayOfWeekStringStart.toLowerCase();
				
				var dayOfWeekStringEnd = ''+end;
				dayOfWeekStringEnd = dayOfWeekStringEnd.substring(0,3);
				dayOfWeekStringEnd = dayOfWeekStringEnd.toLowerCase();
				
				if($('#calendar td[thisday='+dayOfWeekStringStart+']').attr('dayoff')){
					openDialogInformation('N�o � poss�vel inserir dados neste dia, pois o colaborador n�o estar� presente na cl�nica.',true);
				}
				if($('#calendar td[thisday='+dayOfWeekStringEnd+']').attr('dayoff')){
					openDialogInformation('N�o � poss�vel inserir dados neste dia, pois o colaborador n�o estar� presente na cl�nica.',true);
				}
				else{
					$('#allDayValue').val(allDay);
					openDialogCalendarInformation();
				}
				
				calendar.fullCalendar('unselect');
			},
			editable: true,
			slotMinutes:${slotMinutes},
			events: [
			         ${datas}
			        ],
			
			eventRender: function(event, element) {
		        element.qtip({
		            content: '<p><h3>'+event.title.split(' -- ')[0]+'</h3></p><br />'+
		            		 '<p>'+formatDate(event.start,'dd/mm/yyyy HH:nn')+' </p><br />'+
		            		 '<img src=\"'+event.situacaoURL+'">'+event.situacaoDESC+'<br /><br />'+
		            		 '<p><h4>'+event.email+' </h4></p><br />'+
				             '<p><font style=color:#505050;><i>'+event.observacoes+'</i></font></p><br />'+
  				             '<a href="javascript:updateEventById('+event.id+')">Editar</a>', // Give it some content
		            		 
		            position: {
		                  corner: {
		                     tooltip: 'bottomMiddle', // Use the corner...
		                     target: 'topMiddle' // ...and opposite corner
		                  }
		               },
			         hide: {
			            fixed: true // Make it fixed so it can be hovered over
			         },
			         style: {
			            name: 'green', // And style it with the preset dark theme
		            	width: { 
		            		 min:300,
		            		 max: 400 
		            	 }
			         }
		        });
		    }
			
		});

		     
	      //inicio das regras de colora��o dos dias do m�s
	     fullDays = '${fullDays}';
	     partialDays = '${partialDays}';
	     
	     var arrayFullDays = fullDays.split(',');
	     
	     if(fullDays!= '' && fullDays != null && fullDays != '<null>')
		     for ( var i = 0; i < arrayFullDays.length; i++) {
		    	 $('#calendar td[thisday='+arrayFullDays[i]+']').css('background-color','#E3909E');
		    	 $('#calendar td[thisday='+arrayFullDays[i]+']').attr('dayoff','true');

		    	 eval("$('.fc-button-agendaWeek').click(function(){$('.fc-agenda-bg td[thisday="+arrayFullDays[i]+"]').css('background-color','#E3909E'); });");
			 }
			
		});
	
	function openDialogCalendarInformation(){
		preLoadComboHora();
		return false;
	}
	
	
	function openDialogCalendarEdit(id){
		$("#add-event-form").dialog({
			closeText: 'hide',
			closeOnEscape: false,
			resizable: false,
			width: 600,
			modal: true,		
			open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();$('.ui-dialog').css('left','25%');},
			buttons:{
				'Salvar': function(){
					if(verificaHorarios())
						saveAgenda(id);
				},
				Fechar: function(){
					$(this).dialog('destroy');
				}
			}			
		});
		return false;
	}
	
	function openDialogCalendarAfterAjax(){
		$("#add-event-form").dialog({
			closeText: 'hide',
			closeOnEscape: false,
			resizable: false,
			width: 600,
			modal: true,		
			open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();$('.ui-dialog').css('left','25%');},
			buttons:{
				Agendar: function(){
					if(verificaHorarios()){
						if($('#emailNovo').val()!=''){
							if(validaEmailFormulario($('#emailNovo').val()))
								saveAgenda();
						}else{
							saveAgenda();
						}
					}
				},
				Fechar: function(){
					$(this).dialog('destroy');
				},
				'Bloquear dia': function(){
					$(this).dialog('destroy');
				}
			}			
		});
		return false;
	}
	
	function preLoadComboHora(param,id,horaInicio,horaFim){
		$.getJSON('/ClinicalSys/clinic/process/Calendar?ACAO=preLoadComboHora',{'shv':$('#idColaboradorAgenda').val(),'dia':$('#startDate').val()},
	  	function(data){
	  		if(data.erro){
	  			closeDialogAguarde();
	  			openDialogInformation('Houve problemas ao carregas os hor�rios dispon�veis do colaborador. Tente novamente e qualquer eventual d�vida entre em contato com nosso suporte');
	  		}else{
				for(var i=0;i<data.length;i++){
	  		        var obj = data[i];
	  		        for(var key in obj){
	  		            eval(obj[key]);
	  		        }
	  		    }
	  		    if(param!=null && param!=''){
	  		    	openDialogCalendarEdit(id);
	  		    	$('select[name=startHour]').append('<option value='+horaInicio+'>'+horaInicio+'</option>');
	  		    	$('#startHour').val(horaInicio);
	  		    	$('#endHour').val(horaFim);	  				
	  		    }
	  		    else
	  		    	openDialogCalendarAfterAjax();
			}
		});
	}
	function openDialogInformation(msg,error,data){
		var msgFormated = '<p>'+ msg + '</p>';
		if(error){
			msgFormated = "<div class='p_dialog p_dialog_error'>"+
								"<p style='background-color: #FCC;'>"+
									msg +
								"</p>"+
						  "</div>";
		}
		
		$("#dialog-information").html(msgFormated);
		$("#dialog-information").dialog({
			closeText: 'hide',
			closeOnEscape: false,
			resizable: false,
			width: 600,
			modal: true,		
			open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();$('.ui-dialog').css('left','25%');},
			buttons:{
				Fechar: function(){
					if(data != undefined && data.revert){
						openDialogAguarde();
						location.href = location.href;
					}
					$(this).dialog('destroy');
				}
				
			}
		});
		return false;
	}
	
	
	function saveAgenda(param){
		if(param==null || param== ''){
			if(!verificaObrigatoriedade())
				return false;
		}
		//$.ajaxSetup({ scriptCharset: "ISO-8859-1" , contentType: "application/json; charset=ISO-8859-1"});
		$.ajaxSetup({ scriptCharset: "utf-8" , contentType: "application/json; charset=utf-8"});
		$.getJSON('/ClinicalSys/clinic/process/Calendar?ACAO=saveAgenda',
				{
					'operadora':$('#idOperadora').val(),
					'plano':$('#idPlano').val(),
					'startDate':$('#startDate').val(),
					'startHour':$('#startHour').val(),
					'observacoes':$('#observacoes').val(),
					'paciente': $('#paciente').val(),
					'shv':$('#idColaboradorAgenda').val(),
					'autocomplete_id':$('#autocomplete_id').val(),
					'ID':param,
					'endHour':$('#endHour').val(),
					'dtNascimentoNovo':$('#dtNascimentoNovo').val(),
					'cpfNovo':$('#cpfNovo').val(),
					'telFixoNovo':$('#telFixoNovo').val(),
					'celularNovo':$('#celularNovo').val(),
					'allDay':$('#allDayValue').val(),
					'emailNovo':$('#emailNovo').val(),
					'situacao':$('#situacoesAjax').val()
				},
				
			  	function(data){
			  		$("#add-event-form").dialog('destroy');
					if(data.erro){
			  			openDialogInformation(data.msg,true,data);
			  			
			  		}else{
			  			 
			  			 if(param==null || param== ''){
			  				 openDialogAguarde();
				  			 location.href=location.href;
						}else{
							event_obj = $('#calendar').fullCalendar('clientEvents', param);
							
							var diaNovo  = $('#startDate').val();
							var hora = $('#startHour').val();

							if(hora!=null && hora!='' && hora !='null'){
								dias = diaNovo.split('/');
								horas = hora.split(':');
								event_obj[0].start = new Date(parseInt(dias[2],10),(parseInt(dias[1],10)-1),parseInt(dias[0],10),parseInt(horas[0],10),parseInt(horas[1],10),0);
								event_obj[0].startHour   = $('#startHour').val();
								event_obj[0].endHour     = $('#endHour').val();
							}
							event_obj[0].observacoes = $('#observacoes').val();
							$('#calendar').fullCalendar('updateEvent', event_obj[0]);
							$('#calendar').fullCalendar('unselect');
			  				 openDialogAguarde();
				  			 location.href=location.href;

						}
			  		}
				});
	}
	
	function verificaObrigatoriedade(){
		if($('#idOperadora').val()=='' || $('#idOperadora').val()=='<null>'){
			openDialogInformation('Voc� deve selecionar um Conv�nio.');
			return false;
		}
		if($('#idPlano').val()=='' || $('#idPlano').val()=='<null>'){
			openDialogInformation('Voc� deve selecionar um Plano.');
			return false;
		}
		if($('#startDate').val()==''){
			openDialogInformation('Voc� deve selecionar a data da consulta.');
			return false;
		}
		if($('#startHour').val()=='' || $('#startHour').val()=='<null>'){
			openDialogInformation('Voc� deve selecionar a hora da consulta.');
			return false;
		}
		if($('#paciente').val()==''){
			openDialogInformation('Voc� deve selecionar um Paciente.');
			return false;
		}
		if($('#observacoes').val()==''){
			openDialogInformation('Voc� deve descrever uma breve descri��o da queixa do paciente.');
			return false;
		}
		
		//se for novo paciente
		if($('#autocomplete_id').val()==''){
			if($('#dtNascimentoNovo').val()==''){
				openDialogInformation('Voc� deve inserir uma data de nascimento para este novo paciente.');
				return false;
			}	
			if($('#cpfNovo').val()==''){
				openDialogInformation('Voc� deve inserir um CPF para este novo paciente.');
				return false;
			}	
			if($('#telFixoNovo').val()=='' && $('#celularNovo').val()==''){
				openDialogInformation('Voc� deve inserir pelo menos um telefone para este novo paciente.');
				return false;
			}		
		}
		return true;
		
	}
	
	function updateEventById(param){
		event_obj = $('#calendar').fullCalendar('clientEvents', param);
		buscaDadosConsultaAjax(event_obj[0].id);
		/*
		event_obj[0].title = 'abacate';
		$('#calendar').fullCalendar('updateEvent', event_obj[0]);
		$('#calendar').fullCalendar('unselect');
		*/
	}
	
	function clearForm(){
		$('#novoPac').hide();
		
		$('#idOperadora').val('<null>');
		$('#idPlano').val('<null>');
		$('#startHour').val('<null>');
		$('#endHour').val('');
		$('#paciente').val('');
		$('#observacoes').val('');
		$('#dtNascimentoNovo').val('');
		$('#cpfNovo').val('');
		$('#telFixoNovo').val('');
		$('#celularNovo').val('');
	}

	function buscaDadosConsultaAjax(id){
		$.getJSON('/ClinicalSys/clinic/process/Calendar?ACAO=getConsulta',{'ID':id},
			  	function(data){
			  		if(data.erro){
			  			openDialogInformation('Ops! Ocorreu um erro ao buscar os dados da consulta. Tente novamente por favor!',true);
			  		}else{
			  			fillForm(data);
					}
				});	
	}
	
	function fillForm(dataObj){
		clearForm();
		$('#newAgendaConvenio').hide();
		$('#eidtAgendaConvenio').show();
		$('#conveioEdit').html(dataObj.operadora);
		$('#planoEdit').html(dataObj.plano);
		$('#startDate').val(dataObj.dia);
		
		console.log(dataObj);
		$('#paciente').val(dataObj.paciente);
		$('#paciente').attr('disabled','disabled');
		$('#paciente').css('border','none');
		$('#paciente').css('background-color','#FFF5D3');

		$('#endHour').val(dataObj.paciente);
		$('#endHour').attr('disabled','disabled');
		$('#endHour').css('border','none');
		$('#endHour').css('background-color','#FFF5D3');
		
		$('#startDate').attr('disabled','disabled');
		$('#startDate').css('border','none');
		$('#startDate').css('background-color','#FFF5D3');
		$('#situacoesAjax').val(''+dataObj.situacao);
		if(dataObj.situacao=='CANCELADO'){
			openDialogInformation('Este agendamento de consulta n�o pode ser alterado pois foi cancelado.',true);
			return false;
		}
		if(dataObj.situacao=='ATENDIDO'){
			openDialogInformation('Este agendamento de consulta n�o pode ser alterado pois o paciente j� foi atendido.',true);
			return false;
		}
		if(dataObj.situacao=='NAO_COMPARECEU'){
			openDialogInformation('Este agendamento de consulta n�o pode ser alterado pois o paciente n�o compareceu no dia e hora marcados para a consulta.',true);
			return false;
		}
		if(dataObj.situacao=='AGUARDANDO_ATENDIMENTO'){
			openDialogInformation('Este agendamento de consulta n�o pode ser alterado pois o paciente est� aqui na cl�nica aguardando para ser atendido.',true);
			return false;
		}
		
		$('#observacoes').val(dataObj.observacoes);
		$('#observacoes').focus();
		preLoadComboHora(true,dataObj.ID,dataObj.horaInicio,dataObj.horaFim);
	}
	
	function getDateFormatCalendar(data, hora){
		
		
		ano    = parseInt(data.substring(6,10),10);  
		mes    = parseInt(data.substring(3,5),10);
		dia    = parseInt(data.substring(0,2),10);
		hora   = parseInt(hora,10);
		
		var d = new Date(ano,mes,dia,hora);
		return d;		
	}
	
	
	var formatDate = function (formatDate, formatString) {
		if(formatDate instanceof Date) {
			var months = new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
			var yyyy = formatDate.getFullYear();
			var yy = yyyy.toString().substring(2);
			var m = formatDate.getMonth()+1;
			var mm = m < 10 ? "0" + m : m;
			var mmm = months[m];
			var d = formatDate.getDate();
			var dd = d < 10 ? "0" + d : d;
			
			var h = formatDate.getHours();
			var hh = h < 10 ? "0" + h : h;
			var n = formatDate.getMinutes();
			var nn = n < 10 ? "0" + n : n;
			var s = formatDate.getSeconds();
			var ss = s < 10 ? "0" + s : s;
	
			formatString = formatString.replace(/yyyy/i, yyyy);
			formatString = formatString.replace(/yy/i, yy);
			formatString = formatString.replace(/mmm/i, mmm);
			formatString = formatString.replace(/mm/i, mm);
			formatString = formatString.replace(/m/i, m);
			formatString = formatString.replace(/dd/i, dd);
			formatString = formatString.replace(/d/i, d);
			formatString = formatString.replace(/hh/i, hh);
			formatString = formatString.replace(/h/i, h);
			formatString = formatString.replace(/nn/i, nn);
			formatString = formatString.replace(/n/i, n);
			formatString = formatString.replace(/ss/i, ss);
			formatString = formatString.replace(/s/i, s);
	
			return formatString;
		} else {
			return "";
		}
	}
	function verificaHorarios(){
		var start = $('#startHour').val();
		var end   = $('#endHour').val();
		var horaInicio     = parseInt(start.split(':')[0],10); 
		var minutoInicio   = parseInt(start.split(':')[1],10);
		var horaTermino    = parseInt(end.split(':')[0],10);
		var minutoTermino  = parseInt(end.split(':')[1],10);
		
		if((horaTermino < horaInicio)||(horaTermino == horaInicio && minutoTermino < minutoInicio)||(horaTermino == horaInicio && minutoTermino == minutoInicio)){
			openDialogInformation('A hora final deve ser maior que a hora de in�nio da consulta.',true);
			return false;
		}
		return true;
	}
	--></script>
	<style type='text/css'>
	
		
		#calendar {
			width: 900px;
			margin: 0 auto;
		}
		.holiday,
		.fc-agenda .holiday .fc-event-time,
		.holiday a {
		    background-color: green; /* background color */
		    border-color: green;     /* border color */
		    color: #fff;           /* text color */
	    }
	
	</style>
<a name="topo"></a>
<div class="content_wrapper">

	<!-- Begin header -->
	<div id="header">
		<div id="logo">
			<img src="${application}/images/logo.png" alt="logo"/>
		</div>
		
		<div id="account_info">
			<img src="${application}/images/icon_online.png" alt="Online" class="mid_align"/>
			Ol� <a href="">Dr. <%= ClinicalSysUtil.getUsuarioLogado().getLogin() %></a>
			<c:if test="${param.fromInsertOne != 'true'}">
				<c:if test="${param.onePath != 'true'}">
			 		(<a href="">1 nova menssagem</a>) | <a href="${application}/system/Logout">Logout</a>
			 	</c:if>
			</c:if>
			&nbsp;&nbsp;&nbsp;&nbsp;Sua sess�o expira em: <span id="cronometro_tempo"></span>
		</div>
		
	</div>
	<!-- End header -->
	<br/>
	<i id="pathURL">${pathURL}</i>
	<br/>
	<div id='calendar'></div>	

		<!-- Begin footer -->
		<div id="footer">
			 Copyright&copy; by OrionX Technologies
		</div>
		<!-- End footer -->
		
	</div>
	<!-- End content -->
<!-- ui-dialog -->
<!-- Include das menssagens de alerta -->
<jsp:include page="../../menssages/dialogInformation.jsp"></jsp:include>
<jsp:include page="../../menssages/dialogCalendar.jsp"></jsp:include>
		
</body>
</html>
package br.com.orionx.clinicalsys.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

import br.com.orionx.clinicalsys.util.log.Log;

@Entity
@Table(name = "especializacao")
public class Especializacao implements Log{

	private Integer idEspecializacao;
	private String nome;
	private String descricao;
	private boolean ativo;
	
	//Log
	private Integer idUsuarioAltera;
	private Timestamp dtAltera;
	
	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdEspecializacao() {
		return idEspecializacao;
	}
	@Required
	@MaxLength(100)
	@MinLength(5)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	@Required
	@MaxLength(100)
	@MinLength(5)
	public String getDescricao() {
		return descricao;
	}
	public boolean isAtivo() {
		return ativo;
	}
	
	//SET
	public void setIdEspecializacao(Integer idEspecializacao) {
		this.idEspecializacao = idEspecializacao;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	
	//LOG
	@Override
	public void setIdUsuarioAltera(Integer idUsuarioAltera) {
		this.idUsuarioAltera = idUsuarioAltera;
	}

	@Override
	public void setDtAltera(Timestamp dtAltera) {
		this.dtAltera = dtAltera;
	}

	@Override
	public Integer getIdUsuarioAltera() {
		return idUsuarioAltera;
	}

	@Override
	public Timestamp getDtAltera() {
		return dtAltera;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Especializacao) {
			Especializacao esp = (Especializacao) obj;
			return this.getIdEspecializacao().equals(esp.getIdEspecializacao());
		}
		return super.equals(obj);
	}
	
	
}

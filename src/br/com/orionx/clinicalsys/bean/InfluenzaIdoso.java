package br.com.orionx.clinicalsys.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="influenzaidoso")
public class InfluenzaIdoso {

	private Integer idInfluenzaIdoso;
	private Date data;
	private ImunizacoesIdosoPaciente imunizacoesIdosoPaciente;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdInfluenzaIdoso() {
		return idInfluenzaIdoso;
	}
	@Required
	public Date getData() {
		return data;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idImunizacoesIdosoPaciente")
	@ForeignKey(name="fk_imuniz_flu")
	public ImunizacoesIdosoPaciente getImunizacoesIdosoPaciente() {
		return imunizacoesIdosoPaciente;
	}
	public void setIdInfluenzaIdoso(Integer idInfluenzaIdoso) {
		this.idInfluenzaIdoso = idInfluenzaIdoso;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public void setImunizacoesIdosoPaciente(ImunizacoesIdosoPaciente imunizacoesIdosoPaciente) {
		this.imunizacoesIdosoPaciente = imunizacoesIdosoPaciente;
	}
}

package br.com.orionx.clinicalsys.bean;

public enum EnumTipoParto {

	NORMAL(Boolean.TRUE, "Normal"),
	FORCEPS(Boolean.FALSE,"F�rceps"),
	CESAREA(null,"Ces�rea");
	
	private Boolean opcao;
	private String descricao;
	
	private EnumTipoParto(Boolean opcao, String descricao){
		this.opcao = opcao;
		this.descricao = descricao;
	}
	
	public Boolean getOpcao() {
		return opcao;
	}
	public String getDescricao() {
		return descricao;
	}
	@Override
	public String toString() {
		return getDescricao();
	}
}

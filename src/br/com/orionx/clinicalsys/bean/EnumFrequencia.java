package br.com.orionx.clinicalsys.bean;

public enum EnumFrequencia {

	DIA(1,"Dia"),
	SEMANA(2,"Semana"),
	MES(3,"M�s");
	
	private Integer id;
	private String descricao;
	
	private EnumFrequencia(Integer id, String descricao) {
		this.id = id;
		this.descricao = descricao;
	}
	
	public Integer getId() {
		return id;
	}
	public String getDescricao() {
		return descricao;
	}
	@Override
	public String toString() {
		return getDescricao();
	}
}

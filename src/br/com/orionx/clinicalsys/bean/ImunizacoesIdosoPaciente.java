package br.com.orionx.clinicalsys.bean;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "imunizacoesidosopaciente")
public class ImunizacoesIdosoPaciente {
	
	private Integer idImunizacoesIdosoPaciente;
	private boolean hepatiteBDose1;
	private boolean hepatiteBDose2;
	private boolean hepatiteBDose3;
	private Date dT;
	private Date febreAmarela;
	private boolean tripliceViral;
	private boolean pN23;
	private List<InfluenzaIdoso> influenzas;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdImunizacoesIdosoPaciente() {
		return idImunizacoesIdosoPaciente;
	}
	public boolean isHepatiteBDose1() {
		return hepatiteBDose1;
	}
	public boolean isHepatiteBDose2() {
		return hepatiteBDose2;
	}
	public boolean isHepatiteBDose3() {
		return hepatiteBDose3;
	}
	@Column(name="dt")
	public Date getDT() {
		return dT;
	}
	public Date getFebreAmarela() {
		return febreAmarela;
	}
	public boolean isTripliceViral() {
		return tripliceViral;
	}
	public boolean isPN23() {
		return pN23;
	}
	@OneToMany(mappedBy="imunizacoesIdosoPaciente")
	public List<InfluenzaIdoso> getInfluenzas() {
		return influenzas;
	}
	
	public void setIdImunizacoesIdosoPaciente(Integer idImunizacoesIdosoPaciente) {
		this.idImunizacoesIdosoPaciente = idImunizacoesIdosoPaciente;
	}
	public void setHepatiteBDose1(boolean hepatiteBDose1) {
		this.hepatiteBDose1 = hepatiteBDose1;
	}
	public void setHepatiteBDose2(boolean hepatiteBDose2) {
		this.hepatiteBDose2 = hepatiteBDose2;
	}
	public void setHepatiteBDose3(boolean hepatiteBDose3) {
		this.hepatiteBDose3 = hepatiteBDose3;
	}
	public void setDT(Date dt) {
		dT = dt;
	}
	public void setFebreAmarela(Date febreAmarela) {
		this.febreAmarela = febreAmarela;
	}
	public void setTripliceViral(boolean tripliceViral) {
		this.tripliceViral = tripliceViral;
	}
	public void setPN23(boolean pn23) {
		pN23 = pn23;
	}
	public void setInfluenzas(List<InfluenzaIdoso> influenzas) {
		this.influenzas = influenzas;
	}
}

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@page import="br.com.orionx.clinicalsys.util.ClinicalSysUtil"%>

	
	
<div id="add-event-form" style="display:none;" title="Marca��o de Consulta">
	<form>
	<fieldset>
		<label for="name">Paciente</label>
		<input type="text" id="paciente" onkeyUp="ajaxGetName(this.value)" onChange="verificaExistencia()" name="what" class="text ui-widget-content ui-corner-all" style="margin-bottom:12px; width:95%; padding: .4em;" />
		<table id="novoPac" style='display:none;background-color:#FFC;'>
			<tr>
				<td>
					<label>Nascimento</label><br />
					<input name="dtNascimentoNovo" id="dtNascimentoNovo" value="" class="text ui-widget-content ui-corner-all" type="text" OnKeyUp="mascara_data_press($(this))" maxlength="10" datepickerrangedate="true" style="width:100px;margin-bottom:12px;padding: .4em;" /><br />
				</td>
				<td>
					<label for="cpfNovo">CPF</label><br />
					<input type="text" id="cpfNovo" class="text ui-widget-content ui-corner-all" style="margin-bottom:12px; width:120px; padding: .4em;" maxlength="14" onKeydown="mascara_cpf(this)" onkeypress="return valida_tecla(this, event)" onchange="existeCPF(this.value)"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="telFixoNovo">Telefone Fixo</label><br />
					<input type="text" id="telFixoNovo" class="text ui-widget-content ui-corner-all" style="margin-bottom:12px; width:120px; padding: .4em;" maxlength="14" value="" onkeydown="mascara_telefone(this)" onkeypress="return valida_tecla(this, event)"/>
				</td>
				<td>
					<label for="celularNovo">Celular</label><br />
					<input type="text" id="celularNovo" class="text ui-widget-content ui-corner-all" style="margin-bottom:12px; width:120px; padding: .4em;" maxlength="14" value="" onkeydown="mascara_telefone(this)" onkeypress="return valida_tecla(this, event)"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label for="emailNovo">E-Mail</label>
					<input type="text" id="emailNovo" class="text ui-widget-content ui-corner-all" style="margin-bottom:12px; width:95%; padding: .4em;"  value="" onchange="validaEmailFormulario(this.value)"/>
				</td>
			</tr>			
		</table>
		<div id="newAgendaConvenio">
			<n:panel>
				<b>Conv�nio</b><br/>
			</n:panel>
			<n:panel>
				<n:input name="operadoraAjax" id="idOperadora" value="${operadoras}" itens="${operadoras}"  type="SELECT-ONE"  label="Conv�nio" onchange="buscaPlanosDialog(this.value)" />
			</n:panel>
			<br/>
			<n:panel>
				<b>Plano</b><br/>
			</n:panel>
			<n:panel>
				<n:input name="planosAjax" id="idPlano" value="${plano}" itens="${planos}"  type="SELECT-ONE"  label="Pl�nos" />
			</n:panel>
		</div>
		<div id="eidtAgendaConvenio">
			<n:panel>
				<b>Conv�nio:&nbsp;</b><span id="conveioEdit"></span>
			</n:panel>
			<br/>
			<n:panel>
				<b>Plano:&nbsp;</b><span id="planoEdit"></span>
			</n:panel>
			<br />
			<n:panel>
				<b>Situa��o:&nbsp;</b><n:input name="situacoesAjax" id="situacoesAjax" itens="${situacoes}"  type="SELECT-ONE"  label="Situa��es" />
			</n:panel>
		</div>								
		<table style="width:100%; padding:5px;">
			<tr>
				<td>
					<label>Dia</label>
					<input name="startDate" id="startDate" value="" class="text ui-widget-content ui-corner-all" type="text" OnKeyUp="mascara_data_press($(this))" maxlength="10" datepickerrangedate="true" style="margin-bottom:12px;padding: .4em;width:120px;" disabled="disabled"/>				
				</td>
				<td>&nbsp;</td>
				<td>
					<label>Hora</label>
					<select id="startHour" name="startHour" class="text ui-widget-content ui-corner-all" style="margin-bottom:12px; width:50%; padding: .4em;" onchange="calculaFim(this.value)">
					</select>				
				</td>
				<td>
					<label>Fim</label>
					<input type="text" id="endHour" name="endHour" class="text ui-widget-content ui-corner-all" style="margin-bottom:12px; width:50%; padding: .4em;border:none;background-color:#FFF5D3;" disabled="disabled">
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<label>Queixa</label>
					<textarea maxlenght="400" id="observacoes" rows="4" style="width:95%;"></textarea>
				</td>				
			</tr>			
		</table>
	</fieldset>
	</form>
</div>

<script type="text/javascript">
	$("#startDate").datepicker({
		nextText: '&raquo;',
		prevText: '&laquo;',
		changeMonth: true,
		changeYear: true,
		minDate: "-0D",
		yearRange: '1900:2050', 
		showAnim: 'fadeIn',
		dateFormat: 'dd/mm/yy'
	});
	function mascara_data_press(data){
    var mydata = ''; 
    mydata = mydata + data.val(); 
    if (mydata.length == 2){ 
        mydata = mydata + '/'; 
        data.val(mydata); 
    } 
    if (mydata.length == 5){ 
        mydata = mydata + '/'; 
        data.val(mydata); 
    } 
}
function valida_tecla(campo, event, acceptEnter) {
	
	var BACKSPACE = 8;
	var TABFIREFOX = 0;
	var key;
	var tecla;
	CheckTAB=true;
	if(navigator.appName.indexOf("Netscape")!= -1) {
		tecla = event.which;
	}
	else {
		tecla = event.keyCode;
	}
	key = String.fromCharCode(tecla);
	if (tecla == 13) {
		if(acceptEnter){
			return true;
		} else {
			return false;
		}
	}
	if (tecla == BACKSPACE || tecla == TABFIREFOX) {
		return true;
	}
	return (isNum(key));
}
function isNum( caractere ) { 
	var strValidos = '0123456789'; 
	if (strValidos.indexOf(caractere) == -1) {
		return false; 
	}
	return true; 
}

function mascara_telefone(el) {
	var mydata = '';
	mydata = mydata + el.value;
	if (mydata.length == 1) {
		mydata = '(' + mydata;
		el.value = mydata;
	}
	if (mydata.length == 3) {
		mydata = mydata+') ';
		el.value = mydata;
	}
	if (mydata.length == 9) {
		mydata =  mydata + '-';
		el.value = mydata;
	}
}

function verifica_data () { 

    dia = (document.forms[0].data.value.substring(0,2)); 
    mes = (document.forms[0].data.value.substring(3,5)); 
    ano = (document.forms[0].data.value.substring(6,10)); 

    situacao = ""; 
    // verifica o dia valido para cada mes 
    if ((dia < 01)||(dia < 01 || dia > 30) && (  mes == 04 || mes == 06 || mes == 09 || mes == 11 ) || dia > 31) { 
        situacao = "falsa"; 
    } 

    // verifica se o mes e valido 
    if (mes < 01 || mes > 12 ) { 
        situacao = "falsa"; 
    } 

    // verifica se e ano bissexto 
    if (mes == 2 && ( dia < 01 || dia > 29 || ( dia > 28 && (parseInt(ano / 4) != ano / 4)))) { 
        situacao = "falsa"; 
    } 

    if (document.forms[0].data.value == "") { 
        situacao = "falsa"; 
    } 

    if (situacao == "falsa") { 
        alert("Data inv�lida!"); 
        document.forms[0].data.focus(); 
    } 
 } 

function buscaPlanosDialog(operadora){
	if(operadora == '' || operadora == '<null>') {
		$('select[name=planosAjax]').empty();
		return false;
	}
	$.getJSON('/ClinicalSys/clinic/pag/Paciente?ACAO=buscaPlanos',{'operadora':operadora},
		  	function(data){
				$('select[name=planosAjax]').empty();
		  		if(data.erro){
		  			openDialogInformation('Ops! Ocorreu um erro ao buscar os planos. Tente novamente por favor!');
		  		}else{
		  			for(var i=0;i<data.length;i++){
		  		        var obj = data[i];
		  		        for(var key in obj){
		  		            eval(obj[key]);
		  		        }
		  		    }
				}
			});	
}

$(document).ready(function(){
	$('select[name=planosAjax]').empty();
	$('select[name=operaodraAjax]').val(null);
	$("select#idOperadora").val('<null>');
});

function verificaExistencia(){
	if($('#autocomplete_id').val()==''){ 
		$('#novoPac').fadeIn();
		$('#dtNascimentoNovo').focus();
	}
	else
		$('#novoPac').fadeOut();
}
$("#dtNascimentoNovo").datepicker({
	nextText: '&raquo;',
	prevText: '&laquo;',
	changeMonth: true,
	changeYear: true,
	maxDate: "+0D",
	yearRange: '1900:2050', 
	showAnim: 'fadeIn'
});

function mascara_cpf(el) {
	var mydata = '';
	mydata = mydata + el.value;
	if (mydata.length == 3) {
		mydata = mydata + '.';
		el.value = mydata;
	}
	if (mydata.length == 7) {
		mydata = mydata + '.';
		el.value = mydata;
	}
	if (mydata.length == 11) {
		mydata = mydata + '-';
		el.value = mydata;
	}
}

function validaEmailFormulario(param){
	if(!checkEmail(param)){
		openDialogInformation('Este n�o � um e-mail v�lido! Por favor, verifique o enderece�o, pois ele � de extrema import�ncia para podermos nos comunicar.',true);
		return false;
	}else return true;
}

/**
 * Reference: Sandeep V. Tamhankar (stamhankar@hotmail.com),
 * http://javascript.internet.com
 */
function checkEmail(emailStr) {
   if (emailStr.length == 0) {
       return true;
   }
   var emailPat=/^(.+)@(.+)$/;
   var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]";
   var validChars="\[^\\s" + specialChars + "\]";
   var quotedUser="(\"[^\"]*\")";
   var ipDomainPat=/^(\d{1,3})[.](\d{1,3})[.](\d{1,3})[.](\d{1,3})$/;
   var atom=validChars + '+';
   var word="(" + atom + "|" + quotedUser + ")";
   var userPat=new RegExp("^" + word + "(\\." + word + ")*$");
   var domainPat=new RegExp("^" + atom + "(\\." + atom + ")*$");
   var matchArray=emailStr.match(emailPat);
   if (matchArray == null) {
       return false;
   }
   var user=matchArray[1];
   var domain=matchArray[2];
   if (user.match(userPat) == null) {
       return false;
   }
   var IPArray = domain.match(ipDomainPat);
   if (IPArray != null) {
       for (var i = 1; i <= 4; i++) {
          if (IPArray[i] > 255) {
             return false;
          }
       }
       return true;
   }
   var domainArray=domain.match(domainPat);
   if (domainArray == null) {
       return false;
   }
   var atomPat=new RegExp(atom,"g");
   var domArr=domain.match(atomPat);
   var len=domArr.length;
   if ((domArr[domArr.length-1].length < 2) ||
       (domArr[domArr.length-1].length > 3)) {
       return false;
   }
   if (len < 2) {
       return false;
   }
   return true;
}

function valida_cpf(cpf){
	  var numeros, digitos, soma, i, resultado, digitos_iguais;
	  digitos_iguais = 1;
	  if (cpf.length < 11)
			return false;
	  for (i = 0; i < cpf.length - 1; i++)
			if (cpf.charAt(i) != cpf.charAt(i + 1))
			  {
			  digitos_iguais = 0;
			  break;
			  }
	  if (!digitos_iguais)
			{
			numeros = cpf.substring(0,9);
			digitos = cpf.substring(9);
			soma = 0;
			for (i = 10; i > 1; i--)
				  soma += numeros.charAt(10 - i) * i;
			resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
			if (resultado != digitos.charAt(0))
				  return false;
			numeros = cpf.substring(0,10);
			soma = 0;
			for (i = 11; i > 1; i--)
				  soma += numeros.charAt(11 - i) * i;
			resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
				if (resultado != digitos.charAt(1))
				  return false;
			return true;
			}
	  else
			return false;
}

function existeCPF(cpf){
	cpf = cpf.replace(/\./g,'');
	cpf = cpf.replace(/\-/g,'');
	
	if(!valida_cpf(cpf)){
		openDialogInformation('O CPF digitado n�o � um CPF v�lido, por favor redigite o n�mero.',true);
		$('#cpfNovo').val('');
		return false;
	}
	
	
	openDialogAguarde();
	$.getJSON('/ClinicalSys/clinic/pag/Paciente?ACAO=existeCPF',{'cpf':cpf},
	  	function(data){
	  		if(data.existe){
	  			$("#dialog-aguarde").dialog('destroy');	  			
	  			openDialogInformation('Ops! J� existe um paciente com este CPF cadastrado no sistema!');
	  			$('#cpfNovo').val('');
	  		}else
	  			$("#dialog-aguarde").dialog('destroy');
		});	
	
}
/*Funcao dialog de aguarde*/
function openDialogAguarde(){ 
	$("#dialog-aguarde").dialog({
		closeText: 'hide',
		closeOnEscape: false,
		resizable: false,
		width: 600,
		modal: true,
		show: 'bounce', 
		open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();$('.ui-dialog').css('left','25%');}
	});
	return false;
}

function calculaFim(param){
	if(param=='<null>' || param == '')
		$('#endHour').val('');
	else
		$('#endHour').val(somarHorasFormated(param.split(':')[0],param.split(':')[1],$('#slotMinutes').val()));
}

function somarHorasFormated(hour,minute,duracao){
	hour    = parseInt(hour,10);
	minute  = parseInt(minute,10);
	duracao = parseInt(duracao,10);
	
	var d = new Date(0); 
	d.setHours(hour); 
	d.setMinutes(minute + duracao);  
	
	var hourReturn = ''+d.getHours();
	var minutesReturn = ''+d.getMinutes();
	
	if(hourReturn.length == 1)
		hourReturn = '0'+d.getHours();
	
	if(minutesReturn.length == 1)
		minutesReturn = '0'+ d.getMinutes();
	
	return (hourReturn +':'+ minutesReturn);  
}
Array.prototype.contains = function (element) {
	for (var i = 0; i < this.length; i++) {
		if (this[i] == element) {
			return true;
		}
	}
	return false;
}
</script>
<style type="text/css">
#example {
    background: white url(/ClinicalSys/images/find.png) right no-repeat;
    padding-right: 17px;
}


</style>

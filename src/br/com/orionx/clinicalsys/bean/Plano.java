package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name = "plano")
public class Plano {
	
	private Integer idPlano;
	private String nome;
	private boolean ativo;
	private Operadora operadora;
	
	public Plano() {
		// TODO Auto-generated constructor stub
	}
	public Plano(Integer idPlano) {
		this.idPlano = idPlano;
	}
	
	//GET
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getIdPlano() {
		return idPlano;
	}
	@Required
	@DescriptionProperty
	@MinLength(1)
	@MaxLength(100)
	public String getNome() {
		return nome;
	}
	public boolean isAtivo() {
		return ativo;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idOperadora")
	public Operadora getOperadora() {
		return operadora;
	}
	//SET
	public void setIdPlano(Integer idPlano) {
		this.idPlano = idPlano;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	public void setOperadora(Operadora operadora) {
		this.operadora = operadora;
	}

}

/*Funcao dialog de confirmacao*/
function openDialogConfirm(){ 
	$("#dialog-confirm").dialog({
		closeText: 'hide',
		closeOnEscape: false,
		resizable: false,
		width: 600,
		modal: true,		
		open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();}
	});
	return false;
}

function openDialogInformation(msg){
	$("#dialog-information").html('<p>' + msg + '</p>');
	$("#dialog-information").dialog({
		closeText: 'hide',
		closeOnEscape: false,
		resizable: false,
		width: 600,
		modal: true,		
		open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();},
		buttons:{
			Fechar: function(){
				$(this).dialog('destroy');
			}
			
		}
	});
	return false;
}

/*Funcao dialog de delete*/
function openDialogDelete(){ 
	$("#dialog-help-delete").dialog({
		closeText: 'hide',
		closeOnEscape: false,
		resizable: false,
		width: 600,
		modal: true,		
		open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();}
	});
	return false;
}

/*Funcao dialog de aguarde*/
function openDialogAguarde(){ 
	$("#dialog-aguarde").dialog({
		closeText: 'hide',
		closeOnEscape: false,
		resizable: false,
		width: 600,
		modal: true,
		show: 'fade', 
		open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();}
	});
	return false;
}

/*fun��o para buscar o cep*/
function openDialogBuscaCEP(){ 
	$("#dialog-busca-endereco").dialog({
		closeText: 'hide',
		closeOnEscape: false,
		resizable: false,
		width: 600,
		modal: true,		
		open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();},
		buttons:{
			Buscar: function(){
				if(!verifyRequired()){
					openDialogAguarde();
					callWebServiceCEPByAddress();
				}
			},
			Fechar: function(){
				$(this).dialog('destroy');
			}
		}
	});
	return false;
}

function openDialogCalendarInformation(){
	$("#add-event-form").dialog({
		closeText: 'hide',
		closeOnEscape: false,
		resizable: false,
		width: 600,
		modal: true,		
		open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();},
		buttons:{
			Agendar: function(){
				insertEventAgenda();
				$(this).dialog('destroy');
			},
			Fechar: function(){
				$(this).dialog('destroy');
			}
		}			
	});
	return false;
}

function verifyRequired(){
	if($('select[name=municipioAjax]').val()=='' ||$(' select[name=municipioAjax]').val()=='<null>'){
		openDialogInformation("O campo Munic�pio � de preenchimento obrigat�rio para a busca por endere�o");
		return true;
	}else if($('select[name=unidadeFederativaAjax]').val()=='' || $('select[name=unidadeFederativaAjax]').val()=='<null>'){
		openDialogInformation("O campo Unidade Federativa � de preenchimento obrigat�rio para a busca por endere�o");
		return true;
	}else if($('#logradouroAjax').val()==''){
		openDialogInformation("O campo Logadrouro � de preenchimento obrigat�rio para a busca por endere�o");
		return true;
	}else if($('#tipoLogradouroAjax').val()==''){
		openDialogInformation("O campo Tipo de Logadrouro � de preenchimento obrigat�rio para a busca por endere�o");
		return true;
	}else 
		return false;
	
	
}

function closeDialogAguarde(){
	$("#dialog-aguarde").dialog('destroy');
}

function validateRequired(form) {
    var isValid = true;
    var focusField = null;
    var i = 0;
    var fields = new Array();
    var fieldObjs = new Array();
    var formName = form.getAttributeNode("name");
    oRequired = eval('new ' + formName.value + '_required()');

    for (x in oRequired) {
		// form[] n?o funciona direito quando o nome
		// dos elements mudam dinamicamente
        //var field = form[oRequired[x][0]];
		//alert('required loop');
        var field = null;
        var radiofields = new Array();
        
        
        var fprox = formProperties[oRequired[x][0]];
        if(fprox){
        	if(fprox.isArray){
        		radiofields = fprox;
        	} else {
   	            field = fprox;
        	}
        }

        if(radiofields.length > 0){
            var haschecked = false;
        	for(var r = 0; r < radiofields.length; r++){
            	//alert('loop radios');
        		var radio = radiofields[r];
        		if(radio.checked){
            		haschecked = true;
        			//alert('selected: '+radio.value+'  '+radio.name);
        		} //else {
        			//alert('not selected: '+radio.value+'  '+radio.name);
        		//}
        	}
        	if(!haschecked){
	           	fields[i++] = oRequired[x][1];
	            fieldObjs[i] = radiofields[0];
	            isValid = false; 	
	            //alert('Faltando '+oRequired[x][0]);
        	}
        } else if ((field != null && (field.type == 'hidden' ||
            field.type == 'text' ||
            field.type == 'textarea' ||
            field.type == 'file' ||
            field.type == 'select-one' ||
            field.type == 'password')) &&
            field.disabled == false) {
			
            var value = '';
            // get field's value
            if (field.type == "select-one") {
                var si = field.selectedIndex;
                if (si >= 0) {
                    value = field.options[si].value;
                }
            } else if (field.type == 'checkbox') {
                if (field.checked) {
                    value = field.value;
                }
            } else {
                value = field.value;
            }
			if (trim(value).length == 0 || value == '<null>') {
				if (field.type == 'file') {
					if	(document.getElementById(field.name+'_excludeField').value == 'true'
							|| document.getElementById(field.name+'_div').innerHTML == '[vazio]') {
						
	                    if (i == 0) {
	                        focusField = field;
	                    }
	                    fields[i++] = oRequired[x][1];
	                    fieldObjs[i] = form[oRequired[x][0]];
	                    isValid = false;
                    }
                } else {
                		if (i == 0) {
	                        focusField = field;
	                    }
	                    fields[i++] = oRequired[x][1];
	                    fieldObjs[i] = form[oRequired[x][0]];
	                    isValid = false;
                }
            }
        } else if (field != null && field.type == "select-multiple") { 
            var numOptions = field.options.length;
            lastSelected=-1;
            for(loop=numOptions-1;loop>=0;loop--) {
                if(field.options[loop].selected) {
                    lastSelected = loop;
                    value = field.options[loop].value;
                    break;
                }
            }
            if(lastSelected < 0 || trim(value).length == 0) {
                if(i == 0) {
                    focusField = field;
                }
                fields[i++] = oRequired[x][1];
                fieldObjs[i] = form[oRequired[x][0]];
                isValid=false;
            }
        } else if (field != null && (field.length > 0) && (field[0].type == 'radio' || field[0].type == 'checkbox')) {
            isChecked=-1;
            for (loop=0;loop < field.length;loop++) {
                if (field[loop].checked) {
                    isChecked=loop;
                    break; // only one needs to be checked
                }
            }
            if (isChecked < 0) {
                if (i == 0) {
                    focusField = field[0];
                }
                fields[i++] = oRequired[x][1];
                fieldObjs[i] = form[oRequired[x][0]];
                isValid=false;
            }
        }
    }
    if (fields.length > 0) {
       invalidFields(form, fieldObjs, fields, 'required');
    }
    return isValid;
}

/**
 * A field is considered valid if greater than the specified minimum.
 * Fields are not checked if they are disabled.
 * <p>
 * <strong>Caution:</strong> Using <code>validateMinLength</code> on a password field in a 
 *  login page gives unnecessary information away to hackers. While it only slightly
 *  weakens security, we suggest using it only when modifying a password.</p>
 * @param form The form validation is taking place on.
 */
 function validateMinLength(form) {
     var isValid = true;
     var focusField = null;
     var i = 0;
     var fields = new Array();
     var campos = new Array();
     var formName = form.getAttributeNode("name");


     oMinLength = eval('new ' + formName.value + '_minlength()');

     for (x in oMinLength) {
    	 var field = null;
         field = formProperties[oMinLength[x][0]];
         if ((field.type == 'hidden' ||
             field.type == 'text' ||
             field.type == 'password' ||
             field.type == 'textarea') &&
             field.disabled == false) {

             var iMin = parseInt(oMinLength[x][2]("minlength"));
             if ((trim(field.value).length > 0) && (field.value.length < iMin)) {
                 if (i == 0) {
                     focusField = field;
                 }
                 var tmpI = i++;
                 fields[tmpI] = oMinLength[x][1];
                 campos[tmpI] = oMinLength[x][0];
                 isValid = false;
             }
         }
     }
     if (fields.length > 0) {
        try{focusField.focus();}catch(e){}
        clearErrorCSS(form);
        invalidFields(form, campos, fields, "minlength");
        //alert(fields.join('\n'));
     }
     return isValid;
 }

 /**
  * A field is considered valid if less than the specified maximum.
  * Fields are not checked if they are disabled.
  * <p>
  * <strong>Caution:</strong> Using <code>validateMaxLength</code> on a password field in a 
  *  login page gives unnecessary information away to hackers. While it only slightly
  *  weakens security, we suggest using it only when modifying a password.</p>
  * @param form The form validation is taking place on.
  */
  function validateMaxLength(form) {
      var isValid = true;
      var focusField = null;
      var i = 0;
      var fields = new Array();
      var fieldObjs = new Array();
      var formName = form.getAttributeNode("name"); 

      oMaxLength = eval('new ' + formName.value + '_maxlength()');        
      for (x in oMaxLength) {
			
          var field = null;
          field = formProperties[oMaxLength[x][0]];

          if ((field.type == 'hidden' ||
              field.type == 'text' ||
              field.type == 'password' ||
              field.type == 'textarea') &&
              field.disabled == false) {

              var iMax = parseInt(oMaxLength[x][2]("maxlength"));
              if (field.value.length > iMax) {
                  if (i == 0) {
                      focusField = field;
                  }
                  fields[i++] = oMaxLength[x][1];
                  fieldObjs[i] = form[oMaxLength[x][0]];
                  isValid = false;
              }
          }
      }
      if (fields.length > 0) {
         invalidFields(form, fieldObjs, fields, 'maxlength');
      }
      return isValid;
  } 
  
   /**
    *  Verifica determinado campo ? um inscricaoEstadual v?lido
    * <p>
    * @param form The form validation is taking place on.
    */
    function validateInscricaoEstadual(form) {
        var isValid = true;
        var focusField = null;
        var i = 0;
        var fields = new Array();
        var fieldObjs = new Array();
        var formName = form.getAttributeNode("name");

        oInscricaoEstadual = eval('new ' + formName.value + '_inscricaoEstadual()');

        for (x in oInscricaoEstadual) {
			// form[] n?o funciona direito quando o nome
			// dos elements mudam dinamicamente
            //var field = form[oInscricaoEstadual[x][0]];

            var field = null;
            field = formProperties[oInscricaoEstadual[x][0]];
            /*
	        for (var j = 0; j < form.elements.length; j++){
		    	element = form.elements[j];
		    	if(element.name == null) continue;
			    if(element.name == oInscricaoEstadual[x][0]){
				    field = element;
			    }
		    }
		    */
            
            if ((field.type == 'hidden' ||
                field.type == 'text' ||
                field.type == 'password' ||
                field.type == 'textarea') &&
                field.disabled == false) {
				
				if(field.value.replace( /\s*/, "" ).length==0) return true;
				
                if (ApenasNum(field.value).length != 14) {
                    if (i == 0) {
                        focusField = field;
                    }
                    fields[i++] = oInscricaoEstadual[x][1];
                    isValid = false;
                }
            }
        }
        if (fields.length > 0) {
           invalidFields(form, fieldObjs, fields, 'inscricaoEstadual');
        }
        return isValid;
    }
  
  
  /**
   *  Verifica determinado campo ? um cnpj v?lido
   * <p>
   * @param form The form validation is taking place on.
   */

   function validateCnpj(form) {
       var isValid = true;
       var focusField = null;
       var i = 0;
       var fields = new Array();
       var fieldObjs = new Array();
       var formName = form.getAttributeNode("name");

       oCnpj = eval('new ' + formName.value + '_cnpj()');

       for (x in oCnpj) {
             

           var field = null;
           field = formProperties[oCnpj[x][0]];
           
           if ((field.type == 'hidden' ||
               field.type == 'text' ||
               field.type == 'password' ||
               field.type == 'textarea') &&
               field.disabled == false) {

				if(field.value.replace( /\s*/, "" ).length==0) { 
					continue;
				}

               if (!digitoCNPJ(ApenasNum(field.value))) {
                   if (i == 0) {
                       focusField = field;
                   }
                   fields[i++] = oCnpj[x][1];
                   isValid = false;
               }
               
           }
       }
       if (fields.length > 0) {
          invalidFields(form, fieldObjs, fields, 'cnpj');
       }

       if(!isValid){
    	   $('form[name=form] input[name=cnpj]').css('background','#B81900 url(/ClinicalSys/images/requiredicon.png) repeat 50% 50%');
    	   $('form[name=form] input[name=cnpj]').next('span').html('&nbsp;&raquo;&nbsp;Digite um CNPJ v�lido.').css('color','#B81900');
    	   closeDialogAguarde();
       }else{
    	   $('form[name=form] input[name=cnpj]').css('background','');
    	   $('form[name=form] input[name=cnpj]').next('span').html('*').css('color','');
       }
       return isValid;
   }
   
   /**
    *  Verifica determinado campo ? um cpf v?lido
    * <p>
    * @param form The form validation is taking place on.
    */

    function validateCpf(form) {
        var isValid = true;
        var focusField = null;
        var i = 0;
        var fields = new Array();
        var fieldObjs = new Array();
        var formName = form.getAttributeNode("name");

        oCpf = eval('new ' + formName.value + '_cpf()');

        for (x in oCpf) {

            var field = null;
            field = formProperties[oCpf[x][0]];
            if ((field.type == 'hidden' ||
                field.type == 'text' ||
                field.type == 'password' ||
                field.type == 'textarea') &&
                field.disabled == false) {

				if(field.value.replace( /\s*/, "" ).length==0) { 
					continue;
				}


                if (!DigitoCPF(ApenasNum(field.value))) {
                    if (i == 0) {
                        focusField = field;
                    }
                    fields[i++] = oCpf[x][1];
                    isValid = false;
                }
            }
        }
        if (fields.length > 0) {
           invalidFields(form, fieldObjs, fields, 'cpf');
        }
        if(!isValid){
     	   $('form[name=form] input[name=cpf]').css('background','#B81900 url(/ClinicalSys/images/requiredicon.png) repeat 50% 50%');
     	   $('form[name=form] input[name=cpf]').next('span').html('&nbsp;&raquo;&nbsp;Digite um CPF v�lido.').css('color','#B81900');
     	   closeDialogAguarde();
        }else{
     	   $('form[name=form] input[name=cpf]').css('background','');
     	   $('form[name=form] input[name=cpf]').next('span').html('*').css('color','');
        }
        return isValid;
    }
   
function invalidFields(form, fields, msgs, validationName){
	clearErrorCSS(form);
	hasFocus = false;
	var positionMsg = 0;
	var positionMsgMaxLegth = 0;
	$('#ErrosSubmissao').html('');
	var tipoMsg;
	var msg = '';
	if(validationName == 'inscricaoEstadual'){		
		$('form[name=form] input[name=inscricaoEstadual]').css('background','#B81900 url(/ClinicalSys/images/requiredicon.png) repeat 50% 50%').css('border','1px solid #CC0000');
		$('#ErrosSubmissao').html('&nbsp;&raquo;&nbsp;A Incri��o Estadual est� incorreta.');
		closeDialogAguarde();
				
	}
				
	for(i = 0; i < fields.length; i++){
		try{
			if(fields[i] != undefined){
				closeDialogAguarde();
				if(validationName == 'minlength'){
					$('form[name=form] input[name='+fields[i]+']').css('background','#B81900 url(/ClinicalSys/images/requiredicon.png) repeat 50% 50%').css('border','1px solid #CC0000');
					msg += '&nbsp;&raquo;&nbsp;'+msgs[i]+'<br>';
					tipoMsg = 2;
				}
				if(validationName == 'maxlength'){
					$('form[name=form] input[name='+fields[i].name+']').css('background','#B81900 url(/ClinicalSys/images/requiredicon.png) repeat 50% 50%').css('border','1px solid #CC0000');;
					msg += '&nbsp;&raquo;&nbsp;'+msgs[positionMsgMaxLegth]+'<br>';
					tipoMsg = 2;
					positionMsgMaxLegth++;
				}
				if(validationName == 'required'){
					tipoMsg = 1;
					if(fields[i].type == 'select-one'){
						$('form[name=form] select[name='+fields[i].name+']').css('background','#B81900 url(/ClinicalSys/images/requiredicon.png) repeat 50% 50%').css('border','1px solid #CC0000');
					}else if(fields[i].type == 'checkbox'){
						$('input[name='+this.name+']').next().next('span').html('*').css('color','#000');
					}else if(fields[i].type == 'textarea'){
						if(fields[i].id.indexOf('wysiwyg')!=-1){
							var componenteRequired = "'#"+fields[i].id+"IFrame'";
							$(eval(componenteRequired)).css('background','#B81900 url(/ClinicalSys/images/requiredicon.png) repeat 50% 50%').css('border','1px solid #CC0000');
						}
						$('form[name=form] textarea[name='+fields[i].name+']').css('background','#B81900 url(/ClinicalSys/images/requiredicon.png) repeat 50% 50%').css('border','1px solid #CC0000');
					}else{
						$('form[name=form] input[name='+fields[i].name+']').css('background','#B81900 url(/ClinicalSys/images/requiredicon.png) repeat 50% 50%').css('border','1px solid #CC0000');
					}
				}
				if(tipoMsg == 1)
					$('#ErrosSubmissao').html('&nbsp;&raquo;&nbsp;Os campos marcados em vermelho, s�o de preenchimento obrigat�rio.');
				else if(tipoMsg == 2)
					$('#ErrosSubmissao').html(msg);
				if(!hasFocus){
					fields[i].focus();
					hasFocus = true;
				}
				positionMsg++;
			}
		}catch(exception){}
	}
	$('.ui-effects-wrapper').css('display','none');
}

function clearErrorCSS(form){
	$("form[name=form] input, form[name=form] textarea, form[name=form] select").each(function(i){ 
	  //console.log(this);
		if(this.type == 'select-one'){
			$('select[name='+this.name+']').css('background','').css('border','1px solid #C0C0C0');
			$('select[name='+this.name+']').next('span').html('*').css('color','#000');
		}
		else if(this.type == 'checkbox'){
			$('input[name='+this.name+']').next().next('span').html('*').css('color','#000');
		}else if(this.type == 'textarea'){
			$('form[name=form] textarea[name='+this.name+']').css('background','').css('border','1px solid #C0C0C0');
		}
		else{
			$('input[name='+this.name+']').css('background','').css('border','1px solid #C0C0C0');
			$('input[name='+this.name+']').next('span').html('*').css('color','#000');
		}
	 }); 
	
}

/**
 * Fun��o para abrir menu, 
 * @param param representa qual id do m�dulo deve abrir
 */
function openMenuByModulo(param){
	$('#main_menu').accordion('activate', -1);
	hideMenu();
	$('li[path='+param+']').show();
	$('#left_menu').fadeOut();
	$('#show_menu').show();
	$('body').addClass('nobg');
	$('#content').css('marginLeft', 30);
	$('#wysiwyg').css('width', '97%');
	//setNotifications();
	$('#menu_config').show();
	$('#show_menu').click();
}

function hideMenu(){
	$('li[path=system]').hide();
	$('li[path=util]').hide();
	$('li[path=estoque]').hide();
}

function openAguardeAjax(){
	$('#carregandoAjax').fadeIn();
}

function closeAguardeAjax(){
	$('#carregandoAjax').fadeOut();
}


var ufSelecionado;
var cidadeSelecionada;
function ajaxBuscaCEP(cepparam){
	openDialogAguarde();
	$.getJSON('/ClinicalSys/system/process/Clinica?ACAO=callWebServiceCEP',{'cep':cepparam},
	  	function(data){
	  		if(data.erro){
	  			closeDialogAguarde();
	  			openDialogInformation('CEP n�o encontrado.');
	  		}else{
				$('#bairro').val(data.bairro);
				$('#logradouro').val(data.logradouro);
				$('#tipoLogradouro').val(data.tipoLogradouro);
				$('#pais').val(data.pais);
				$('#pais').change();
				ufSelecionado = data.uf;
				cidadeSelecionada = data.cidade;
				setTimeout("updateUf()",1500);
			}
		});
}

function callWebServiceCEPByAddress(){
	var logradouro     = $('#logradouroAjax').val();
	var municipio      = $('select[name=municipioAjax] option:selected').val();
	var tipoLogradouro = $('select[name=tipoLogradouroAjax] option:selected').val();
	var bairro         = $('#bairroAjax').val();

	$.ajaxSetup({ scriptCharset: "ISO-8859-1" , contentType: "application/json; charset=ISO-8859-1"});
	$.getJSON('/ClinicalSys/system/process/Clinica?ACAO=callWebServiceCEPByAddress',{'logradouro':logradouro,'municipio':municipio,'bairro':bairro,'tipoLogradouro':tipoLogradouro},
	  	function(data){
	  		if(data.erro){
	  			closeDialogAguarde();
	  			openDialogInformation('CEP n�o encontrado.');
	  		}else{
	  			closeDialogAguarde();
				openDialogInformation("Selecione sua Op��o de CEP:<br/><br/><p><a href='javascript:selectedCEP("+data.cep+");'>"+data.cep+"</a></p>");
			}
		});
}

function selectedCEP(param){
	param = ''+param;
	$("#dialog-busca-endereco").dialog('destroy');
	$("#dialog-information").dialog('destroy');
	
	var cep 	= '' + param.substr(0,5) + '-' + param.substr(5,6);
	$('#cep').val(cep);
	ajaxBuscaCEP(param);

}

function ajaxBuscaCid10(id,tipo){
	openDialogAguarde();
	$.getJSON('/ClinicalSys/clinic/process/CID10?ACAO=getTextCID10',{'id':id,'tipo':tipo},
	  	function(data){
	  		if(data.erro){
	  			closeDialogAguarde();
	  			openDialogInformation('Ops! houve um problema ao buscar as informa��es do CID10, tente novamente por favor!');
	  		}else{
	  			closeDialogAguarde();
				$('#textoCID10').html(data.texto);
			}
		});
}

function comboReloadAjaxModal(id){
	if(id == "" || id == "<null>"){
		$('select[name=municipioAjax]').empty();
		return false;
	}
	openDialogAguarde();
	$.getJSON('/ClinicalSys/system/process/Clinica?ACAO=callMunicipio',{'id':id},
	  	function(data){
	  		if(data.erro){
	  			closeDialogAguarde();
	  			openDialogInformation('CEP n�o encontrado.');
	  		}else{
	  			closeDialogAguarde();
				for(var i=0;i<data.length;i++){
	  		        var obj = data[i];
	  		        for(var key in obj){
	  		            eval(obj[key]);
	  		        }
	  		    }
	  		    
			}
		});
}

function updateUf(){
	$('#uf').val(ufSelecionado);
	$('#uf').change();
	setTimeout("updateMunicipio()",1500);
}
function updateMunicipio(){
	$('#cidade').val(cidadeSelecionada);
	$('#cidade').change();
	$('#numero').focus();
	closeDialogAguarde();
	runEffectWebServiceCep();
}

function runEffectWebServiceCep() {
	
	// most effect types need no options passed by default
	var options = {};

	// run the effect
	$('#bairro').effect( 'highlight', options, 500, callback );
	$('#logradouro').effect( 'highlight', options, 500, callback );
	$('#numero').effect( 'highlight', options, 500, callback );
};

function callback() {
	setTimeout(function() {
	}, 1000 );
};

function validaEmailFormulario(param){
	if(!checkEmail(param.value)){
		$(param).css('background','#B81900 url(/ClinicalSys/images/requiredicon.png) repeat 50% 50%').css('border','1px solid #CC0000');
		$(param).next('span').hide().css('color','#B81900').html('&nbsp;&raquo;&nbsp; e-mail inv�lido.').fadeIn();
		return false;
	}else{
		$(param).css('background','').css('border','1px solid #C0C0C0');
		$(param).next('span').hide().html('*').css('color','#000').fadeIn();
		return true;
	}
}

function equalsEmail(param,param2){
	if(param.value != param2.value){
		$(param).css('background','#B81900 url(/ClinicalSys/images/requiredicon.png) repeat 50% 50%').css('border','1px solid #CC0000');
		$(param).next('span').css('color','#B81900').hide().html('&nbsp;&raquo;&nbsp; A redigita��o de e-mail n�o confere.').fadeIn();
		openDialogInformation('A redigita��o de e-mail n�o confere.');
		return false;
	}else{
		$(param).css('background','').css('border','1px solid #C0C0C0');
		$(param).next('span').hide().html('*').css('color','#000').fadeIn();
		return true;
	}
}

function equalsPassword(param,param2){
	if(param.value != param2.value){
		$(param).css('background','#B81900 url(/ClinicalSys/images/requiredicon.png) repeat 50% 50%').css('border','1px solid #CC0000');
		$(param).next('span').css('color','#B81900').hide().html('&nbsp;&raquo;&nbsp; A redigita��o de senha n�o confere.').fadeIn();
		return false;
	}else{
		$(param).css('background','').css('border','1px solid #C0C0C0');
		$(param).next('span').hide().html('*').css('color','#000').fadeIn();
		return true;
	}
}


$('#pathURL a').click(function(){
	openDialogAguarde();
});


function validateCnpjOver(obj){
	if(!verificaCNPJ(obj))
		openDialogInformation('O CNPJ digitado n�o � v�lido, este erro pode ter ocorrido por digita��o ou d�gito verificador.');
}

function verificaCNPJ(obj){
	s = obj.value;
	s = s.replace('.','');
	s = s.replace('.','');
	s = s.replace('/','');
	s = s.replace('-','');
	console.log(s);
	if (isNaN(s)) {
	 return false;
	}
	var i;
	var c = s.substr(0,12);
	var dv = s.substr(12,2);
	var d1 = 0;
	for (i = 0; i <12; i++){
	 d1 += c.charAt(11-i)*(2+(i % 8));
	}
	if (d1 == 0)
	 return false;
	d1 = 11 - (d1 % 11);
	if (d1 > 9) d1 = 0;
	if (dv.charAt(0) != d1){
	 return false;
	}
	d1 *= 2;
	for (i = 0; i < 12; i++){
	 d1 += c.charAt(11-i)*(2+((i+1) % 8));
	}
	d1 = 11 - (d1 % 11);
	if (d1 > 9)
	 d1 = 0;
	if (dv.charAt(1) != d1){
	 return false;
	}
	return true;
}

function buscaPlanos(operadora,index){
	$.getJSON('/ClinicalSys/clinic/pag/Paciente?ACAO=buscaPlanos',{'operadora':operadora,'index':index},
		  	function(data){
		  		if(data.erro){
		  			openDialogInformation('Ops! Ocorreu um erro ao buscar os planos. Tente novamente por favor!');
		  		}else{
		  			for(var i=0;i<data.length;i++){
		  		        var obj = data[i];
		  		        for(var key in obj){
		  		            eval(obj[key]);
		  		        }
		  		    }
				}
			});	
}
function buscaPlanosDialog(operadora){
	$.getJSON('/ClinicalSys/clinic/pag/Paciente?ACAO=buscaPlanos',{'operadora':operadora},
		  	function(data){
		  		if(data.erro){
		  			openDialogInformation('Ops! Ocorreu um erro ao buscar os planos. Tente novamente por favor!');
		  		}else{
		  			for(var i=0;i<data.length;i++){
		  		        var obj = data[i];
		  		        for(var key in obj){
		  		            eval(obj[key]);
		  		        }
		  		    }
				}
			});	
}

function buscaMedicamentos(classificacao,index){
	$.getJSON('/ClinicalSys/clinic/pag/Medicamento?ACAO=buscaMedicamentos',{'classificacao':classificacao,'index':index},
		  	function(data){
				closeAguardeAjax();
		  		if(data.erro){
		  			openDialogInformation('Ops! Ocorreu um erro ao buscar os medicamentos. Tente novamente por favor!');
		  		}else{
		  			for(var i=0;i<data.length;i++){
		  		        var obj = data[i];
		  		        for(var key in obj){
		  		            eval(obj[key]);
		  		        }
		  		    }
				}
			});	
}


function calcular_idade(dataNasc){ 
	var dataAtual = new Date();
	var anoAtual = dataAtual.getFullYear();
	var anoNascParts = dataNasc.split('/');
	var diaNasc =anoNascParts[0];
	var mesNasc =anoNascParts[1];
	var anoNasc =anoNascParts[2];
	var idade = anoAtual - anoNasc;
	var mesAtual = dataAtual.getMonth() + 1; 
	//se m�s atual for menor que o nascimento, nao fez aniversario ainda; (26/10/2009) 
	if(mesAtual < mesNasc){
		idade--; 
	}else {
		//se estiver no mes do nasc, verificar o dia
		if(mesAtual == mesNasc){ 
			if(dataAtual.getDate() < diaNasc ){ 
			//se a data atual for menor que o dia de nascimento ele ainda nao fez aniversario
			idade--; 
			}
		}
	} 
	return idade; 
}

function data_americana_brasileira(valor,pattern) {
	var dataSplit = valor.split('-');
	var mydata = dataSplit[2] + '/' + dataSplit[1] + '/' + dataSplit[0];
	return mydata;
}

function mascara_data_press(data){
    var mydata = ''; 
    mydata = mydata + data.val(); 
    if (mydata.length == 2){ 
        mydata = mydata + '/'; 
        data.val(mydata); 
    } 
    if (mydata.length == 5){ 
        mydata = mydata + '/'; 
        data.val(mydata); 
    } 
}

function verifica_data () { 

    dia = (document.forms[0].data.value.substring(0,2)); 
    mes = (document.forms[0].data.value.substring(3,5)); 
    ano = (document.forms[0].data.value.substring(6,10)); 

    situacao = ""; 
    // verifica o dia valido para cada mes 
    if ((dia < 01)||(dia < 01 || dia > 30) && (  mes == 04 || mes == 06 || mes == 09 || mes == 11 ) || dia > 31) { 
        situacao = "falsa"; 
    } 

    // verifica se o mes e valido 
    if (mes < 01 || mes > 12 ) { 
        situacao = "falsa"; 
    } 

    // verifica se e ano bissexto 
    if (mes == 2 && ( dia < 01 || dia > 29 || ( dia > 28 && (parseInt(ano / 4) != ano / 4)))) { 
        situacao = "falsa"; 
    } 

    if (document.forms[0].data.value == "") { 
        situacao = "falsa"; 
    } 

    if (situacao == "falsa") { 
        alert("Data inv�lida!"); 
        document.forms[0].data.focus(); 
    } 
 } 

function mascaraPeso(v){
    v=v.replace(/\D/g,"");                          //Remove tudo o que n�o � d�gito
    v = v.replace('.','');
    if(v.length >= 6)
    	v=v.replace(/^(\d{3})(\d)/,"$1.$2");           //Coloca ponto entre o terceiro e quarto d�gitos
    else
    	v=v.replace(/^(\d{2})(\d)/,"$1.$2");           //Coloca ponto entre o segundo e o terceiro d�gitos
    return v;
}

function mascaraAltura(v){
    v=v.replace(/\D/g,"");                          //Remove tudo o que n�o � d�gito
    v=v.replace(/^(\d{1})(\d)/,"$1.$2");           //Coloca ponto entre o segundo e o terceiro d�gitos
    return v;
}


function mascara(o,f){
    v_obj=o;
    v_fun=f;
    setTimeout("execmascara()",1)
}

function execmascara(){
    v_obj.value=v_fun(v_obj.value);
}

function findMedicineByLetter(param, objeto){
	removeActive();
	$('#'+param).addClass('active');
	var medicamentos = '';
	$.getJSON('/ClinicalSys/clinic/process/Medicamentos?ACAO=findMedicineByLetter',{'letter':param},
	  	function(data){
	  		if(data.erro){
	  			closeDialogAguarde();
	  			openDialogInformation('Algo aconteceu no momento de buscar os medicamentos, por favor tente novamente.');
	  		}else{
	  			closeDialogAguarde();
				for(var i=0;i<data.length;i++){
	  		        var obj = data[i];
	  		        medicamentos += "<a href='javascript:openDialogAguarde();getMedicine(" + obj.idMed + ");'>" + obj.nomeMed + "</a><br/>";
	  		    }
				$('#listaMed').html(medicamentos);
	  		    
			}
		});
}

function removeActive(){
	$('#a').removeClass('active');
	$('#b').removeClass('active');
	$('#c').removeClass('active');
	$('#d').removeClass('active');
	$('#e').removeClass('active');
	$('#f').removeClass('active');
	$('#g').removeClass('active');
	$('#h').removeClass('active');
	$('#i').removeClass('active');
	$('#j').removeClass('active');
	$('#k').removeClass('active');
	$('#l').removeClass('active');
	$('#m').removeClass('active');
	$('#n').removeClass('active');
	$('#o').removeClass('active');
	$('#p').removeClass('active');
	$('#q').removeClass('active');
	$('#r').removeClass('active');
	$('#s').removeClass('active');
	$('#t').removeClass('active');
	$('#u').removeClass('active');
	$('#v').removeClass('active');
	$('#w').removeClass('active');
	$('#x').removeClass('active');
	$('#y').removeClass('active');
	$('#z').removeClass('active');
}

function getMedicine(param){
	
	$.getJSON('/ClinicalSys/clinic/process/Medicamentos?ACAO=findDescription',{'idMedicamento':param},
	  	function(data){
	  		if(data.erro){
	  			closeDialogAguarde();
	  			openDialogInformation('Algo aconteceu no momento de buscar os medicamentos, por favor tente novamente.');
	  		}else{
	  			closeDialogAguarde();
	  			$('#contentMed').html(data.descricao);
	  			$('#nomeMed').html(data.principioAtivo);
	  			$('#nomeComercial').html(data.nomeComercial);
	  			$('#classificacao').html(data.classificacao);
	  			$('#ied').html(data.ied);
	  			$('#efeitosColateraisCom').html(data.efeitosColateraisCom);
	  			$('#efeitosColateraisMF').html(data.efeitosColateraisMF);
	  			$('#bula').attr('href','http://www4.anvisa.gov.br/BularioEletronico/default.asp?txtPrincipioAtivo='+data.principioAtivo);
	  			eval($('#bula'));
			}
		});
}

function buscaMedicamento(param1, param2){
	$.getJSON('/ClinicalSys/clinic/process/Medicamentos?ACAO=findMedicamentos',{'pa':param1,'nomeComercial':param2},
		  	function(data){
		  		if(data.erro){
		  			closeDialogAguarde();
		  			openDialogInformation('Algo aconteceu no momento de buscar os medicamentos, por favor tente novamente.');
		  		}else{
		  			closeDialogAguarde();
		  			
				}
			});
}

function openFindMedicamento(){
	$("#dialog-busca-medicamento").dialog({
		closeText: 'hide',
		closeOnEscape: false,
		resizable: false,
		width: 600,
		modal: true,		
		buttons:{
			Fechar: function(){
				$(this).dialog('destroy');
			},
			Buscar: function(){
				buscaMedicamento($('#principioAtivo').val(),$('#nomeComercial').val());
			}
			
		},
		open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();}
	});
	return false;
}


var cronometro_min=30; 			
var cronometro_seg=00;  			
function startCountdown(){
	if(cronometro_min == 2 && cronometro_seg == 00){
		openDialogSessao();
	}
	if (cronometro_seg<=0){   			            
		cronometro_seg=60; 					    
		cronometro_min-=1; 					 
	}  					 
	if (cronometro_min<=-1){  					    
		cronometro_seg=0; 					    
		cronometro_seg+=1; 					    
		$("#cronometro_tempo").html(" "); 					    
		$("#cronometro_tempo").html("0:00");
		closeDialogSessao();
		openDialogSessaoExpirada();
	} else {  						
		cronometro_seg-=1; 						
		if(cronometro_seg < 10) { 							
			cronometro_seg = "0" + cronometro_seg; 						
		}  					    
		$("#cronometro_tempo").html(" "+ cronometro_min + ":" +cronometro_seg); 					    
		setTimeout("startCountdown()",1000);  					
	}   			
}		  			
function openDialogSessao(){ 
	$("#dialog-sessao").dialog({
		closeText: 'hide',
		closeOnEscape: false,
		resizable: false,
		width: 600,
		modal: true,		
		open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();}
	});
	return false;
}

function openDialogSessaoExpirada(){ 
	$("#dialog-sessao-expirada").dialog({
		closeText: 'hide',
		closeOnEscape: false,
		resizable: false,
		width: 600,
		modal: true,		
		open: function(event, ui) { $(".ui-dialog-titlebar-close").hide();}
	});
	return false;
}

function closeDialogSessao(){
	$("#dialog-sessao").dialog('destroy');
}

function renovaSessao(){
	sendRequest('/ClinicalSys/system','ACAO=renovaSessao','POST', ajaxCallBackSessao, erroCallbackSessao);
}

function ajaxCallBackSessao(){
	closeDialogSessao();
	cronometro_min = 30;
	cronometro_seg = 0;
	openDialogInformation('Sua sess�o foi renovada com sucesso!');
}
function erroCallbackSessao(){
	openDialogInformation('Ups! <br>Algo aconteceu no momento de renovar a sua sess�o, por favor, tente novamente.');
}

function verifyExt(){
	var bool = imagemValida(); 
	if(!bool){
		openDialogInformation("Ups!<br>Voc� deve escolher uma das extens�es abaixo para sua foto:<br> png, jpg, jpeg, gif e bmp");
		$('#foto').val('');
	}
	return bool;
}

function imagemValida(){
	var arquivoValue = $("#foto").val();
	var extensao = arquivoValue.substring(arquivoValue.length-3,arquivoValue.length).toUpperCase();
	if (arquivoValue != ""){
		if(extensao == "PNG" || extensao == "JPG" || extensao == "JPEG" || extensao == "GIF" || extensao == "BMP"){
			return true;
		} else {
			return false;
		}
	}else return true;
}

function maskHora(objeto, evt) { 
	return mascaraHoraAux(objeto, evt, '##:##');
}
function mascaraHoraAux(objeto, evt, mask) {
	var LetrasU = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	var LetrasL = 'abcdefghijklmnopqrstuvwxyz';
	var Letras  = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	var Numeros = '0123456789';
	var Fixos  = '().-:/ '; 
	var Charset = " !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_/`abcdefghijklmnopqrstuvwxyz{|}~";
	
	evt = (evt) ? evt : (window.event) ? window.event : "";
	var value = objeto.value;
	if (evt) {
	 var ntecla = (evt.which) ? evt.which : evt.keyCode;
	 tecla = Charset.substr(ntecla - 32, 1);
	 if (ntecla < 32) return true;
	
	 var tamanho = value.length;
	 if (tamanho >= mask.length) return false;
	
	 var pos = mask.substr(tamanho,1); 
	 while (Fixos.indexOf(pos) != -1) {
	  value += pos;
	  tamanho = value.length;
	  if (tamanho >= mask.length) return false;
	  pos = mask.substr(tamanho,1);
	 }
	
	 switch (pos) {
	   case '#' : if (Numeros.indexOf(tecla) == -1) return false; break;
	   case 'A' : if (LetrasU.indexOf(tecla) == -1) return false; break;
	   case 'a' : if (LetrasL.indexOf(tecla) == -1) return false; break;
	   case 'Z' : if (Letras.indexOf(tecla) == -1) return false; break;
	   case '*' : objeto.value = value; return true; break;
	   default : return false; break;
	 }
	}
	objeto.value = value; 
	return true;
}

function somarHorasFormated(hour,minute,duracao){
	hour    = parseInt(hour,10);
	minute  = parseInt(minute,10);
	duracao = parseInt(duracao,10);
	
	var d = new Date(0); 
	d.setHours(hour); 
	d.setMinutes(minute + duracao);  
	
	var hourReturn = ''+d.getHours();
	var minutesReturn = ''+d.getMinutes();
	
	if(hourReturn.length == 1)
		hourReturn = '0'+d.getHours();
	
	if(minutesReturn.length == 1)
		minutesReturn = '0'+ d.getMinutes();
	
	return (hourReturn +':'+ minutesReturn);  
}

function insertEventAgenda(){
	var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();
	$('#calendar').fullCalendar('renderEvent',
		{
		title: '',
		start: new Date(y, m, d, 19, 0),
		end: new Date(y, m, d+1, 22, 30),
		allDay: false
		},
		true // make the event "stick"
	);
	
	$('#calendar').fullCalendar( 'refetchEvents' );
}

function openCalendar(param){
	openDialogAguarde();
	location.href = '/ClinicalSys/clinic/process/Calendar?shv='+param;
}

Array.prototype.contains = function (element) {
	for (var i = 0; i < this.length; i++) {
		if (this[i] == element) {
			return true;
		}
	}
	return false;
}

function voltarListagem(param){
	location.href = param; 
}

function ajaxGetName(nome){
 	if(nome!='')
     	sendRequest('/ClinicalSys/clinic/pag/Paciente','ACAO=getPacienteAutoComplete&nomeParc='+nome,'POST', ajaxCallbackNome, erroCallbackNome);
 }
 
 function ajaxCallbackNome(data){
	$("#paciente").setOptions({data: data.split(';')});
 }
 
 function erroCallbackNome(request){
	openDialogInformation("Ups. ocorreu um erro",true);
 }

 function checa_seguranca(pass){  
     var senha = pass.value;  
     var entrada = 0;  
     var resultado;  
       
     if(senha.length < 7){  
             entrada = entrada - 1;  
     }  
       
     if(!senha.match(/[a-z_]/i) || !senha.match(/[0-9]/)){  
             entrada = entrada - 1;  
     }  
       
     if(!senha.match(/\W/)){  
             entrada = entrada - 1;  
     }  
       
     if(entrada == 0){  
             resultado = 'A Seguran�a de sua senha �: <font color=\'#99C55D\'>EXCELENTE</font>';  
     } else if(entrada == -1){  
             resultado = 'A Seguran�a de sua senha �: <font color=\'#7F7FFF\'>BOM</font>';  
     } else if(entrada == -2){  
             resultado = 'A Seguran�a de sua senha �: <font color=\'#FF5F55\'>BAIXA</font>';  
     } else if(entrada == -3){  
             resultado = 'A Seguran�a de sua senha �: <font color=\'#A04040\'>MUITO BAIXA</font>';  
     }  
     
     $('#forcaSenha').html(resultado);       
       
     return;  
}  
 

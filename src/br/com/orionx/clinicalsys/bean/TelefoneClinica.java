package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.types.Telefone;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name = "telefoneclinica")
public class TelefoneClinica {
	
	private Integer idTelefoneClinica;
	private Telefone telefone;
	private TelefoneTipo telefoneTipo;
	private Clinica clinica;
	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdTelefoneClinica() {
		return idTelefoneClinica;
	}
	@Required
	public Telefone getTelefone() {
		return telefone;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "idTelefoneTipo")
	public TelefoneTipo getTelefoneTipo() {
		return telefoneTipo;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "idClinica")
	@ForeignKey(name = "fk_telclinica_clinica")
	public Clinica getClinica() {
		return clinica;
	}
	
	//SET
	public void setIdTelefoneClinica(Integer idTelefoneClinica) {
		this.idTelefoneClinica = idTelefoneClinica;
	}
	public void setTelefone(Telefone telefone) {
		this.telefone = telefone;
	}
	public void setTelefoneTipo(TelefoneTipo telefoneTipo) {
		this.telefoneTipo = telefoneTipo;
	}
	public void setClinica(Clinica clinica) {
		this.clinica = clinica;
	}

}

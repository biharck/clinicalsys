package br.com.orionx.clinicalsys.dao;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;

import br.com.orionx.clinicalsys.bean.UnidadeFederativa;
import br.com.orionx.clinicalsys.filtros.UnidadeFederativaFiltro;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

@DefaultOrderBy("unidadeFederativa.nome")
public class UnidadeFederativaDAO extends GenericDAO<UnidadeFederativa> {

	
	@Override
	public void updateListagemQuery(QueryBuilder<UnidadeFederativa> query,FiltroListagem _filtro) {
		UnidadeFederativaFiltro filtro = (UnidadeFederativaFiltro) _filtro;
		query.where("unidadeFederativa.pais=?",filtro.getPais()).whereLikeIgnoreAll("unidadeFederativa.nome", filtro.getNome());
		super.updateListagemQuery(query, filtro);
	}
}

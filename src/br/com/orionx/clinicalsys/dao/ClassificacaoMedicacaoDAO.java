package br.com.orionx.clinicalsys.dao;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.QueryBuilder;

import br.com.orionx.clinicalsys.bean.ClassificacaoMedicacao;
import br.com.orionx.clinicalsys.bean.Medicamento;
import br.com.orionx.clinicalsys.filtros.ClassificacaoMedicacaoFiltro;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class ClassificacaoMedicacaoDAO extends GenericDAO<ClassificacaoMedicacao>{

	
	@Override
	public void updateListagemQuery(QueryBuilder<ClassificacaoMedicacao> query,FiltroListagem _filtro) {
		ClassificacaoMedicacaoFiltro filtro = (ClassificacaoMedicacaoFiltro) _filtro;		
		query.whereLikeIgnoreAll("classificacaoMedicacao.nome",filtro.getNome());
		super.updateListagemQuery(query, filtro);
	}
	
	/**
	 * <p> método responsável em retornar a classificação de um determinado medicamento
	 * @param m {@link Medicamento}
	 * @return {@link ClassificacaoMedicacao}
	 */
	public ClassificacaoMedicacao getClassificacaoByMedicamento(Medicamento m){
		return query().select("classificacaoMedicacao").leftOuterJoin("classificacaoMedicacao.medicamentos m").where("m=?",m).unique();
	}
}

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada  >
	<t:property name="idMunicipio" type="hidden" label="" write="false"/>
	<t:janelaEntrada>
		<t:tabelaEntrada columns="1">
			<n:comboReloadGroup>
				<t:property name="unidadeFederativa.pais" renderAs="doubleline" onchange="openAguardeAjax()" id="pais"/>
				<t:property name="unidadeFederativa" renderAs="doubleline"/>
			</n:comboReloadGroup>
			<t:property name="nome" renderAs="doubleline"/>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>

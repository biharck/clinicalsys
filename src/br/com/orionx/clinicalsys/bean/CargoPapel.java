package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name="cargopapel")
public class CargoPapel {

	private Integer idCargoPapel;
	private Cargo cargo;
	private Papel papel;
	
	public CargoPapel(){}
	
	public CargoPapel(Integer idCargoPapel,Cargo cargo, Papel papel){
		this.idCargoPapel = idCargoPapel;
		this.cargo = cargo;
		this.papel = papel;
	}
	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdCargoPapel() {
		return idCargoPapel;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idCargo")
	@ForeignKey(name="fk_cargo_cargopapel")
	public Cargo getCargo() {
		return cargo;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idPapel")
	@ForeignKey(name="fk_papel_cargopapel")
	public Papel getPapel() {
		return papel;
	}
	
	//SET
	public void setIdCargoPapel(Integer idCargoPapel) {
		this.idCargoPapel = idCargoPapel;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	public void setPapel(Papel papel) {
		this.papel = papel;
	}
	
	
}

package br.com.orionx.clinicalsys.clinica.controller.crud;

import java.io.IOException;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

import br.com.orionx.clinicalsys.bean.CID10Capitulo;
import br.com.orionx.clinicalsys.bean.CID10Categoria;
import br.com.orionx.clinicalsys.bean.CID10Grupo;
import br.com.orionx.clinicalsys.bean.CID10SubCategoria;
import br.com.orionx.clinicalsys.service.CID10CapituloService;
import br.com.orionx.clinicalsys.service.CID10CategoriaService;
import br.com.orionx.clinicalsys.service.CID10GrupoService;
import br.com.orionx.clinicalsys.service.CID10SubCategoriaService;
import br.com.orionx.clinicalsys.util.TreeView;

@Controller(path="/clinic/process/CID10",authorizationModule=ProcessAuthorizationModule.class)
public class CID10Process extends MultiActionController{
	
	private CID10CapituloService cid10CapituloService;
	private CID10GrupoService cid10GrupoService;
	private CID10CategoriaService cID10CategoriaService;
	private CID10SubCategoriaService cid10SubCategoriaService;
	
	public void setCid10CapituloService(CID10CapituloService cid10CapituloService) {
		this.cid10CapituloService = cid10CapituloService;
	}
	public void setCid10GrupoService(CID10GrupoService cid10GrupoService) {
		this.cid10GrupoService = cid10GrupoService;
	}
	public void setCID10CategoriaService(CID10CategoriaService categoriaService) {
		cID10CategoriaService = categoriaService;
	}
	public void setCid10SubCategoriaService(CID10SubCategoriaService cid10SubCategoriaService) {
		this.cid10SubCategoriaService = cid10SubCategoriaService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request)throws CrudException {
		request.setAttribute("pathURL", getPathURL());
		List<CID10Capitulo> capitulos = cid10CapituloService.findAll();
		for (CID10Capitulo capitulo : capitulos) {
			List<CID10Grupo> grupos = cid10GrupoService.getGruposByCapitulo(capitulo.getCategoriaInicial(), capitulo.getCategoriaFinal());
			for (CID10Grupo g : grupos) {
				g.setCategorias(cID10CategoriaService.getCategoriasByGrupos(g.getCatInic(), g.getCatFim()));
			}
			capitulo.setGrupos(grupos);
		}
		request.setAttribute("capitulos", capitulos);
		
		TreeView treeView = new TreeView();
		treeView.addHeader("CID-10");
		int indice = 1;
		int mae = 1, mae2 = 0;
		for (CID10Capitulo capitulo : capitulos) {
			treeView.addNode(indice, 0, "Cap�tulo "+capitulo.getDescrabrev(), "javascript:d.o("+indice+");getText("+capitulo.getIdCid10Capitulo()+",1);");
			mae = indice;
			for (CID10Grupo grupo : capitulo.getGrupos()) {
				indice ++;
				treeView.addNode(indice, mae, grupo.getCatInic() +"-"+grupo.getCatFim() +" "+grupo.getDescrabrev(),  "javascript:d.o("+indice+");getText("+grupo.getIdCid10Grupo()+",2);");
				mae2 = indice;
				for (CID10Categoria categoria : grupo.getCategorias()) {
					indice ++;
					treeView.addNode(indice, mae2,categoria.getDescrabrev(),  "javascript:getText("+categoria.getIdCid10Categoria()+",3);");
				}
			}
			indice++;			
		}
		
		request.setAttribute("tree", treeView.getTreeView());
		
		return new ModelAndView("process/cid10");
	}

	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">home</a> &raquo; Cl�nica &raquo; <a href=\"/ClinicalSys/clinic/process/CID10\">CID-10</a></p>";
	}
	
	@Action("getTextCID10")
	public void getTextCID10(WebRequestContext context) throws JSONException, IOException{
		String tipo = context.getParameter("tipo");
		/**
		 * ATENCAO, AINDA NAO CONSIDERA O TIPO
		 * 
		 * 
		 * 
		 * 
		 * 
		 */
		String id = context.getParameter("id");
		CID10Categoria c = new CID10Categoria();
		c.setIdCid10Categoria(Integer.parseInt(id));
		c = cID10CategoriaService.load(c);
		List<CID10SubCategoria> subCat = cid10SubCategoriaService.getCid10SubCategoria(c.getCategoria());
		StringBuilder sb = new StringBuilder();
		for (CID10SubCategoria cid10SubCategoria : subCat) {
			if(cid10SubCategoria.getExcluidos()!=null && !cid10SubCategoria.getExcluidos().isEmpty())
				sb.append("<h4>Exclui</h4><span class=\"quote\">	&quot;(").append(cid10SubCategoria.getExcluidos()).append(")&quot;</span>").append("\n");
			sb.append("<span class=\"quote\">	&quot;"+cid10SubCategoria.getDescricao()+"&quot;</span>");
		}
		try {
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("erro", false);
			jsonObj.put("texto", sb.toString());
			context.getServletResponse().setContentType("application/json");
			context.getServletResponse().setCharacterEncoding("ISO-8859-1");
			context.getServletResponse().getWriter().println( jsonObj );
		} catch (Exception e) {
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("erro", true);
			context.getServletResponse().setContentType("application/json");
			context.getServletResponse().setCharacterEncoding("ISO-8859-1");
			context.getServletResponse().getWriter().println( jsonObj );
			e.printStackTrace();
		} 
	}
}

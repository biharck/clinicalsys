package br.com.orionx.clinicalsys.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.types.Cep;
import org.nextframework.types.Cnpj;
import org.nextframework.types.Telefone;
import org.nextframework.validation.annotation.Email;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

import br.com.orionx.clinicalsys.util.log.Log;

@Entity
@Table(name = "operadora")
public class Operadora implements Log{
	
	private Integer idOperadora;
	private Cnpj cnpj;
	private String razaoSocial;
	private String nomeFantasia;
	private Integer registroANS;
	private String suaIdentificacao;
	private VersaoXMLTISS versaoXMLTISS;
	private TabelaTISS tabelaTISS;
	private Telefone telefone;
	private Telefone telefoneAutorizacao;
	private String email;
	private String website;
	private boolean ativo;
	private Arquivo logomarca;
	private List<Plano> planos;
	private Cep cep;
	private TipoLogradouro tipoLogradouro;
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairro;
	private Municipio municipio;
	
	
	public Operadora() {}
	public Operadora(Integer idOperadora) {
		this.idOperadora = idOperadora;
	}
	
	//Log
	private Integer idUsuarioAltera;
	private Timestamp dtAltera;
	
	//TRANSIENT
	private Pais pais;
	private UnidadeFederativa unidadeFederativa;

	//GET
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getIdOperadora() {
		return idOperadora;
	}
	@Required
	public Cnpj getCnpj() {
		return cnpj;
	}
	@Required
	@MinLength(1)
	@MaxLength(200)
	public String getRazaoSocial() {
		return razaoSocial;
	}
	@MinLength(1)
	@MaxLength(200)
	@DescriptionProperty
	public String getNomeFantasia() {
		return nomeFantasia;
	}
	@MinLength(6)
	@MaxLength(6)
	@Required
	public Integer getRegistroANS() {
		return registroANS;
	}
	@MinLength(2)
	@Required
	public String getSuaIdentificacao() {
		return suaIdentificacao;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idVersaoXMLTISS")
	@Required
	public VersaoXMLTISS getVersaoXMLTISS() {
		return versaoXMLTISS;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idTabelaTISS")
	@Required
	public TabelaTISS getTabelaTISS() {
		return tabelaTISS;
	}
	@Required
	public Telefone getTelefone() {
		return telefone;
	}
	@Required
	public Telefone getTelefoneAutorizacao() {
		return telefoneAutorizacao;
	}
	@Email
	@Required
	public String getEmail() {
		return email;
	}
	public String getWebsite() {
		return website;
	}
	public boolean isAtivo() {
		return ativo;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cdfile")
	public Arquivo getLogomarca() {
		return logomarca;
	}
	@OneToMany(mappedBy = "operadora")
	public List<Plano> getPlanos() {
		return planos;
	}
	@Required
	public Cep getCep() {
		return cep;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idTipoLogradouro")
	@Required
	public TipoLogradouro getTipoLogradouro() {
		return tipoLogradouro;
	}
	@MinLength(1)
	@MaxLength(200)
	@Required
	public String getLogradouro() {
		return logradouro;
	}
	@MaxLength(10)
	public String getNumero() {
		return numero;
	}
	@MaxLength(50)
	public String getComplemento() {
		return complemento;
	}
	@MaxLength(50)
	public String getBairro() {
		return bairro;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idMunicipio")
	@Required
	public Municipio getMunicipio() {
		return municipio;
	}
	@Transient
	@Required
	public UnidadeFederativa getUnidadeFederativa() {
		return unidadeFederativa;
	}
	@Transient
	@Required
	public Pais getPais() {
		return pais;
	}
	
	//SET
	public void setIdOperadora(Integer idOperadora) {
		this.idOperadora = idOperadora;
	}
	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}
	public void setRegistroANS(Integer registroANS) {
		this.registroANS = registroANS;
	}
	public void setSuaIdentificacao(String suaIdentificacao) {
		this.suaIdentificacao = suaIdentificacao;
	}
	public void setVersaoXMLTISS(VersaoXMLTISS versaoXMLTISS) {
		this.versaoXMLTISS = versaoXMLTISS;
	}
	public void setTabelaTISS(TabelaTISS tabelaTISS) {
		this.tabelaTISS = tabelaTISS;
	}
	public void setTelefone(Telefone telefone) {
		this.telefone = telefone;
	}
	public void setTelefoneAutorizacao(Telefone telefoneAutorizacao) {
		this.telefoneAutorizacao = telefoneAutorizacao;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	public void setLogomarca(Arquivo logomarca) {
		this.logomarca = logomarca;
	}
	public void setPlanos(List<Plano> planos) {
		this.planos = planos;
	}
	public void setCep(Cep cep) {
		this.cep = cep;
	}
	public void setTipoLogradouro(TipoLogradouro tipoLogradouro) {
		this.tipoLogradouro = tipoLogradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setUnidadeFederativa(UnidadeFederativa unidadeFederativa) {
		this.unidadeFederativa = unidadeFederativa;
	}
	public void setPais(Pais pais) {
		this.pais = pais;
	}
	
	//LOG
	@Override
	public void setIdUsuarioAltera(Integer idUsuarioAltera) {
		this.idUsuarioAltera = idUsuarioAltera;
	}

	@Override
	public void setDtAltera(Timestamp dtAltera) {
		this.dtAltera = dtAltera;
	}

	@Override
	public Integer getIdUsuarioAltera() {
		return idUsuarioAltera;
	}

	@Override
	public Timestamp getDtAltera() {
		return dtAltera;
	}
	
}


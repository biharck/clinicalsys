package br.com.orionx.clinicalsys.bean;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="prescricao")
public class Prescricao {

	private Integer idPrescricao;
	private Paciente paciente;
	private Date dataPrescricao;
	private Time horaPrescricao;
	private List<ItemPrescricao> interacoes;
	private String prescricaoManual;
	

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdPrescricao() {
		return idPrescricao;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idPaciente")
	public Paciente getPaciente() {
		return paciente;
	}
	
	public Date getDataPrescricao() {
		return dataPrescricao;
	}
	public Time getHoraPrescricao() {
		return horaPrescricao;
	}

	@OneToMany(mappedBy="prescricao")
	public List<ItemPrescricao> getInteracoes() {
		return interacoes;
	}
	public String getPrescricaoManual() {
		return prescricaoManual;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}
	public void setInteracoes(List<ItemPrescricao> interacoes) {
		this.interacoes = interacoes;
	}
	public void setIdPrescricao(Integer idPrescricao) {
		this.idPrescricao = idPrescricao;
	}
	public void setHoraPrescricao(Time horaPrescricao) {
		this.horaPrescricao = horaPrescricao;
	}
	public void setDataPrescricao(Date dataPrescricao) {
		this.dataPrescricao = dataPrescricao;
	}
	public void setPrescricaoManual(String prescricaoManual) {
		this.prescricaoManual = prescricaoManual;
	}
}

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>
<n:panel title="${Tdetalhe.detailDysplayName}">
	<n:dataGrid itens="${Tdetalhe.itens}" cellspacing="0" dynaLine="true" id="${Tdetalhe.tableId}"  var="${Tdetalhe.detailVar}" 
			styleClass="data tablefull"
			>
		<n:bean name="${Tdetalhe.detailVar}" valueType="${Tdetalhe.detailClass}" propertyPrefix="${Tdetalhe.fullNestedName}" propertyIndex="${index}">
			<n:getContent tagName="acaoTag" vars="acoes">
				<t:propertyConfig renderAs="column">
					<n:doBody />
				</t:propertyConfig>
				<c:if test="${Tdetalhe.showColunaAcao && !consultar}">
				<n:column header="${Tdetalhe.nomeColunaAcao}" colspan="2">
					${acoes}
					<c:if test="${Tdetalhe.showBotaoRemover}">
						<c:if test="${!propertyConfigDisabled || dataGridDynaline}">
							<a   onclick="excluirLinhaPorNome(this.id);reindexFormPorNome(this.id, forms[0], '${Tdetalhe.fullNestedName}', true)" id="button.excluir[table_id=${Tdetalhe.tableId}, indice=${rowIndex}]">
								<img src="/ClinicalSys/images/remove.png" class="help" alt="Remover" title="Remover" />
							</button>
						</c:if>
						<c:if test="${propertyConfigDisabled && !dataGridDynaline}">	
							<a disabled="disabled" onclick="excluirLinhaPorNome(this.id);reindexFormPorNome(this.id, forms[0], '${Tdetalhe.fullNestedName}', true)" id="button.excluir[table_id=${Tdetalhe.tableId}, indice=${rowIndex}]">
								<img src="/ClinicalSys/images/remove.png" class="help" alt="Remover" title="Remover" />
							</button>						
						</c:if>					
					</c:if>
				</n:column>
				</c:if>
			</n:getContent>
		</n:bean>
	</n:dataGrid>
	<c:if test="${Tdetalhe.showBotaoNovaLinha && !consultar}">
		<c:if test="${empty Tdetalhe.dynamicAttributesMap['labelnovalinha']}">
			<c:set value="Adicionar Registro" scope="page" var="labelnovalinha"/>
		</c:if>
		<c:if test="${!empty Tdetalhe.dynamicAttributesMap['labelnovalinha']}">
			<c:set value="${Tdetalhe.dynamicAttributesMap['labelnovalinha']}" scope="page" var="labelnovalinha"/>
		</c:if>
		
		<c:if test="${!propertyConfigDisabled}">
			<center>
				<a type="button"  onclick="newLine${Tdetalhe.tableId}();${Tdetalhe.onNewLine}">
					<img src="/ClinicalSys/images/add.png" class="help" alt="${labelnovalinha}" title="${labelnovalinha}" />
				</a>
			</center>
		</c:if>
		<c:if test="${propertyConfigDisabled}">
			<center>
				<a  disabled="disabled" onclick="newLine${Tdetalhe.tableId}();${Tdetalhe.onNewLine}">
					<img src="/ClinicalSys/images/add.png" class="help" alt="${labelnovalinha}" title="${labelnovalinha}" />
				<a>
			</center>
		</c:if>
	</c:if>
</n:panel>

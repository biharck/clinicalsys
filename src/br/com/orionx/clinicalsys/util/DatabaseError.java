package br.com.orionx.clinicalsys.util;

import java.sql.SQLException;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;


public class DatabaseError {
	
	public static Boolean isKeyPresent(DataIntegrityViolationException e,String key) {
		ConstraintViolationException exception = (ConstraintViolationException) e.getCause();
		SQLException nextException = exception.getSQLException();
		
		String message = nextException==null?exception.getMessage():nextException.getMessage();
		if (message != null && !"".equals(message)) message = message.toUpperCase();			
		if (key != null && !"".equals(key)) key = key.toUpperCase();
		
		return message.contains(key) || message.contains("DUPLICATE ENTRY");
	}

	/**
	 * <p>M�todo que verifica integridade caso tenha mais de uma chave unique
	 * neste caso deve ser passado a ordem da columa no banco de dados
	 * @param e {@link DataIntegrityViolationException} a exce��o lan�ada
	 * @param key {@link String}  a chave no banco ex: idx_nome_pais
	 * @param orderKey int contendo qual � a ordem na coluna no banco de dados, no caso do mysql come�a em 1 
	 * @return {@link Boolean} falando se houve erro ou n�o
	 * @author biharck
	 * 
	 * 
	 */
	public static Boolean isKeyPresentWithKey(DataIntegrityViolationException e,String key,int orderKey) {
		ConstraintViolationException exception = (ConstraintViolationException) e.getCause();
		SQLException nextException = exception.getSQLException();
		
		String message = nextException==null?exception.getMessage():nextException.getMessage();
		if (message != null && !"".equals(message)) message = message.toUpperCase();			
		if (key != null && !"".equals(key)) key = key.toUpperCase();
		
		return message.contains(key) || (message.contains("DUPLICATE ENTRY") && message.contains("FOR KEY "+orderKey));
	}
	
	
}	

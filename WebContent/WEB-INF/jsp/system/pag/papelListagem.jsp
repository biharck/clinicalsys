<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="cs" uri="clinicalsys"%>


<t:listagem showdelete="false">
	<t:janelaFiltro>
		<t:tabelaFiltro showreport="true">
			<t:property name="nome" renderAs="doubleline" id="nome"/>
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados>
			<n:column header="Nome" order="papel.name">
				${cs:trunc(papel.name,50)}
		  	</n:column>
			<n:column header="Descri��o" order="papel.description">
				${cs:trunc(papel.description,50)}
		  	</n:column>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
<script type="text/javascript">
$(document).ready(function(){
	$('#nome').focus();
});
</script>
package br.com.orionx.clinicalsys.bean;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.types.Cep;
import org.nextframework.types.Cpf;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

import br.com.orionx.clinicalsys.util.log.Inclusao;
import br.com.orionx.clinicalsys.util.log.Log;
/**
*pelo menos um telefone � obrigat�rio para o reenvio de senha do usu�rio...
*/
@Entity
@Table(name = "colaborador")
public class Colaborador implements Log,Inclusao{
	
	private Integer idColaborador;
	private String nome;
	private Sexo sexo;
	private Cpf cpf;
	private String rg;
	private Date dataEmissao;
	private String orgaoExpedidor;
	private String ctps;
	protected String cnh;
	protected UnidadeFederativa ufcnh;
	protected Date dtemissaocnh;
	protected Date dtvalidadecnh;
	protected String tituloeleitoral;
	protected String zonaeleitoral;
	protected String secaoeleitoral;
	protected String documentomilitar;
	protected Integer seriectps;
	protected UnidadeFederativa ufctps;
	protected Date dtEmissaoCtps;
	private String pisPasep;
	private Date dataNascimento;
	private Cep cep;
	private TipoLogradouro tipoLogradouro;
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairro;
	private Municipio municipio;
	private List<TelefoneColaborador> telefones;
	private String email;
	private Usuario usuario;
	private Boolean ativo;
	private Nacionalidade nacionalidade;
	private EstadoCivil estadoCivil;
	private String naturalidade;
	protected String mae;
	protected String pai;
	protected Arquivo foto;
	private String apelido;
	private Boolean permissaoAgenda;
	
	//atividade profissional
	private List<ColaboradorCargo> cargos;
	
	//qualifica��o profissional
	private Escolaridade escolaridade;
	private List<EspecializacaoColaborador> listaEspecializacaoColaboradors;
	
	
	//dados banc�rios
	private Banco banco;
	private String numeroBanco;
	private String numeroAgencia;
	private String dvAgencia;
	private String numeroConta;
	private String digitoConta;
	
	//Log
	private Integer idUsuarioAltera;
	private Timestamp dtAltera;
	
	//Inclusao
	private Integer idUsuarioInclusao;
	private Timestamp dtInclusao;
	
	//TRANSIENT
	private Pais pais;
	private UnidadeFederativa unidadeFederativa;
	private String confirmacaoEmail;
	
	//usado para concatenar as especializacoes do usuario
	private StringBuilder especializacoesConcat;
	
	@Transient
	public StringBuilder getEspecializacoesConcat() {
		especializacoesConcat = new StringBuilder();
		List<EspecializacaoColaborador> esp = getListaEspecializacaoColaboradors();
		if(esp!=null && !esp.isEmpty()){
			for (EspecializacaoColaborador ec : esp) {
				if(ec.getEspecializacao()!=null && ec.getEspecializacao().getNome()!=null)
					especializacoesConcat.append(ec.getEspecializacao().getNome()).append(", ");
			}
			return new StringBuilder( especializacoesConcat.substring(0,especializacoesConcat.length()-2));
		}
		else return especializacoesConcat;
	}
	public void setEspecializacoesConcat(StringBuilder especializacoesConcat) {
		this.especializacoesConcat = especializacoesConcat;
	}

	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdColaborador() {
		return idColaborador;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "idUsuario")
	@ForeignKey(name = "fk_colaborador_usuario")
	public Usuario getUsuario() {
		return usuario;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "idSexo")
	@Required
	@ForeignKey(name = "fk_colaborador_sexo")
	public Sexo getSexo() {
		return sexo;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "idEscolaridade")
	@ForeignKey(name = "fk_colaborador_escolaridade")
	public Escolaridade getEscolaridade() {
		return escolaridade;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "idEstadoCivil")
	@ForeignKey(name = "fk_colaborador_estadocivil")
	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}
	
	
	@Required
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	@Required
	public Cpf getCpf() {
		return cpf;
	}
	@Required
	public String getRg() {
		return rg;
	}
	@Required
	public Date getDataEmissao() {
		return dataEmissao;
	}
	@Required
	public String getOrgaoExpedidor() {
		return orgaoExpedidor;
	}
	public String getCtps() {
		return ctps;
	}
	
	public String getCnh() {
		return cnh;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idUFCNH")
	public UnidadeFederativa getUfcnh() {
		return ufcnh;
	}
	public Date getDtemissaocnh() {
		return dtemissaocnh;
	}
	public Date getDtvalidadecnh() {
		return dtvalidadecnh;
	}	
	public String getTituloeleitoral() {
		return tituloeleitoral;
	}
	public String getZonaeleitoral() {
		return zonaeleitoral;
	}
	public String getSecaoeleitoral() {
		return secaoeleitoral;
	}
	public String getDocumentomilitar() {
		return documentomilitar;
	}
	public Integer getSeriectps() {
		return seriectps;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idUFCTPS")
	public UnidadeFederativa getUfctps() {
		return ufctps;
	}

	public Date getDtEmissaoCtps() {
		return dtEmissaoCtps;
	}
	public String getPisPasep() {
		return pisPasep;
	}
	@Required
	public Date getDataNascimento() {
		return dataNascimento;
	}
	@Required
	public Cep getCep() {
		return cep;
	}
	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idTipoLogradouro")
	public TipoLogradouro getTipoLogradouro() {
		return tipoLogradouro;
	}
	@Required
	public String getLogradouro() {
		return logradouro;
	}
	public String getNumero() {
		return numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public String getBairro() {
		return bairro;
	}
	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idMunicipio")
	public Municipio getMunicipio() {
		return municipio;
	}
	@OneToMany(mappedBy = "colaborador")
	public List<TelefoneColaborador> getTelefones() {
		return telefones;
	}
	@OneToMany(mappedBy = "colaborador")
	public List<EspecializacaoColaborador> getListaEspecializacaoColaboradors() {
		return listaEspecializacaoColaboradors;
	}
	
	@Required
	public String getEmail() {
		return email;
	}
	@Required
	public Boolean getAtivo() {
		return ativo;
	}
	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idNacionalidade")
	public Nacionalidade getNacionalidade() {
		return nacionalidade;
	}
	@Required
	@MaxLength(100)
	@MinLength(1)
	public String getNaturalidade() {
		return naturalidade;
	}
	@Required
	public String getMae() {
		return mae;
	}
	public String getPai() {
		return pai;
	}
	@ManyToOne
	@JoinColumn(name = "id")
	public Arquivo getFoto() {
		return foto;
	}
	@MaxLength(50)
	@Required
	public String getApelido() {
		return apelido;
	}
	public Boolean getPermissaoAgenda() {
		return permissaoAgenda;
	}
	@OneToMany(mappedBy = "colaborador")
	public List<ColaboradorCargo> getCargos() {
		return cargos;
	}
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idBanco")
	public Banco getBanco() {
		return banco;
	}
	public String getNumeroBanco() {
		return numeroBanco;
	}
	@MaxLength(4)
	@MinLength(4)
	public String getNumeroAgencia() {
		return numeroAgencia;
	}
	@MaxLength(1)
	public String getDvAgencia() {
		return dvAgencia;
	}
	public String getNumeroConta() {
		return numeroConta;
	}
	@MaxLength(1)
	public String getDigitoConta() {
		return digitoConta;
	}
	@Required
	@Transient
	public Pais getPais() {
		return pais;
	}
	@Required
	@Transient
	public UnidadeFederativa getUnidadeFederativa() {
		return unidadeFederativa;
	}
	@Required
	public String getConfirmacaoEmail() {
		return confirmacaoEmail;
	}
	
	//SET
	public void setIdColaborador(Integer idColaborador) {
		this.idColaborador = idColaborador;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}
	public void setEscolaridade(Escolaridade escolaridade) {
		this.escolaridade = escolaridade;
	}
	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	

	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCpf(Cpf cpf) {
		this.cpf = cpf;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public void setDataEmissao(Date dataEmissao) {
		this.dataEmissao = dataEmissao;
	}
	public void setOrgaoExpedidor(String orgaoExpedidor) {
		this.orgaoExpedidor = orgaoExpedidor;
	}
	public void setCtps(String ctps) {
		this.ctps = ctps;
	}
	public void setCnh(String cnh) {
		this.cnh = cnh;
	}
	public void setUfcnh(UnidadeFederativa ufcnh) {
		this.ufcnh = ufcnh;
	}
	public void setDtemissaocnh(Date dtemissaocnh) {
		this.dtemissaocnh = dtemissaocnh;
	}
	public void setDtvalidadecnh(Date dtvalidadecnh) {
		this.dtvalidadecnh = dtvalidadecnh;
	}
	public void setTituloeleitoral(String tituloeleitoral) {
		this.tituloeleitoral = tituloeleitoral;
	}
	public void setZonaeleitoral(String zonaeleitoral) {
		this.zonaeleitoral = zonaeleitoral;
	}
	public void setSecaoeleitoral(String secaoeleitoral) {
		this.secaoeleitoral = secaoeleitoral;
	}
	public void setDocumentomilitar(String documentomilitar) {
		this.documentomilitar = documentomilitar;
	}
	public void setSeriectps(Integer seriectps) {
		this.seriectps = seriectps;
	}
	public void setUfctps(UnidadeFederativa ufctps) {
		this.ufctps = ufctps;
	}
	public void setDtEmissaoCtps(Date dtEmissaoCtps) {
		this.dtEmissaoCtps = dtEmissaoCtps;
	}
	public void setPisPasep(String pisPasep) {
		this.pisPasep = pisPasep;
	}
	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public void setCep(Cep cep) {
		this.cep = cep;
	}
	public void setTipoLogradouro(TipoLogradouro tipoLogradouro) {
		this.tipoLogradouro = tipoLogradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setTelefones(List<TelefoneColaborador> telefones) {
		this.telefones = telefones;
	}
	public void setListaEspecializacaoColaboradors(List<EspecializacaoColaborador> listaEspecializacaoColaboradors) {
		this.listaEspecializacaoColaboradors = listaEspecializacaoColaboradors;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setNacionalidade(Nacionalidade nacionalidade) {
		this.nacionalidade = nacionalidade;
	}
	public void setNaturalidade(String naturalidade) {
		this.naturalidade = naturalidade;
	}
	public void setMae(String mae) {
		this.mae = mae;
	}
	public void setPai(String pai) {
		this.pai = pai;
	}
	public void setFoto(Arquivo foto) {
		this.foto = foto;
	}
	public void setApelido(String apelido) {
		this.apelido = apelido;
	}
	public void setPermissaoAgenda(Boolean permissaoAgenda) {
		this.permissaoAgenda = permissaoAgenda;
	}
	public void setCargos(List<ColaboradorCargo> cargos) {
		this.cargos = cargos;
	}
	
	public void setBanco(Banco banco) {
		this.banco = banco;
	}
	public void setNumeroBanco(String numeroBanco) {
		this.numeroBanco = numeroBanco;
	}
	public void setDvAgencia(String dvAgencia) {
		this.dvAgencia = dvAgencia;
	}
	public void setNumeroAgencia(String numeroAgencia) {
		this.numeroAgencia = numeroAgencia;
	}
	public void setNumeroConta(String numeroConta) {
		this.numeroConta = numeroConta;
	}
	public void setDigitoConta(String digitoConta) {
		this.digitoConta = digitoConta;
	}
	public void setPais(Pais pais) {
		this.pais = pais;
	}
	public void setUnidadeFederativa(UnidadeFederativa unidadeFederativa) {
		this.unidadeFederativa = unidadeFederativa;
	}
	public void setConfirmacaoEmail(String confirmacaoEmail) {
		this.confirmacaoEmail = confirmacaoEmail;
	}
	//LOG
	@Override
	public void setIdUsuarioAltera(Integer idUsuarioAltera) {
		this.idUsuarioAltera = idUsuarioAltera;
	}

	@Override
	public void setDtAltera(Timestamp dtAltera) {
		this.dtAltera = dtAltera;
	}

	@Override
	public Integer getIdUsuarioAltera() {
		return idUsuarioAltera;
	}

	@Override
	public Timestamp getDtAltera() {
		return dtAltera;
	}
	
	//INCLUSAO
	@Override
	public void setIdUsuarioInclusao(Integer idUsuarioInclusao) {
		this.idUsuarioInclusao = idUsuarioInclusao;
	}

	@Override
	public void setDtInclusao(Timestamp dtInclusao) {
		this.dtInclusao = dtInclusao;
	}

	@Override
	public Integer getIdUsuarioInclusao() {
		return idUsuarioInclusao;
	}

	@Override
	public Timestamp getDtInclusao() {
		return dtInclusao;
	}

}

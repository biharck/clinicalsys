<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="cs" uri="clinicalsys"%>

<c:set var="ultimoid"   value="${idUsuarioAltera}"/>
<c:set var="dataAlt"   value="${dtAltera}"/>
<div class="onecolumn">
	<div class="header">
		<span>Cl�nica ${clinica.nomeFantasia}</span>
	</div>
	<br class="clear"/>
	<div class="content">
		<t:tela>
			<div class="linkBar" align="right">
				<n:submit action="salvar" validate="true" class="noboard" ><img src="${application}/images/shortcut/save.png" alt="Salvar" class="help" title="Salvar"/></n:submit>
			</div>
			<div id="ErrosSubmissao"></div>
			<div align="center" class="alert_info" style="display:none;position:absolute;width:400px; margin: 0px 30%; text-align: center;" id="carregandoAjax">Carregando...</div>
			<n:bean name="clinica">
				<t:property name="idClinica" renderAs="doubleline" type="hidden" showLabel="false" write="false" label=""/>
				<t:tabelaEntrada  colspan="2" style="width:100%">
					<n:panelGrid columns="6">
						<t:property name="cnpj" renderAs="doubleline" id="cnpj" colspan="6" style="width:150px;" onchange="validateCnpjOver(this)"/>
						<t:property name="razaoSocial"  renderAs="doubleline" colspan="6" style="width:500px"/>
						<t:property name="nomeFantasia" renderAs="doubleline" colspan="6" style="width:500px"/>
						<t:property name="inscricaoMunicipal" renderAs="doubleline" colspan="3" style="width:250px"/>
						<t:property name="inscricaoEstadual" renderAs="doubleline" colspan="3" style="width:250px"/>
						<t:property name="email" renderAs="doubleline" colspan="6" onchange="validaEmailFormulario(this)" style="width:500px" />
						<t:property name="emailAlternativo" renderAs="doubleline" colspan="6" style="width:500px" onchange="validaEmailFormulario(this)"/>
						<t:property name="twitter" renderAs="doubleline" colspan="3" style="width:250px"/>
						<t:property name="webSite" renderAs="doubleline" colspan="3" style="width:250px"/>
					</n:panelGrid>
					<n:panel id="picture" style="text-align:center;">
						<c:if test="${param.ACAO != 'criar'}">
							<c:if test="${showpicture}">
								<n:link url="/DOWNLOADFILE/${clinica.logomarca.cdfile}">
									<img src="${pageContext.request.contextPath}/DOWNLOADFILE/${clinica.logomarca.cdfile}" id="imagem" class="foto"/>
								</n:link>
							</c:if>
							<c:if test="${!showpicture}">
								<div class="image">
									<img src="${pageContext.request.contextPath}/images/imagenotfound.png" id="imagem" style="padding-top:13px;"/>
								</div>
							</c:if>
							<c:if test="${param.ACAO == 'criar' || param.ACAO == 'editar'}">
								<br>							
								
							</c:if>
						</c:if>
						<t:property name="logomarca"  id="foto" label="" renderAs="doubleline" onchange="verifyExt();" />
					</n:panel>
					<n:panel colspan="2"><span class="titulo">Endere�o</span></n:panel>
					<n:panelGrid columns="6" colspan="2">
						<t:property name="cep" id="cep" renderAs="doubleline" colspan="1" onchange="ajaxBuscaCEP(this.value)" style="width:100px;"/>
						<n:panel colspan="1" id="modalLink">
							<label for="btn_busca_end">N�o sabe o CEP?</label><br>
							<a id="btn_busca_end"  href="javascript:openDialogBuscaCEP()"><img src="/ClinicalSys/images/shortcut/find-help.png"/> Clique aqui</a>
						</n:panel>
						<n:panel colspan="4"></n:panel>
						<t:property name="tipoLogradouro" renderAs="doubleline" colspan="2" id="tipoLogradouro"/>
						<t:property name="logradouro" renderAs="doubleline" colspan="3" style="width:400px;" id="logradouro"/>
						<t:property name="numero" renderAs="doubleline" colspan="2" id="numero"/>
						<t:property name="complemento" renderAs="doubleline" colspan="2" style="width:100px;"/>
						<t:property name="bairro" renderAs="doubleline" colspan="2" id="bairro" style="width:200px;"/>
						<n:comboReloadGroup useAjax="true">
							<t:property name="pais" renderAs="doubleline" colspan="2" onchange="openAguardeAjax()" id="pais"/>
							<t:property name="unidadeFederativa" renderAs="doubleline" colspan="2" onchange="openAguardeAjax()" id="uf"/>
							<t:property name="municipio" renderAs="doubleline" colspan="2" id="cidade"/>
						</n:comboReloadGroup>
					</n:panelGrid>
					<n:panel colspan="1"><span class="titulo">Telefones</span></n:panel>
					<n:panel colspan="1"><span class="titulo">Ramais</span></n:panel>
					<n:panelGrid columns="2" >
						<t:detalhe name="telefones" >
							<t:property name="telefoneTipo"/>
							<t:property name="telefone"/>
						</t:detalhe>
					</n:panelGrid>
					<t:detalhe name="ramais">
						<t:property name="numero"/>
						<t:property name="localizacao"/>
					</t:detalhe>
				</t:tabelaEntrada>
				<br><br>
			</n:bean>
			<div class="linkBar" align="right">
				<n:submit action="salvar" validate="true" url="/system/process/Clinica" class="noboard" ><img src="${application}/images/shortcut/save.png" alt="Salvar" class="help" title="Salvar"/></n:submit>
			</div>
			<div class="ultimaalteracao">
				<n:panel>�ltima altera��o neste registro foi feita por <font style="color:#E77272">${cs:getuserbyid(ultimoid)}</font> no dia <font style="color:#E77272">${cs:dateformat(dataAlt)}</font> �s <font style="color:#E77272">${cs:hourformat(dataAlt)}</font> </n:panel>
			</div>
		</t:tela>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('#cnpj').focus();
});

</script>
package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name = "banco")
public class Banco {
	
	protected Integer idBanco;
	protected String nome;
	protected Integer numero;
	protected Boolean ativo;

	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdBanco() {
		return idBanco;
	}
	
	@Required
	@MaxLength(50)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	@MaxLength(3)
	@Required
	public Integer getNumero() {
		return numero;
	}

	public Boolean getAtivo() {
		return ativo;
	}
	
	
	//SET
	public void setIdBanco(Integer idBanco) {
		this.idBanco = idBanco;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Banco) {
			Banco banco = (Banco) obj;
			return this.getIdBanco().equals(banco.getIdBanco());
		}
		return super.equals(obj);
	}
	

}

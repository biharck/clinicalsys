package br.com.orionx.clinicalsys.filtros;

import br.com.orionx.clinicalsys.util.generics.FiltroListagem;

public class ClassificacaoMedicacaoFiltro extends FiltroListagem{

	private String nome;
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
}

package br.com.orionx.clinicalsys.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.orionx.clinicalsys.bean.DoencasDaInfancia;
import br.com.orionx.clinicalsys.bean.HistoriaPregressa;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class DoencasDaInfanciaDAO extends GenericDAO<DoencasDaInfancia> {

	
	public List<DoencasDaInfancia> findByHistoriaPregressa(HistoriaPregressa hp){
		if(hp.getIdHistoriaPregressa() == null){
			return new ArrayList<DoencasDaInfancia>();
		}
		return query()
				.select("doencasDaInfancia")
				.from(DoencasDaInfancia.class)
				.leftOuterJoin("doencasDaInfancia.doencasDaInfanciaPacientes di")
				.where("di.historiaPregressa = ?", hp)
				.list();
	}
	
	
}

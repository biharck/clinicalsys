package br.com.orionx.clinicalsys.filtros;

import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;

import br.com.orionx.clinicalsys.bean.Pais;
import br.com.orionx.clinicalsys.util.generics.FiltroListagem;

public class UnidadeFederativaFiltro extends FiltroListagem{

	private Pais pais;
	private String nome;
	
	//GET
	public Pais getPais() {
		return pais;
	}
	@MinLength(2)
	@MaxLength(100)
	public String getNome() {
		return nome;
	}
	//SET
	public void setPais(Pais pais) {
		this.pais = pais;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}

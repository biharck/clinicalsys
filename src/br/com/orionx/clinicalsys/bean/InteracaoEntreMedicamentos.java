package br.com.orionx.clinicalsys.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name = "interacaoentremedicamentos")
/**
 * Faz a rela��o n p/ n entre medicamentos e interacaoMedicamentosa
 */
public class InteracaoEntreMedicamentos {
	
	private Integer idInteracaoEntreMedicamentos;
	private Medicamento medicamento;
	private InteracaoMedicamentosa interacaoMedicamentosa;
	private String consideracao;
	private ClassificacaoMedicacao classificacaoMedicacao;
	
	
	//GET
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getIdInteracaoEntreMedicamentos() {
		return idInteracaoEntreMedicamentos;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idMedicamento")
	@Required
	public Medicamento getMedicamento() {
		return medicamento;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idInteracaoMedicamentosa")
	public InteracaoMedicamentosa getInteracaoMedicamentosa() {
		return interacaoMedicamentosa;
	}
	@MaxLength(4000)
	@Column(length=4000)
	public String getConsideracao() {
		return consideracao;
	}
	@Transient
	public ClassificacaoMedicacao getClassificacaoMedicacao() {
		return classificacaoMedicacao;
	}
	
	//SET
	public void setIdInteracaoEntreMedicamentos(Integer idInteracaoEntreMedicamentos) {
		this.idInteracaoEntreMedicamentos = idInteracaoEntreMedicamentos;
	}
	public void setMedicamento(Medicamento medicamento) {
		this.medicamento = medicamento;
	}
	public void setInteracaoMedicamentosa(InteracaoMedicamentosa interacaoMedicamentosa) {
		this.interacaoMedicamentosa = interacaoMedicamentosa;
	}
	public void setConsideracao(String consideracao) {
		this.consideracao = consideracao;
	}
	public void setClassificacaoMedicacao(
			ClassificacaoMedicacao classificacaoMedicacao) {
		this.classificacaoMedicacao = classificacaoMedicacao;
	}
	
	

}

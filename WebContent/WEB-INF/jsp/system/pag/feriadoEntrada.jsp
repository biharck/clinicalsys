<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<t:property name="idFeriado" type="hidden" label="" write="false"/>
	<t:janelaEntrada>
		<t:tabelaEntrada columns="2" >
			<t:property name="nome" id="nome" renderAs="doubleline" style="width:300px" colspan="4"/>
			<t:property name="data" renderAs="doubleline" datepicker="true" type="text" maxlength="10" style="width:90px;" colspan="4"/>
			<t:property name="tipoFeriado" trueFalseNullLabels="Nacional,Estadual,Municipal" type="select-one-radio" colspan="4"/>
			<t:property name="facultativo" renderAs="doubleline"/>
			<t:property name="sempre" renderAs="doubleline"/>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>
<script type="text/javascript">
	$(document).ready(function(){
		$('#nome').focus();
	});
</script>
package br.com.orionx.clinicalsys.service;

import java.util.List;

import br.com.orionx.clinicalsys.bean.Papel;
import br.com.orionx.clinicalsys.bean.Permissao;
import br.com.orionx.clinicalsys.dao.PermissaoDAO;
import br.com.orionx.clinicalsys.util.generics.GenericService;

public class PermissaoService extends GenericService<Permissao> {

	private PermissaoDAO permissaoDAO;
	public void setPermissaoDAO(PermissaoDAO permissaoDAO) {
		this.permissaoDAO = permissaoDAO;
	}
	
	public List<Permissao> getPermissoesUsuario(Papel papel){
		return permissaoDAO.getPermissoesUsuario(papel); 
	}
}

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"> 
<%@page import="br.com.orionx.clinicalsys.util.ClinicalSysUtil"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"> 
 
<!-- Website Title --> 
<title>ClinicalSys</title>

<!-- Meta data for SEO -->
<meta name="description" content="">
<meta name="keywords" content="">

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<!-- Template stylesheet -->
<link href="${application}/css/blue/screen.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/blue/datepicker.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/tipsy.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/js/visualize/visualize.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/js/jwysiwyg/jquery.wysiwyg.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/js/fancybox/jquery.fancybox-1.3.0.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/tipsy.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/autocomplete.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/default.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/ui.theme.css" rel="stylesheet" type="text/css" media="all">

</head>
<body class="nobg">

<div class="content_wrapper">

	<!-- Begin header -->
	<div id="header">
		<div id="logo">
			<img src="${application}/images/logo.png" alt="logo"/>
		</div>
		
		<div id="account_info">
			<img src="${application}/images/icon_online.png" alt="Online" class="mid_align"/>
			Ol� <a href="">Dr. <%= ClinicalSysUtil.getUsuarioLogado().getLogin() %></a>| <a href="${application}/system/Logout">Logout</a>
		</div>
	</div>
	<!-- End header -->
	
	
	<!-- Begin content -->
	<div id="content" style="margin-left:30px;">
		<div class="inner">
			
			<br class="clear"/>
			<div class="messageOuterDiv">
				
				<div class="divError" >
					<center><img src="/ClinicalSys/images/shortcut/lock.png"></center>
					<font class="ops"><b>Ups!</b></font><br>
					Voc� n�o tem permiss�o para acessar este conte�do.<br> Qualquer eventual d�vida entre em contato
					com o administrador do sistema.<br><br>
					<a href="javascript:history.back(1);"> Clique aqui para voltar</a>
				</div>
			</div>

		<!-- Begin footer -->
		<div id="footer">
			 Copyright&copy; by OrionX Technologies
		</div>
		<!-- End footer -->
		
		
	</div>
	<!-- End content -->
</div>

<!-- ui-dialog -->
</script>
<style type="text/css">
	.divError{
		border: 1px solid #dadada;
		width: 400px;
		height: 220px;
		text-align: center;
		margin-left: 400px;
		border-radius: 10px 10px 10px 10px;		
		
	}
	.ops{
		font-size: 16px;
	}
	
</style>
</body>
</html>
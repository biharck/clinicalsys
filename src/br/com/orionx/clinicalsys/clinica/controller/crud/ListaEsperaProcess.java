package br.com.orionx.clinicalsys.clinica.controller.crud;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.view.DownloadFileServlet;
import org.nextframework.view.ajax.View;
import org.springframework.web.servlet.ModelAndView;

import br.com.orionx.clinicalsys.bean.AgendamentoConsulta;
import br.com.orionx.clinicalsys.dao.ArquivoDAO;
import br.com.orionx.clinicalsys.service.AgendamentoConsultaService;
import br.com.orionx.clinicalsys.util.EnumSituacoesAgendamento;

@Controller(path="/clinic/process/listaEspera", authorizationModule=ProcessAuthorizationModule.class)
public class ListaEsperaProcess extends MultiActionController {

	private AgendamentoConsultaService agendamentoConsultaService;
	private ArquivoDAO arquivoDAO;
	
	public void setAgendamentoConsultaService(AgendamentoConsultaService agendamentoConsultaService) {
		this.agendamentoConsultaService = agendamentoConsultaService;
	}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request){
		request.setAttribute("pathURL", getPathURL());
		List<AgendamentoConsulta> aguardandoAtendimento = new ArrayList<AgendamentoConsulta>();
		List<AgendamentoConsulta> consultaAgendadaMasNaoChegou = new ArrayList<AgendamentoConsulta>();
		List<AgendamentoConsulta> desistencia = new ArrayList<AgendamentoConsulta>();
		List<AgendamentoConsulta> todasConsultas = agendamentoConsultaService.getConsultasAgendadasDiaCorrente();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		for (AgendamentoConsulta agendamentoConsulta : todasConsultas) {
			agendamentoConsulta.setNascimento(format.format(agendamentoConsulta.getDia()));
			if(agendamentoConsulta.getPaciente().getFotoPaciente() != null){
				arquivoDAO.loadAsImage(agendamentoConsulta.getPaciente().getFotoPaciente());
				DownloadFileServlet.addCdfile(request.getSession(), agendamentoConsulta.getPaciente().getFotoPaciente().getCdfile());
			}
			if(agendamentoConsulta.getColaborador().getFoto() != null){
				arquivoDAO.loadAsImage(agendamentoConsulta.getColaborador().getFoto());
				DownloadFileServlet.addCdfile(request.getSession(), agendamentoConsulta.getColaborador().getFoto().getCdfile());
			}
		}
		for (AgendamentoConsulta ac : todasConsultas) {
			ac.setSituacaoDESC(EnumSituacoesAgendamento.getEnumById(ac.getSituacao()).getDescricao());
			ac.setSituacaoURL(EnumSituacoesAgendamento.getEnumById(ac.getSituacao()).getImg());
			if(ac.getSituacao() == 1 || ac.getSituacao() == 2)
				consultaAgendadaMasNaoChegou.add(ac);
			if(ac.getSituacao() == 3)
				aguardandoAtendimento.add(ac);
			if(ac.getSituacao() == 5)
				desistencia.add(ac);
		}
		
		request.setAttribute("aguardandoAtendimento", aguardandoAtendimento);
		request.setAttribute("naoChegou", consultaAgendadaMasNaoChegou );
		request.setAttribute("desistencia", desistencia);
		
		
		
		return new ModelAndView("process/listaEspera");
	}
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Cl�nica &raquo; <a href=\"/ClinicalSys/system/process/listaEspera\">Lista de Espera</a></p>";
	}
	
	public void updateListaEspera(WebRequestContext request) throws JSONException, IOException{
		JSONObject json = new JSONObject();
		try {
			Integer idAtendimento = Integer.parseInt(getParameter("idAtendimento"));
			Integer idStatus = Integer.parseInt(getParameter("idStatus"));
			AgendamentoConsulta ac = agendamentoConsultaService.getConsultaAgendadaById(idAtendimento);
			ac.setSituacao(idStatus);
			agendamentoConsultaService.saveOrUpdate(ac);
			json.put("msg", "Registro Atualizado com Sucesso!");
			
		}catch (Exception e) {
			e.printStackTrace();
			json.put("msg", "Ups! Houve um pequeno problema ao tentar atualizar este registro, tente novamente por favor!");
		}
		request.getServletResponse().getWriter().println( json );
		request.getServletResponse().setContentType("application/json");
		request.getServletResponse().setCharacterEncoding("ISO-8859-1");
	}
}

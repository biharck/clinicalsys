package br.com.orionx.clinicalsys.filtros;

import br.com.orionx.clinicalsys.util.generics.FiltroListagem;

public class ViaAdministracaoFiltro extends FiltroListagem{

	private String nome;
	private String sigla;
	
	public String getNome() {
		return nome;
	}
	public String getSigla() {
		return sigla;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
}

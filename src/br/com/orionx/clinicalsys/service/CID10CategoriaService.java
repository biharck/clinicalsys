package br.com.orionx.clinicalsys.service;

import java.util.List;

import br.com.orionx.clinicalsys.bean.CID10Categoria;
import br.com.orionx.clinicalsys.dao.CID10CategoriaDAO;
import br.com.orionx.clinicalsys.util.generics.GenericService;

public class CID10CategoriaService extends GenericService<CID10Categoria>{

	private CID10CategoriaDAO cID10categoriaDAO;
	
	public void setCID10categoriaDAO(CID10CategoriaDAO cid10categoriadao) {
		cID10categoriaDAO = cid10categoriadao;
	}
	
	/**
	 * <p>M�todo de refer�ncia ao dao
	 * @param catInic categoria inicial {@link String}
	 * @param catFim categoria final {@link String}
	 * @see br.com.orionx.clinicalsys.dao.CID10CategoriaDAO#getGruposByCapitulo
	 * @return {@link List} {@link CID10Categoria}
	 */
	public List<CID10Categoria> getCategoriasByGrupos(String catInic, String catFim){
		return cID10categoriaDAO.getCategoriasByGrupos(catInic, catFim);
	}
	
}

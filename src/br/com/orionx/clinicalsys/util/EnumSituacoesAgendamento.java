package br.com.orionx.clinicalsys.util;

public enum EnumSituacoesAgendamento {

	AGUARDANDO_CONFIRMACAO(1,"Aguardando Confrima��o","AGUARDANDO_CONFIRMACAO","/ClinicalSys/images/waiting.png"),
	CONFIRMADO(2,"Confirmado","CONFIRMADO","/ClinicalSys/images/confirmed.png"),
	AGUARDANDO_ATENDIMENTO(3,"Aguardando Atendimento","AGUARDANDO_ATENDIMENTO","/ClinicalSys/images/waiting_room.png"),
	ATENDIDO(4,"Atendido","ATENDIDO","/ClinicalSys/images/atendido.png"),
	CANCELADO(5,"Cancelado","CANCELADO","/ClinicalSys/images/remove.png"),
	NAO_COMPARECEU(6,"N�o Compareceu","NAO_COMPARECEU","/ClinicalSys/images/n_apareceu.png");
	
	private Integer idSituacao;
	private String descricao;
	private String valueCombo;
	private String img;
	
	private EnumSituacoesAgendamento(Integer idSituacao, String descricao,String valueCombo,String img){
		this.idSituacao = idSituacao;
		this.descricao = descricao;
		this.valueCombo = valueCombo;
		this.img = img;
	}
	
	public Integer getIdSituacao() {
		return idSituacao;
	}
	public String getDescricao() {
		return descricao;
	}
	public String getValueCombo() {
		return valueCombo;
	}
	public String getImg() {
		return img;
	}
	
	public static EnumSituacoesAgendamento getEnumById(int id){
		switch (id) {
		case 1:
			return EnumSituacoesAgendamento.AGUARDANDO_CONFIRMACAO;
		case 2:
			return EnumSituacoesAgendamento.CONFIRMADO;
		case 3:
			return EnumSituacoesAgendamento.AGUARDANDO_ATENDIMENTO;
		case 4:
			return EnumSituacoesAgendamento.ATENDIDO;
		case 5:
			return EnumSituacoesAgendamento.CANCELADO;
		case 6:
			return EnumSituacoesAgendamento.NAO_COMPARECEU;

		}
		return null;
	}
	
	public static EnumSituacoesAgendamento getEnumByValue(String value){
		if(value.equals("AGUARDANDO_CONFIRMACAO"))
			return EnumSituacoesAgendamento.AGUARDANDO_CONFIRMACAO;
		if(value.equals("CONFIRMADO"))
			return EnumSituacoesAgendamento.CONFIRMADO;
		if(value.equals("AGUARDANDO_ATENDIMENTO"))
			return EnumSituacoesAgendamento.AGUARDANDO_ATENDIMENTO;
		if(value.equals("ATENDIDO"))
			return EnumSituacoesAgendamento.ATENDIDO;
		if(value.equals("CANCELADO"))
			return EnumSituacoesAgendamento.CANCELADO;
		if(value.equals("NAO_COMPARECEU"))
			return EnumSituacoesAgendamento.NAO_COMPARECEU;

		return null;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
	
}

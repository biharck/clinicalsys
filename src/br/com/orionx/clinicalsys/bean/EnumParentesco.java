package br.com.orionx.clinicalsys.bean;

public enum EnumParentesco {

	PAI(1,"Pai"),
	MAE(2,"M�e"),
	IRMAO_IRMA(3,"Irm�o(�)"),
	TIO_TIA(4,"Tio(a)"),
	AVO_AVOH(5,"Av�(�)"),
	PRIMO_PRIMA(6,"Primo(a)"),
	TIO_AVO_TIO_AVOH(7,"Tio Av�(�)"),
	MAE_E_PAI(8,"M�e e Pai");
	
	private Integer id;
	private String nome;
	
	
	private EnumParentesco(Integer id, String nome){
		this.id = id;
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}
	public Integer getId() {
		return id;
	}
	
	@Override
	public String toString() {
		return getNome();
	}
}

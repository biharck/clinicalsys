package br.com.orionx.clinicalsys.dao;

import java.util.ArrayList;
import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.QueryBuilder;
import org.nextframework.persistence.SaveOrUpdateStrategy;
import org.nextframework.util.CollectionsUtil;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import br.com.orionx.clinicalsys.bean.ClassificacaoMedicacao;
import br.com.orionx.clinicalsys.bean.Medicamento;
import br.com.orionx.clinicalsys.filtros.MedicamentoFiltro;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class MedicamentoDAO extends GenericDAO<Medicamento>{
	
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Medicamento> query) {
		query.select("medicamento.idMedicamento,medicamento.contraIndicacoes, medicamento.principioAtivo, medicamento.descricaoDroga, medicamento.efeitosColateraisComuns," +
				"medicamento.efeitosColateraisMenosFrequente,ncd.idNomeComercialDroga,ncd.nomeComercial,apresentacao.idApresentacaoMedicacao, apresentacao.nome,ncd.dosagem," +
				"ncd.qtdDisponivelEmbalagem, ncd.generico,cm.idClassificacaoMedicacao,cm.nome,ied.idIndicacaoEspecificaDose," +
				"ied.indicacao, ied.doseAdultos, ied.doseCriancas, ied.nivelSericoDisfRenalHepa, ied.consideracoesRecomendacoes,medicamento.nivelSericoDisfRenalHepa, medicamento.consideracoesRecomendacoes," +
				"medicamento.idUsuarioAltera, medicamento.dtAltera,bula.cdfile,bula.contenttype,bula.name.bula.size,bula.date," +
				"bula.content,medicamento.ativo,if.idIndustriaFarmaceutica, if.nome")
		.leftOuterJoin("medicamento.classificacaoMedicacao cm")
		.leftOuterJoin("medicamento.nomesComerciaisDroga ncd")
		.leftOuterJoin("ncd.industriaFarmaceutica if")
		.leftOuterJoin("medicamento.bula bula")
		.leftOuterJoin("ncd.apresentacao apresentacao")
		.leftOuterJoin("medicamento.indicacoesEspecificasEDoses ied");
	}
	
	@Override
	public Medicamento loadForEntrada(Medicamento bean) {
		return query().select("medicamento.idMedicamento,medicamento.contraIndicacoes, medicamento.principioAtivo, medicamento.descricaoDroga, medicamento.efeitosColateraisComuns," +
				"medicamento.efeitosColateraisMenosFrequente,ncd.idNomeComercialDroga,ncd.nomeComercial,apresentacao.idApresentacaoMedicacao, apresentacao.nome,ncd.dosagem," +
				"ncd.qtdDisponivelEmbalagem, ncd.generico,cm.idClassificacaoMedicacao,cm.nome,ied.idIndicacaoEspecificaDose," +
				"ied.indicacao, ied.doseAdultos, ied.doseCriancas, ied.nivelSericoDisfRenalHepa, ied.consideracoesRecomendacoes,medicamento.nivelSericoDisfRenalHepa, medicamento.consideracoesRecomendacoes," +
				"medicamento.idUsuarioAltera, medicamento.dtAltera,bula.cdfile,bula.contenttype,bula.name,bula.size," +
				"bula.content,medicamento.ativo,if.idIndustriaFarmaceutica, if.nome")
		.leftOuterJoin("medicamento.classificacaoMedicacao cm")
		.leftOuterJoin("medicamento.nomesComerciaisDroga ncd")
		.leftOuterJoin("ncd.industriaFarmaceutica if")
		.leftOuterJoin("medicamento.bula bula")
		.leftOuterJoin("medicamento.indicacoesEspecificasEDoses ied")
		.leftOuterJoin("ncd.apresentacao apresentacao")
		.where("medicamento.idMedicamento=?",bean.getIdMedicamento())
		.unique();
		
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("nomesComerciaisDroga");
		save.saveOrUpdateManaged("indicacoesEspecificasEDoses");
	}
	
	
	@Override
	public void updateListagemQuery(QueryBuilder<Medicamento> query,FiltroListagem _filtro) {
		MedicamentoFiltro filtro = (MedicamentoFiltro) _filtro;
		if(filtro.getAtivoEnum()!=null)
			if(filtro.getAtivoEnum().getSituacao()!=null && (filtro.getAtivoEnum().getSituacao() || !filtro.getAtivoEnum().getSituacao()))
				query.where("medicamento.ativo is "+filtro.getAtivoEnum().getSituacao());

		query.whereLikeIgnoreAll("medicamento.principioAtivo",filtro.getPrincipioAtivo())
		.whereLikeIgnoreAll("medicamento.descricaoDroga",filtro.getDescricaoDroga())
		.leftOuterJoin("medicamento.nomesComerciaisDroga ncd")
		.whereLikeIgnoreAll("ncd.nomeComercial", filtro.getNomeComercial());
		super.updateListagemQuery(query, filtro);
	}
	
	/**
	 * <p>M�todo usado para retornar todos os medicamentos de uma determinada classifica��o de medicamentos
	 * @param classificacaoMedicacao {@link ClassificacaoMedicacao}
	 * @return {@link List} {@link Medicamento}
	 * @author Biharck
	 */
	public List<Medicamento> getMedicamentosByClassif(ClassificacaoMedicacao classificacaoMedicacao){
		return query().select("medicamento.idMedicamento,medicamento.contraIndicacoes, medicamento.principioAtivo, medicamento.descricaoDroga, medicamento.efeitosColateraisComuns," +
				"medicamento.efeitosColateraisMenosFrequente,ncd.idNomeComercialDroga,ncd.nomeComercial,ncd.industriaFarmaceutica,apresentacao.idApresentacaoMedicacao, apresentacao.nome,ncd.dosagem," +
				"ncd.qtdDisponivelEmbalagem, ncd.generico,cm.idClassificacaoMedicacao,cm.nome,ied.idIndicacaoEspecificaDose," +
				"ied.indicacao, ied.doseAdultos, ied.doseCriancas, ied.nivelSericoDisfRenalHepa, ied.consideracoesRecomendacoes,medicamento.nivelSericoDisfRenalHepa, medicamento.consideracoesRecomendacoes," +
				"medicamento.idUsuarioAltera, medicamento.dtAltera,bula.cdfile,bula.contenttype,bula.name,bula.size," +
				"bula.content,medicamento.ativo,if.idIndustriaFarmaceutica, if.nome")
		.leftOuterJoin("medicamento.classificacaoMedicacao cm")
		.leftOuterJoin("medicamento.nomesComerciaisDroga ncd")
		.leftOuterJoin("ncd.industriaFarmaceutica if")
		.leftOuterJoin("medicamento.bula bula")
		.leftOuterJoin("medicamento.indicacoesEspecificasEDoses ied")
		.leftOuterJoin("ncd.apresentacao apresentacao")
		.where("cm=?",classificacaoMedicacao)
		.list();
	}
	
	/**
	 * <p>M�todo usado para retornar todos os medicamentos de uma determinada letra inicial
	 * @param letter {@link String} contendo a letra
	 * @return {@link List} {@link Medicamento}
	 * @author Biharck
	 */
	public List<Medicamento> getMedicamentosByLetra(String letter){
		return query().select("medicamento.idMedicamento,medicamento.contraIndicacoes, medicamento.principioAtivo, medicamento.descricaoDroga, medicamento.efeitosColateraisComuns," +
				"medicamento.efeitosColateraisMenosFrequente,ncd.idNomeComercialDroga,ncd.nomeComercial,ncd.industriaFarmaceutica,apresentacao.idApresentacaoMedicacao, apresentacao.nome,ncd.dosagem," +
				"ncd.qtdDisponivelEmbalagem, ncd.generico,cm.idClassificacaoMedicacao,cm.nome,ied.idIndicacaoEspecificaDose," +
				"ied.indicacao, ied.doseAdultos, ied.doseCriancas, ied.nivelSericoDisfRenalHepa, ied.consideracoesRecomendacoes,medicamento.nivelSericoDisfRenalHepa, medicamento.consideracoesRecomendacoes," +
				"medicamento.idUsuarioAltera, medicamento.dtAltera,bula.cdfile,bula.contenttype,bula.name,bula.size," +
				"bula.content,medicamento.ativo,if.idIndustriaFarmaceutica, if.nome")
		.leftOuterJoin("medicamento.classificacaoMedicacao cm")
		.leftOuterJoin("medicamento.nomesComerciaisDroga ncd")
		.leftOuterJoin("ncd.industriaFarmaceutica if")
		.leftOuterJoin("medicamento.bula bula")
		.leftOuterJoin("ncd.apresentacao apresentacao")
		.leftOuterJoin("medicamento.indicacoesEspecificasEDoses ied")
		.whereIn("medicamento.idMedicamento",getIdsMedicamentoByLetra(letter))
		.list();
	}
	
	/**
	 * <p>M�todo que retorna uma {@link String} de id's dos medicamento a partir de sua letra inicial, concatenados por ","
	 * @param letra {@link String} contendo a letra
	 * @return {@link String} contendo os ids concatenados por ","
	 */
	public String getIdsMedicamentoByLetra(String letra){
		List<Medicamento> medicamentos = new ArrayList<Medicamento>();
		SqlRowSet srs = getJdbcTemplate().queryForRowSet(
				" SELECT idMedicamento "+
				" FROM medicamento "+
				" WHERE principioativo like _utf8 '"+letra.toUpperCase()+"%' COLLATE utf8_unicode_ci ");
		while (srs.next()) {
			medicamentos.add(new Medicamento(srs.getInt("idMedicamento")));
		}
		return CollectionsUtil.listAndConcatenate(medicamentos, "idMedicamento", ",");
	}
	
	@Override
	public List<Medicamento> findAll() {
		return query().where("medicamento.ativo is true").list();
	}
	
	@Override
	public List<Medicamento> findForCombo(String... extraFields) {
		extraFields[0] += "ativo";
		return super.findForCombo(extraFields);
	}

}

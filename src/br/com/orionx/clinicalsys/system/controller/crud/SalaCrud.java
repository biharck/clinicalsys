package br.com.orionx.clinicalsys.system.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;

import br.com.orionx.clinicalsys.bean.Sala;
import br.com.orionx.clinicalsys.filtros.SalaFiltro;
import br.com.orionx.clinicalsys.util.generics.CrudController;

@Controller(path="/system/pag/Sala",authorizationModule=CrudAuthorizationModule.class)
public class SalaCrud extends CrudController<SalaFiltro, Sala, Sala>{

	@Override
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Administração &raquo; <a href=\"/ClinicalSys/system/pag/Sala\">Salas</a></p>";
	}

}

package br.com.orionx.clinicalsys.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

import br.com.orionx.clinicalsys.util.log.Log;

@Entity
@Table(name = "sala")
public class Sala implements Log{
	
	private Integer idSala;
	private String nome;
	private Setor setor;
	private boolean ativo;

	//Log
	private Integer idUsuarioAltera;
	private Timestamp dtAltera;

	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdSala() {
		return idSala;
	}
	@Required
	@MinLength(1)
	@MaxLength(30)
	public String getNome() {
		return nome;
	}
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "idSetor")
	@ForeignKey(name = "fk_sala_setor")
	public Setor getSetor() {
		return setor;
	}
	public boolean isAtivo() {
		return ativo;
	}
	
	//SET
	public void setIdSala(Integer idSala) {
		this.idSala = idSala;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setSetor(Setor setor) {
		this.setor = setor;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	
	//LOG
	@Override
	public void setIdUsuarioAltera(Integer idUsuarioAltera) {
		this.idUsuarioAltera = idUsuarioAltera;
	}

	@Override
	public void setDtAltera(Timestamp dtAltera) {
		this.dtAltera = dtAltera;
	}

	@Override
	public Integer getIdUsuarioAltera() {
		return idUsuarioAltera;
	}

	@Override
	public Timestamp getDtAltera() {
		return dtAltera;
	}
}

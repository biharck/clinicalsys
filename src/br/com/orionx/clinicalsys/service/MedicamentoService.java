package br.com.orionx.clinicalsys.service;

import java.util.List;

import br.com.orionx.clinicalsys.bean.ClassificacaoMedicacao;
import br.com.orionx.clinicalsys.bean.Medicamento;
import br.com.orionx.clinicalsys.dao.MedicamentoDAO;
import br.com.orionx.clinicalsys.util.generics.GenericService;

public class MedicamentoService extends GenericService<Medicamento>{

	private MedicamentoDAO medicamentoDAO;
	public void setMedicamentoDAO(MedicamentoDAO medicamentoDAO) {
		this.medicamentoDAO = medicamentoDAO;
	}

	/**
	 * <p>M�todo de refer�ncia ao dao
	 * @param letter {@link String}
	 * @return {@link List} {@link Medicamento}
	 * @see br.com.orionx.clinicalsys.dao.MedicamentoDAO#getMedicamentosByLetra
	 */
	public List<Medicamento> getMedicamentosByLetra(String letter) {
		return medicamentoDAO.getMedicamentosByLetra(letter);
	}
	
	
	/**
	 * <p>M�todo de refer�ncia ao DAO
	 * @param classificacaoMedicacao {@link ClassificacaoMedicacao}
	 * @return {@link List} {@link Medicamento}
	 * @author biharck
	 */
	public List<Medicamento> getMedicamentosByClassif(ClassificacaoMedicacao classificacaoMedicacao){
		return medicamentoDAO.getMedicamentosByClassif(classificacaoMedicacao);
	}
}

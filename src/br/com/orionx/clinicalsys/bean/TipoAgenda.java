package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name = "tipoagenda")
public class TipoAgenda {
	
	private Integer idTipoAgenda;
	private String descricao;
	
	
	//GET
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getIdTipoAgenda() {
		return idTipoAgenda;
	}
	
	@Required
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	//SET
	public void setIdTipoAgenda(Integer idTipoAgenda) {
		this.idTipoAgenda = idTipoAgenda;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}

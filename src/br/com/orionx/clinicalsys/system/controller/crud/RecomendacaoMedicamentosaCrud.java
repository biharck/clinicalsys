package br.com.orionx.clinicalsys.system.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.orionx.clinicalsys.bean.RecomendacaoMedicamentosa;
import br.com.orionx.clinicalsys.filtros.RecomendacaoMedicamentosaFiltro;
import br.com.orionx.clinicalsys.util.DatabaseError;
import br.com.orionx.clinicalsys.util.exception.ClinicalSysException;
import br.com.orionx.clinicalsys.util.generics.CrudController;

@Controller(path="/system/pag/RecomendacaoMedicamentosa",authorizationModule=CrudAuthorizationModule.class)
public class RecomendacaoMedicamentosaCrud extends CrudController<RecomendacaoMedicamentosaFiltro, RecomendacaoMedicamentosa, RecomendacaoMedicamentosa>{

	@Override
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Administra��o &raquo; <a href=\"/ClinicalSys/system/pag/RecomendacaoMedicamentosa\">Recomendac�es Medicamentosas</a></p>";
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, RecomendacaoMedicamentosa form)throws CrudException {
		return saveUnique(request, form,true);
	}
	
	@Override
	public ModelAndView saveAndNew(WebRequestContext request, RecomendacaoMedicamentosa form)throws Exception {
		return saveUnique(request, form,false);
	}
	
	public ModelAndView saveUnique(WebRequestContext request, RecomendacaoMedicamentosa form, boolean retornaListagem){
		try {
			salvar(request, form);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_nome_recommed")) throw new ClinicalSysException("J� existe uma recomenda��o medicamentosa com este nome cadastrado no sistema.");
			return sendRedirectToAction("entrada");
		}
		catch (Exception e) {
			return sendRedirectToAction("entrada");
		}
		if(retornaListagem)return sendRedirectToAction("listagem");
		else return sendRedirectToAction("entrada");
	}

}

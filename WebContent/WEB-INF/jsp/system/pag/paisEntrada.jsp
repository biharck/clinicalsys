<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<t:property name="idPais" type="hidden" label="" write="false"/>
	<t:janelaEntrada>
		<t:tabelaEntrada columns="1" >
			<t:property name="nome" renderAs="doubleline" id="nome"/>
			<t:property name="sigla" renderAs="doubleline"/>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>
<script type="text/javascript">
	$(document).ready(function(){
		$('#nome').focus();
	});
</script>
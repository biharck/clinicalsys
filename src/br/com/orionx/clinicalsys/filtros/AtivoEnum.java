package br.com.orionx.clinicalsys.filtros;

public enum AtivoEnum {
	
	ATIVO("Ativos",true),
	INATIVO("Inativos",false),
	TODOS("Todos",null);
	
	private Boolean situacao;
	private String descricao;
	
	private AtivoEnum(String descricao,Boolean situacao){
		this.situacao = situacao;
		this.descricao = descricao;
	}
	
	public Boolean getSituacao() {
		return situacao;
	}
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}

}

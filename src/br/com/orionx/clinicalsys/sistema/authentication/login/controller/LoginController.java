package br.com.orionx.clinicalsys.sistema.authentication.login.controller;

import java.util.Locale;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.nextframework.authorization.User;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MessageType;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.view.DownloadFileServlet;
import org.nextframework.view.menu.MenuTag;
import org.springframework.web.servlet.ModelAndView;

import br.com.orionx.clinicalsys.bean.Colaborador;
import br.com.orionx.clinicalsys.bean.Usuario;
import br.com.orionx.clinicalsys.dao.ArquivoDAO;
import br.com.orionx.clinicalsys.service.ColaboradorService;
import br.com.orionx.clinicalsys.sistema.authorization.dao.NextAuthorizationDAO;

/**
 * <p>Controller que far� o login do usu�rio na aplica�o.<br>
 * Se o login for efetuado com sucesso ir� redirecionar para /secured/Index</p>
 * 
 * @author biharck
 */
@Controller(path="/pub/login")
public class LoginController extends MultiActionController {
 
    private static final String AFTER_LOGIN_GO_TO = "/system";
    
    NextAuthorizationDAO authorizationDAO;
    ColaboradorService colaboradorService;
    ArquivoDAO arquivoDAO;
    
    public void setAuthorizationDAO(NextAuthorizationDAO authorizationDAO) {
        this.authorizationDAO = authorizationDAO;
    }
    public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
    public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
    
    /**
     * Action que envia para a p�gina de login
     */
    @DefaultAction
    public ModelAndView doPage(WebRequestContext request, Usuario usuario){
        return new ModelAndView("login", "usuario", usuario);
    }
    
    /**
     * Efetua o login do usu�rio
     */
    public ModelAndView doLogin(WebRequestContext request, Usuario usuario){
    	if(usuario == null || usuario.getLogin() == null || usuario.getPassword() == null || usuario.getLogin().trim().equals("")){
    		request.addMessage("Login e/ou senha incorretos", MessageType.ERROR);
    		usuario.setPassword(null);
    		return doPage(request, usuario);
    	}
    	Locale locale = new Locale("pt", "BR"); 
    	// define o Locale padr�o da JVM
    	Locale.setDefault(locale);
    	String login = usuario.getLogin();

    	
        //se foi passado o login na requisi�o, iremos verificar se o usu�rio existe e a senha est� correta
        if(login != null){
            //buscamos o usu�rio do banco pelo login
            User userByLogin = authorizationDAO.findUserByLogin(login);
            StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
            
            Colaborador c = colaboradorService.getColaboradorByUser(userByLogin.getLogin());
            if(c!=null && !c.getAtivo()){
            	request.addMessage("Usu�rio Desativado.", MessageType.ERROR);
            	usuario.setPassword(null);
                return doPage(request, usuario);
            }
            
            
            // se o usu�rio existe e a senha est� correta
            if(userByLogin != null && encryptor.checkPassword(usuario.getPassword(), userByLogin.getPassword())){
                //Setando o atributo de se�o USER fazemos o login do usu�rio no sistema. 
                request.setUserAttribute("USER", userByLogin);
                
                //Limpamos o cache de permiss�es o menu.
                //O menu ser� refeito levando em considera�o as permiss�es do usu�rio
                request.setUserAttribute(MenuTag.MENU_CACHE_MAP, null);
                if(c!=null)
	                if(c.getFoto()!=null && c.getFoto().getCdfile()!=null){
						if(c.getFoto().getContent()==null)
							arquivoDAO.loadAsImage(c.getFoto());
						DownloadFileServlet.addCdfile(request.getSession(), c.getFoto().getCdfile());
						request.getSession().setAttribute("fotoLogin", c.getFoto());
					}
                return new ModelAndView("redirect:"+AFTER_LOGIN_GO_TO);                
            }
            
            //Se o login e/ou a senha n�o estiverem corretos, avisar o usu�rio
            request.addMessage("Login e/ou senha incorretos", MessageType.ERROR);
        }
        
        //limpar o campo senha, e enviar para a tela de login j� que o processo falhou
        usuario.setPassword(null);
        return doPage(request, usuario);
    }
}
 

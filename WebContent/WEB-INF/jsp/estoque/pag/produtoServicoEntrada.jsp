<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<t:property name="idProdutoServico" type="hidden" write="false" showLabel="false"/>
	<t:janelaEntrada>
		<t:tabelaEntrada columns="6">
			<t:property name="codigo" renderAs="doubleline" id="codigo" style="width:400px" colspan="6"/>
			<t:property name="descricao" renderAs="doubleline" style="width:400px" rows="2" colspan="6" maxlength="120"/>
			<t:property name="produtoOuServico" renderAs="doubleline" colspan="3" type="SELECT-ONE" trueFalseNullLabels="Produto, Servi�o," />
			<t:property name="categoriaProdutos" renderAs="doubleline" colspan="3"/>
			<t:property name="EAN" renderAs="doubleline" colspan="2"/>
			<t:property name="NCM" renderAs="doubleline" colspan="2"/>
			<t:property name="CFPO" renderAs="doubleline" colspan="2"/>
			<t:property name="exTPI" renderAs="doubleline" colspan="2"/>
			<t:property name="genero" renderAs="doubleline" colspan="2"/>
			<t:property name="unidadeComercial" renderAs="doubleline" colspan="2"/>
			<t:property name="quantidadeComercial" renderAs="doubleline" colspan="2"/>
			<t:property name="valorUnitarioComercial" renderAs="doubleline" colspan="2"/>
			<t:property name="unidadeTributada" renderAs="doubleline" colspan="2"/>
			<t:property name="quantidadeTributada" renderAs="doubleline" colspan="2"/>
			<t:property name="eANTributado" renderAs="doubleline" colspan="2"/>
			<t:property name="valorTotalFrete" renderAs="doubleline" colspan="2"/>
			<t:property name="valorTotalSeguro" renderAs="doubleline" colspan="2"/>
			<t:property name="valorDesconto" renderAs="doubleline" colspan="2"/>
			<t:property name="valorTotalBruto" renderAs="doubleline" colspan="2"/>
			<t:property name="ativo" renderAs="doubleline" colspan="2"/>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>
<script type="text/javascript">
$(document).ready(function(){
	$('#codigo').focus();
});
</script>
package br.com.orionx.clinicalsys.dao;

import java.util.ArrayList;
import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;
import org.nextframework.persistence.SaveOrUpdateStrategy;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import br.com.orionx.clinicalsys.bean.Cargo;
import br.com.orionx.clinicalsys.bean.CargoPapel;
import br.com.orionx.clinicalsys.bean.Papel;
import br.com.orionx.clinicalsys.filtros.CargoFiltro;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

@DefaultOrderBy("cargo.nome")
public class CargoDAO extends GenericDAO<Cargo> {
	
	private CargoPapelDAO cargoPapelDAO;
	
	public void setCargoPapelDAO(CargoPapelDAO cargoPapelDAO) {
		this.cargoPapelDAO = cargoPapelDAO;
	}
	
	@Override
	public void saveOrUpdate(final Cargo bean) {
		transactionTemplate.execute(new TransactionCallback<Object>(){
			public Object doInTransaction(TransactionStatus arg0) {
				if(bean!=null && bean.getIdCargo()!=null)
					cargoPapelDAO.deleteByCargo(bean);
				List<CargoPapel> listaCargosPapeis = new ArrayList<CargoPapel>();
				if(bean.getPapeis()!=null){
					bean.setListaCargosPapeis(new ArrayList<CargoPapel>());
					for (Papel p : bean.getPapeis()) {
						listaCargosPapeis.add(new CargoPapel(null, bean, p));
					}
				}
				bean.setListaCargosPapeis(listaCargosPapeis);
				CargoDAO.super.saveOrUpdate(bean);

			return null;
			}
		});
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Cargo> query,FiltroListagem _filtro) {
		CargoFiltro filtro = (CargoFiltro) _filtro;
		if(filtro.getAtivoEnum()!=null)
			if(filtro.getAtivoEnum().getSituacao()!=null && (filtro.getAtivoEnum().getSituacao() || !filtro.getAtivoEnum().getSituacao()))
				query.where("cargo.ativo is "+filtro.getAtivoEnum().getSituacao());
		query.whereLikeIgnoreAll("cargo.nome", filtro.getNome()).whereLikeIgnoreAll("cargo.descricao", filtro.getDescricao()).where("cargo.codigoCBO=?",filtro.getCodigoCBO());
		super.updateListagemQuery(query, filtro);
	}
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("listaCargosPapeis");
	}
	
	@Override
	public List<Cargo> findAll() {
		return query().where("cargo.ativo is true").list();
	}
	
	@Override
	public List<Cargo> findForCombo(String... extraFields) {
		extraFields[0] += "ativo";
		return super.findForCombo(extraFields);
	}

	
}

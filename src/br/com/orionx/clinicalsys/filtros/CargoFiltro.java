package br.com.orionx.clinicalsys.filtros;

import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;

import br.com.orionx.clinicalsys.util.generics.FiltroListagem;

public class CargoFiltro extends FiltroListagem{

	protected String nome;
	protected String codigoCBO;
	protected String descricao;
	protected AtivoEnum ativoEnum;
	
	@MaxLength(50)
	@MinLength(2)
	public String getNome() {
		return nome;
	}
	@MaxLength(5)
	@MinLength(5)
	public String getCodigoCBO() {
		return codigoCBO;
	}
	@MaxLength(100)
	@MinLength(2)
	public String getDescricao() {
		return descricao;
	}
	public AtivoEnum getAtivoEnum() {
		return ativoEnum;
	}
	
	//SET
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCodigoCBO(String codigoCBO) {
		this.codigoCBO = codigoCBO;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setAtivoEnum(AtivoEnum ativoEnum) {
		this.ativoEnum = ativoEnum;
	}
	
}

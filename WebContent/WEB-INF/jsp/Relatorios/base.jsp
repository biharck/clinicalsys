<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<%--
	Arquivo base.jsp exemplo para ser utilizado como container para os JSPs das p�ginas.
	Deve ser colocado na pasta /WEB-INF/jsp/[modulo] onde [modulo] � o m�dulo da aplica��o que se deseja usar o base
	O arquivo base.jsp � detectado automaticamente pelo Next se ele existir na pasta especificada.
	Cada m�dulo pode ter um arquivo base.jsp diferente
	
	Funcionamento do arquivo base.jsp:
		Sempre que um controller direcionar para um JSP, por exemplo meuArquivo.jsp, o Next ir� interceptar e incluir� primeiro o base.jsp
		e o base.jsp inclui o meuArquivo.jsp. 
		
		Exemplo de conte�do do arquivo meuArquivo.jsp:
			<h1>Esse � meu arquivo JSP</h1>
			<h2>Ele ser� incluido pelo base.jsp</h2>
			
		Exemplo de conte�do do arquivo base.jsp:
			<html>
				<body>
					<jsp:include page="${bodyPage}" />
				</body>
			</html>
		
		Resultado final para o cliente:
			<html>
				<body>
					<h1>Esse � meu arquivo JSP</h1>
					<h2>Ele ser� incluido pelo base.jsp</h2>					
				</body>
			</html>
 --%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<HTML>
	<HEAD>
		<%-- 
			A tag n:head inclui os arquivos CSS e JS padr�o do Next
			Se desejar incluir outros CSSs basta criar uma pasta /css na aplica��o e colocar os arquivos .css que essa tag far� o include
			Se desejar incluir outros JSs basta criar uma pasta /js na aplica��o e colocar os arquivos .js que essa tag far� o include
			
			Ser� incluido um default.css padr�o do Next que pode ser substituido se existir um arquivo /css/default.css, nesse caso ser� 
			utilizado o arquivo da aplica��o e n�o o do framework.
		 --%>
		
		<n:head/>
	</HEAD>
	<BODY leftmargin="0" topmargin="0">
		<div class="applicationTitle">
		${application}
		</div>
		
		<%-- Descomente o trecho de c�digo a seguir para habilitar o MENU --%>
		<div class="menubar">
			<!--<img src="${pageContext.request.contextPath}/resource/img/menugrade.gif" style="float:left; margin-top: 1px; margin-left: 2px;">-->
			<div class="menubar-before"></div>
			<div class="menubar-after"></div>
			<div class="menubar-in">
				<n:menu menupath="/WEB-INF/menu.xml"/>
			</div>
		</div>
		<div class="messageOuterDiv">
			<n:messages/> <%-- Imprime as mensagens do sistema --%>
		</div>
		<div class="body">
			<jsp:include page="${bodyPage}" /> <%-- Inclui a p�gina originalmente chamada --%>
		</div>
		
	</BODY>
</HTML>
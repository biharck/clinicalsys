<html> 
<head> 
 
<!-- Website Title --> 
<title>ClinicalSys | Login</title>

<!-- Meta data for SEO -->
<meta name="description" content="">
<meta name="keywords" content="">

<%@ taglib prefix="n" uri="next"%>

<!-- Template stylesheet -->
<link href="${application}/css/blue/screen.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/blue/datepicker.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/js/visualize/visualize.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/js/jwysiwyg/jquery.wysiwyg.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/js/fancybox/jquery.fancybox-1.3.0.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/default.css" rel="stylesheet" type="text/css" media="all">

<!--[if IE]>
	<link href="${application}/css/ie.css" rel="stylesheet" type="text/css" media="all">
	<meta http-equiv="X-UA-Compatible" content="IE=7" />
<![endif]-->

<!-- Jquery and plugins -->
<script type="text/javascript" src="${application}/js/jquery.js"></script>
<script type="text/javascript" src="${application}/js/jquery-ui.js"></script>
<script type="text/javascript" src="${application}/js/jquery.img.preload.js"></script>
<script type="text/javascript" src="${application}/js/hint.js"></script>
<script type="text/javascript" src="${application}/js/visualize/jquery.visualize.js"></script>
<script type="text/javascript" src="${application}/js/jwysiwyg/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="${application}/js/fancybox/jquery.fancybox-1.3.0.js"></script>
<script type="text/javascript" charset="iso-8859-1"> 
$(function(){ 
    // find all the input elements with title attributes
    $('input[title!=""]').hint();
    $('#login_info').click(function(){
		$(this).fadeOut('fast');
	});
});
$(document).ready(function(){
	$('#username').focus();
});
</script>
</head>
<body class="login">

	<!-- Begin login window -->
	<div id="login_wrapper">
		<div id="login_info"  style="width:350px;margin:auto;padding:auto;">
			<p><n:messages/> <%-- Imprime as mensagens do sistema --%></p>
		</div>
		<div id="login_top_window">
			<img src="${application}/images/blue/top_login_window.png" alt="top window"/>
		</div>
		
		<!-- Begin content -->
		<div id="login_body_window">
			<div class="inner">
				<img src="${application}/images/login_logo.png" alt="logo"/>
				<form method="POST" enctype="multipart/form-data" name="form" action="${application}/pub/login">
					<input type="hidden" name="ACAO" value=""/>
					<input type="hidden" name="suppressValidation" value="false"/>
					<input type="hidden" name="suppressErrors" value="false"/>
					<p>
						<input type="text" id="username" name="login" style="width:285px" title="Username"/>
					</p>
					<p>
						<input type="password" id="password" name="password" style="width:285px" title="******"/>
					</p>
					<p style="margin-top:50px">
						<input type="submit" onclick="form.ACAO.value ='doLogin';form.action = '/ClinicalSys/pub/login'; submitForm()" id="submit" name="submit" value="Login" class="Login" style="margin-right:5px"/>
						<input type="checkbox" id="remember" name="remember"/>Lembrar minha senha
					</p>
				</form>
			</div>
		</div>
		<!-- End content -->
		
		<div id="login_footer_window">
			<img src="${application}/images/blue/footer_login_window.png" alt="footer window"/>
		</div>
		<div id="login_reflect">
			<img src="${application}/images/blue/reflect.png" alt="window reflect"/>
		</div>
	</div>
	<!-- End login window -->

<script language="javascript">
	function submitForm(){
		document.forms["form"].submit();
	}
</script>
	

</body>
</html>

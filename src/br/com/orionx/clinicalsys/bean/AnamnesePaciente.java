package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Table(name="anamnesepaciente")
@Entity
public class AnamnesePaciente {
	
	public Integer idAnamnesePaciente;
	public Anamnese anamnese;
	public Paciente paciente;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdAnamnesePaciente() {
		return idAnamnesePaciente;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idAnamnese")
	@ForeignKey(name="fk_ap_an")
	public Anamnese getAnamnese() {
		return anamnese;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idPaciente")
	@ForeignKey(name="fk_ap_pct")
	public Paciente getPaciente() {
		return paciente;
	}
	
	public void setIdAnamnesePaciente(Integer idAnamnesePaciente) {
		this.idAnamnesePaciente = idAnamnesePaciente;
	}
	public void setAnamnese(Anamnese anamnese) {
		this.anamnese = anamnese;
	}
	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}
}

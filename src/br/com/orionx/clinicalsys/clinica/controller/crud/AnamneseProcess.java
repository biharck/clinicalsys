package br.com.orionx.clinicalsys.clinica.controller.crud;

import java.sql.Date;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

import br.com.orionx.clinicalsys.bean.Anamnese;
import br.com.orionx.clinicalsys.bean.AnamnesePaciente;
import br.com.orionx.clinicalsys.bean.Paciente;
import br.com.orionx.clinicalsys.service.AnamnesePacienteService;
import br.com.orionx.clinicalsys.service.AnamneseService;
import br.com.orionx.clinicalsys.service.PacienteService;

@Controller(path="/clinic/process/Anamnese",authorizationModule=ProcessAuthorizationModule.class)
public class AnamneseProcess extends MultiActionController{

	private PacienteService pacienteService;
	private AnamneseService anamneseService;
	private AnamnesePacienteService anamnesePacienteService;
	
	public void setPacienteService(PacienteService pacienteService) {
		this.pacienteService = pacienteService;
	}
	public void setAnamneseService(AnamneseService anamneseService) {
		this.anamneseService = anamneseService;
	}
	public void setAnamnesePacienteService(AnamnesePacienteService anamnesePacienteService) {
		this.anamnesePacienteService = anamnesePacienteService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request){
		Paciente p = new Paciente();
		p.setIdPaciente(Integer.parseInt(getParameter("idPaciente")));
		p = pacienteService.getPacienteProntuario(p);
		request.setAttribute("paciente", p);
		request.setAttribute("anamneses", anamnesePacienteService.getAnamnesesByPaciente(p));
		return new ModelAndView("direct:process/listagemAnamnese");
	}
	
	
	public ModelAndView novo(WebRequestContext request){
		Paciente p = new Paciente();
		p.setIdPaciente(Integer.parseInt(getParameter("idPaciente")));
		p = pacienteService.getPacienteProntuario(p);
		Anamnese anamnese = new Anamnese();
		request.setAttribute("pathURL", getPathURL(p.getIdPaciente()));
		request.setAttribute("paciente", p);
		
		return new ModelAndView("direct:process/anamnese","anamnese",anamnese);
	}
	
	public String getPathURL(Integer id) {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; <a href=\"/ClinicalSys/clinic/process/prontuario?idPaciente="+id+"\">Prontuário</a> &raquo; <a href=\"/ClinicalSys/clinic/process/Anamnese?idPaciente="+id+"\">Anamnese</a></p>";
	}
	
	@Action("salvar")
	public ModelAndView salvar(WebRequestContext request, Anamnese anm){
		request.getAttribute("idPaciente");
		Paciente p = new Paciente();
		p.setIdPaciente(Integer.parseInt(getParameter("idPaciente")));
		anm.setDataAnamnese(new Date(System.currentTimeMillis()));
		anm.setPaciente(p);
		anamneseService.saveOrUpdate(anm);
		AnamnesePaciente ap = new AnamnesePaciente();
		ap.setPaciente(p);
		ap.setAnamnese(anm);
		anamnesePacienteService.saveOrUpdate(ap);
		
		request.addMessage("Registro salvo com sucesso." );		
		return index(request);
	}
	
	public ModelAndView visualizar(WebRequestContext request){
		Paciente p = new Paciente();
		p.setIdPaciente(Integer.parseInt(getParameter("idPaciente")));
		Anamnese an = new Anamnese();
		an.setIdAnamnese(Integer.parseInt(request.getParameter("idAnamnese")));
		an = anamneseService.loadForEntrada(an);
		request.setAttribute("pathURL", getPathURL(p.getIdPaciente()));
		request.setAttribute("paciente", p);
		request.setAttribute("type", "edit");
		return new ModelAndView("direct:process/anamnese","anamnese",an);
	}
	
	public ModelAndView atualizar(WebRequestContext request, Anamnese anm){
		anamneseService.saveOrUpdate(anm);
		request.addMessage("Registro atualizado com sucesso." );		
		return index(request);
	}
	
}

package br.com.orionx.clinicalsys.util.log;

import java.sql.Timestamp;

public interface Log {
	
	void setIdUsuarioAltera(Integer idUsuarioAltera);
	void setDtAltera(Timestamp dtAltera);
	public Integer getIdUsuarioAltera();
	public Timestamp getDtAltera();
	
}

package br.com.orionx.clinicalsys.estoque.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;

import br.com.orionx.clinicalsys.bean.ProdutoServico;
import br.com.orionx.clinicalsys.filtros.ProdutoServicoFiltro;
import br.com.orionx.clinicalsys.util.generics.CrudController;

@Controller(path="/estoque/pag/ProdutoServico",authorizationModule=CrudAuthorizationModule.class)
public class ProdutoServicoCrud extends CrudController<ProdutoServicoFiltro, ProdutoServico, ProdutoServico>{
	
	@Override
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Estoque &raquo; <a href=\"/ClinicalSys/estoque/pag/ProdutoServico\">Produtos e Servi�os</a></p>";
	}

}

package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cidocategoria")
public class CIDOCategoria {

	private Integer idCIDOCategoria;
	private String cat;
	private String descricao;
	private String refer;
	
	public CIDOCategoria() {}
	
	public CIDOCategoria(Integer idCIDOCategoria, String cat, String descricao, String refer) {
		this.idCIDOCategoria = idCIDOCategoria;
		this.cat = cat;
		this.descricao = descricao;
		this.refer = refer;
	}
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdCIDOCategoria() {
		return idCIDOCategoria;
	}

	public String getCat() {
		return cat;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getRefer() {
		return refer;
	}

	public void setIdCIDOCategoria(Integer idCIDOCategoria) {
		this.idCIDOCategoria = idCIDOCategoria;
	}

	public void setCat(String cat) {
		this.cat = cat;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setRefer(String refer) {
		this.refer = refer;
	}
	
}

package br.com.orionx.clinicalsys.dao;

import br.com.orionx.clinicalsys.bean.Anamnese;
import br.com.orionx.clinicalsys.bean.Paciente;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class AnamneseDAO extends GenericDAO<Anamnese>{

	
	public Anamnese getAnamneseByPaciente(Paciente p){
		return query().select("anamnese").leftOuterJoin("anamnese.paciente p ").where("p=?",p).unique();
	}
	
}

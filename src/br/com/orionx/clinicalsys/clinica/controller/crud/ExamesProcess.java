package br.com.orionx.clinicalsys.clinica.controller.crud;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

import br.com.orionx.clinicalsys.bean.Paciente;
import br.com.orionx.clinicalsys.service.PacienteService;


@Controller(path="/clinic/process/exames",authorizationModule=ProcessAuthorizationModule.class)
public class ExamesProcess extends MultiActionController{
	
	private PacienteService pacienteService;
	
	public void setPacienteService(PacienteService pacienteService) {
		this.pacienteService = pacienteService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request){
		Paciente p = new Paciente();
		p.setIdPaciente(Integer.parseInt(getParameter("idPaciente")));
		p = pacienteService.getPacienteProntuario(p);
		
		return new ModelAndView("direct:process/exames","paciente",p);
	}
}

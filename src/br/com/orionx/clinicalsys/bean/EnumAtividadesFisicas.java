package br.com.orionx.clinicalsys.bean;

public enum EnumAtividadesFisicas {

	SEDENTARIO(1,"Sedent�rio"),
	OCASIONAL(2,"Exerce Atividade F�sica Ocasional"),
	MODERADA(3,"Exerce Atividade F�sica Moderada"),
	INTENSA(4,"Exerce Atividade F�sica Intensa e Constante");
	
	private Integer id;
	private String descricao;

	private EnumAtividadesFisicas(Integer id, String descricao){
		this.id = id;
		this.descricao = descricao;
	}
	
	public Integer getId() {
		return id;
	}
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}

}

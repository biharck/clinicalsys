<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"> 
<%@page import="br.com.orionx.clinicalsys.util.ClinicalSysUtil"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"> 
 
<!-- Website Title --> 
<title>ClinicalSys</title>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="cs" uri="clinicalsys"%>

<!-- Template stylesheet -->
<link id="screenColor" href="${application}/css/blue/screen.css" rel="stylesheet" type="text/css" media="all">
<link id="datepickerColor" href="${application}/css/blue/datepicker.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/tipsy.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/js/visualize/visualize.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/js/jwysiwyg/jquery.wysiwyg.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/js/fancybox/jquery.fancybox-1.3.0.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/tipsy.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/autocomplete.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/default.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/ui.datepicker.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/ui.theme.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/ui.dialog.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/dtree.css" rel="stylesheet" type="text/css" media="all">


<!--[if IE]>
	<link href="${application}/css/ie.css" rel="stylesheet" type="text/css" media="all">
	<script type="text/javascript" src="${application}/js/excanvas.js"></script>
<![endif]-->

<!-- Jquery and plugins -->
<script type="text/javascript" src="${application}/js/jquery.js"></script>
<script type="text/javascript" src="${application}/js/jquery-ui.js"></script>
<script type="text/javascript" src="${application}/js/jquery.img.preload.js"></script>
<script type="text/javascript" src="${application}/js/hint.js"></script>
<script type="text/javascript" src="${application}/js/visualize/jquery.visualize.js"></script>
<script type="text/javascript" src="${application}/js/jwysiwyg/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="${application}/js/fancybox/jquery.fancybox-1.3.0.js"></script>
<script type="text/javascript" src="${application}/js/jquery.tipsy.js"></script>
<script type="text/javascript" src="${application}/js/custom_blue.js"></script>
<script type="text/javascript" src="${application}/js/jquery.autocomplete.js"></script>
<script type="text/javascript" src="${application}/js/first.autocomplete.js"></script>
<script type="text/javascript" src="${application}/js/validate.js"></script>
<script type="text/javascript" src="${application}/js/functions.js"></script>
<script type="text/javascript" src="${application}/js/dtree.js"></script>
<script type="text/javascript" src="${application}/js/util.js"></script>
<script type="text/javascript" src="${application}/js/ajax.js"></script>
<script type="text/javascript" src="${application}/js/input.js"></script>
<script type="text/javascript" src="${application}/js/dynatable.js"></script>
<script type="text/javascript" src="${application}/js/clinicalSysUtil.js"></script>
<!-- lib do qtip -->
<script type="text/javascript" src="${application}/js/jquery-qtip-1.0.0-rc3140944/jquery.qtip-1.0.0-rc3.min.js"></script>


</head>
<body class="nobg">
<a name="topo"></a>
<div class="content_wrapper">

	<!-- Begin header -->
	<div id="header">
		<div id="logo">
			<img src="${application}/images/logo.png" alt="logo"/>
		</div>
		<c:if test="${param.fromInsertOne != 'true'}">
			<c:if test="${param.onePath != 'true'}">
				<div id="search">
					<form action="dashboard.html" id="search_form" name="search_form" method="get">
						<input type="text" id="CityLocal" id="CityLocal" name="q" title="Procurar" class="search noshadow"/>			
					</form>
				</div>
			</c:if>
		</c:if>
		<div id="account_info">
			<img src="${application}/images/icon_online.png" alt="Online" class="mid_align"/>
			Ol� <a href="">Dr. <%= ClinicalSysUtil.getUsuarioLogado().getLogin() %></a>
			<c:if test="${param.fromInsertOne != 'true'}">
				<c:if test="${param.onePath != 'true'}">
			 		(<a href="">1 nova menssagem</a>) | <a href="${application}/system/Logout">Logout</a>
			 	</c:if>
			</c:if>
			&nbsp;&nbsp;&nbsp;&nbsp;Sua sess�o expira em: <span id="cronometro_tempo"></span>
		</div>
	</div>
	<!-- End header -->
	
	<!-- Begin content -->
	<div id="content" style="margin-left:30px;">
		<div class="inner">
			<c:if test="${param.ACAO != 'criar'}">
				<c:if test="${param.fromInsertOne != 'true'}">
					<c:if test="${param.onePath != 'true'}">
						<i id="pathURL">${pathURL}</i>
					</c:if>
				</c:if>
			</c:if>
			
			<br class="clear"/>
			<div class="messageOuterDiv">
				<n:messages/> <%-- Imprime as mensagens do sistema --%>
			</div>
			<!-- conte�do da aba -->
			<div class="onecolumn">
				<t:tela>
					<n:bean name="hp" >
							<div class="header">
								<span>Hist�ria Pregressa</span>
								<div class="switch" style="width:455px">
									<table width="100px" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td>
												<input type="button" id="tab1" name="tab1" class="left_switch active" value="Antecedentes" style="width:100px"/>
											</td>
											<td>
												<input type="button" id="tab2" name="tab2" class="middle_switch" value="H�bitos" style="width:80px"/>
											</td>
											<td>
												<input type="button" id="tab3" name="tab3" class="middle_switch" value="S�cioecon�mico" style="width:130px"/>
											</td>
											<td>
												<input type="button" id="tab4" name="tab4" class="right_switch" value="Outros Dados" style="width:130px"/>
											</td>
										</tr>
									</tbody>
									</table>
								</div>
							</div>
							<br class="clear"/>
							<div class="content">
								<div id="tab1_content" class="tab_content">
									<h4>${paciente.nome}</h4>
									<div class="linkBar" align="right">
										<n:submit action="salvar" parameters="idPaciente=${paciente.idPaciente}" validate="true"  class="noboard" ><img src="${application}/images/shortcut/save.png" alt="Salvar" class="help" title="Salvar"/></n:submit>
									</div>
									<input type="hidden" value="${hp.idHistoriaPregressa}" name="idHistoriaPregressa">
									<div class="onecolumn">
										<div class="header">
											<span>Antecedentes Pessoais e Familiares</span>
										</div>
										<br class="clear"/>
										<div class="content">
											<div id="accordion1">
												<h3><a href="#">Gesta��o e Nascimento</a></h3>
												<div class="acc2">
													<n:panelGrid columns="6">
														<t:property name="usoMedicamentoGenitora" trueFalseNullLabels="Sim,N�o" renderAs="double" type="select-one-radio" includeBlank="false" />
														<t:property name="usoMedicamentoGenitoraString" renderAs="double" type="TEXT-AREA"/>
														<n:panel colspan="2"></n:panel>
														<t:property name="virosesContraidasDuranteGestacao" trueFalseNullLabels="Sim,N�o" renderAs="double" type="select-one-radio" includeBlank="false" />
														<t:property name="virosesContraidasDuranteGestacaoString" renderAs="double" type="TEXT-AREA"/>
														<n:panel colspan="2"></n:panel>
														<t:property name="tipoParto" renderAs="double"/>
													</n:panelGrid>
												</div>
												<h3><a href="#">Desenvolvimento Psicomotor e Neural</a></h3>
												<div class="acc2">
													<n:panelGrid columns="4">
														<n:panel colspan="2">
															<t:property name="idadeComecouEngatinhar" id="idadeComEng" renderAs="double"/>anos
														</n:panel>
														<n:panel colspan="2">
															<t:property name="pesoAoNascer" renderAs="double"/>Kg
														</n:panel>
														<n:panel colspan="2">
															<t:property name="idadeComecouAndar" renderAs="double"/>anos
														</n:panel>
														<n:panel colspan="2">
															<t:property name="idadeComecouFalar" renderAs="double"/>anos
														</n:panel>
														<n:panel colspan="2">
															<t:property name="tamanhoAoNascer" renderAs="double"/>cm
														</n:panel>
														<n:panel colspan="2">
														
														</n:panel>
														<n:panel colspan="2">
															<t:property name="controleEsfincter" renderAs="double" trueFalseNullLabels="Adequado, Inadequado" includeBlank="false" type="select-one-radio"/>
														</n:panel>
													</n:panelGrid>													
												</div>
												<h3><a href="#">Desenvolvimento Sexual</a></h3>
												<div class="acc2">
													<n:panelGrid columns="4">
														<t:propertyConfig renderAs="double" >
															<n:panel colspan="2">
																<t:property name="idadeInicioPuberdade" renderAs="double"/>anos
															</n:panel>
															<n:panel colspan="2">
																<t:property name="idadePrimeiraMenstruacao" renderAs="double"/>anos
															</n:panel>
															<n:panel colspan="2">
																<t:property name="idadeInicioMenopausa" renderAs="double"/>anos
															</n:panel>
															<n:panel colspan="2">
																<t:property name="idadePrimeiraRelacaoSexual" renderAs="double"/>anos
															</n:panel>
														</t:propertyConfig>
													</n:panelGrid>
												</div>
											</div>
										</div>
									</div>
								<div class="onecolumn">
									<div class="header">
										<span>Antecedentes Pessoais Patol�gicos</span>
									</div>
									<br class="clear"/>
									<div class="content">
										<div id="accordion2">
											<h3><a href="#">Doen�as na Inf�ncia</a></h3>
											<div class="acc3">
												<c:set var="contadorDoencasInfancia" value="0" />
												<table>
													<tr>
														<c:forEach items="${listadoencas}" var="de" >
															<td>
																<label for="de_${de.idDoencasDaInfancia}"  >
																	<input type="checkbox" name="doencasDaInfanciasTransient"  value="${de.idDoencasDaInfancia}" id="de_${de.idDoencasDaInfancia}" checkedList="${de.checked}"/>
																	${de.nome}
																</label>
															</td>
															
															<c:if test="${contadorDoencasInfancia == 4}">
																	<c:set var="contadorDoencasInfancia" value="-1" />
																	<tr/>
																	<tr>
															</c:if>
															<c:set var="contadorDoencasInfancia" value="${contadorDoencasInfancia + 1}" />
																
														</c:forEach>
													</tr>
												</table>
												<br /><br />
												<t:property name="outrasDoencasInfancia" renderAs="double" type="TEXT-AREA" rows="4" cols="30"/>
											</div>
											<h3><a href="#">Demais Doen�as</a></h3>
											<div class="acc3">
											<c:set var="contadorDemaisDoencas" value="0" />
												<table>
													<tr>
														<c:forEach items="${listademaisdoencas}" var="dd" >
															<td>
																<label for="de_${dd.idDemaisDoencas}"  >
																	<input type="checkbox" name="demaisDoencasTransient"  value="${dd.idDemaisDoencas}" id="de_${dd.idDemaisDoencas}" checkedList="${dd.checked}"/>
																	${dd.nome}
																</label>
															</td>
															
															<c:if test="${contadorDemaisDoencas == 4}">
																	<c:set var="contadorDemaisDoencas" value="-1" />
																	<tr/>
																	<tr>
															</c:if>
															<c:set var="contadorDemaisDoencas" value="${contadorDemaisDoencas + 1}" />
																
														</c:forEach>
													</tr>
												</table>
												<br /><br />
												<t:property name="outrasDemaisDoencas" renderAs="double" type="TEXT-AREA" rows="4" cols="30"/>
											</div>
											<h3><a href="#">Alergias</a></h3>
											<div class="acc3">
												<n:panelGrid columns="6">
													<t:property name="alergiaAlimentos" renderAs="double" trueFalseNullLabels="Sim,N�o" includeBlank="false" type="select-one-radio"/>
													<t:property name="alergiaAlimentosString" renderAs="double"/>
													<n:panel colspan="2"></n:panel>
													<t:property name="alergiaMedicamentos" renderAs="double" trueFalseNullLabels="Sim,N�o" includeBlank="false" type="select-one-radio"/>
													<t:property name="alergiaMedicamentosString" renderAs="double"/>
													<n:panel colspan="2"></n:panel>
													<t:property name="outras" renderAs="double" type="TEXT-AREA" rows="4" cols="30"/>
												</n:panelGrid>
											</div>
											<h3><a href="#">Cirurgias Pr�vias</a></h3>
											<div class="acc2">
												<t:detalhe name="cirurgias">
													<t:property name="tipoCirurgia"  style="width:300px;"/>
													<t:property name="mesAnoCirurgia"  type="text" OnKeyUp="javascript:mascara_data_mesano_press($(this))" maxlength="7" style="width:100px;"/>
												</t:detalhe>
											</div>
											<h3><a href="#">Transfus�es Sanguineas</a></h3>
											<div class="acc2">
												<t:detalhe name="transfusoes">
													<t:property name="numeroTransfusoes" />
													<t:property name="mesAnoQuando" datepickeronlymonthandyear="true" type="text" OnKeyUp="mascara_data_press($(this))" maxlength="10" datepickerafter="true" style="width:100px;" onclick="insereDatepicker($(this))"/>
													<t:property name="porque" style="width:500px" colspan="6"/>
												</t:detalhe>
											</div>
											<h3><a href="#">Imuniza��es</a></h3>
											<div class="acc6">
												<n:group legend="Inf�ncia">
													<n:panel>
														<span class="headerSpan">Ao Nascer</span>
													</n:panel>
													<n:panel>
														<c:set var="contador" value="0" />
															<table>
																<tr>
																	<c:forEach items="${listaImunizacoesInfancia0Meses}" var="ii" >
																		<td>
																			<label for="ii_${ii.idImunizacoesInfancia}"  >
																				<input type="checkbox" name="imunizacoesTransient"  value="${ii.idImunizacoesInfancia}" id="ii_${ii.idImunizacoesInfancia}" checkedList="${ii.checked}"/>
																				${ii.nome}
																			</label>
																		</td>
																		
																		<c:if test="${contador == 4}">
																				<c:set var="contador" value="-1" />
																				<tr/>
																				<tr>
																		</c:if>
																		<c:set var="contador" value="${contador + 1}" />
																			
																	</c:forEach>
																</tr>
															</table>
													</n:panel>
													<n:panel>
														<span class="headerSpan">1 M�s</span>
													</n:panel>
													<n:panel>
														<c:set var="contador" value="0" />
															<table>
																<tr>
																	<c:forEach items="${listaImunizacoesInfancia1Meses}" var="ii" >
																		<td>
																			<label for="ii_${ii.idImunizacoesInfancia}"  >
																				<input type="checkbox" name="imunizacoesTransient"  value="${ii.idImunizacoesInfancia}" id="ii_${ii.idImunizacoesInfancia}" checkedList="${ii.checked}"/>
																				${ii.nome}
																			</label>
																		</td>
																		
																		<c:if test="${contador == 4}">
																				<c:set var="contador" value="-1" />
																				<tr/>
																				<tr>
																		</c:if>
																		<c:set var="contador" value="${contador + 1}" />
																			
																	</c:forEach>
																</tr>
															</table>
													</n:panel>
													<n:panel>
														<span class="headerSpan">2 Meses</span>
													</n:panel>
													<n:panel>
														<c:set var="contador" value="0" />
															<table>
																<tr>
																	<c:forEach items="${listaImunizacoesInfancia2Meses}" var="ii" >
																		<td>
																			<label for="ii_${ii.idImunizacoesInfancia}"  >
																				<input type="checkbox" name="imunizacoesTransient"  value="${ii.idImunizacoesInfancia}" id="ii_${ii.idImunizacoesInfancia}" checkedList="${ii.checked}"/>
																				${ii.nome}
																			</label>
																		</td>
																		
																		<c:if test="${contador == 4}">
																				<c:set var="contador" value="-1" />
																				<tr/>
																				<tr>
																		</c:if>
																		<c:set var="contador" value="${contador + 1}" />
																			
																	</c:forEach>
																</tr>
															</table>
													</n:panel>
													<n:panel>
														<span class="headerSpan">3 Meses</span>
													</n:panel>
													<n:panel>
														<c:set var="contador" value="0" />
															<table>
																<tr>
																	<c:forEach items="${listaImunizacoesInfancia3Meses}" var="ii" >
																		<td>
																			<label for="ii_${ii.idImunizacoesInfancia}"  >
																				<input type="checkbox" name="imunizacoesTransient"  value="${ii.idImunizacoesInfancia}" id="ii_${ii.idImunizacoesInfancia}" checkedList="${ii.checked}"/>
																				${ii.nome}
																			</label>
																		</td>
																		
																		<c:if test="${contador == 4}">
																				<c:set var="contador" value="-1" />
																				<tr/>
																				<tr>
																		</c:if>
																		<c:set var="contador" value="${contador + 1}" />
																			
																	</c:forEach>
																</tr>
															</table>
													</n:panel>
													<n:panel>
														<span class="headerSpan">4 Meses</span>
													</n:panel>
													<n:panel>
														<c:set var="contador" value="0" />
															<table>
																<tr>
																	<c:forEach items="${listaImunizacoesInfancia4Meses}" var="ii" >
																		<td>
																			<label for="ii_${ii.idImunizacoesInfancia}"  >
																				<input type="checkbox" name="imunizacoesTransient"  value="${ii.idImunizacoesInfancia}" id="ii_${ii.idImunizacoesInfancia}" checkedList="${ii.checked}"/>
																				${ii.nome}
																			</label>
																		</td>
																		
																		<c:if test="${contador == 4}">
																				<c:set var="contador" value="-1" />
																				<tr/>
																				<tr>
																		</c:if>
																		<c:set var="contador" value="${contador + 1}" />
																			
																	</c:forEach>
																</tr>
															</table>
													</n:panel>
													<n:panel>
														<span class="headerSpan">5 Meses</span>
													</n:panel>
													<n:panel>
														<c:set var="contador" value="0" />
															<table>
																<tr>
																	<c:forEach items="${listaImunizacoesInfancia5Meses}" var="ii" >
																		<td>
																			<label for="ii_${ii.idImunizacoesInfancia}"  >
																				<input type="checkbox" name="imunizacoesTransient"  value="${ii.idImunizacoesInfancia}" id="ii_${ii.idImunizacoesInfancia}" checkedList="${ii.checked}"/>
																				${ii.nome}
																			</label>
																		</td>
																		
																		<c:if test="${contador == 4}">
																				<c:set var="contador" value="-1" />
																				<tr/>
																				<tr>
																		</c:if>
																		<c:set var="contador" value="${contador + 1}" />
																			
																	</c:forEach>
																</tr>
															</table>
													</n:panel>
													<n:panel>
														<span class="headerSpan">6 Meses</span>
													</n:panel>
													<n:panel>
														<c:set var="contador" value="0" />
															<table>
																<tr>
																	<c:forEach items="${listaImunizacoesInfancia6Meses}" var="ii" >
																		<td>
																			<label for="ii_${ii.idImunizacoesInfancia}"  >
																				<input type="checkbox" name="imunizacoesTransient"  value="${ii.idImunizacoesInfancia}" id="ii_${ii.idImunizacoesInfancia}" checkedList="${ii.checked}"/>
																				${ii.nome}
																			</label>
																		</td>
																		
																		<c:if test="${contador == 4}">
																				<c:set var="contador" value="-1" />
																				<tr/>
																				<tr>
																		</c:if>
																		<c:set var="contador" value="${contador + 1}" />
																			
																	</c:forEach>
																</tr>
															</table>
													</n:panel>
													<n:panel>
														<span class="headerSpan">9 Meses</span>
													</n:panel>
													<n:panel>
														<c:set var="contador" value="0" />
															<table>
																<tr>
																	<c:forEach items="${listaImunizacoesInfancia9Meses}" var="ii" >
																		<td>
																			<label for="ii_${ii.idImunizacoesInfancia}"  >
																				<input type="checkbox" name="imunizacoesTransient"  value="${ii.idImunizacoesInfancia}" id="ii_${ii.idImunizacoesInfancia}" checkedList="${ii.checked}"/>
																				${ii.nome}
																			</label>
																		</td>
																		
																		<c:if test="${contador == 4}">
																				<c:set var="contador" value="-1" />
																				<tr/>
																				<tr>
																		</c:if>
																		<c:set var="contador" value="${contador + 1}" />
																			
																	</c:forEach>
																</tr>
															</table>
													</n:panel>
													<n:panel>
														<span class="headerSpan">12 Meses</span>
													</n:panel>
													<n:panel>
														<c:set var="contador" value="0" />
															<table>
																<tr>
																	<c:forEach items="${listaImunizacoesInfancia12Meses}" var="ii" >
																		<td>
																			<label for="ii_${ii.idImunizacoesInfancia}"  >
																				<input type="checkbox" name="imunizacoesTransient"  value="${ii.idImunizacoesInfancia}" id="ii_${ii.idImunizacoesInfancia}" checkedList="${ii.checked}"/>
																				${ii.nome}
																			</label>
																		</td>
																		
																		<c:if test="${contador == 4}">
																				<c:set var="contador" value="-1" />
																				<tr/>
																				<tr>
																		</c:if>
																		<c:set var="contador" value="${contador + 1}" />
																			
																	</c:forEach>
																</tr>
															</table>
													</n:panel>
													<n:panel>
														<span class="headerSpan">15 Meses</span>
													</n:panel>
													<n:panel>
														<c:set var="contador" value="0" />
															<table>
																<tr>
																	<c:forEach items="${listaImunizacoesInfancia15Meses}" var="ii" >
																		<td>
																			<label for="ii_${ii.idImunizacoesInfancia}"  >
																				<input type="checkbox" name="imunizacoesTransient"  value="${ii.idImunizacoesInfancia}" id="ii_${ii.idImunizacoesInfancia}" checkedList="${ii.checked}"/>
																				${ii.nome}
																			</label>
																		</td>
																		
																		<c:if test="${contador == 4}">
																				<c:set var="contador" value="-1" />
																				<tr/>
																				<tr>
																		</c:if>
																		<c:set var="contador" value="${contador + 1}" />
																			
																	</c:forEach>
																</tr>
															</table>
													</n:panel>
													<n:panel>
														<span class="headerSpan">4 anos</span>
													</n:panel>
													<n:panel>
														<c:set var="contador" value="0" />
															<table>
																<tr>
																	<c:forEach items="${listaImunizacoesInfancia48Meses}" var="ii" >
																		<td>
																			<label for="ii_${ii.idImunizacoesInfancia}"  >
																				<input type="checkbox" name="imunizacoesTransient"  value="${ii.idImunizacoesInfancia}" id="ii_${ii.idImunizacoesInfancia}" checkedList="${ii.checked}"/>
																				${ii.nome}
																			</label>
																		</td>
																		
																		<c:if test="${contador == 4}">
																				<c:set var="contador" value="-1" />
																				<tr/>
																				<tr>
																		</c:if>
																		<c:set var="contador" value="${contador + 1}" />
																			
																	</c:forEach>
																</tr>
															</table>
													</n:panel>
													<n:panel>
														<span class="headerSpan">10 anos</span>
													</n:panel>
													<n:panel>
														<c:set var="contador" value="0" />
															<table>
																<tr>
																	<c:forEach items="${listaImunizacoesInfancia120Meses}" var="ii" >
																		<td>
																			<label for="ii_${ii.idImunizacoesInfancia}"  >
																				<input type="checkbox" name="imunizacoesTransient"  value="${ii.idImunizacoesInfancia}" id="ii_${ii.idImunizacoesInfancia}" checkedList="${ii.checked}"/>
																				${ii.nome}
																			</label>
																		</td>
																		
																		<c:if test="${contador == 4}">
																				<c:set var="contador" value="-1" />
																				<tr/>
																				<tr>
																		</c:if>
																		<c:set var="contador" value="${contador + 1}" />
																			
																	</c:forEach>
																</tr>
															</table>
													</n:panel>
												</n:group>
												<n:group legend="Adolesc�ncia 11 � 19 anos">
													<n:panelGrid columns="4">
														<t:property name="imunizacoesAdolescenciaPaciente.hepatiteBDose1" renderAs="double"/>
														<t:property name="imunizacoesAdolescenciaPaciente.hepatiteBDose2" renderAs="double"/>
														<t:property name="imunizacoesAdolescenciaPaciente.hepatiteBDose3" renderAs="double"/>
														<t:property name="imunizacoesAdolescenciaPaciente.DT" type="text" renderAs="double" colspan="1" datepickerrangedate="true"/>
														<t:property name="imunizacoesAdolescenciaPaciente.febreAmarela" type="text" renderAs="double" colspan="1" datepickerrangedate="true"/>
														<t:property name="imunizacoesAdolescenciaPaciente.tripliceViral" renderAs="double"/>
													</n:panelGrid>
												</n:group>
												<n:group legend="Adulto">
													<n:panelGrid columns="4">
														<t:property name="imunizacoesAdultoPaciente.hepatiteBDose1" renderAs="double"/>
														<t:property name="imunizacoesAdultoPaciente.hepatiteBDose2" renderAs="double"/>
														<t:property name="imunizacoesAdultoPaciente.hepatiteBDose3" renderAs="double"/>
														<t:property name="imunizacoesAdultoPaciente.DT" type="text" renderAs="double" colspan="1" datepickerrangedate="true"/>
														<t:property name="imunizacoesAdultoPaciente.febreAmarela" type="text" renderAs="double" colspan="1" datepickerrangedate="true"/>
														<t:property name="imunizacoesAdultoPaciente.tripliceViral" renderAs="double"/>
													</n:panelGrid>
												</n:group>
												<n:group legend="Idoso">
													<n:panelGrid columns="4">
														<t:property name="imunizacoesIdosoPaciente.hepatiteBDose1" renderAs="double"/>
														<t:property name="imunizacoesIdosoPaciente.hepatiteBDose2" renderAs="double"/>
														<t:property name="imunizacoesIdosoPaciente.hepatiteBDose3" renderAs="double"/>
														<t:property name="imunizacoesIdosoPaciente.DT" type="text" renderAs="double" colspan="1" datepickerrangedate="true"/>
														<t:property name="imunizacoesIdosoPaciente.febreAmarela" type="text" renderAs="double" colspan="1" datepickerrangedate="true"/>
														<t:property name="imunizacoesIdosoPaciente.tripliceViral" renderAs="double"/>
														<t:property name="imunizacoesIdosoPaciente.PN23" renderAs="double"/>
													</n:panelGrid>
													<t:detalhe name="imunizacoesIdosoPaciente.influenzas">
														<t:property name="data" datepickeronlymonthandyear="true" type="text" OnKeyUp="mascara_data_press($(this))" maxlength="10" datepickerafter="true" style="width:100px;" onclick="insereDatepicker($(this))"/>
													</t:detalhe>
												</n:group>
	
											</div>
											<h3><a href="#">Medicamentos em Uso</a></h3>
											<div class="acc5">
												<n:panelGrid>
													<t:detalhe name="interacoes" beforeNewLine="javascript:clearCombo()" onNewLine="clearCombo()">
														<n:column header="Medicamentos">
															<n:panelGrid columns="2">
																<t:propertyConfig mode="input" renderAs="double" showLabel="true">
																	<t:property name="classificacaoMedicacao" onchange="buscaMedicamentos(this.value,${index})"/>
																	<t:property name="medicamento"/>
																	<t:property name="posologia" style="width:400px" />
																</t:propertyConfig>
															</n:panelGrid>
														</n:column>
													</t:detalhe>
												</n:panelGrid>
											</div>
										</div>
									</div>
								</div>
								<div class="onecolumn">
									<div class="header">
										<span>Antecedentes Familiares</span>
									</div>
									<div class="content">
										<n:panelGrid columns="4" styleClass="gridData">
											<t:property name="aVC" renderAs="double" trueFalseNullLabels="Sim,N�o" includeBlank="false" type="select-one-radio"/>
											<t:property name="parentescoAVC" renderAs="double"/>
											<t:property name="coleltiase" renderAs="double" trueFalseNullLabels="Sim,N�o" includeBlank="false" type="select-one-radio"/>
											<t:property name="parentescoColeltiase" renderAs="double"/>
											<t:property name="dAC" renderAs="double" trueFalseNullLabels="Sim,N�o" includeBlank="false" type="select-one-radio"/>
											<t:property name="parentescoDAC" renderAs="double"/>
											<t:property name="dislipidemias" renderAs="double" trueFalseNullLabels="Sim,N�o" includeBlank="false" type="select-one-radio"/>
											<t:property name="parentescoDislipidemias" renderAs="double"/>
											<t:property name="dM" renderAs="double" trueFalseNullLabels="Sim,N�o" includeBlank="false" type="select-one-radio"/>
											<t:property name="parentescoDM" renderAs="double"/>
											<t:property name="enxaqueca" renderAs="double" trueFalseNullLabels="Sim,N�o" includeBlank="false" type="select-one-radio"/>
											<t:property name="parentescoEnxaqueca" renderAs="double"/>
											<t:property name="hAS" renderAs="double" trueFalseNullLabels="Sim,N�o" includeBlank="false" type="select-one-radio"/>
											<t:property name="parentescoHAS" renderAs="double"/>
											<t:property name="morteSubita" renderAs="double" trueFalseNullLabels="Sim,N�o" includeBlank="false" type="select-one-radio"/>
											<t:property name="parentescoMorteSubita" renderAs="double"/>
											<t:property name="tBC" renderAs="double" trueFalseNullLabels="Sim,N�o" includeBlank="false" type="select-one-radio"/>
											<t:property name="parentescoTBC" renderAs="double"/>
											<t:property name="ulceraPeptica" renderAs="double" trueFalseNullLabels="Sim,N�o" includeBlank="false" type="select-one-radio"/>
											<t:property name="parentescoUlceraPeptica" renderAs="double"/>
											<t:property name="varizes" renderAs="double" trueFalseNullLabels="Sim,N�o" includeBlank="false" type="select-one-radio"/>
											<t:property name="parentescoVarizes" renderAs="double"/>
											<t:property name="outrosAntecedentesFamiliares" renderAs="double" trueFalseNullLabels="Sim,N�o" includeBlank="false" type="select-one-radio"/>
											<t:property name="parentescoOutrosAntecedentesFamiliares" renderAs="double"/>
										</n:panelGrid>
											<t:property name="outrosString" renderAs="double" type="TEXT-AREA" rows="4" cols="40"/>
									</div>
								</div>
								</div>
								<div id="tab2_content" class="tab_content hide">
									<h4>${paciente.nome}</h4>
									<div class="linkBar" align="right">
										<n:submit action="salvar" parameters="idPaciente=${paciente.idPaciente}" validate="true"  class="noboard" ><img src="${application}/images/shortcut/save.png" alt="Salvar" class="help" title="Salvar"/></n:submit>
									</div>
									<div class="onecolumn">
										<div class="header">
											<span>H�bitos</span>
										</div>
										<br class="clear"/>
										<div class="content">
											<div id="accordion1">
												<h3><a href="#">H�bitos de Vida</a></h3>
												<div class="acc3">
													<c:set var="contadorAlimentacao" value="0" />
													<table>
														<tr>
															<c:forEach items="${listaalimentacao}" var="alimentacao" >
																<td>
																	<label for="alim_${alimentacao.idAlimentacao}"  >
																		<input type="checkbox" name="alimentacaosTransient"  value="${alimentacao.idAlimentacao}" id="alim_${alimentacao.idAlimentacao}" checkedList="${alimentacao.checked}"/>
																		${alimentacao.nome}
																	</label>
																</td>
																
																<c:if test="${contadorAlimentacao == 3}">
																	<c:set var="contadorAlimentacao" value="-1" />
																	<tr/>
																	<tr>
																</c:if>
																<c:set var="contadorAlimentacao" value="${contadorAlimentacao + 1}" />
															</c:forEach>
														</tr>
													</table>
													<br /><br />
												</div>
												<h3><a href="#">Ocupa��o Atual e Anteriores</a></h3>
												<div class="acc3">
													<t:detalhe name="ocupacoes">
														<t:property name="ocupacao" style="width:400px;"/>
														<t:property name="inicio" type="text" />
														<t:property name="fim" />
													</t:detalhe>													
												</div>
												<h3><a href="#">Atividades F�sicas</a></h3>
												<div class="acc2">
													<t:property name="atividadesFisicas" renderAs="double"/>
												</div>
												<h3><a href="#">H�bitos</a></h3>
												<div class="acc4">
													<br />
														<n:group legend="Tabaco">
															<n:panelGrid columns="6">
																<t:property name="qtdCigarros" renderAs="double"/>
																<t:property name="frequencia" renderAs="double"/>
																<t:property name="inicioVicio" renderAs="double" type="text" OnKeyUp="mascara_data_press($(this))" maxlength="10" datepickerafter="true" style="width:100px;" onclick="insereDatepicker($(this))"/>
																<t:property name="exFumanteHaQntsAnos" renderAs="double"/>
															</n:panelGrid>
														</n:group>
														<n:group legend="Bebidas Alc�olicas">
															<n:panelGrid columns="6">
																<t:property name="tipoBebida" renderAs="double" colspan="6"/>
																<t:property name="frequenciaBebida" renderAs="double" colspan="6"/>
																<t:property name="exAlcolatraHaQntsAnos" renderAs="double"/>
															</n:panelGrid>
														</n:group>
														<n:group legend="Anabolizantes e Anfetaminas">
															<t:property name="usoAnabolizantesAnfetaminas" renderAs="double" includeBlank="false" trueFalseNullLabels="Sim,N�o" type="select-one-radio"/>
														</n:group>
														<n:group legend="Drogas Il�citas">
															<t:detalhe name="drogasIlicitas">
																<t:property name="drogasIlicitas"/>
																<t:property name="quantidade"/>
																<t:property name="frequencia"/>
																<t:property name="inicioVicio" type="text" OnKeyUp="mascara_data_press($(this))" maxlength="10" datepickerafter="true" style="width:100px;" onclick="insereDatepicker($(this))"/>														
															</t:detalhe>
															<t:property name="naoUtilizaDrogasIlicitasHaQntsAnos" renderAs="double"/>
														</n:group>
												</div>
											</div>
										</div>
									</div>
			            			
									<br class="clear"/>
								</div>
								<div id="tab3_content" class="tab_content hide">
									<h4>${paciente.nome}</h4>
									<div class="linkBar" align="right">
										<n:submit action="salvar" parameters="idPaciente=${paciente.idPaciente}" validate="true"  class="noboard" ><img src="${application}/images/shortcut/save.png" alt="Salvar" class="help" title="Salvar"/></n:submit>
									</div>
									<div class="onecolumn">
										<div class="header">
											<span>Condi��es socioecon�micas e culturais</span>
										</div>
										<br class="clear"/>
										<div class="content">
											<div id="accordion1">
												<div class="acc2">
													<t:property name="meioUrbano" renderAs="double" />
													<br />
													<t:property name="nivelCultural" renderAs="double" />
												</div>
											</div>
										</div>
									</div>
			            			
									<br class="clear"/>
								</div>
								<div id="tab4_content" class="tab_content hide">
									<h4>${paciente.nome}</h4>
									<div class="linkBar" align="right">
										<n:submit action="salvar" parameters="idPaciente=${paciente.idPaciente}" validate="true"  class="noboard" ><img src="${application}/images/shortcut/save.png" alt="Salvar" class="help" title="Salvar"/></n:submit>
									</div>
									<div class="onecolumn">
										<div class="header">
											<span>Outras informa��es Relevantes</span>
										</div>
										<br class="clear"/>
										<div class="content">
											<div class="acc2">
												<t:property name="tipoSanguineo" renderAs="double" colspan="2"/>
												<n:panelGrid columns="2">
													<n:panel colspan="2"></n:panel>
												</n:panelGrid>
											</div>
											<div class="acc3">
												<n:group legend="Medidas Anteriores">
													<t:detalhe name="listaCrescimentoPacienteTransient" showColunaAcao="false" showBotaoNovaLinha="false">
														<t:property mode="output" name="pesoString"  style="width:60px;" />
														<t:property mode="output" name="circAbdominalString"  style="width:60px;" />
														<t:property mode="output" name="alturaString" style="width:60px;"  />
														<t:property mode="output" name="imc"/>
														<n:column header="Situa��o">
															<t:property name="descricao" mode="output"/>
														</n:column>
														<n:column header="Data de Avalia��o">
															<t:property name="data" mode="output"/>
														</n:column>
													</t:detalhe>
												</n:group>
												<br>
												<br>
												<t:detalhe name="listaCrescimentoPaciente">
													<t:property name="peso"  style="width:60px;" onkeypress="mascara(this,mascaraPeso)" maxlength="7"/>
													<t:property name="circAbdominal"  style="width:60px;" onkeypress="mascara(this,mascaraAltura)" maxlength="4"/>
													<t:property name="altura" style="width:60px;"  onkeypress="mascara(this,mascaraAltura)" maxlength="4" id="altura" onblur="calculaIMC(${index})"/>
													<t:property name="imc" disabled="disabled"/>
													<n:column header="Situa��o">
														<n:panel >
															<span id="panel_${index}"></span>
														</n:panel>
													</n:column>
												</t:detalhe>
											</div>
										</div>
									</div>
			            			
									<br class="clear"/>
								</div>
							</div>
					</n:bean>
				</t:tela>
			</div>
			<!-- End one column tab content window -->
			
			<!-- Begin footer -->
			<div id="footer">
				 Copyright&copy; by OrionX Technologies
			</div>
			<!-- End footer -->
			
		</div>
	</div><!-- End inner -->
</div>
<!-- End content -->

<div id="menuLateral">
	<ul id="shortcut">
		<li>
   		   <a rel="modalFrame" href="/ClinicalSys/clinic/pag/Paciente?ACAO=editar&onePath=true&idPaciente=${paciente.idPaciente}" title="Cadastro do Paciente">
		    <img src="${application}/images/shortcut/cadastro.png" alt="Cadastro do Paciente"/><br/>
		    <strong>Cadastro</strong>
		  </a>
		</li>
		<li>
   		   <a href="javascript:redirectHistoriaPregressa(${paciente.idPaciente})" title="Hist�ria Pregressa">
		    <img src="${application}/images/shortcut/historico.png" alt="Hist�ria Pregressa"/><br/>
		    <strong>Hist. Preg</strong>
		  </a>
		</li>
		<li>
   		   <a href="javascript:redirectAnamnese(${paciente.idPaciente})" title="Anamnese">
		    <img src="${application}/images/shortcut/anamnese.png" alt="Anamnese"/><br/>
		    <strong>Anamnese</strong>
		  </a>
		</li>
		<li>
   		    <a href="javascript:redirectPrescricao(${paciente.idPaciente})" title="Prescri��o">
		    <img src="${application}/images/shortcut/exameFisico.png" alt="Prescri��o"/><br/>
		    <strong>Prescri��o</strong>
		  </a>
		</li>
		<li>
   		   <a href="javascript:redirectExames(${paciente.idPaciente})" title="Exames">
		    <img src="${application}/images/shortcut/exames.png" alt="Exames"/><br/>
		    <strong>Exames</strong>
		  </a>
		</li>
		<li>
   		   <a href="javascript:redirectFormularios(${paciente.idPaciente})" title="Formul�rios">
		    <img src="${application}/images/shortcut/pdf.png" alt="Formul�rios"/><br/>
		    <strong>Formul�rios</strong>
		  </a>
		</li>
	</ul>
	<div id="headerMenuLateral">
		<div id="actionMenuLateral">&laquo;</div>
	</div>
</div>
<div id="actionMenuLateral-open">&raquo;</div>

<c:if test="${param.onePath != 'true'}">
	<div id="rodapeFixo">
	<div id="headerFooter"><div align="right"><div id="divButtonClose">Fechar</div></div></div>
		
		<!-- Begin shortcut menu -->
		<ul id="shortcut">
   			<li>
   			   <a href="#" onclick="openDialogAguarde();location.href='${application}/clinic/process/listaEspera'" title="Lista de Espera">
			    <img src="${application}/images/shortcut/atendimento.png" alt="Lista de Espera"/><br/>
			    <strong>Espera</strong>
			  </a>
			</li>
   			<li>
   			  <a href="#" onclick="openDialogAguarde();location.href='${application}/clinic/process/Calendar'" title="Agenda">
			    <img src="${application}/images/shortcut/agenda.png" alt="Agenda"/><br/>
			    <strong>Agenda</strong>
			  </a>
			</li>
			<li>
   			  <a href="#" onclick="openDialogAguarde();location.href='${application}/clinic/pag/Paciente'" id="shortcut_contacts" title="Cadastro de Pacientes">
			    <img src="${application}/images/shortcut/patient.png" alt="Cadastro de Pacientes"/><br/>
			    <strong>Paciente</strong>
			  </a>
			</li>
			<li>
   			  <a href="#" onclick="openDialogAguarde();location.href='${application}/clinic/process/Medicamentos'" id="shortcut_contacts" title="Medicamentos">
			    <img src="${application}/images/shortcut/bookmedicine.png" alt="Medicamentos"/><br/>
			    <strong>Drogas</strong>
			  </a>
			</li>
   			<li>
   			  <a href="modal_window.html" title="Relat�rios">
			    <img src="${application}/images/shortcut/report.png" alt="Relat�rios"/><br/>
			    <strong>Relat�rios</strong>
			  </a>
			</li>
   			<li>
   			  <a href="modal_window.html" title="Dashboard">
			    <img src="${application}/images/shortcut/stats.png" alt="Dashboard"/><br/>
			    <strong>Dashboard</strong>
			  </a>
			</li>
			<li>
   			  <a href="#" onclick="openMenuByModulo('estoque')" title="Administra��o de estoque e financeiro">
			    <img src="${application}/images/shortcut/stock.png" alt="Administra��o de estoque e financeiro"/><br/>
			    <strong>Adm</strong>
			  </a>
			</li>
			<li>
   			  <a href="#" onclick="openMenuByModulo('util')" title="Utilit�rios">
			    <img src="${application}/images/shortcut/util.png" alt="Utilit�rios"/><br/>
			    <strong>Utilit�rios</strong>
			  </a>
			</li>
			<li>
   			  <a href="#" onclick="openMenuByModulo('system')" title="Configura��es do sistema">
			    <img src="${application}/images/shortcut/setting.png" alt="Configura��es"/><br/>
			    <strong>Config</strong>
			  </a>
			</li>
 			</ul>
		<!-- End shortcut menu -->
	</div>
	<div id="openRodapeFixo">
		<div id="divButton">Abrir</div>
	</div>
</c:if>
<!-- ui-dialog -->
<!-- Include das menssagens de alerta -->
<jsp:include page="../../menssages/dialogInformation.jsp"></jsp:include>
<style type="text/css">
	.threecolumn_each{
		width: 45%;
	}
	
	h3{
		background-color: #fafafa;		
		-webkit-box-shadow: #CCC 0px 1px 2px;
		border: 1px solid #CDCDCD;
		border-radius: 4px 4px 4px 4px;
		
	}
	
</style>
<script type="text/javascript">
var icons = {
		header: "ui-icon-circle-arrow-e",
		headerSelected: "ui-icon-circle-arrow-s"
	};
$(function() {
	$( "#accordion1,#accordion2,#accordion3,#accordion4,#accordion5" ).accordion({
		fillSpace: true,
		navigation: true
	});
});

$(document).ready(function(){
	$('#show_menu').fadeIn();
	$('#wysiwyg').css('width', '97%');
	//setNotifications();
	startCountdown();
	
	$('input[checkedList=true]').each(function(){
		this.click();	
	});
	
	$('#divButtonClose').click(function(){
		$('#rodapeFixo').slideUp();
		$('#openRodapeFixo').slideDown();
	});
	$('#openRodapeFixo').click(function(){
		$('#openRodapeFixo').slideUp();
		$('#rodapeFixo').slideDown();
	});
	$('#actionMenuLateral').click(function(){
		//$('#menuLateral').hide('slide', {direction: 'left'}, 600);
		$('#menuLateral').animate({width: 'hide'});
		$('#actionMenuLateral-open').animate({width: 'show'});
	});
	$('#actionMenuLateral-open').click(function(){
		$('#menuLateral').animate({width: 'show'});
		$('#actionMenuLateral-open').animate({width: 'hide'});
	});
	$('.acc').height(260);
	$('.acc2').height(140);
	$('.acc3').height(230);
	$('.acc4').height(630);
	$('.acc5').height(400);
	$('.acc6').height(500);
});
	$(function() {
		$('a[rel=modalFrame]').click(function(e) {
			e.preventDefault();
			var $this = $(this);
			var horizontalPadding = 30;
			var verticalPadding = 30;
	        $('<iframe id="cadPaciente" class="externalSite" src="' + this.href + '" />').dialog({
	            title: ($this.attr('title')) ? $this.attr('title') : '',
	            autoOpen: true,
	            width: 1000,
	            height: 630,
	            modal: true,
	            resizable: true,
				autoResize: true,
	            overlay: {
	                opacity: 0.5,
	                background: "black"
	            }
	        }).width(1000 - horizontalPadding).height(630 - verticalPadding);	        
		});
	});
	function closeIframe(){
	   var iframe = document.getElementById('cadPaciente');
	   iframe.parentNode.removeChild(iframe);
	   openDialogAguarde();
	   location.href = location.href;
	}
	function insereDatepicker(param){
		param.datepicker();
		$('#idadeComEng').click();
		$('#idadeComEng').focus();
		param.focus();
		param.click();
	}
	function clearCombo(){
		$('select[name*=.medicamento]:last').empty();
	}
	function redirectHistoriaPregressa(param){
		openDialogAguarde();
		location.href= '/ClinicalSys/clinic/process/historiaPregressa?idPaciente='+param;
	}
	
	function calculaIMC(idx){
		var peso   = $(form["listaCrescimentoPaciente["+idx+"].peso"]).val();
		var altura = $(form["listaCrescimentoPaciente["+idx+"].altura"]).val();
		var imc = peso/ Math.pow(altura,2);
		imc = imc.toFixed(2);
		$(form["listaCrescimentoPaciente["+idx+"].imc"]).val(imc);
		var msg = "";
		
		if(imc < 18.5){
			msg = "Abaixo do peso ideal";
		}else if(imc >= 18.5 && imc <= 24.9){
			msg = "Peso Ideal";
		}else if(imc >= 25 && imc <= 34.9){
			msg = "Sobrepeso";
		}else if(imc >= 30.0 && imc <= 34.9){
			msg = "Obesidade grau 1";
		}else if(imc >= 35.0 && imc <= 39.9){
			msg = "Obesidade grau 2";
		}else if(imc >= 40.0){
			msg = "Obesidade grau 3";
		}
		
		$('#panel_'+idx).html(msg);
	}
	function redirectAnamnese(param){
		openDialogAguarde();
		location.href= '/ClinicalSys/clinic/process/Anamnese?idPaciente='+param;
	}
	function redirectPrescricao(param){
		openDialogAguarde();
		location.href= '/ClinicalSys/clinic/process/prescricao?idPaciente='+param;
	}
	function redirectFormularios(param){
		openDialogAguarde();
		location.href= '/ClinicalSys/clinic/process/formularios?idPaciente='+param;
	}
	function redirectExames(param){
		openDialogAguarde();
		location.href= '/ClinicalSys/clinic/process/exames?idPaciente='+param;
	}
	function redirectHistoriaPregressa(param){
		openDialogAguarde();
		location.href= '/ClinicalSys/clinic/process/historiaPregressa?idPaciente='+param;
	}�
	function mascara_data_mesano_press(data){
	    var mydata = ''; 
	    mydata = mydata + data.val(); 
	    if (mydata.length == 2){ 
	        mydata = mydata + '/'; 
	        data.val(mydata); 
	    }
	}

</script>
</body>
</html>
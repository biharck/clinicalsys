package br.com.orionx.clinicalsys.service;

import java.util.List;

import br.com.orionx.clinicalsys.bean.HistoriaPregressa;
import br.com.orionx.clinicalsys.bean.MedicamentosEmUsoPaciente;
import br.com.orionx.clinicalsys.dao.MedicamentosEmUsoPacienteDAO;
import br.com.orionx.clinicalsys.util.generics.GenericService;

public class MedicamentosEmUsoPacienteService extends GenericService<MedicamentosEmUsoPaciente>{

	public MedicamentosEmUsoPacienteDAO dao;
	public void setDao(MedicamentosEmUsoPacienteDAO dao) {
		this.dao = dao;
	}
	
	public List<MedicamentosEmUsoPaciente> getByHistoriaPregressa(HistoriaPregressa hp){
		return dao.getByHistoriaPregressa(hp);
	}
}

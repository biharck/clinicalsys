package br.com.orionx.clinicalsys.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.orionx.clinicalsys.bean.CrescimentoPaciente;
import br.com.orionx.clinicalsys.bean.HistoriaPregressa;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class CrescimentoPacienteDAO extends GenericDAO<CrescimentoPaciente>{

	
	public List<CrescimentoPaciente> findByHistoriaPregressa(HistoriaPregressa hp){
		if(hp.getIdHistoriaPregressa() == null){
			return new ArrayList<CrescimentoPaciente>();
		}
		return query()
				.select("crescimentoPaciente")
				.from(CrescimentoPaciente.class)
				.where("crescimentoPaciente.historiaPregressa = ?", hp)
				.list();
	}
	
	
}

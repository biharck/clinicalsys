package br.com.orionx.clinicalsys.filtros;

import br.com.orionx.clinicalsys.bean.Setor;
import br.com.orionx.clinicalsys.util.generics.FiltroListagem;

public class SalaFiltro extends FiltroListagem{

	private String nome;
	private Setor setor;
	private AtivoEnum ativoEnum;
	
	public String getNome() {
		return nome;
	}
	public Setor getSetor() {
		return setor;
	}
	public AtivoEnum getAtivoEnum() {
		return ativoEnum;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setAtivoEnum(AtivoEnum ativoEnum) {
		this.ativoEnum = ativoEnum;
	}
	public void setSetor(Setor setor) {
		this.setor = setor;
	}
}

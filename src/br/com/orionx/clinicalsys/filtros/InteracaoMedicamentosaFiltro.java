package br.com.orionx.clinicalsys.filtros;

import br.com.orionx.clinicalsys.bean.ClassificacaoMedicacao;
import br.com.orionx.clinicalsys.bean.Medicamento;
import br.com.orionx.clinicalsys.util.generics.FiltroListagem;

public class InteracaoMedicamentosaFiltro  extends FiltroListagem{

	private ClassificacaoMedicacao classificacaoMedicacao;
	private Medicamento medicamento;
	private AtivoEnum ativoEnum;
	
	//GET
	public ClassificacaoMedicacao getClassificacaoMedicacao() {
		return classificacaoMedicacao;
	}
	public Medicamento getMedicamento() {
		return medicamento;
	}
	public AtivoEnum getAtivoEnum() {
		return ativoEnum;
	}
	
	//SET
	public void setClassificacaoMedicacao(ClassificacaoMedicacao classificacaoMedicacao) {
		this.classificacaoMedicacao = classificacaoMedicacao;
	}
	public void setMedicamento(Medicamento medicamento) {
		this.medicamento = medicamento;
	}
	public void setAtivoEnum(AtivoEnum ativoEnum) {
		this.ativoEnum = ativoEnum;
	}
	
	
	
}

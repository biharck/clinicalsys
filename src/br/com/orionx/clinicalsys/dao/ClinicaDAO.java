package br.com.orionx.clinicalsys.dao;

import org.nextframework.core.standard.Next;
import org.nextframework.persistence.SaveOrUpdateStrategy;

import br.com.orionx.clinicalsys.bean.Clinica;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class ClinicaDAO extends GenericDAO<Clinica>{
	
	/**
	 * <p>M�todo respons�vel em retornar os dados da cl�nica
	 * @return {@link Clinica}
	 * @author biharck
	 */
	public Clinica getClinica(){
		return
			query().select("clinica.idUsuarioAltera,clinica.dtAltera,clinica.idClinica,clinica.razaoSocial,clinica.nomeFantasia," +
					"clinica.cnpj,clinica.inscricaoMunicipal,clinica.cep,clinica.inscricaoEstadual,clinica.logradouro," +
					"clinica.numero,clinica.complemento,clinica.bairro,clinica.email,clinica.emailAlternativo,clinica.twitter," +
					"clinica.webSite,tipoLogradouro.codigo,tipoLogradouro.descricao,tipoLogradouro.idTipoLogradouro," +
					"municipio.idMunicipio, municipio.nome, uf.idUF, uf.nome, pais.idPais, pais.nome,telefones.idTelefoneClinica," +
					"telefones.telefone,telefoneTipo.idTelefoneTipo, telefoneTipo.tipo,ramais.localizacao, ramais.idRamal, ramais.numero," +
					"logomarca.cdfile, logomarca.contenttype, logomarca.name, logomarca.size, logomarca.content")
			.leftOuterJoin("clinica.tipoLogradouro tipoLogradouro")
			.leftOuterJoin("clinica.municipio municipio")
			.leftOuterJoin("municipio.unidadeFederativa uf")
			.leftOuterJoin("uf.pais pais")
			.leftOuterJoin("clinica.ramais ramais")
			.leftOuterJoin("clinica.telefones telefones")
			.leftOuterJoin("telefones.telefoneTipo telefoneTipo")
			.leftOuterJoin("clinica.logomarca logomarca")
			.where("clinica.idClinica = "+ getMaxId() )
			.unique();
	}
	
	
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("ramais");
		save.saveOrUpdateManaged("telefones");
	}
	
	public Integer getMaxId(){
		return getJdbcTemplate().queryForInt("select coalesce(max(idClinica),1) from clinica");
	}
	
	/* singleton */
	private static ClinicaDAO instance;

	public static ClinicaDAO getInstance() {
		if (instance == null) {
			instance = Next.getObject(ClinicaDAO.class);
		}
		return instance;
	}
	

}

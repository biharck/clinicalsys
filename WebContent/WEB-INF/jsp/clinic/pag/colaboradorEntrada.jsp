<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="cs" uri="clinicalsys"%>


<t:entrada>
<c:if test="${param.ACAO != 'criar'}">
	<div class="ultimaalteracao">
		<n:panel>Registro criado por <font style="color:#E77272">${cs:getuserbyid(colaborador.idColaborador)}</font> no dia <font style="color:#E77272">${cs:dateformat(colaborador.dtInclusao)}</font> �s <font style="color:#E77272">${cs:hourformat(colaborador.dtInclusao)}</font> </n:panel>
	</div>
	<br/>
</c:if>
	<t:property name="idColaborador" type="hidden" write="false"/>
	<t:janelaEntrada>
		<t:tabelaEntrada columns="1">
			<n:panel  >
				<n:panelGrid columns="2" columnStyles="width:400px,width:300px">
					<n:group legend="Dados Pessoais" colspan="1">
						<n:panelGrid columns="6" >
							<t:property name="nome" id="nome" style="width:400px;" renderAs="doubleline" colspan="4" tabindex="1"/>
							<t:property name="apelido" style="width:200px;" renderAs="doubleline" colspan="4" tabindex="2"/>
							<t:property name="sexo" renderAs="doubleline" colspan="2" tabindex="3"/>
							<t:property name="cpf" renderAs="doubleline" style="width:150px;" colspan="2" tabindex="4" onchange="validateCpfOver(this)"/>
							<t:property name="rg" renderAs="doubleline" style="width:200px;" colspan="2" tabindex="5"/>
							<t:property name="dataEmissao" type="text" OnKeyUp="mascara_data_press($(this))" maxlength="10" renderAs="doubleline" datepickerrangedate="true" style="width:100px;"  colspan="2" tabindex="6"/>
							<t:property name="orgaoExpedidor" renderAs="doubleline" style="width:100px;" colspan="2" tabindex="7"/>
							<t:property name="dataNascimento" type="text" OnKeyUp="mascara_data_press($(this))" maxlength="10" renderAs="doubleline" datepickerrangedate="true" style="width:100px;"  colspan="2" tabindex="8"/>
							<t:property name="ctps" renderAs="doubleline" style="width:150px;" colspan="2" tabindex="9"/>
							<t:property name="dtEmissaoCtps" type="text" OnKeyUp="mascara_data_press($(this))" maxlength="10" renderAs="doubleline" datepickerrangedate="true" style="width:100px;"  colspan="2" tabindex="10"/>
							<t:property name="ufctps" renderAs="doubleline" colspan="2" tabindex="11"/>
							<t:property name="seriectps" renderAs="doubleline" style="width:100px;" colspan="2" tabindex="12"/>
							<t:property name="cnh" renderAs="doubleline" style="width:150px;" colspan="2" tabindex="13"/>
							<t:property name="dtemissaocnh" type="text" OnKeyUp="mascara_data_press($(this))" maxlength="10" renderAs="doubleline" datepickerrangedate="true" style="width:100px;"  colspan="2" tabindex="14"/>
							<t:property name="dtvalidadecnh" type="text" OnKeyUp="mascara_data_press($(this))" maxlength="10" renderAs="doubleline" datepickerrangedate="true" style="width:100px;"  colspan="2" tabindex="15"/>
							<t:property name="ufcnh" renderAs="doubleline" colspan="2" tabindex="16"/>
							<t:property name="estadoCivil" renderAs="doubleline" colspan="2" tabindex="17"/>
							<t:property name="nacionalidade" renderAs="doubleline" colspan="2" tabindex="18" insertPath="/system/pag/Nacionalidade"/>
							<t:property name="documentomilitar" renderAs="doubleline" style="width:100px;" colspan="2" tabindex="19"/>
							<t:property name="pisPasep" renderAs="doubleline" style="width:100px;" colspan="2" tabindex="20"/>
							<t:property name="mae" renderAs="doubleline" colspan="6" tabindex="21" style="width:400px;"/>
							<t:property name="pai" renderAs="doubleline" colspan="6" tabindex="22" style="width:400px;"/>
							<t:property name="tituloeleitoral" renderAs="doubleline" style="width:100px;" colspan="2" tabindex="23"/>
							<t:property name="zonaeleitoral" renderAs="doubleline" style="width:100px;" colspan="2" tabindex="24"/>
							<t:property name="secaoeleitoral" renderAs="doubleline" style="width:100px;" colspan="2" tabindex="25"/>
							<t:property name="naturalidade" renderAs="doubleline" colspan="4" tabindex="26"/>
							<t:property name="ativo" renderAs="doubleline" colspan="2" tabindex="27"  trueFalseNullLabels="Ativo,Inativo,"/>
							<t:property name="email" renderAs="doubleline" colspan="6" tabindex="28" style="width:400px;" onchange="validaEmailFormulario(this)" id="email"/>
							<c:if test="${empty colaborador.idColaborador}">
								<t:property name="confirmacaoEmail" renderAs="doubleline" colspan="6" tabindex="29" style="width:400px;" onchange="validaEmailFormulario(this);equalsEmail(this,document.getElementById('email'));"/>
							</c:if>
							
						</n:panelGrid>
				</n:group>
				<n:panel id="picture" style="text-align:center;">
					<n:group legend="Foto">
						<c:if test="${showpicture}">
							<n:link url="/DOWNLOADFILE/${colaborador.foto.cdfile}">
								<center><img src="${pageContext.request.contextPath}/DOWNLOADFILE/${colaborador.foto.cdfile}" id="imagem" class="foto"/></center>
							</n:link>
						</c:if>
						<c:if test="${!showpicture}">
							<div class="image">
								<center><img src="${pageContext.request.contextPath}/images/peopleNotFound.png" id="imagem" style="padding-top:13px;"/></center>
							</div>
						</c:if>
						<t:property name="foto"  id="foto" label="" renderAs="doubleline" onchange="verifyExt();"  tabindex="30"/>
					</n:group>
					<br>
					<n:group legend="Usu�rio">
						<c:if test="${!empty colaborador.idColaborador}">
							<c:if test="${!empty colaborador.usuario.idUsuario}">
								<t:property name="usuario.login" colspan="6" renderAs="doubleline" tabindex="31" mode="output" style="color: red;"/>
							</c:if>
						</c:if>
						<c:if test="${empty colaborador.idColaborador}">
							<c:if test="${empty colaborador.usuario.idUsuario}">
								<t:property name="usuario.login" colspan="6" renderAs="doubleline" tabindex="32"/>
								<t:property name="usuario.password" colspan="6" renderAs="doubleline" tabindex="33" id="psw"/>
								<t:property name="usuario.confirmacaoSenha" colspan="6" renderAs="doubleline" tabindex="34" id="pswconf" onchange="equalsPassword(this,document.getElementById('psw'));" type="password"/>
							</c:if>
						</c:if>
						
					</n:group>
					<n:group legend="Telefones">
					<t:detalhe name="telefones" showBotaoNovaLinha="true">
						<t:property name="telefone" style="width:120px;"/>
						<t:property name="telefoneTipo"/>
					</t:detalhe>
					</n:group>
				</n:panel>
				<n:group legend="Permiss�es" colspan="2">
					<cs:checklist itens="${listaPapel}" name="usuario.papeis" renderas="doubleline"/>
					<t:property name="permissaoAgenda" renderAs="doubleline"/>
				</n:group>
				<n:group legend="Endere�o" colspan="2">
				<n:panelGrid columns="6" colspan="2">
					<t:property name="cep" id="cep" renderAs="doubleline" colspan="1" onchange="ajaxBuscaCEP(this.value)" style="width:100px;"/>
					<n:panel colspan="1" id="modalLink">
						<label for="btn_busca_end">N�o sabe o CEP?</label><br>
						<a id="btn_busca_end"  href="javascript:openDialogBuscaCEP()"><img src="/ClinicalSys/images/shortcut/find-help.png"/> Clique aqui</a>
					</n:panel>
					<n:panel colspan="4"></n:panel>
					<t:property name="tipoLogradouro" renderAs="doubleline" colspan="2" id="tipoLogradouro"/>
					<t:property name="logradouro" renderAs="doubleline" colspan="3" style="width:400px;" id="logradouro"/>
					<t:property name="numero" renderAs="doubleline" colspan="2" id="numero"/>
					<t:property name="complemento" renderAs="doubleline" colspan="2" style="width:100px;"/>
					<t:property name="bairro" renderAs="doubleline" colspan="2" id="bairro" style="width:200px;"/>
					<n:comboReloadGroup useAjax="true">
						<t:property name="pais" renderAs="doubleline" colspan="2" onchange="openAguardeAjax()" id="pais"/>
						<t:property name="unidadeFederativa" renderAs="doubleline" colspan="2" onchange="openAguardeAjax()" id="uf"/>
						<t:property name="municipio" renderAs="doubleline" colspan="2" id="cidade"/>
					</n:comboReloadGroup>
				</n:panelGrid>
			</n:group>				
				
				</n:panelGrid>
			</n:panel>
		</t:tabelaEntrada>
				
		<t:tabelaEntrada title="Qualifica��o Profissional">
			<n:panel  >
				<n:panelGrid columns="1" columnStyles="width:400px,width:300px">
					<n:group legend="Grau de Instru��o">
						<t:property name="escolaridade" renderAs="doubleline" colspan="6"/>
					</n:group>
					<br/>
					<n:group legend="Especializa��es">
						<t:detalhe name="listaEspecializacaoColaboradors" nomeColunaAcao="Apagar?">
							<t:property name="especializacao" insertPath="/system/pag/Especializacao" />
							<t:property name="conselho" />
							<t:property name="numeroConselho" />
							<t:property name="ufConselho" />
						</t:detalhe>
					</n:group>
				</n:panelGrid>
			</n:panel>
		</t:tabelaEntrada>
		<t:detalhe name="cargos" nomeColunaAcao="Apagar?">
			<t:property name="cargo"/>
			<t:property name="regimeTrabalho" />
			<t:property name="dataAdmissao" type="text" OnKeyUp="mascara_data_press($(this))" maxlength="10" datepickerafter="true" style="width:100px;" onclick="insereDatepicker($(this))"/>
			<t:property name="dataDesligamento"  type="text" OnKeyUp="mascara_data_press($(this))" maxlength="10" datepickerafter="true" style="width:100px;" onclick="insereDatepicker($(this))"/>
		</t:detalhe>
		<t:tabelaEntrada title="Conta Banc�ria">
			<n:panel  >
				<n:panelGrid columns="4" columnStyles="width:400px,width:300px">
					<n:group legend="Dados Banc�rios">
						<n:panelGrid columns="2">
							<t:property name="banco" renderAs="doubleline" colspan="2"/>
							<t:property name="numeroAgencia" style="width:40px;"  renderAs="doubleline" colspan="1"/>
		                    <t:property style="width:24px;" name="dvAgencia" id="dvagencia" renderAs="doubleline" colspan="1"/>
							<t:property name="numeroConta" style="width:120px;" id="conta" renderAs="doubleline" colspan="1"/> 
							<t:property style="width:24px;" name="digitoConta" id="dvconta" renderAs="doubleline" colspan="1"/>
						</n:panelGrid>
					</n:group>
					<n:panelGrid columns="2">
					</n:panelGrid>
					<n:panelGrid columns="2">
					</n:panelGrid>
					<n:panelGrid columns="2">
					</n:panelGrid>
					
				</n:panelGrid>
			</n:panel>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>
<script type="text/javascript">
$(document).ready(function(){
	$('#nome').focus();
});
function showjanelaEntrada(panel, index, linkid) {
    hidejanelaEntrada('janelaEntrada_0');
    unselectjanelaEntrada('janelaEntrada_0', 'janelaEntrada_link_0');
    hidejanelaEntrada('janelaEntrada_1');
    unselectjanelaEntrada('janelaEntrada_1', 'janelaEntrada_link_1');
    hidejanelaEntrada('janelaEntrada_2');
    unselectjanelaEntrada('janelaEntrada_2', 'janelaEntrada_link_2');
    hidejanelaEntrada('janelaEntrada_3');
    unselectjanelaEntrada('janelaEntrada_3', 'janelaEntrada_link_3');
    $("#"+panel).show();
    selectjanelaEntrada(panel, index, linkid);
}
function hidejanelaEntrada(panel) {
	$("#"+panel).hide();
}
function selectjanelaEntrada(panel, index, linkid) {
    document.forms["form"].TABPANEL_janelaEntrada.value = index;
    document.getElementById(linkid).parentNode.id = 'current';
}
function unselectjanelaEntrada(panel, linkid) {
    document.getElementById(linkid).parentNode.id = '';
}
function insereDatepicker(param){
	param.datepicker();
	$('#email').click();
	$('#email').focus();
	param.focus();
	param.click();
}
</script>

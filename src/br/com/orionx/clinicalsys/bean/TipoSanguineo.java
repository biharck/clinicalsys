package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name = "tiposanguineo")
public class TipoSanguineo {
	
	private Integer idTipoSanguineo;
	private String nome;
	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdTipoSanguineo() {
		return idTipoSanguineo;
	}
	@Required
	@MinLength(2)
	@MaxLength(15)
	@DisplayName("Nome")
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	//SET
	public void setIdTipoSanguineo(Integer idTipoSanguineo) {
		this.idTipoSanguineo = idTipoSanguineo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

}

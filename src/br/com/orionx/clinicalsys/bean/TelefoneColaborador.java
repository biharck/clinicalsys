package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.types.Telefone;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name = "telefonecolaborador")
public class TelefoneColaborador {
	
	private Integer idTelefoneColaborador;
	private Telefone telefone;
	private TelefoneTipo telefoneTipo;
	private Colaborador colaborador;
	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdTelefoneColaborador() {
		return idTelefoneColaborador;
	}
	@Required
	public Telefone getTelefone() {
		return telefone;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "idTelefoneTipo")
	public TelefoneTipo getTelefoneTipo() {
		return telefoneTipo;
	}
	
	@Required
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "idColaborador")
	@ForeignKey(name = "fk_telColaborador_colaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}
	
	//SET
	public void setIdTelefoneColaborador(Integer idTelefoneColaborador) {
		this.idTelefoneColaborador = idTelefoneColaborador;
	}
	public void setTelefone(Telefone telefone) {
		this.telefone = telefone;
	}
	public void setTelefoneTipo(TelefoneTipo telefoneTipo) {
		this.telefoneTipo = telefoneTipo;
	}
	public void setColaborador(Colaborador Colaborador) {
		this.colaborador = Colaborador;
	}

}

package br.com.orionx.clinicalsys.dao;

import java.util.List;

import org.nextframework.persistence.SaveOrUpdateStrategy;

import br.com.orionx.clinicalsys.bean.Paciente;
import br.com.orionx.clinicalsys.bean.Prescricao;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class PrescricaoDAO extends GenericDAO<Prescricao>{

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("interacoes");
	}
	
	
	public List<Prescricao> getPrescricoesByPaciente(Paciente p){
		return query().select("prescricao").join("prescricao.paciente p").where("p=?",p).list();
	}

	public Prescricao getPrescricaoById(Integer idPrescricao){
		return query()
				.select("prescricao.idPrescricao, prescricao.paciente, prescricao.dataPrescricao, prescricao.horaPrescricao, prescricao.prescricaoManual, " +
						"it.idItemPrescricao,it.dose, it.outrasRecomendacoes," +
						"med.idMedicamento, med.principioAtivo, rec.idRecomendacaoMedicamentosa, rec.nome")
				.leftOuterJoin("prescricao.interacoes it")
				.leftOuterJoin("it.medicamento med")
				.leftOuterJoin("it.recomendacaoMedicamentosa rec")
				.where("prescricao.idPrescricao =?",idPrescricao)
				.unique();
				
	}
	
}

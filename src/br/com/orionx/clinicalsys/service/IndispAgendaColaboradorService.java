package br.com.orionx.clinicalsys.service;

import java.util.List;

import br.com.orionx.clinicalsys.bean.ColaboradorAgenda;
import br.com.orionx.clinicalsys.bean.IndispAgendaColaborador;
import br.com.orionx.clinicalsys.dao.IndispAgendaColaboradorDAO;
import br.com.orionx.clinicalsys.util.generics.GenericService;

public class IndispAgendaColaboradorService extends GenericService<IndispAgendaColaborador>{

	private IndispAgendaColaboradorDAO indispAgendaColaboradorDAO;
	
	public void setIndispAgendaColaboradorDAO(IndispAgendaColaboradorDAO indispAgendaColaboradorDAO) {
		this.indispAgendaColaboradorDAO = indispAgendaColaboradorDAO;
	}
	
	
	/**
	 * <p>M�todo de refer�ncia ao DAO
	 * @param ca {@link ColaboradorAgenda}
	 * @return {@link List} {@link IndispAgendaColaborador}
	 */
	public List<IndispAgendaColaborador> getIndisponibilidades(ColaboradorAgenda ca){
		return indispAgendaColaboradorDAO.getIndisponibilidades(ca);
	}
}

package br.com.orionx.clinicalsys.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name = "interacaoentreclassificacoes")
/**
 * Faz a rela��o n p/ n entre classificacoes e interacaoMedicamentosa
 */
public class InteracaoEntreClassificacoes {
	
	private Integer idInteracaoEntreClassificacoes;
	private ClassificacaoMedicacao classificacaoMedicacao;
	private InteracaoMedicamentosa interacaoMedicamentosa;
	private String consideracao;
	
	
	//GET
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getIdInteracaoEntreClassificacoes() {
		return idInteracaoEntreClassificacoes;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idClassificacaoMedicacao")
	@Required
	public ClassificacaoMedicacao getClassificacaoMedicacao() {
		return classificacaoMedicacao;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idInteracaoMedicamentosa")
	public InteracaoMedicamentosa getInteracaoMedicamentosa() {
		return interacaoMedicamentosa;
	}
	@MaxLength(4000)
	@Column(length=4000)
	public String getConsideracao() {
		return consideracao;
	}
	
	//SET
	public void setInteracaoMedicamentosa(InteracaoMedicamentosa interacaoMedicamentosa) {
		this.interacaoMedicamentosa = interacaoMedicamentosa;
	}
	public void setConsideracao(String consideracao) {
		this.consideracao = consideracao;
	}
	public void setClassificacaoMedicacao(ClassificacaoMedicacao classificacaoMedicacao) {
		this.classificacaoMedicacao = classificacaoMedicacao;
	}
	public void setIdInteracaoEntreClassificacoes(Integer idInteracaoEntreClassificacoes) {
		this.idInteracaoEntreClassificacoes = idInteracaoEntreClassificacoes;
	}
	
	

}

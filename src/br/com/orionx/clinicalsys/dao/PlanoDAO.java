package br.com.orionx.clinicalsys.dao;

import java.util.List;

import br.com.orionx.clinicalsys.bean.Operadora;
import br.com.orionx.clinicalsys.bean.Plano;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class PlanoDAO extends GenericDAO<Plano>{

	
	/**
	 * <p> M�todo que retorna todos os planos de uma determinada operadora
	 * @param operadora {@link Operadora}
	 * @return {@link List} {@link Plano}
	 */
	public List<Plano> getPlanosByOperadora(Operadora operadora){
		return query().select("plano").leftOuterJoin("plano.operadora operadora").where("operadora.idOperadora=?",operadora.getIdOperadora()).list();
	}
	
	@Override
	public List<Plano> findAll() {
		return query().where("plano.ativo is true").list();
	}
	
	@Override
	public List<Plano> findForCombo(String... extraFields) {
		extraFields[0] += "ativo";
		return super.findForCombo(extraFields);
	}

}

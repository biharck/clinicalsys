package br.com.orionx.clinicalsys.service;

import java.util.List;

import br.com.orionx.clinicalsys.bean.AnamnesePaciente;
import br.com.orionx.clinicalsys.bean.Paciente;
import br.com.orionx.clinicalsys.sistema.authorization.dao.AnamnesePacienteDAO;
import br.com.orionx.clinicalsys.util.generics.GenericService;

public class AnamnesePacienteService extends GenericService<AnamnesePaciente>{

	private AnamnesePacienteDAO anamnesePacienteDAO;
	public void setAnamnesePacienteDAO(AnamnesePacienteDAO anamnesePacienteDAO) {
		this.anamnesePacienteDAO = anamnesePacienteDAO;
	}
	
	
	public List<AnamnesePaciente> getAnamnesesByPaciente(Paciente p){
		return anamnesePacienteDAO.getAnamnesesByPaciente(p); 
	}
}

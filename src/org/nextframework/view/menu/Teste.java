/*
 * Next Framework http://www.nextframework.org
 * Copyright (C) 2009 the original author or authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * You may obtain a copy of the license at
 * 
 *     http://www.gnu.org/copyleft/lesser.html
 * 
 */
package org.nextframework.view.menu;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class Teste {
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		Menu menu = new Menu();
		
		Menu menu1 = new Menu();
		menu1.setDescription("asdf");
		menu1.setTitle("Associado");
		menu1.setIcon("icon");
		menu1.setUrl("url");
		menu.addMenu(menu1);
		
		Menu menu11 = new Menu();
		menu11.setDescription("asdf1");
		menu11.setTitle("Associado1");
		menu11.setIcon("icon1");
		menu11.setUrl("url1");
		menu1.addMenu(menu11);
		
		Menu menu12 = new Menu();
		menu12.setDescription("asdf1");
		menu12.setTitle("Associado2");
		menu12.setIcon("icon1");
		menu12.setUrl("url1");
		menu1.addMenu(menu12);
		
		Menu menu121 = new Menu();
		menu121.setDescription("asdf1");
		menu121.setTitle("Associado21");
		menu121.setIcon("icon1");
		menu121.setUrl("url1");
		menu12.addMenu(menu121);
		
		Menu menu2 = new Menu();
		menu2.setDescription("asdf");
		menu2.setTitle("Associado");
		menu2.setIcon("icon");
		menu2.setUrl("url");
		menu.addMenu(menu2);
		
		Menu menu3 = new Menu();
		menu3.setDescription("asdf");
		menu3.setTitle("Associado");
		menu3.setIcon("icon");
		menu3.setUrl("url");
		menu.addMenu(menu3);
		
		Menu menu4 = new Menu();
		menu4.setDescription("asdf");
		menu4.setTitle("Associado");
		menu4.setIcon("icon");
		menu4.setUrl("url");
		menu.addMenu(menu4);
		
		Menu menu5 = new Menu();
		menu5.setDescription("asdf");
		menu5.setTitle("Associado");
		menu5.setIcon("icon");
		menu5.setUrl("url");
		menu.addMenu(menu5);
		
		
		
		MenuParser menuParser = new MenuParser();
		Menu menu111 = menuParser.parse(Teste.class.getClassLoader().getResourceAsStream("menu.xml"));
		
//		System.out.println(new MenuBuilder().build(menu111));
	}
}

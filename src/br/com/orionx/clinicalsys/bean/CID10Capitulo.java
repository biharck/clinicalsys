package br.com.orionx.clinicalsys.bean;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * http://pt.wikipedia.org/wiki/Anexo:Lista_de_c�digos_da_CID-10
 * sub n�veis em formato de �rvore
 * 
 * @author biharck
 * representam os cap�tulos do cid10
 *
 */
@Entity
@Table(name = "cid10capitulo")
public class CID10Capitulo{
	
	private Integer idCid10Capitulo;
	private String categoriaInicial;
	private String categoriaFinal;
	private Integer numeroCapitulo;
	private String descricao;
	private String descrabrev;
	
	private List<CID10Grupo> grupos;
	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdCid10Capitulo() {
		return idCid10Capitulo;
	}

	@Column(name="catInic")
	public String getCategoriaInicial() {
		return categoriaInicial;
	}
	
	@Column(name="catFim")
	public String getCategoriaFinal() {
		return categoriaFinal;
	}

	@Column(name="numCap")
	public Integer getNumeroCapitulo() {
		return numeroCapitulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getDescrabrev() {
		return descrabrev;
	}
	
	@Transient
	public List<CID10Grupo> getGrupos() {
		return grupos;
	}

	//SET
	public void setIdCid10Capitulo(Integer idCid10Capitulo) {
		this.idCid10Capitulo = idCid10Capitulo;
	}

	public void setCategoriaInicial(String categoriaInicial) {
		this.categoriaInicial = categoriaInicial;
	}
	
	public void setCategoriaFinal(String categoriaFinal) {
		this.categoriaFinal = categoriaFinal;
	}

	public void setNumeroCapitulo(Integer numeroCapitulo) {
		this.numeroCapitulo = numeroCapitulo;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setDescrabrev(String descrabrev) {
		this.descrabrev = descrabrev;
	}
	public void setGrupos(List<CID10Grupo> grupos) {
		this.grupos = grupos;
	}
}

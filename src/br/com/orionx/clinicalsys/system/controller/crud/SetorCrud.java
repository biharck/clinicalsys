package br.com.orionx.clinicalsys.system.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;

import br.com.orionx.clinicalsys.bean.Setor;
import br.com.orionx.clinicalsys.filtros.SetorFiltro;
import br.com.orionx.clinicalsys.util.generics.CrudController;

@Controller(path="/system/pag/Setor",authorizationModule=CrudAuthorizationModule.class)
public class SetorCrud extends CrudController<SetorFiltro, Setor, Setor>{

	@Override
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Administração &raquo; <a href=\"/ClinicalSys/system/pag/Setor\">Setores</a></p>";
	}

}

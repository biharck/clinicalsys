package br.com.orionx.clinicalsys.system.controller.crud;

import java.io.IOException;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.view.DownloadFileServlet;
import org.springframework.web.servlet.ModelAndView;

import br.com.orionx.clinicalsys.bean.Clinica;
import br.com.orionx.clinicalsys.bean.Municipio;
import br.com.orionx.clinicalsys.bean.TipoLogradouro;
import br.com.orionx.clinicalsys.dao.ArquivoDAO;
import br.com.orionx.clinicalsys.dao.ClinicaDAO;
import br.com.orionx.clinicalsys.dao.MunicipioDAO;
import br.com.orionx.clinicalsys.dao.TipoLogradouroDAO;
import br.com.orionx.clinicalsys.service.TipoLogradouroService;
import br.com.orionx.clinicalsys.util.ClinicalSysUtil;
import br.com.orionx.clinicalsys.util.webservice.CepService;
import br.com.orionx.clinicalsys.util.webservice.Retorno;
import br.com.orionx.clinicalsys.util.webservice.WebServiceCEP;

@Controller(path="/system/process/Clinica",authorizationModule=ProcessAuthorizationModule.class)
public class ClinicaProcess extends MultiActionController{

	
	ClinicaDAO clinicaDAO;
	ArquivoDAO arquivoDAO;
	MunicipioDAO municipioDAO;
	TipoLogradouroDAO tipoLogradouroDAO;
	TipoLogradouroService tipoLogradouroService;
	
	public void setClinicaDAO(ClinicaDAO clinicaDAO) {
		this.clinicaDAO = clinicaDAO;
	}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setMunicipioDAO(MunicipioDAO municipioDAO) {
		this.municipioDAO = municipioDAO;
	}
	public void setTipoLogradouroDAO(TipoLogradouroDAO tipoLogradouroDAO) {
		this.tipoLogradouroDAO = tipoLogradouroDAO;
	}
	public void setTipoLogradouroService(TipoLogradouroService tipoLogradouroService) {
		this.tipoLogradouroService = tipoLogradouroService;
	}

	@DefaultAction
	public ModelAndView index(WebRequestContext request,Clinica clinica){
		request.setAttribute("pathURL", getPathURL());
		clinica = clinicaDAO.getClinica();
		request.setAttribute("dtAltera", clinica.getDtAltera() );
		request.setAttribute("idUsuarioAltera", clinica.getIdUsuarioAltera());
		if(clinica!=null && clinica.getIdClinica()!=null){
			try {
				clinica.setPais(clinica.getMunicipio().getUnidadeFederativa().getPais());
				clinica.setUnidadeFederativa(clinica.getMunicipio().getUnidadeFederativa());
				if(clinica.getLogomarca()!=null && clinica.getLogomarca().getCdfile()!=null){
					arquivoDAO.loadAsImage(clinica.getLogomarca());
					DownloadFileServlet.addCdfile(request.getSession(), clinica.getLogomarca().getCdfile());
					request.getSession().setAttribute("showpicture", true);
				}else
					request.getSession().setAttribute("showpicture", false);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			clinica = new Clinica();
			request.getSession().setAttribute("showpicture", false);
		}
		return new ModelAndView("process/clinica","clinica",clinica);
	}
	
	
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Administra��o &raquo; <a href=\"/ClinicalSys/system/pag/Clinica\">Cl�nica</a></p>";
	}
	
	public void callMunicipio(WebRequestContext request) throws IOException, JSONException{
		Municipio m = new Municipio();
		m.setIdMunicipio(Integer.parseInt(getParameter("id")));
		request.getServletResponse().setContentType("application/json");
		request.getServletResponse().setCharacterEncoding("ISO-8859-1");
		request.getServletResponse().getWriter().println(formatCombo(municipioDAO.getMunicipiosCompletos(m.getIdMunicipio())));
		
	}
	
	public void callWebServiceCEPByAddress(WebRequestContext request) throws IOException, JSONException{
		if(getParameter("logradouro")!=null && getParameter("bairro")!=null && getParameter("municipio")!=null && getParameter("tipoLogradouro") !=null
				&& !getParameter("logradouro").trim().equals("") && !getParameter("bairro").trim().equals("") && !getParameter("municipio").trim().equals("") && !getParameter("tipoLogradouro").equals("")){
			
			WebServiceCEP serviceCEP = new WebServiceCEP();
			Retorno r = new Retorno();
			r.setLogradouro(getParameter("logradouro"));
			r.setBairro(getParameter("bairro"));
			
			Municipio m = new Municipio();
			m.setIdMunicipio(ClinicalSysUtil.returnIdWithParameters(getParameter("municipio")));
			m = municipioDAO.load(m);
			
			TipoLogradouro tl = new TipoLogradouro();
			tl.setIdTipoLogradouro(ClinicalSysUtil.returnIdWithParameters(getParameter("tipoLogradouro")));
			tl = tipoLogradouroDAO.load(tl);
			
			r.setTipoLogradouro(tl.getDescricao());
			r.setCidade(m.getNome());
			
			serviceCEP.setRetorno(r);
			try {
				WebServiceCEP wsc = CepService.findCEP(serviceCEP);
				if(wsc.getQuantidade() == null){
					JSONObject jsonObj = new JSONObject();
					jsonObj.put("erro", true);
					request.getServletResponse().setContentType("application/json");
					request.getServletResponse().setCharacterEncoding("UTF-8");
					request.getServletResponse().getWriter().println( jsonObj );
				}else{
					JSONObject jsonObj = new JSONObject();
					jsonObj.put("erro", false);
					jsonObj.put("cep", wsc.getRetorno().getCep());
					request.getServletResponse().getWriter().println( jsonObj );
				}
			} catch (Exception e) {
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("erro", true);
				request.getServletResponse().setContentType("application/json");
				request.getServletResponse().setCharacterEncoding("UTF-8");
				request.getServletResponse().getWriter().println( jsonObj );
				e.printStackTrace();
			}
		}else{
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("erro", true);
			request.getServletResponse().getWriter().println( jsonObj );
		}
		
		request.getServletResponse().setContentType("application/json");
		request.getServletResponse().setCharacterEncoding("ISO-8859-1");
	}
	
	public void callWebServiceCEP(WebRequestContext request) throws IOException, JSONException{
		try {
			WebServiceCEP cep = CepService.findAddress((String)request.getParameter("cep"));
			
			Municipio m = municipioDAO.getMunicipioByWebService(cep.getRetorno().getIbgeMunicipioVerificador());
			
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("erro", false);
			jsonObj.put("bairro", cep.getRetorno().getBairro());
			jsonObj.put("logradouro", cep.getRetorno().getLogradouro());
			jsonObj.put("tipoLogradouro", ClinicalSysUtil.getComboFormat(new TipoLogradouro(),"idTipoLogradouro",tipoLogradouroService.getTipoByName(cep.getRetorno().getTipoLogradouro()).getIdTipoLogradouro()));
			jsonObj.put("ibgeMunicipio", cep.getRetorno().getIbgeMunicipio());
			jsonObj.put("uf", ClinicalSysUtil.getComboFormat(m.getUnidadeFederativa(), "idUF", m.getUnidadeFederativa().getIdUF()));
			jsonObj.put("cidade", ClinicalSysUtil.getComboFormat(m, "idMunicipio", m.getIdMunicipio()));
			jsonObj.put("pais", ClinicalSysUtil.getComboFormat(m.getUnidadeFederativa().getPais(), "idPais", m.getUnidadeFederativa().getPais().getIdPais()));
			request.getServletResponse().setContentType("application/json");
			request.getServletResponse().setCharacterEncoding("ISO-8859-1");
			request.getServletResponse().getWriter().println( jsonObj );
		} catch (Exception e) {
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("erro", true);
			request.getServletResponse().setContentType("application/json");
			request.getServletResponse().setCharacterEncoding("UTF-8");
			request.getServletResponse().getWriter().println( jsonObj );
			e.printStackTrace();
		}
	}
	
	public ModelAndView errorSalvar(WebRequestContext request,Clinica clinica){
		return new ModelAndView("process/clinica","clinica",clinica);
	}
	
	@Action("salvar")
	public ModelAndView salvar(WebRequestContext request, Clinica bean){
		
		try {
			if(bean.getLogomarca()!=null && bean.getLogomarca().getCdfile()!=null)
				bean.setLogomarca(arquivoDAO.load(bean.getLogomarca()));
			if(bean.getLogomarca()!=null)
				arquivoDAO.saveOrUpdate(bean.getLogomarca());
			clinicaDAO.saveOrUpdate(bean);
			if(bean.getLogomarca()!=null && bean.getLogomarca().getCdfile()!=null){
				arquivoDAO.loadAsImage(bean.getLogomarca());
				DownloadFileServlet.addCdfile(request.getSession(), bean.getLogomarca().getCdfile());
				request.getSession().setAttribute("showpicture", true);
			}else
				request.getSession().setAttribute("showpicture", false);
		} catch (Exception e) {
			e.printStackTrace();
			request.addError("Falha ao salvar o registro, entre em contato com o administrador do sistema");
			return errorSalvar(request, bean);
		}
		request.addMessage("Registro salvo com sucesso");
		return index(request, bean);
	}

	/**
	 * <p>Fun��o respons�vel em montar toda estrutura de um combo para povoar o html
	 * @return {@link JSONArray} formatada para enviar via JSON
	 * @throws JSONException 
	 */
	public JSONArray formatCombo(List<Municipio> m) throws JSONException{
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("jscript", "$('select[name=municipioAjax]').empty();");
		jsonArray.put(jsonObject);
		jsonObject = new JSONObject();
		jsonObject.put("jscript", "$('select[name=municipioAjax]').append('<option value=\"<null>\"></option>');");
		jsonArray.put(jsonObject);
		for (Municipio municipio : m) {
			jsonObject = new JSONObject();
			jsonObject.put("jscript", "$('select[name=municipioAjax]').append('<option value=\""+ClinicalSysUtil.getComboFormat(municipio, "idMunicipio", municipio.getIdMunicipio())+"\">"+municipio.getNome()+"</option>');");
			jsonArray.put(jsonObject);
		}
		return jsonArray;
		
	}

}

package br.com.orionx.clinicalsys.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
/**
 * <p>Faz o relacionamento entre Usu�rio e Papel<br>
 * Relacionamento muitos para muitos entre usuario e papel</p>
 * 
 * @author biharck
 */
@Entity
@Table(name = "usuariopapel")
public class UsuarioPapel {

	Integer idUsuarioPapel;
	Usuario usuario;
	Papel papel;

	/*
	 * GET
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "idUsuarioPapel")
	public Integer getIdUsuarioPapel() {
		return idUsuarioPapel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idPapel")
	@ForeignKey(name = "fk_usuariopapel_papel")
	@DescriptionProperty
	public Papel getPapel() {
		return papel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idUsuario")
	@ForeignKey(name = "fk_usuariopapel_usuario")
	public Usuario getUsuario() {
		return usuario;
	}
	
	/*
	 * SET
	 */
	public void setIdUsuarioPapel(Integer idUsuarioPapel) {
		this.idUsuarioPapel = idUsuarioPapel;
	}

	public void setPapel(Papel papel) {
		this.papel = papel;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}

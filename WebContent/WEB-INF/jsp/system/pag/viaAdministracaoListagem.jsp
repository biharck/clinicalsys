<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:listagem >
	<t:janelaFiltro >
		<t:tabelaFiltro columns="1">
		<t:property name="nome" renderAs="doubleline" style="width:300px" id="nome"/>
		<t:property name="sigla" renderAs="doubleline" />
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados >
		<t:tabelaResultados >
			<t:property name="nome"/>
			<t:property name="sigla"/>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
<script type="text/javascript">
	$(document).ready(function(){
		$('#nome').focus();
	});
</script>
package br.com.orionx.clinicalsys.bean;

import java.sql.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.validation.annotation.MaxLength;

@Entity
@Table(name = "historiapregressa")
public class HistoriaPregressa {
	
	private Integer idHistoriaPregressa;
	private Paciente paciente;
	
	//Gestao e nascimento
	private Boolean usoMedicamentoGenitora;
	private String usoMedicamentoGenitoraString;
	private Boolean virosesContraidasDuranteGestacao;
	private String virosesContraidasDuranteGestacaoString;
	private EnumTipoParto tipoParto;
	//fim Gestao e nascimento
	
	//desenvolvimento psicomotor e neural
	private Integer idadeComecouEngatinhar;
	private Integer idadeComecouAndar;
	private Integer idadeComecouFalar;
	private Integer pesoAoNascer;
	private Integer tamanhoAoNascer;
	private Boolean controleEsfincter;//true = adequado, false = inadequado
	//fim desenvolvimento psicomotor e neural
	
	//Desenvolvimento sexual
	private Integer idadeInicioPuberdade;
	private Integer idadePrimeiraMenstruacao;//somente para mulheres
	private Integer idadePrimeiraRelacaoSexual;
	private Integer idadeInicioMenopausa;//somente para mulheres
	//fim Desenvolvimento sexual

	//Antecedentes pessoais patol�gicos
	private List<DoencasDaInfanciaPaciente> doencasDaInfancia; // montar um checklist
	private List<DoencasDaInfancia> doencasDaInfanciasTransient; // dados transient
	private String outrasDoencasInfancia;
	private List<DemaisDoencasPaciente> demaisDoencas;
	private List<DemaisDoencas> demaisDoencasTransient; // dados transient 
	private String outrasDemaisDoencas;
	private Boolean alergiaAlimentos;
	private String alergiaAlimentosString;
	private Boolean alergiaMedicamentos;
	private String alergiaMedicamentosString;
	private String outras;
	private List<CirurgiaPreviaHistoriaPregressa> cirurgias;
	private List<TransfusoesSanguineasHistoriaPregressa> transfusoes;
	
	//Imuniza��es
	private List<ImunizacoesInfanciaPaciente> imunizacoes;
	private List<ImunizacoesInfancia> imunizacoesTransient;
	private ImunizacoesAdolescenciaPaciente imunizacoesAdolescenciaPaciente;
	private ImunizacoesAdultoPaciente imunizacoesAdultoPaciente;
	private ImunizacoesIdosoPaciente imunizacoesIdosoPaciente;
	
	private Boolean medicamentosEmUso;
	private List<MedicamentosEmUsoPaciente> interacoes;
	
	
	//fim Antecedentes pessoais patol�gicos
	
	//Antecedentes Familiares
	private Boolean aVC;
	private EnumParentesco parentescoAVC;
	private Boolean coleltiase;
	private EnumParentesco parentescoColeltiase;
	private Boolean dAC;
	private EnumParentesco parentescoDAC;
	private Boolean dislipidemias;
	private EnumParentesco parentescoDislipidemias;
	private Boolean dM;
	private EnumParentesco parentescoDM;
	private Boolean enxaqueca;
	private EnumParentesco parentescoEnxaqueca;
	private Boolean hAS;
	private EnumParentesco parentescoHAS;
	private Boolean morteSubita;
	private EnumParentesco parentescoMorteSubita;
	private Boolean tBC;
	private EnumParentesco parentescoTBC;
	private Boolean ulceraPeptica;
	private EnumParentesco parentescoUlceraPeptica;
	private Boolean varizes;
	private EnumParentesco parentescoVarizes;
	private Boolean outrosAntecedentesFamiliares;
	private EnumParentesco parentescoOutrosAntecedentesFamiliares;
	private String outrosString;
	//fim Antecedentes Familiares
	
	//Habitos de Vida
	private List<AlimentacaoPaciente> alimentacao;
	private List<Alimentacao> alimentacaosTransient;
	//fim Habitos de Vida
	
	//ocupacao atual e ocupacao anteriores
	private List<OcupacaoHistoriaPregressa> ocupacoes;//mestre detalhe
	//fim ocupacao atual e ocupacao anteriores
	
	//Atividades F�sicas
	private EnumAtividadesFisicas atividadesFisicas;
	//Fim Atividades F�sicas
	
	//Habitos
	//tabaco
	private Boolean usaTabaco;
	private Integer qtdCigarros;
	private EnumFrequencia frequencia;
	private Date inicioVicio;
	private Integer exFumanteHaQntsAnos;
	//fim tabaco
	
	//bebidas
	private String tipoBebida;
	private EnumFrequenciaBebida frequenciaBebida;
	private Integer exAlcolatraHaQntsAnos;
	//fim bebidas
	
	//anabolizantes
	private Boolean usoAnabolizantesAnfetaminas;
	//fim anabolizantes
	
	//drogas
	private List<DrogasIlicitasHistoriaPregressa> drogasIlicitas;
	private Integer naoUtilizaDrogasIlicitasHaQntsAnos;
		//fim drogas
	//fim Habitos
		
	//condicoes socioenconomicas
	private EnumMeioUrbano meioUrbano;
	private EnumNivelCultural nivelCultural;
	//fimcondicoes socioenconomicas

	
	//Outros Dados
	private TipoSanguineo tipoSanguineo;
	private List<CrescimentoPaciente> listaCrescimentoPaciente;
	private List<CrescimentoPaciente> listaCrescimentoPacienteTransient;
	//Fim outros dados
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getIdHistoriaPregressa() {
		return idHistoriaPregressa;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idPaciente")
	@ForeignKey(name = "fk_historiapregressa_pct")
	public Paciente getPaciente() {
		return paciente;
	}

	public Boolean getUsoMedicamentoGenitora() {
		return usoMedicamentoGenitora;
	}

	public String getUsoMedicamentoGenitoraString() {
		return usoMedicamentoGenitoraString;
	}

	public Boolean getVirosesContraidasDuranteGestacao() {
		return virosesContraidasDuranteGestacao;
	}

	public String getVirosesContraidasDuranteGestacaoString() {
		return virosesContraidasDuranteGestacaoString;
	}

	public EnumTipoParto getTipoParto() {
		return tipoParto;
	}

	public Integer getIdadeComecouEngatinhar() {
		return idadeComecouEngatinhar;
	}

	public Integer getIdadeComecouAndar() {
		return idadeComecouAndar;
	}

	public Integer getIdadeComecouFalar() {
		return idadeComecouFalar;
	}

	public Integer getPesoAoNascer() {
		return pesoAoNascer;
	}

	public Integer getTamanhoAoNascer() {
		return tamanhoAoNascer;
	}

	public Boolean getControleEsfincter() {
		return controleEsfincter;
	}
	

	public Integer getIdadeInicioPuberdade() {
		return idadeInicioPuberdade;
	}

	public Integer getIdadePrimeiraMenstruacao() {
		return idadePrimeiraMenstruacao;
	}

	public Integer getIdadePrimeiraRelacaoSexual() {
		return idadePrimeiraRelacaoSexual;
	}

	public Integer getIdadeInicioMenopausa() {
		return idadeInicioMenopausa;
	}

	@OneToMany(mappedBy = "historiaPregressa")
	public List<DoencasDaInfanciaPaciente> getDoencasDaInfancia() {
		return doencasDaInfancia;
	}

	@OneToMany(mappedBy = "historiaPregressa")
	public List<DemaisDoencasPaciente> getDemaisDoencas() {
		return demaisDoencas;
	}

	public String getOutrasDoencasInfancia() {
		return outrasDoencasInfancia;
	}
	
	public String getOutrasDemaisDoencas() {
		return outrasDemaisDoencas;
	}

	public Boolean getAlergiaMedicamentos() {
		return alergiaMedicamentos;
	}

	public String getAlergiaAlimentosString() {
		return alergiaAlimentosString;
	}

	public Boolean getAlergiaAlimentos() {
		return alergiaAlimentos;
	}

	public String getAlergiaMedicamentosString() {
		return alergiaMedicamentosString;
	}

	public String getOutras() {
		return outras;
	}

	@OneToMany(mappedBy="historiaPregressa")
	public List<CirurgiaPreviaHistoriaPregressa> getCirurgias() {
		return cirurgias;
	}
	@OneToMany(mappedBy="historiaPregressa")
	public List<TransfusoesSanguineasHistoriaPregressa> getTransfusoes() {
		return transfusoes;
	}
	
	@OneToMany(mappedBy="historiaPregressa")
	public List<ImunizacoesInfanciaPaciente> getImunizacoes() {
		return imunizacoes;
	}
	@Transient
	public List<ImunizacoesInfancia> getImunizacoesTransient() {
		return imunizacoesTransient;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "idImunizacoesAdolescenciaPaciente")
	@ForeignKey(name="fk_imun_adoles")
	public ImunizacoesAdolescenciaPaciente getImunizacoesAdolescenciaPaciente() {
		return imunizacoesAdolescenciaPaciente;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "idImunizacoesAdultoPaciente")
	@ForeignKey(name="fk_imun_adulto")
	public ImunizacoesAdultoPaciente getImunizacoesAdultoPaciente() {
		return imunizacoesAdultoPaciente;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "idImunizacoesIdosoPaciente")
	@ForeignKey(name="fk_imun_idoso")
	public ImunizacoesIdosoPaciente getImunizacoesIdosoPaciente() {
		return imunizacoesIdosoPaciente;
	}
	
	public Boolean getMedicamentosEmUso() {
		return medicamentosEmUso;
	}

	public Boolean getaVC() {
		return aVC;
	}

	public EnumParentesco getParentescoAVC() {
		return parentescoAVC;
	}

	public Boolean getColeltiase() {
		return coleltiase;
	}

	public EnumParentesco getParentescoColeltiase() {
		return parentescoColeltiase;
	}

	public Boolean getdAC() {
		return dAC;
	}

	public EnumParentesco getParentescoDAC() {
		return parentescoDAC;
	}

	public Boolean getDislipidemias() {
		return dislipidemias;
	}

	public EnumParentesco getParentescoDislipidemias() {
		return parentescoDislipidemias;
	}

	public Boolean getdM() {
		return dM;
	}

	public EnumParentesco getParentescoDM() {
		return parentescoDM;
	}

	public Boolean getEnxaqueca() {
		return enxaqueca;
	}

	public EnumParentesco getParentescoEnxaqueca() {
		return parentescoEnxaqueca;
	}

	public Boolean gethAS() {
		return hAS;
	}

	public EnumParentesco getParentescoHAS() {
		return parentescoHAS;
	}

	public Boolean getMorteSubita() {
		return morteSubita;
	}

	public EnumParentesco getParentescoMorteSubita() {
		return parentescoMorteSubita;
	}

	public Boolean gettBC() {
		return tBC;
	}

	public EnumParentesco getParentescoTBC() {
		return parentescoTBC;
	}

	public Boolean getUlceraPeptica() {
		return ulceraPeptica;
	}

	public EnumParentesco getParentescoUlceraPeptica() {
		return parentescoUlceraPeptica;
	}

	public Boolean getVarizes() {
		return varizes;
	}

	public EnumParentesco getParentescoVarizes() {
		return parentescoVarizes;
	}

	public Boolean getOutrosAntecedentesFamiliares() {
		return outrosAntecedentesFamiliares;
	}

	public EnumParentesco getParentescoOutrosAntecedentesFamiliares() {
		return parentescoOutrosAntecedentesFamiliares;
	}
	@MaxLength(400)
	public String getOutrosString() {
		return outrosString;
	}

	@OneToMany(mappedBy = "historiaPregressa")
	public List<AlimentacaoPaciente> getAlimentacao() {
		return alimentacao;
	}

	@OneToMany(mappedBy = "historiaPregressa")
	public List<OcupacaoHistoriaPregressa> getOcupacoes() {
		return ocupacoes;
	}

	public EnumAtividadesFisicas getAtividadesFisicas() {
		return atividadesFisicas;
	}


	public Boolean getUsaTabaco() {
		return usaTabaco;
	}

	public Integer getQtdCigarros() {
		return qtdCigarros;
	}

	public EnumFrequencia getFrequencia() {
		return frequencia;
	}

	public Date getInicioVicio() {
		return inicioVicio;
	}

	public Integer getExFumanteHaQntsAnos() {
		return exFumanteHaQntsAnos;
	}

	public String getTipoBebida() {
		return tipoBebida;
	}
	
	public EnumFrequenciaBebida getFrequenciaBebida() {
		return frequenciaBebida;
	}

	public Integer getExAlcolatraHaQntsAnos() {
		return exAlcolatraHaQntsAnos;
	}

	public Boolean getUsoAnabolizantesAnfetaminas() {
		return usoAnabolizantesAnfetaminas;
	}


	@OneToMany(mappedBy = "historiaPregressa")
	public List<DrogasIlicitasHistoriaPregressa> getDrogasIlicitas() {
		return drogasIlicitas;
	}
	public void setImunizacoes(List<ImunizacoesInfanciaPaciente> imunizacoes) {
		this.imunizacoes = imunizacoes;
	}
	public void setImunizacoesTransient(List<ImunizacoesInfancia> imunizacoesTransient) {
		this.imunizacoesTransient = imunizacoesTransient;
	}
	public void setImunizacoesAdolescenciaPaciente(ImunizacoesAdolescenciaPaciente imunizacoesAdolescenciaPaciente) {
		this.imunizacoesAdolescenciaPaciente = imunizacoesAdolescenciaPaciente;
	}
	
	public void setImunizacoesAdultoPaciente(ImunizacoesAdultoPaciente imunizacoesAdultoPaciente) {
		this.imunizacoesAdultoPaciente = imunizacoesAdultoPaciente;
	}
	
	public void setImunizacoesIdosoPaciente(ImunizacoesIdosoPaciente imunizacoesIdosoPaciente) {
		this.imunizacoesIdosoPaciente = imunizacoesIdosoPaciente;
	}
	@OneToMany(mappedBy = "historiaPregressa")
	public List<MedicamentosEmUsoPaciente> getInteracoes() {
		return interacoes;
	}

	public Integer getNaoUtilizaDrogasIlicitasHaQntsAnos() {
		return naoUtilizaDrogasIlicitasHaQntsAnos;
	}

	public EnumMeioUrbano getMeioUrbano() {
		return meioUrbano;
	}
	public EnumNivelCultural getNivelCultural() {
		return nivelCultural;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "idTipoSanguineo")
	@ForeignKey(name = "fk_paciente_tiposang")
	public TipoSanguineo getTipoSanguineo() {
		return tipoSanguineo;
	}
	@OneToMany(mappedBy="historiaPregressa")
	public List<CrescimentoPaciente> getListaCrescimentoPaciente() {
		return listaCrescimentoPaciente;
	}
	@Transient
	public List<CrescimentoPaciente> getListaCrescimentoPacienteTransient() {
		return listaCrescimentoPacienteTransient;
	}

	public void setIdHistoriaPregressa(Integer idHistoriaPregressa) {
		this.idHistoriaPregressa = idHistoriaPregressa;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public void setUsoMedicamentoGenitora(Boolean usoMedicamentoGenitora) {
		this.usoMedicamentoGenitora = usoMedicamentoGenitora;
	}

	public void setUsoMedicamentoGenitoraString(String usoMedicamentoGenitoraString) {
		this.usoMedicamentoGenitoraString = usoMedicamentoGenitoraString;
	}

	public void setVirosesContraidasDuranteGestacao(
			Boolean virosesContraidasDuranteGestacao) {
		this.virosesContraidasDuranteGestacao = virosesContraidasDuranteGestacao;
	}

	public void setVirosesContraidasDuranteGestacaoString(
			String virosesContraidasDuranteGestacaoString) {
		this.virosesContraidasDuranteGestacaoString = virosesContraidasDuranteGestacaoString;
	}

	public void setTipoParto(EnumTipoParto tipoParto) {
		this.tipoParto = tipoParto;
	}

	public void setIdadeComecouEngatinhar(Integer idadeComecouEngatinhar) {
		this.idadeComecouEngatinhar = idadeComecouEngatinhar;
	}

	public void setIdadeComecouAndar(Integer idadeComecouAndar) {
		this.idadeComecouAndar = idadeComecouAndar;
	}

	public void setIdadeComecouFalar(Integer idadeComecouFalar) {
		this.idadeComecouFalar = idadeComecouFalar;
	}

	public void setPesoAoNascer(Integer pesoAoNascer) {
		this.pesoAoNascer = pesoAoNascer;
	}

	public void setTamanhoAoNascer(Integer tamanhoAoNascer) {
		this.tamanhoAoNascer = tamanhoAoNascer;
	}

	public void setControleEsfincter(Boolean controleEsfincter) {
		this.controleEsfincter = controleEsfincter;
	}

	public void setIdadeInicioPuberdade(Integer idadeInicioPuberdade) {
		this.idadeInicioPuberdade = idadeInicioPuberdade;
	}

	public void setIdadePrimeiraMenstruacao(Integer idadePrimeiraMenstruacao) {
		this.idadePrimeiraMenstruacao = idadePrimeiraMenstruacao;
	}

	public void setIdadePrimeiraRelacaoSexual(Integer idadePrimeiraRelacaoSexual) {
		this.idadePrimeiraRelacaoSexual = idadePrimeiraRelacaoSexual;
	}

	public void setIdadeInicioMenopausa(Integer idadeInicioMenopausa) {
		this.idadeInicioMenopausa = idadeInicioMenopausa;
	}

	public void setDoencasDaInfancia(List<DoencasDaInfanciaPaciente> doencasDaInfancia) {
		this.doencasDaInfancia = doencasDaInfancia;
	}

	public void setDemaisDoencas(List<DemaisDoencasPaciente> demaisDoencas) {
		this.demaisDoencas = demaisDoencas;
	}

	public void setOutrasDemaisDoencas(String outrasDemaisDoencas) {
		this.outrasDemaisDoencas = outrasDemaisDoencas;
	}
	public void setOutrasDoencasInfancia(String outrasDoencasInfancia) {
		this.outrasDoencasInfancia = outrasDoencasInfancia;
	}

	public void setAlergiaAlimentos(Boolean alergiaAlimentos) {
		this.alergiaAlimentos = alergiaAlimentos;
	}

	public void setAlergiaAlimentosString(String alergiaAlimentosString) {
		this.alergiaAlimentosString = alergiaAlimentosString;
	}

	public void setAlergiaMedicamentos(Boolean alergiaMedicamentos) {
		this.alergiaMedicamentos = alergiaMedicamentos;
	}

	public void setAlergiaMedicamentosString(String alergiaMedicamentosString) {
		this.alergiaMedicamentosString = alergiaMedicamentosString;
	}

	public void setOutras(String outras) {
		this.outras = outras;
	}

	public void setCirurgias(List<CirurgiaPreviaHistoriaPregressa> cirurgias) {
		this.cirurgias = cirurgias;
	}
	public void setTransfusoes(List<TransfusoesSanguineasHistoriaPregressa> transfusoes) {
		this.transfusoes = transfusoes;
	}
	
	public void setMedicamentosEmUso(Boolean medicamentosEmUso) {
		this.medicamentosEmUso = medicamentosEmUso;
	}

	public void setaVC(Boolean aVC) {
		this.aVC = aVC;
	}

	public void setParentescoAVC(EnumParentesco parentescoAVC) {
		this.parentescoAVC = parentescoAVC;
	}

	public void setColeltiase(Boolean coleltiase) {
		this.coleltiase = coleltiase;
	}

	public void setParentescoColeltiase(EnumParentesco parentescoColeltiase) {
		this.parentescoColeltiase = parentescoColeltiase;
	}

	public void setdAC(Boolean dAC) {
		this.dAC = dAC;
	}

	public void setParentescoDAC(EnumParentesco parentescoDAC) {
		this.parentescoDAC = parentescoDAC;
	}

	public void setDislipidemias(Boolean dislipidemias) {
		this.dislipidemias = dislipidemias;
	}

	public void setParentescoDislipidemias(EnumParentesco parentescoDislipidemias) {
		this.parentescoDislipidemias = parentescoDislipidemias;
	}

	public void setdM(Boolean dM) {
		this.dM = dM;
	}

	public void setParentescoDM(EnumParentesco parentescoDM) {
		this.parentescoDM = parentescoDM;
	}

	public void setEnxaqueca(Boolean enxaqueca) {
		this.enxaqueca = enxaqueca;
	}

	public void setParentescoEnxaqueca(EnumParentesco parentescoEnxaqueca) {
		this.parentescoEnxaqueca = parentescoEnxaqueca;
	}

	public void sethAS(Boolean hAS) {
		this.hAS = hAS;
	}

	public void setParentescoHAS(EnumParentesco parentescoHAS) {
		this.parentescoHAS = parentescoHAS;
	}

	public void setMorteSubita(Boolean morteSubita) {
		this.morteSubita = morteSubita;
	}

	public void setParentescoMorteSubita(EnumParentesco parentescoMorteSubita) {
		this.parentescoMorteSubita = parentescoMorteSubita;
	}

	public void settBC(Boolean tBC) {
		this.tBC = tBC;
	}

	public void setParentescoTBC(EnumParentesco parentescoTBC) {
		this.parentescoTBC = parentescoTBC;
	}

	public void setUlceraPeptica(Boolean ulceraPeptica) {
		this.ulceraPeptica = ulceraPeptica;
	}

	public void setParentescoUlceraPeptica(EnumParentesco parentescoUlceraPeptica) {
		this.parentescoUlceraPeptica = parentescoUlceraPeptica;
	}

	public void setVarizes(Boolean varizes) {
		this.varizes = varizes;
	}

	public void setParentescoVarizes(EnumParentesco parentescoVarizes) {
		this.parentescoVarizes = parentescoVarizes;
	}

	public void setOutrosAntecedentesFamiliares(Boolean outrosAntecedentesFamiliares) {
		this.outrosAntecedentesFamiliares = outrosAntecedentesFamiliares;
	}

	public void setOutrosString(String outrosString) {
		this.outrosString = outrosString;
	}
	public void setParentescoOutrosAntecedentesFamiliares(
			EnumParentesco parentescoOutrosAntecedentesFamiliares) {
		this.parentescoOutrosAntecedentesFamiliares = parentescoOutrosAntecedentesFamiliares;
	}

	public void setAlimentacao(List<AlimentacaoPaciente> alimentacao) {
		this.alimentacao = alimentacao;
	}

	public void setOcupacoes(List<OcupacaoHistoriaPregressa> ocupacoes) {
		this.ocupacoes = ocupacoes;
	}

	public void setAtividadesFisicas(EnumAtividadesFisicas atividadesFisicas) {
		this.atividadesFisicas = atividadesFisicas;
	}


	public void setUsaTabaco(Boolean usaTabaco) {
		this.usaTabaco = usaTabaco;
	}

	public void setQtdCigarros(Integer qtdCigarros) {
		this.qtdCigarros = qtdCigarros;
	}

	public void setFrequencia(EnumFrequencia frequencia) {
		this.frequencia = frequencia;
	}

	public void setInicioVicio(Date inicioVicio) {
		this.inicioVicio = inicioVicio;
	}

	public void setExFumanteHaQntsAnos(Integer exFumanteHaQntsAnos) {
		this.exFumanteHaQntsAnos = exFumanteHaQntsAnos;
	}

	public void setTipoBebida(String tipoBebida) {
		this.tipoBebida = tipoBebida;
	}

	public void setFrequenciaBebida(EnumFrequenciaBebida frequenciaBebida) {
		this.frequenciaBebida = frequenciaBebida;
	}

	public void setExAlcolatraHaQntsAnos(Integer exAlcolatraHaQntsAnos) {
		this.exAlcolatraHaQntsAnos = exAlcolatraHaQntsAnos;
	}

	public void setUsoAnabolizantesAnfetaminas(Boolean usoAnabolizantesAnfetaminas) {
		this.usoAnabolizantesAnfetaminas = usoAnabolizantesAnfetaminas;
	}

	public void setDrogasIlicitas(List<DrogasIlicitasHistoriaPregressa> drogasIlicitas) {
		this.drogasIlicitas = drogasIlicitas;
	}

	public void setNaoUtilizaDrogasIlicitasHaQntsAnos(
			Integer naoUtilizaDrogasIlicitasHaQntsAnos) {
		this.naoUtilizaDrogasIlicitasHaQntsAnos = naoUtilizaDrogasIlicitasHaQntsAnos;
	}

	public void setMeioUrbano(EnumMeioUrbano meioUrbano) {
		this.meioUrbano = meioUrbano;
	}
	public void setNivelCultural(EnumNivelCultural nivelCultural) {
		this.nivelCultural = nivelCultural;
	}
	
	public void setInteracoes(List<MedicamentosEmUsoPaciente> interacoes) {
		this.interacoes = interacoes;
	}

	public void setTipoSanguineo(TipoSanguineo tipoSanguineo) {
		this.tipoSanguineo = tipoSanguineo;
	}
	
	public void setListaCrescimentoPaciente(List<CrescimentoPaciente> listaCrescimentoPaciente) {
		this.listaCrescimentoPaciente = listaCrescimentoPaciente;
	}
	public void setListaCrescimentoPacienteTransient(List<CrescimentoPaciente> listaCrescimentoPacienteTransient) {
		this.listaCrescimentoPacienteTransient = listaCrescimentoPacienteTransient;
	}
	
	//transient
	@Transient
	public List<DemaisDoencas> getDemaisDoencasTransient() {
		return demaisDoencasTransient;
	}
	@Transient
	public List<DoencasDaInfancia> getDoencasDaInfanciasTransient() {
		return doencasDaInfanciasTransient;
	}
	@Transient
	public List<Alimentacao> getAlimentacaosTransient() {
		return alimentacaosTransient;
	}
	
	public void setDemaisDoencasTransient(List<DemaisDoencas> demaisDoencasTransient) {
		this.demaisDoencasTransient = demaisDoencasTransient;
	}
	public void setDoencasDaInfanciasTransient(List<DoencasDaInfancia> doencasDaInfanciasTransient) {
		this.doencasDaInfanciasTransient = doencasDaInfanciasTransient;
	}
	public void setAlimentacaosTransient(List<Alimentacao> alimentacaosTransient) {
		this.alimentacaosTransient = alimentacaosTransient;
	}
	
}

package br.com.orionx.clinicalsys.carga;


import java.util.HashMap;
import java.util.Map;

import org.nextframework.authorization.AuthorizationDAO;
import org.nextframework.controller.Controller;
import org.nextframework.core.standard.Next;
import org.nextframework.core.standard.NextStandard;

import br.com.orionx.clinicalsys.bean.Papel;
import br.com.orionx.clinicalsys.bean.Usuario;
import br.com.orionx.clinicalsys.bean.UsuarioPapel;
import br.com.orionx.clinicalsys.dao.PapelDAO;
import br.com.orionx.clinicalsys.dao.UsuarioDAO;
import br.com.orionx.clinicalsys.dao.UsuarioPapelDAO;
import br.com.orionx.clinicalsys.sistema.authorization.dao.ClinicalSysAuthorizationProcess;

public class Carga {
	
	
	//Para rodar essa classe � necess�rio adicionar a biblioteca log4j-1.2.15.jar ao classpath
	//Ela nao foi incluida pois da conflito com as libs do jboss
	public static void main(String[] args) {
		insereAdministrador();
	}
	private static void insereAdministrador(){
		NextStandard.createNextContext();
		
		UsuarioDAO usuarioDAO = Next.getObject(UsuarioDAO.class);
		PapelDAO papelDAO = Next.getObject(PapelDAO.class);
		UsuarioPapelDAO usuarioPapelDAO = Next.getObject(UsuarioPapelDAO.class);
		
		Usuario usuario = new Usuario();
		usuario.setLogin("admin");
		usuario.setPassword("admin");

		Papel papel = new Papel();
		papel.setName("Admiministrador");
		papel.setDescription("Administrador do sistema");
		
		UsuarioPapel usuarioPapel = new UsuarioPapel();
		usuarioPapel.setUsuario(usuario);
		usuarioPapel.setPapel(papel);
		
		usuarioDAO.saveOrUpdate(usuario);
		papelDAO.saveOrUpdate(papel);
		usuarioPapelDAO.saveOrUpdate(usuarioPapel);
		
		AuthorizationDAO authorizationDAO = Next.getObject(AuthorizationDAO.class);
		
		String permissionControllerPath = ClinicalSysAuthorizationProcess.class.getAnnotation(Controller.class).path()[0];
		Map<String, String> permissionMap = new HashMap<String, String>();
		permissionMap.put("execute", "true");
		authorizationDAO.savePermission(permissionControllerPath, papel, permissionMap);
		
		System.out.println("Usu�rio Admin configurado com sucesso!");
		System.out.println("Fa�a login como Administrador, acesse a tela de permiss�o e configure as permiss�es do Administrador");		
	}
}

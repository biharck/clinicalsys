package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Table(name="cirurgiapreviahistoriapregressa")
@Entity
public class CirurgiaPreviaHistoriaPregressa {

	private Integer idCirurgiaPreviaHistoriaPregressa;
	private String tipoCirurgia;
	private String mesAnoCirurgia;//montar somente com m�s e ano
	private HistoriaPregressa historiaPregressa;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdCirurgiaPreviaHistoriaPregressa() {
		return idCirurgiaPreviaHistoriaPregressa;
	}
	@Required
	@MaxLength(200)
	public String getTipoCirurgia() {
		return tipoCirurgia;
	}
	@Required
	public String getMesAnoCirurgia() {
		return mesAnoCirurgia;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idHistoriaPregressa")
	@ForeignKey(name = "fk_hp_cirurgias")
	public HistoriaPregressa getHistoriaPregressa() {
		return historiaPregressa;
	}
	public void setIdCirurgiaPreviaHistoriaPregressa(Integer idCirurgiaPreviaHistoriaPregressa) {
		this.idCirurgiaPreviaHistoriaPregressa = idCirurgiaPreviaHistoriaPregressa;
	}
	public void setTipoCirurgia(String tipoCirurgia) {
		this.tipoCirurgia = tipoCirurgia;
	}
	public void setMesAnoCirurgia(String mesAnoCirurgia) {
		this.mesAnoCirurgia = mesAnoCirurgia;
	}
	public void setHistoriaPregressa(HistoriaPregressa historiaPregressa) {
		this.historiaPregressa = historiaPregressa;
	}
	
	
	
}

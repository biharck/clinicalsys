package br.com.orionx.clinicalsys.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.nextframework.bean.annotation.DescriptionProperty;

@Entity
@Table(name= "alimentacao")
public class Alimentacao {
	
	private Integer idAlimentacao;
	private String nome;
	private List<AlimentacaoPaciente> alimentacoesPaciente;
	private boolean checked;
	
	public Alimentacao(){}

	public Alimentacao(Integer idAlimentacao){
		this.idAlimentacao = idAlimentacao;
	}
	
	@Transient
	public boolean isChecked() {
		return checked;
	}
	
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdAlimentacao() {
		return idAlimentacao;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	@OneToMany(mappedBy = "alimentacao")
	public List<AlimentacaoPaciente> getAlimentacoesPaciente() {
		return alimentacoesPaciente;
	}
	public void setIdAlimentacao(Integer idAlimentacao) {
		this.idAlimentacao = idAlimentacao;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setAlimentacoesPaciente(List<AlimentacaoPaciente> alimentacoesPaciente) {
		this.alimentacoesPaciente = alimentacoesPaciente;
	}

	@Override
	public boolean equals(Object o) {
		if(o instanceof Alimentacao){
			Alimentacao alimentacao = (Alimentacao) o;
			return this.getIdAlimentacao().equals(alimentacao.getIdAlimentacao());
		}
		return false;
	}
	
}

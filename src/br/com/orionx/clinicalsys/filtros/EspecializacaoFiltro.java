package br.com.orionx.clinicalsys.filtros;

import org.nextframework.validation.annotation.MaxLength;

import br.com.orionx.clinicalsys.util.generics.FiltroListagem;

public class EspecializacaoFiltro extends FiltroListagem{

	protected String nome;
	protected String descricao;
	protected AtivoEnum ativoEnum;
	
	@MaxLength(100)
	public String getNome() {
		return nome;
	}
	@MaxLength(100)
	public String getDescricao() {
		return descricao;
	}
	public AtivoEnum getAtivoEnum() {
		return ativoEnum;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setAtivoEnum(AtivoEnum ativoEnum) {
		this.ativoEnum = ativoEnum;
	}
	
	

}

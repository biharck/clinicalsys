package br.com.orionx.clinicalsys.clinica.report;

import java.awt.Image;
import java.io.IOException;

import org.nextframework.authorization.report.ReportAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;
import org.nextframework.util.NextImageResolver;

import br.com.orionx.clinicalsys.filtros.PacienteFiltro;
import br.com.orionx.clinicalsys.util.ClinicalSysReport;

@Controller(path = "/clinic/report/prescricao", authorizationModule = ReportAuthorizationModule.class)
public class PrescricaoReport extends ClinicalSysReport<PacienteFiltro>{

	private NextImageResolver nextImageResolver;
	
	public void setNextImageResolver(NextImageResolver nextImageResolver) {
		this.nextImageResolver = nextImageResolver;
	}
	
	@Override
	public IReport createReportClinicalSys(WebRequestContext request, PacienteFiltro filtro) throws Exception {
		
		Image logoClinical = null;
		Image logoEmpresa = null;
		try {
			logoClinical = nextImageResolver.getImage("/images/logo.png");
			logoEmpresa = nextImageResolver.getImage("/images/logo.png");
		} catch (IOException e) {
			e.printStackTrace();
		}
		Report report = new Report("prescricao");
		report.addParameter("logoClinical", logoClinical);
		report.addParameter("logoEmpresa", logoEmpresa);

		report.addParameter("titulo",getTitulo());
		return report;
	}

	@Override
	public String getTitulo() {
		return "Prescri��o M�dica";
	}

	
}

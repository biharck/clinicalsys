package br.com.orionx.clinicalsys.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.types.Cep;
import org.nextframework.types.Cnpj;
import org.nextframework.types.InscricaoEstadual;
import org.nextframework.validation.annotation.Email;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

import br.com.orionx.clinicalsys.util.log.Log;
import br.com.orionx.clinicalsys.validation.annotation.GenerateSpan;

@Entity
@Table(name = "clinica")
public class Clinica implements Log {

	private Integer idClinica;
	private String razaoSocial;
	private String nomeFantasia;
	private Arquivo logomarca;
	private Cnpj cnpj;
	private String inscricaoMunicipal;
	private InscricaoEstadual inscricaoEstadual;
	private Cep cep;
	private TipoLogradouro tipoLogradouro;
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairro;
	private Municipio municipio;
	private List<Ramal> ramais;
	private List<TelefoneClinica> telefones;
	private String email;
	private String emailAlternativo;
	private String twitter;
	private String webSite;

	//Log
	private Integer idUsuarioAltera;
	private Timestamp dtAltera;
	
	
	//TRANSIENT
	private Pais pais;
	private UnidadeFederativa unidadeFederativa;
	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdClinica() {
		return idClinica;
	}
	@Required
	@MinLength(5)
	@MaxLength(100)
	public String getRazaoSocial() {
		return razaoSocial;
	}
	@MaxLength(100)
	public String getNomeFantasia() {
		return nomeFantasia;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "id")
	@ForeignKey(name = "fk_clinica_arquivo")
	public Arquivo getLogomarca() {
		return logomarca;
	}
	@Required
	@DisplayName("CNPJ")
	public Cnpj getCnpj() {
		return cnpj;
	}
	@MaxLength(100)
	public String getInscricaoMunicipal() {
		return inscricaoMunicipal;
	}
	@Required
	public Cep getCep() {
		return cep;
	}
	@Required
	public InscricaoEstadual getInscricaoEstadual() {
		return inscricaoEstadual;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idTipoLogradouro")
	@Required
	public TipoLogradouro getTipoLogradouro() {
		return tipoLogradouro;
	}
	@MinLength(1)
	@MaxLength(200)
	@Required
	public String getLogradouro() {
		return logradouro;
	}
	@MaxLength(10)
	public String getNumero() {
		return numero;
	}
	@MaxLength(50)
	public String getComplemento() {
		return complemento;
	}
	@MaxLength(50)
	public String getBairro() {
		return bairro;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idMunicipio")
	@Required
	public Municipio getMunicipio() {
		return municipio;
	}
	@OneToMany(mappedBy = "clinica")
	public List<Ramal> getRamais() {
		return ramais;
	}
	@OneToMany(mappedBy = "clinica")
	public List<TelefoneClinica> getTelefones() {
		return telefones;
	}
	@Email
	@Required
	public String getEmail() {
		return email;
	}
	@Email
	@GenerateSpan
	public String getEmailAlternativo() {
		return emailAlternativo;
	}
	@MinLength(2)
	public String getTwitter() {
		return twitter;
	}
	@MinLength(5)
	public String getWebSite() {
		return webSite;
	}
	@Transient
	@Required
	public UnidadeFederativa getUnidadeFederativa() {
		return unidadeFederativa;
	}
	@Transient
	@Required
	public Pais getPais() {
		return pais;
	}
	
	//SET
	public void setIdClinica(Integer idClinica) {
		this.idClinica = idClinica;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}
	public void setLogomarca(Arquivo logomarca) {
		this.logomarca = logomarca;
	}
	public void setCnpj(Cnpj cnpj) {
		this.cnpj = cnpj;
	}
	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		this.inscricaoMunicipal = inscricaoMunicipal;
	}
	public void setCep(Cep cep) {
		this.cep = cep;
	}
	public void setInscricaoEstadual(InscricaoEstadual inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}
	public void setTipoLogradouro(TipoLogradouro tipoLogradouro) {
		this.tipoLogradouro = tipoLogradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public void setRamais(List<Ramal> ramais) {
		this.ramais = ramais;
	}
	public void setTelefones(List<TelefoneClinica> telefones) {
		this.telefones = telefones;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setEmailAlternativo(String emailAlternativo) {
		this.emailAlternativo = emailAlternativo;
	}
	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}
	public void setWebSite(String webSite) {
		this.webSite = webSite;
	}
	public void setPais(Pais pais) {
		this.pais = pais;
	}
	public void setUnidadeFederativa(UnidadeFederativa unidadeFederativa) {
		this.unidadeFederativa = unidadeFederativa;
	}
	
	//LOG
	@Override
	public void setIdUsuarioAltera(Integer idUsuarioAltera) {
		this.idUsuarioAltera = idUsuarioAltera;
	}

	@Override
	public void setDtAltera(Timestamp dtAltera) {
		this.dtAltera = dtAltera;
	}

	@Override
	public Integer getIdUsuarioAltera() {
		return idUsuarioAltera;
	}

	@Override
	public Timestamp getDtAltera() {
		return dtAltera;
	}

	

}

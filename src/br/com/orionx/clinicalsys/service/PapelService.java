package br.com.orionx.clinicalsys.service;

import java.util.List;

import br.com.orionx.clinicalsys.bean.Cargo;
import br.com.orionx.clinicalsys.bean.Papel;
import br.com.orionx.clinicalsys.bean.Usuario;
import br.com.orionx.clinicalsys.dao.PapelDAO;
import br.com.orionx.clinicalsys.util.generics.GenericService;

public class PapelService extends GenericService<Papel>{

	private PapelDAO papelDAO;
	public void setPapelDAO(PapelDAO papelDAO) {
		this.papelDAO = papelDAO;
	}
	
	/**
	 * @see br.com.orionx.clinicalsys.dao.PapelDAO#getListaPapelByCargo
	 * @return {@link List} {@link Papel}
	 * @author biharck
	 */
	public List<Papel> getListaPapelByCargo(Cargo cargo){
		return papelDAO.getListaPapelByCargo(cargo);
	}
	
	public List<Papel> findByUsuario(Usuario usuario){
		return papelDAO.findByUsuario(usuario);
	}
}

package br.com.orionx.clinicalsys.util;

/**
 * 
 * @author Biharck
 *
 */
public class TreeView {

	public TreeView() {
		tree.append(CONSTRUCTOR).append(NEW_LINE);
	}
	
	private StringBuilder tree = new StringBuilder();
	
	private static final String CONSTRUCTOR = "d = new dTree('d'); ";
	private static final String NEW_LINE = "\n";
	private static final String COMMA = ",";
	private static final String CLOSE_PARENTHESES = ")";
	private static final String OPEN_PARENTHESES = "(";
	private static final String APOSTROPHE = "'";
	private static final String END_FUNCTION = ";";
	
	
	/**
	 * <p>add a new node
	 * @param nivel
	 * ex:  - node 1<br/>
	 * 			- node 1.1<br/>
	 * 			- node 1.2<br/>
	 * 		- node 2<br/>
	 * 			- node 2.1<br/>
	 * 				- node 2.1.1<br/><br/><br/>
	 * 
	 * <code>
	 * addNode(1,0, "node 1"     ,null)<br/>
	 * addNode(2,1, "node 1.1"   ,null)<br/>
	 * addNode(3,1, "node 1.2"   ,null)<br/>
	 * addNode(4,0, "node 2"     ,null)<br/>
	 * addNode(5,4, "node 2.1"   ,null)<br/>
	 * addNode(6,5, "node 2.1.1" ,null)<br/>
	 * </code>
	 * 
	 * 
	 * 
	 * @author Biharck
	 */
	public void addNode(int nivel, int matherIs, String description, String url){
		tree.append("d.add")
			.append(OPEN_PARENTHESES)
				.append(nivel).append(COMMA)
				.append(matherIs).append(COMMA)
				.append(APOSTROPHE).append(description).append(APOSTROPHE).append(COMMA)
				.append(APOSTROPHE).append(url).append(APOSTROPHE)
			.append(CLOSE_PARENTHESES).append(END_FUNCTION).append(NEW_LINE);
	}
	
	/**
	 * <p>add a new node
	 * @param nivel
	 * ex:  - node 1<br/>
	 * 			- node 1.1<br/>
	 * 			- node 1.2<br/>
	 * 		- node 2<br/>
	 * 			- node 2.1<br/>
	 * 				- node 2.1.1<br/><br/><br/>
	 * 
	 * <code>
	 * addNode(1,0, "node 1"     ,null,null)<br/>
	 * addNode(2,1, "node 1.1"   ,null,null)<br/>
	 * addNode(3,1, "node 1.2"   ,null,null)<br/>
	 * addNode(4,0, "node 2"     ,null,null)<br/>
	 * addNode(5,4, "node 2.1"   ,null,null)<br/>
	 * addNode(6,5, "node 2.1.1" ,null,null)<br/>
	 * </code>
	 * 
	 * 
	 * 
	 * @author Biharck
	 */
	public void addNode(int nivel, int matherIs, String description, String url,String title){
		tree.append("d.add")
			.append(OPEN_PARENTHESES)
				.append(nivel).append(COMMA)
				.append(matherIs).append(COMMA)
				.append(APOSTROPHE).append(description).append(APOSTROPHE).append(COMMA)
				.append(APOSTROPHE).append(url).append(APOSTROPHE).append(COMMA)
				.append(APOSTROPHE).append(title).append(APOSTROPHE)
			.append(CLOSE_PARENTHESES).append(END_FUNCTION).append(NEW_LINE);
	}
	
	/**
	 * <p>add a new node
	 * @param nivel
	 * ex:  - node 1<br/>
	 * 			- node 1.1<br/>
	 * 			- node 1.2<br/>
	 * 		- node 2<br/>
	 * 			- node 2.1<br/>
	 * 				- node 2.1.1<br/><br/><br/>
	 * 
	 * <code>
	 * addNode(1,0, "node 1"     ,null,null,null)<br/>
	 * addNode(2,1, "node 1.1"   ,null,null,null)<br/>
	 * addNode(3,1, "node 1.2"   ,null,null,null)<br/>
	 * addNode(4,0, "node 2"     ,null,null,null)<br/>
	 * addNode(5,4, "node 2.1"   ,null,null,null)<br/>
	 * addNode(6,5, "node 2.1.1" ,null,null,null)<br/>
	 * </code>
	 * 
	 * 
	 * 
	 * @author Biharck
	 */
	public void addNode(int nivel, int matherIs, String description, String url,String title, String target,String icon, String iconOpen){
		tree.append("d.add")
			.append(OPEN_PARENTHESES)
				.append(nivel).append(COMMA)
				.append(matherIs).append(COMMA)
				.append(APOSTROPHE).append(description).append(APOSTROPHE).append(COMMA)
				.append(APOSTROPHE).append(url).append(APOSTROPHE).append(COMMA)
				.append(APOSTROPHE).append(title).append(APOSTROPHE).append(COMMA)
				.append(APOSTROPHE).append(target).append(APOSTROPHE).append(COMMA)
				.append(APOSTROPHE).append(icon).append(APOSTROPHE).append(COMMA)
				.append(APOSTROPHE).append(iconOpen).append(APOSTROPHE)
			.append(CLOSE_PARENTHESES).append(END_FUNCTION).append(NEW_LINE);
	}
	
	/**
	 * <p>add a new node
	 * @param nivel
	 * ex:  - node 1<br/>
	 * 			- node 1.1<br/>
	 * 			- node 1.2<br/>
	 * 		- node 2<br/>
	 * 			- node 2.1<br/>
	 * 				- node 2.1.1<br/><br/><br/>
	 * 
	 * <code>
	 * addNode(1,0, "node 1"     ,null,null,null)<br/>
	 * addNode(2,1, "node 1.1"   ,null,null,null)<br/>
	 * addNode(3,1, "node 1.2"   ,null,null,null)<br/>
	 * addNode(4,0, "node 2"     ,null,null,null)<br/>
	 * addNode(5,4, "node 2.1"   ,null,null,null)<br/>
	 * addNode(6,5, "node 2.1.1" ,null,null,null)<br/>
	 * </code>
	 * 
	 * 
	 * 
	 * @author Biharck
	 */
	public void addNode(int nivel, int matherIs, String description, String url,String title, String target){
		tree.append("d.add")
			.append(OPEN_PARENTHESES)
				.append(nivel).append(COMMA)
				.append(matherIs).append(COMMA)
				.append(APOSTROPHE).append(description).append(APOSTROPHE).append(COMMA)
				.append(APOSTROPHE).append(url).append(APOSTROPHE).append(COMMA)
				.append(APOSTROPHE).append(title).append(APOSTROPHE).append(COMMA)
				.append(APOSTROPHE).append(target).append(APOSTROPHE)
			.append(CLOSE_PARENTHESES).append(END_FUNCTION).append(NEW_LINE);
	}
	
	/**
	 * <p> add a header 
	 * @param description
	 * 
	 * @author Biharck
	 */
	public void addHeader(String description){
		tree.append("d.add(0,-1,'").append(description).append("');").append(NEW_LINE);;
	}
	
	/**
	 * 
	 * @return the tree 
	 * @author Biharck
	 */
	public String getTreeView(){
		return tree.append(NEW_LINE).append("document.write(d);").toString();
	}

}

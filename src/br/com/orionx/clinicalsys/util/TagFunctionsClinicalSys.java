package br.com.orionx.clinicalsys.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import br.com.orionx.clinicalsys.bean.Usuario;
import br.com.orionx.clinicalsys.service.UsuarioService;

public class TagFunctionsClinicalSys {

	/**
	 * Fun��o respons�vel em representar a tld de truncar um texto
	 * @param texto o texto a ser truncado
	 * @param size tamanho m�ximo do campo
	 * @return texto formatado
	 */
	public static String trunc(String texto, Integer size) {
		if (texto != null && texto.length() >= size)
			texto = texto.substring(0, size) + " ...";
		return texto;
	}

	/**
	 * <p>Fun��o respons�vel em formatar a data no padr�o brasileiro dd/MM/yyyy HH:mm
	 * @param data data a ser formatada
	 * @return data formatada
	 */
	public static String dateFormat(Date data) {
		if (data != null) {
			return new SimpleDateFormat("dd/MM/yyyy").format(data);
		} else {
			return "";
		}
	}

	/**
	 * <p>Fun��o respons�vel em formatar a hora no padr�o brasileiro  HH:mm
	 * @param data data a ser formatada
	 * @return hora formatada
	 */
	public static String hourFormat(Date data) {
		if (data != null) {
			return new SimpleDateFormat("HH:mm:ss").format(data);
		} else {
			return "";
		}
	}
	
	/**
	 * M�todo respons�vel em retornar o nome do usu�rio pelo seu ID
	 * @param code id do usu�rio
	 * @return String contendo o nome do usu�rio
	 */
	public static String getUserById(Integer code){
		Usuario user = UsuarioService.getInstance().getUsuarioById(code);
		if(user != null){
			return user.getLogin();
		}
		return null;
	}
	
	/**
	 * <p>M�todo usado para verificar se uma String � num�rica
	 * @param text texto do jsp
	 * @return {@link Boolean} true se for num�rica e false caso contr�rio
	 */
	public static Boolean isNumeric(String text){
		return StringUtils.isNumeric(text);
	}
	
	/**
	 * <p>M�todo que retorna a idade a partir da data de nascimento
	 * @param date data de nascimento
	 * @return 
	 */
	public static Integer getIdade(java.util.Date date){
		if(date!=null){
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");  
			df.setLenient(false); //somente cria a data se a forma��o da mesma for correta  
			Date d = date;  
			  
			Calendar dataAtual = Calendar.getInstance();  
			dataAtual.setTime(new Date());  
			  
			Calendar dataNascimento = Calendar.getInstance();  
			dataNascimento.setTime(d);  
			          
			Integer idade = dataAtual.get( Calendar.YEAR ) -   
			                dataNascimento.get( Calendar.YEAR );  
			  
			// N�o fez anivers�rio?  
			if( dataAtual.get( Calendar.MONTH ) <   
			    dataNascimento.get( Calendar.MONTH ) ){  
			    idade = idade - 1;   
			}  
			else if( (dataAtual.get( Calendar.MONTH )==    
			          dataNascimento.get( Calendar.MONTH )) &&                    
			         (dataAtual.get( Calendar.DAY_OF_MONTH ) <=   
			          dataNascimento.get( Calendar.DAY_OF_MONTH )) ){  
			    idade = idade - 1;                
			}
			return idade;
		}
		return null;
	}

}

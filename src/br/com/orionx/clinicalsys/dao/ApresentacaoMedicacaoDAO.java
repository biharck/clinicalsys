package br.com.orionx.clinicalsys.dao;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.QueryBuilder;

import br.com.orionx.clinicalsys.bean.ApresentacaoMedicacao;
import br.com.orionx.clinicalsys.filtros.ApresentacaoMedicacaoFiltro;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class ApresentacaoMedicacaoDAO extends GenericDAO<ApresentacaoMedicacao>{

	
	@Override
	public void updateListagemQuery(QueryBuilder<ApresentacaoMedicacao> query,FiltroListagem _filtro) {
		ApresentacaoMedicacaoFiltro filtro = (ApresentacaoMedicacaoFiltro) _filtro;		
		query.whereLikeIgnoreAll("apresentacaoMedicacao.nome",filtro.getNome());
		super.updateListagemQuery(query, filtro);
	}
}

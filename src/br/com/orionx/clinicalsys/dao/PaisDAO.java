package br.com.orionx.clinicalsys.dao;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.DefaultOrderBy;
import org.nextframework.persistence.QueryBuilder;

import br.com.orionx.clinicalsys.bean.Pais;
import br.com.orionx.clinicalsys.filtros.PaisFiltro;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

@DefaultOrderBy("pais.nome")
public class PaisDAO extends GenericDAO<Pais> {

	
	@Override
	public void updateListagemQuery(QueryBuilder<Pais> query,FiltroListagem _filtro) {
		PaisFiltro filtro = (PaisFiltro) _filtro;
		query.whereLikeIgnoreAll("pais.nome",filtro.getNome());
	}
}

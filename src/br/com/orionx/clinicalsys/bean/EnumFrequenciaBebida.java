package br.com.orionx.clinicalsys.bean;

public enum EnumFrequenciaBebida {

	OCASIONAL_MODERADO(1,"Uso Ocasional em Quantidades Moderadas."),
	OCASIONAL_GRANDES_QUANTIDADES(2,"Uso Ocasional, em Grandes Quantidades, Chegando ao Estado de Embriagez."),
	FREQUENTE_MODERADA(3,"Uso Frequente em Quantidade Moderada."),
	DIARIO_PQN_QTD(4,"Uso Di�rio em Pequena Quantidade."),
	DIARIO_GRD_QTD(5,"Uso Di�rio em Quantidade para Determinar a Embriagez."),
	DIARIO_EXAGERADO(6,"Uso Di�rio, em Quantidade Exagerada, Chegando o Paciente a Avan�ado Estado de Embriagez.");
	
	
	private Integer id;
	private String descricao;
	
	private EnumFrequenciaBebida(Integer id, String descricao){
		this.id = id;
		this.descricao = descricao;
	}
	
	public Integer getId() {
		return id;
	}
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}
}


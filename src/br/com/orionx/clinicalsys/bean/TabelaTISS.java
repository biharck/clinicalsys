package br.com.orionx.clinicalsys.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.Required;
/**
 * tabela de precos da guia tiss
 * @author biharck
 *
 */
@Entity
@Table(name = "tabelatiss")
public class TabelaTISS {
	
	private Integer idTabelaTISS;
	private String codigo;
	private String descricao;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getIdTabelaTISS() {
		return idTabelaTISS;
	}
	@Required
	@Column(nullable=false, length=2)
	public String getCodigo() {
		return codigo;
	}
	@Required
	@DescriptionProperty
	@Column(nullable=false, length=200)
	public String getDescricao() {
		return descricao;
	}
	
	public void setIdTabelaTISS(Integer idTabelaTISS) {
		this.idTabelaTISS = idTabelaTISS;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}

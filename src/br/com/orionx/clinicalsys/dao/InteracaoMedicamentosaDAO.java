package br.com.orionx.clinicalsys.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.QueryBuilder;
import org.nextframework.persistence.SaveOrUpdateStrategy;

import br.com.orionx.clinicalsys.bean.InteracaoMedicamentosa;
import br.com.orionx.clinicalsys.filtros.InteracaoMedicamentosaFiltro;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class InteracaoMedicamentosaDAO extends GenericDAO<InteracaoMedicamentosa>{

	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("interacoes");
		save.saveOrUpdateManaged("interacoesClass");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<InteracaoMedicamentosa> query, FiltroListagem _filtro) {
		InteracaoMedicamentosaFiltro filtro = (InteracaoMedicamentosaFiltro) _filtro;
		if(filtro.getAtivoEnum()!=null)
			if(filtro.getAtivoEnum().getSituacao()!=null && (filtro.getAtivoEnum().getSituacao() || !filtro.getAtivoEnum().getSituacao()))
				query.where("interacaoMedicamentosa.ativo is "+filtro.getAtivoEnum().getSituacao());
		
		query
			.join("interacaoMedicamentosa.medicamento m")
			.join("m.classificacaoMedicacao classificacaoMedicacao ")
			.where("classificacaoMedicacao=?",filtro.getClassificacaoMedicacao())
			.where("m=?",filtro.getMedicamento());
		
		
		super.updateListagemQuery(query, filtro);
	}
	
	@Override
	public List<InteracaoMedicamentosa> findAll() {
		return query().where("interacaoMedicamentosa.ativo is true").list();
	}
	
	@Override
	public List<InteracaoMedicamentosa> findForCombo(String... extraFields) {
		extraFields[0] += "ativo";
		return super.findForCombo(extraFields);
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<InteracaoMedicamentosa> query) {
		query.leftOuterJoinFetch("interacaoMedicamentosa.interacoes");

	}

}

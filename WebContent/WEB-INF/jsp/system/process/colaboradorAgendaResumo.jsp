<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="cs" uri="clinicalsys"%>


<div class="onecolumn">
	<div class="header">
		<span>Agenda dos Colaboradores</span>
	</div>
	<br class="clear"/>
	<div class="content">
		<t:tela>
		<div align="right">
			<n:link action="index" parameters="newCA=true" onclick="openDialogAguarde()" ><img src="${application}/images/shortcut/add.png" alt="Novo" class="help" title="Novo"/></n:link>
		</div>
		<n:dataGrid headerStyleClass="" footerStyleClass="" var="colaboradorAgenda" styleClass="data" bodyStyleClasses=""  itens="${lista}" itemType="br.com.orionx.clinicalsys.bean.ColaboradorAgenda"
					width="100%" cellspacing="1" 
		            rowonclick="javascript:$csu.coloreLinha('tabelaPermissao',this)" 
					rowonmouseover="javascript:$csu.mouseonOverTabela('tabelaPermissao',this)" 
					rowonmouseout="javascript:$csu.mouseonOutTabela('tabelaPermissao',this)" 
					id="tabelaPermissao" varIndex="index">
				<t:property name="colaborador" mode="output"/>
				<t:property name="tipoAgenda" mode="output"/>
				<c:set var="ultimadata" value="colaboradorAgenda.dtAltera"/>
				<n:column header="Data de Cria��o">
					<n:panel><font style="color:#E77272">${cs:dateformat(n:reevaluate(ultimadata,pageContext))}</font> �s <font style="color:#E77272">${cs:hourformat(n:reevaluate(ultimadata,pageContext))}</font> </n:panel>
				</n:column>
				<n:column header="Visualizar">
					<a href="javascript:openDialogAguarde();getText(${colaboradorAgenda.idColaboradorAgenda})"><img src="/ClinicalSys/images/find.png" title="Ver Agenda" class="help"></a>
				</n:column>
				<n:column header="Desativar">
					<a href="javascript:openDialogAguarde();desativarAgenda(${colaboradorAgenda.idColaboradorAgenda},${index})"><img src="/ClinicalSys/images/remove.png" title="Desativar Agenda" class="help"></a>
				</n:column>
			</n:dataGrid>
		</t:tela>
	</div>
</div>
<script type="text/javascript">
	var linha;
	function getText(id){
		sendRequest('/ClinicalSys/system/process/ColaboradorAgenda','ACAO=getAgendaColaborador&showResumen=true&saveConfigAgenda=true&idColaboradorAgenda='+id,'POST', ajaxCallback, erroCallback);
    }
    function desativarAgenda(id, idx){
    	linha = idx;
    	sendRequest('/ClinicalSys/system/process/ColaboradorAgenda','ACAO=desativarAgenda&idColaboradorAgenda='+id,'POST', ajaxCallbackDesativar, erroCallback);
    }
    
    function ajaxCallback(data){
    	closeDialogAguarde();
    	openDialogInformation(data);
    }
    
    function ajaxCallbackDesativar(data){
    	eval(data);
    	closeDialogAguarde();
   		openDialogInformation(msg);
    	if(ok){
    		var contador = 0;
    		$('#tabelaPermissao tbody tr').each(function(){ 
				if(contador == linha)    			
    				$(this).remove();
    			contador ++;
    		});
    	}
    }
    
    function erroCallback(request){
    	closeDialogAguarde();
    	openDialogInformation('Erro no ajax!\n' + request.responseText);
    }
    
    
    
</script>
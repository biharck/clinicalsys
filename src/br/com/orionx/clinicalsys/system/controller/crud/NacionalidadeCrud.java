package br.com.orionx.clinicalsys.system.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.orionx.clinicalsys.bean.Nacionalidade;
import br.com.orionx.clinicalsys.filtros.NacionalidadeFiltro;
import br.com.orionx.clinicalsys.util.DatabaseError;
import br.com.orionx.clinicalsys.util.exception.ClinicalSysException;
import br.com.orionx.clinicalsys.util.generics.CrudController;

@Controller(path="/system/pag/Nacionalidade",authorizationModule=CrudAuthorizationModule.class)
public class NacionalidadeCrud extends CrudController<NacionalidadeFiltro, Nacionalidade, Nacionalidade> {

	@Override
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Administra��o &raquo; <a href=\"/ClinicalSys/system/pag/Nacionalidade\">Nacionaliade</a></p>";
	}

	@Override
	public ModelAndView doSalvar(WebRequestContext request, Nacionalidade form)	throws CrudException {
		return saveUnique(request, form,true);
	}
	
	@Override
	public ModelAndView saveAndNew(WebRequestContext request, Nacionalidade form)throws Exception {
		return saveUnique(request, form,false);
	}
	
	public ModelAndView saveUnique(WebRequestContext request, Nacionalidade form, boolean retornaListagem){
		try {
			salvar(request, form);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_nome_nacional")) throw new ClinicalSysException("J� existe uma nacionalidade com este nome cadastrada no sistema.");
			return sendRedirectToAction("entrada");
		}
		catch (Exception e) {
			return sendRedirectToAction("entrada");
		}
		if(retornaListagem)return sendRedirectToAction("listagem");
		else return sendRedirectToAction("entrada");
	}


}

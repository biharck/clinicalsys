package br.com.orionx.clinicalsys.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.nextframework.bean.annotation.DescriptionProperty;

@Entity
@Table(name = "imunizacoesinfancia")
public class ImunizacoesInfancia {

	private Integer idImunizacoesInfancia;
	private String nome;
	private Integer meses;
	private List<ImunizacoesInfanciaPaciente> imunizacoesInfanciaPaciente; 
	private boolean checked;
	
	
	public ImunizacoesInfancia() {}
	public ImunizacoesInfancia(Integer idImunizacoesInfancia) {
		this.idImunizacoesInfancia = idImunizacoesInfancia;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdImunizacoesInfancia() {
		return idImunizacoesInfancia;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	public Integer getMeses() {
		return meses;
	}
	@OneToMany(mappedBy="imunizacaoPaciente")
	public List<ImunizacoesInfanciaPaciente> getImunizacoesInfanciaPaciente() {
		return imunizacoesInfanciaPaciente;
	}
	@Transient
	public boolean isChecked() {
		return checked;
	}
	public void setIdImunizacoesInfancia(Integer idImunizacoesInfancia) {
		this.idImunizacoesInfancia = idImunizacoesInfancia;
	}
	public void setMeses(Integer meses) {
		this.meses = meses;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setImunizacoesInfanciaPaciente(List<ImunizacoesInfanciaPaciente> imunizacoesInfanciaPaciente) {
		this.imunizacoesInfanciaPaciente = imunizacoesInfanciaPaciente;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o instanceof ImunizacoesInfancia){
			ImunizacoesInfancia ii = (ImunizacoesInfancia) o;
			return this.getIdImunizacoesInfancia().equals(ii.getIdImunizacoesInfancia());
		}
		return false;
	}
}

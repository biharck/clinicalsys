package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.types.Money;
import org.nextframework.validation.annotation.MinValue;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name = "precoconsultaespecialidade")
public class PrecoConsultaEspecialidade {

	private Integer idPrecoConsultaEspecialidade;
	private Especializacao especializacao;
	private Money valorConsulta;
	private Integer tempoConsulta;//em minutos tempo de atendimento
	private ConfiguracaoAtendimento configuracaoAtendimento;
	private boolean ativo = true;

	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdPrecoConsultaEspecialidade() {
		return idPrecoConsultaEspecialidade;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idEspecializacao")
	@ForeignKey(name = "fk_precoConsEspe_espe")
	@Required
	public Especializacao getEspecializacao() {
		return especializacao;
	}
	
	@Required
	@MinValue(1)
	public Money getValorConsulta() {
		return valorConsulta;
	}
	
	@Required
	@MinValue(1)
	public Integer getTempoConsulta() {
		return tempoConsulta;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idConfiguracaoAtendimento")
	public ConfiguracaoAtendimento getConfiguracaoAtendimento() {
		return configuracaoAtendimento;
	}
	/**
	 * <p>ser� usado quando o valor for alterado, o registro ser� desativado e o pre�o ser� alterado,
	 * vinculando ao novo registro
	 * @return
	 */
	public boolean isAtivo() {
		return ativo;
	}
	
	//SET
	public void setIdPrecoConsultaEspecialidade(Integer idPrecoConsultaEspecialidade) {
		this.idPrecoConsultaEspecialidade = idPrecoConsultaEspecialidade;
	}
	public void setEspecializacao(Especializacao especializacao) {
		this.especializacao = especializacao;
	}
	public void setValorConsulta(Money valorConsulta) {
		this.valorConsulta = valorConsulta;
	}
	public void setTempoConsulta(Integer tempoConsulta) {
		this.tempoConsulta = tempoConsulta;
	}
	public void setConfiguracaoAtendimento(ConfiguracaoAtendimento configuracaoAtendimento) {
		this.configuracaoAtendimento = configuracaoAtendimento;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof PrecoConsultaEspecialidade){
			PrecoConsultaEspecialidade pce = (PrecoConsultaEspecialidade) obj;
			if(pce.getIdPrecoConsultaEspecialidade()==null) 
				return false;
			return pce.getIdPrecoConsultaEspecialidade().equals(this.getIdPrecoConsultaEspecialidade());
		}
		return super.equals(obj);
	}
	
}

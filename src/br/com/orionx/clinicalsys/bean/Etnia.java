package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="etnia")
public class Etnia {
	
	private Integer idEtnia;
	private String nome;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdEtnia() {
		return idEtnia;
	}
	@DescriptionProperty
	@Required
	public String getNome() {
		return nome;
	}
	
	public void setIdEtnia(Integer idEtnia) {
		this.idEtnia = idEtnia;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}

}

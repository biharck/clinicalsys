package br.com.orionx.clinicalsys.util.webservice;

/**
 * Classe usada para preencher o resultado da busca pelo webservice
 * @author biharck
 *
 */
public class Retorno {
	
	 String cep;
	 String uf;
	 String cidade;
	 String bairro;
	 String tipoLogradouro;
	 String logradouro;
	 String data;
	 String resultado;
	 String resultadoTxt;
	 String limiteBuscas;
	 String ibgeUf;
	 String ibgeMunicipio;
	 String ibgeMunicipioVerificador;
	
	public Retorno(){
		
	}
	public Retorno(String cep, String uf, String cidade, String bairro, String tipoLogradouro,String logradouro,
			String data, String resultado, String resultadoTxt, String limiteBuscas, String ibgeUf, String ibgeMunicipio, String ibgeMunicipioVerificador){
		this.cep = cep;
		this.uf = uf;
		this.cidade = cidade;
		this.bairro = bairro;
		this.tipoLogradouro = tipoLogradouro;
		this.logradouro = logradouro;
		this.data = data;
		this.resultado = resultado;
		this.resultadoTxt = resultadoTxt;
		this.limiteBuscas = limiteBuscas;
		this.ibgeUf = ibgeUf;
		this.ibgeMunicipio = ibgeMunicipio;
		this.ibgeMunicipioVerificador = ibgeMunicipioVerificador;
	}
	public String getCep() {
		return cep;
	}
	public String getUf() {
		return uf;
	}
	public String getCidade() {
		return cidade;
	}
	public String getBairro() {
		return bairro;
	}
	public String getTipoLogradouro() {
		return tipoLogradouro;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public String getData() {
		return data;
	}
	public String getResultado() {
		return resultado;
	}
	public String getResultadoTxt() {
		return resultadoTxt;
	}
	public String getLimiteBuscas() {
		return limiteBuscas;
	}
	public String getIbgeUf() {
		return ibgeUf;
	}
	public String getIbgeMunicipio() {
		return ibgeMunicipio;
	}
	public String getIbgeMunicipioVerificador() {
		return ibgeMunicipioVerificador;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public void setTipoLogradouro(String tipoLogradouro) {
		this.tipoLogradouro = tipoLogradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public void setData(String data) {
		this.data = data;
	}
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
	public void setResultadoTxt(String resultadoTxt) {
		this.resultadoTxt = resultadoTxt;
	}
	public void setLimiteBuscas(String limiteBuscas) {
		this.limiteBuscas = limiteBuscas;
	}
	public void setIbgeUf(String ibgeUf) {
		this.ibgeUf = ibgeUf;
	}
	public void setIbgeMunicipio(String ibgeMunicipio) {
		this.ibgeMunicipio = ibgeMunicipio;
	}
	public void setIbgeMunicipioVerificador(String ibgeMunicipioVerificador) {
		this.ibgeMunicipioVerificador = ibgeMunicipioVerificador;
	}
}

package br.com.orionx.clinicalsys.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name="crescimentopaciente")
public class CrescimentoPaciente {

	private Integer idCrecimentoPaciente;
	private Float peso;
	private Float circAbdominal;// circunferencia abdominal
	private Float altura;
	private Float imc;
	private HistoriaPregressa historiaPregressa;
	private String descricao;
	private Date data = new Date(System.currentTimeMillis());
	
	private String alturaString;
	private String pesoString;
	private String circAbdominalString;
	
	@Transient
	public String getAlturaString() {
		return alturaString;
	}
	@Transient
	public String getPesoString() {
		return pesoString;
	}
	@Transient
	public String getCircAbdominalString() {
		return circAbdominalString;
	}
	public void setAlturaString(String alturaString) {
		this.alturaString = alturaString;
	}
	public void setPesoString(String pesoString) {
		this.pesoString = pesoString;
	}
	public void setCircAbdominalString(String circAbdominalString) {
		this.circAbdominalString = circAbdominalString;
	}

	@Transient
	public Float getImc() {
		return imc;
	}
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdCrecimentoPaciente() {
		return idCrecimentoPaciente;
	}
	@Required
	public Float getPeso() {
		return peso;
	}
	public Float getCircAbdominal() {
		return circAbdominal;
	}
	@Required
	public Float getAltura() {
		return altura;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idHistoriaPregressa")
	@ForeignKey(name = "fk_crescimento_HP")
	public HistoriaPregressa getHistoriaPregressa() {
		return historiaPregressa;
	}
	@Transient
	public String getDescricao() {
		return descricao;
	}
	public Date getData() {
		return data;
	}

	public void setIdCrecimentoPaciente(Integer idCrecimentoPaciente) {
		this.idCrecimentoPaciente = idCrecimentoPaciente;
	}
	public void setPeso(Float peso) {
		this.peso = peso;
	}
	public void setCircAbdominal(Float circAbdominal) {
		this.circAbdominal = circAbdominal;
	}
	public void setAltura(Float altura) {
		this.altura = altura;
	}
	public void setImc(Float imc) {
		this.imc = imc;
	}
	public void setHistoriaPregressa(HistoriaPregressa historiaPregressa) {
		this.historiaPregressa = historiaPregressa;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setData(Date data) {
		this.data = data;
	}

}

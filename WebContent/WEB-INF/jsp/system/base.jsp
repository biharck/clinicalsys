<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"> 
<%@page import="br.com.orionx.clinicalsys.util.ClinicalSysUtil"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"> 
 
<!-- Website Title --> 
<title>ClinicalSys</title>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<!-- Template stylesheet -->
<link id="screenColor" href="${application}/css/blue/screen.css" rel="stylesheet" type="text/css" media="all">
<link id="datepickerColor" href="${application}/css/blue/datepicker.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/tipsy.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/js/visualize/visualize.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/js/jwysiwyg/jquery.wysiwyg.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/js/fancybox/jquery.fancybox-1.3.0.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/tipsy.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/autocomplete.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/default.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/dtree.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/ui.theme.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/ui.dialog.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/ui.datepicker.css" rel="stylesheet" type="text/css" media="all">



<!--[if IE]>
	<link href="${application}/css/ie.css" rel="stylesheet" type="text/css" media="all">
	<script type="text/javascript" src="${application}/js/excanvas.js"></script>
<![endif]-->

<!-- Jquery and plugins -->
<script type="text/javascript" src="${application}/js/jquery.js"></script>
<script type="text/javascript" src="${application}/js/jquery-ui.js"></script>
<script type="text/javascript" src="${application}/js/jquery.img.preload.js"></script>
<script type="text/javascript" src="${application}/js/hint.js"></script>
<script type="text/javascript" src="${application}/js/visualize/jquery.visualize.js"></script>
<script type="text/javascript" src="${application}/js/jwysiwyg/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="${application}/js/fancybox/jquery.fancybox-1.3.0.js"></script>
<script type="text/javascript" src="${application}/js/jquery.tipsy.js"></script>
<script type="text/javascript" src="${application}/js/custom_blue.js"></script>
<script type="text/javascript" src="${application}/js/validate.js"></script>
<script type="text/javascript" src="${application}/js/functions.js"></script>
<script type="text/javascript" src="${application}/js/dtree.js"></script>
<script type="text/javascript" src="${application}/js/util.js"></script>
<script type="text/javascript" src="${application}/js/ajax.js"></script>
<script type="text/javascript" src="${application}/js/input.js"></script>
<script type="text/javascript" src="${application}/js/dynatable.js"></script>
<script type="text/javascript" src="${application}/js/clinicalSysUtil.js"></script>
<script type="text/javascript" src="${application}/js/autocomplete/jquery.bgiframe.min.js"></script>
<script type="text/javascript" src="${application}/js/autocomplete/jquery.autocomplete.js"></script>


</head>
<body class="nobg">
<a name="topo"></a>
<div class="content_wrapper">

	<!-- Begin header -->
	<div id="header">
		<div id="logo">
			<img src="${application}/images/logo.png" alt="logo"/>
		</div>
		<c:if test="${param.fromInsertOne != 'true'}">
			<c:if test="${param.onePath != 'true'}">
				<div id="search">
					<form action="${application}/clinic/process/prontuario" id="search_form" name="search_form" method="get">
						<input type="text" id="paciente" onkeyUp="ajaxGetName(this.value)" onChange="verificaExistencia()" name="what" title="Procurar" class="search noshadow" />
						<input type="hidden" name="autocomplete_id" id="autocomplete_id"/>
					</form>
				</div>
			</c:if>
		</c:if>
		<div id="account_info">
			<c:if test="${empty fotoLogin.cdfile}">
				<img src="${pageContext.request.contextPath}/images/peopleNotFound.png" id="imagem_listagem" />
			</c:if>
			<c:if test="${!empty fotoLogin.cdfile}">
				<img src="${pageContext.request.contextPath}/DOWNLOADFILE/${fotoLogin.cdfile}" id="imagem_listagem"/>
			</c:if>
			Olá <a href="">Dr. <%= ClinicalSysUtil.getUsuarioLogado().getLogin() %></a>
			<c:if test="${param.fromInsertOne != 'true'}">
				<c:if test="${param.onePath != 'true'}">
			 		(<a href="javascript:openDialogInformation('Aguarde, em breve uma nova funcionalidade estará disponível para você!')">1 nova menssagem</a>) | <a href="${application}/system/Logout">Logout</a>
			 	</c:if>
			</c:if>
			&nbsp;&nbsp;&nbsp;&nbsp;Sua sessão expira em: <span id="cronometro_tempo"></span>
		</div>
	</div>
	<!-- End header -->
	
	<c:if test="${param.fromInsertOne != 'true'}">
		<c:if test="${param.onePath != 'true'}">
			<!-- Begin left panel -->
			<a href="javascript:;" id="show_menu" >&raquo;</a>
			<div id="left_menu" style="display:none" >
				<a href="javascript:;" id="hide_menu">&laquo;</a>
				<!-- Begin left panel calendar -->
				<div id="calendar"></div>
				<!-- End left panel calendar -->
				<div id="menu_config" style="display:none;">
					<ul id='main_menu' class=''>
						<n:menu menupath="/WEB-INF/menu/system.xml"/>
						<n:menu menupath="/WEB-INF/menu/estoque.xml"/>
						<n:menu menupath="/WEB-INF/menu/util.xml"/>
					</ul>
				</div>
				<br class="clear"/>
				
				
			</div>
			<!-- End left panel -->
		</c:if>
	</c:if>
	
	
	<!-- Begin content -->
	<div id="content" style="margin-left:30px;">
		<div class="inner">
			<c:if test="${param.ACAO != 'criar'}">
				<c:if test="${param.fromInsertOne != 'true'}">
					<c:if test="${param.onePath != 'true'}">
						<i id="pathURL">${pathURL}</i>
					</c:if>
				</c:if>
				<!-- Begin shortcut menu -->
				<ul id="shortcut" style="display:none">
	    			<li>
	    			   <a href="#" onclick="openDialogAguarde();location.href='${application}/clinic/process/listaEspera'" title="Lista de Espera">
					    <img src="${application}/images/shortcut/atendimento.png" alt="Lista de Espera"/><br/>
					    <strong>Espera</strong>
					  </a>
					</li>
	    			<li>
	    			  <a href="#" onclick="openDialogAguarde();location.href='${application}/clinic/process/Calendar'" title="Agenda">
					    <img src="${application}/images/shortcut/agenda.png" alt="Agenda"/><br/>
					    <strong>Agenda</strong>
					  </a>
					</li>
					<li>
	    			  <a href="#" onclick="openDialogAguarde();location.href='${application}/clinic/pag/Paciente'" id="shortcut_contacts" title="Cadastro de Pacientes">
					    <img src="${application}/images/shortcut/patient.png" alt="Cadastro de Pacientes"/><br/>
					    <strong>Paciente</strong>
					  </a>
					</li>
					<li>
	    			  <a href="#" onclick="openDialogAguarde();location.href='${application}/clinic/process/Medicamentos'" id="shortcut_contacts" title="Medicamentos">
					    <img src="${application}/images/shortcut/bookmedicine.png" alt="Medicamentos"/><br/>
					    <strong>Drogas</strong>
					  </a>
					</li>
	    			<li>
	    			    <a href="#" onclick="openDialogAguarde();location.href='${application}/clinic/process/Reports'" title="Relatórios">
					    <img src="${application}/images/shortcut/report.png" alt="Relatórios"/><br/>
					    <strong>Relatórios</strong>
					  </a>
					</li>
	    			<li>
	    			  <a href="#" onclick="openDialogAguarde();location.href='${application}/clinic/process/DashBoard'" title="Dashboard">
					    <img src="${application}/images/shortcut/stats.png" alt="Dashboard"/><br/>
					    <strong>Dashboard</strong>
					  </a>
					</li>
					<li>
	    			  <a href="#" onclick="openMenuByModulo('estoque')" title="Administração de estoque e financeiro">
					    <img src="${application}/images/shortcut/stock.png" alt="Administração de estoque e financeiro"/><br/>
					    <strong>Adm</strong>
					  </a>
					</li>
					<li>
	    			  <a href="#" onclick="openMenuByModulo('util')" title="Utilitários">
					    <img src="${application}/images/shortcut/util.png" alt="Utilitários"/><br/>
					    <strong>Utilitários</strong>
					  </a>
					</li>
					<li>
	    			  <a href="#" onclick="openMenuByModulo('system')" title="Configurações do sistema">
					    <img src="${application}/images/shortcut/setting.png" alt="Configurações"/><br/>
					    <strong>Config</strong>
					  </a>
					</li>
	  			</ul>
				<!-- End shortcut menu -->
			
				<!-- End shortcut noficaton -->
			</c:if>
			
			<br class="clear"/>
			<div class="messageOuterDiv">
				<n:messages/> <%-- Imprime as mensagens do sistema --%>
			</div>
			<jsp:include page="${bodyPage}" />			

		<!-- Begin footer -->
		<div id="footer">
			 Copyright&copy; by OrionX Technologies
		</div>
		<!-- End footer -->
		
	</div>
	<!-- End content -->
</div>

<c:if test="${param.onePath != 'true'}">
	<div id="rodapeFixo">
	<div id="headerFooter"><div align="right"><div id="divButtonClose">Fechar</div></div></div>
		
		<!-- Begin shortcut menu -->
		<ul id="shortcut">
   			<li>
   			   <a href="#" onclick="openDialogAguarde();location.href='${application}/clinic/process/listaEspera'" title="Lista de Espera">
			    <img src="${application}/images/shortcut/atendimento.png" alt="Lista de Espera"/><br/>
			    <strong>Espera</strong>
			  </a>
			</li>
   			<li>
   			  <a href="#" onclick="openDialogAguarde();location.href='${application}/clinic/process/Calendar'" title="Agenda">
			    <img src="${application}/images/shortcut/agenda.png" alt="Agenda"/><br/>
			    <strong>Agenda</strong>
			  </a>
			</li>
			<li>
   			  <a href="#" onclick="openDialogAguarde();location.href='${application}/clinic/pag/Paciente'" id="shortcut_contacts" title="Cadastro de Pacientes">
			    <img src="${application}/images/shortcut/patient.png" alt="Cadastro de Pacientes"/><br/>
			    <strong>Paciente</strong>
			  </a>
			</li>
			<li>
   			  <a href="#" onclick="openDialogAguarde();location.href='${application}/clinic/process/Medicamentos'" id="shortcut_contacts" title="Medicamentos">
			    <img src="${application}/images/shortcut/bookmedicine.png" alt="Medicamentos"/><br/>
			    <strong>Drogas</strong>
			  </a>
			</li>
   			<li>
   			  <a href="modal_window.html" title="Relatórios">
			    <img src="${application}/images/shortcut/report.png" alt="Relatórios"/><br/>
			    <strong>Relatórios</strong>
			  </a>
			</li>
   			<li>
   			  <a href="modal_window.html" title="Dashboard">
			    <img src="${application}/images/shortcut/stats.png" alt="Dashboard"/><br/>
			    <strong>Dashboard</strong>
			  </a>
			</li>
			<li>
   			  <a href="#" onclick="openMenuByModulo('estoque')" title="Administração de estoque e financeiro">
			    <img src="${application}/images/shortcut/stock.png" alt="Administração de estoque e financeiro"/><br/>
			    <strong>Adm</strong>
			  </a>
			</li>
			<li>
   			  <a href="#" onclick="openMenuByModulo('util')" title="Utilitários">
			    <img src="${application}/images/shortcut/util.png" alt="Utilitários"/><br/>
			    <strong>Utilitários</strong>
			  </a>
			</li>
			<li>
   			  <a href="#" onclick="openMenuByModulo('system')" title="Configurações do sistema">
			    <img src="${application}/images/shortcut/setting.png" alt="Configurações"/><br/>
			    <strong>Config</strong>
			  </a>
			</li>
 			</ul>
		<!-- End shortcut menu -->
	</div>
	<div id="openRodapeFixo">
		<div id="divButton">Abrir</div>
	</div>
</c:if>
<!-- ui-dialog -->
<!-- Include das menssagens de alerta -->
<jsp:include page="../menssages/dialogInformation.jsp"></jsp:include>
<script type="text/javascript">
$(document).ready(function(){
	$('#show_menu').fadeIn();
	$('#wysiwyg').css('width', '97%');
	//setNotifications();
	startCountdown();
	$('#divButtonClose').click(function(){
		$('#rodapeFixo').slideUp();
		$('#openRodapeFixo').slideDown();
	});
	$('#openRodapeFixo').click(function(){
		$('#openRodapeFixo').slideUp();
		$('#rodapeFixo').slideDown();
	});
	$('#actionMenuLateral').click(function(){
		$('#menuLateral').hide('slide', {direction: 'left'}, 600);
	});
	var dados = "";
	$("#paciente").autocomplete(dados.split(';'), {
		max: 10,
		scroll: true,
		scrollHeight: 300
		
	});
});
function ajaxGetName(nome){
 	if(nome!='')
     	sendRequest('/ClinicalSys/clinic/pag/Paciente','ACAO=getPacienteAutoComplete&nomeParc='+nome,'POST', ajaxCallbackNome, erroCallbackNome);
 }
 
 function ajaxCallbackNome(data){
	$("#paciente").setOptions({data: data.split(';')});
 }
 
 function erroCallbackNome(request){
	openDialogInformation("Ups. ocorreu um erro",true);
 }
</script>
</body>
</html>
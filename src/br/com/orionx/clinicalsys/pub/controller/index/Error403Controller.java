package br.com.orionx.clinicalsys.pub.controller.index;

import org.nextframework.controller.Action;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

@Controller(path="/pub/403")
public class Error403Controller extends MultiActionController {

	
    @DefaultAction
    public ModelAndView doPage(WebRequestContext request){
        return new ModelAndView("erro403");
    }

    @Action("criar")
    public ModelAndView getIndex(WebRequestContext request){
    	return doPage(request);
    }
}
package br.com.orionx.clinicalsys.dao;

import br.com.orionx.clinicalsys.bean.DoencasDaInfanciaPaciente;
import br.com.orionx.clinicalsys.bean.HistoriaPregressa;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class DoencasDaInfanciaPacienteDAO extends GenericDAO<DoencasDaInfanciaPaciente>{

	public void deleteByHistoriaPregressa(HistoriaPregressa hp){
		getHibernateTemplate().bulkUpdate("delete from DoencasDaInfanciaPaciente where historiaPregressa = ?", hp);
	}
	
}

package br.com.orionx.clinicalsys.dao;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.QueryBuilder;

import br.com.orionx.clinicalsys.bean.RecomendacaoMedicamentosa;
import br.com.orionx.clinicalsys.filtros.RecomendacaoMedicamentosaFiltro;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class RecomendacaoMedicamentosaDAO extends GenericDAO<RecomendacaoMedicamentosa>{

	
	@Override
	public void updateListagemQuery(QueryBuilder<RecomendacaoMedicamentosa> query,FiltroListagem _filtro) {
		RecomendacaoMedicamentosaFiltro filtro = (RecomendacaoMedicamentosaFiltro) _filtro;		
		query.whereLikeIgnoreAll("recomendacaoMedicamentosa.nome",filtro.getNome());
		super.updateListagemQuery(query, filtro);
	}
}

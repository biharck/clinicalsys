package br.com.orionx.clinicalsys.clinica.controller.crud;

import java.util.List;

import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

import br.com.orionx.clinicalsys.bean.CIDOCategoria;
import br.com.orionx.clinicalsys.bean.CIDOGrupo;
import br.com.orionx.clinicalsys.service.CIDOCategoriaService;
import br.com.orionx.clinicalsys.service.CIDOGrupoService;
import br.com.orionx.clinicalsys.util.TreeView;

@Controller(path="/clinic/process/CIDO",authorizationModule=ProcessAuthorizationModule.class)
public class CIDOProcess extends MultiActionController{

	private CIDOGrupoService cidoGrupoService;
	private CIDOCategoriaService cidoCategoriaService;
	
	public void setCidoGrupoService(CIDOGrupoService cidoGrupoService) {
		this.cidoGrupoService = cidoGrupoService;
	}
	public void setCidoCategoriaService(CIDOCategoriaService cidoCategoriaService) {
		this.cidoCategoriaService = cidoCategoriaService;
	}
	
	@DefaultAction
	public ModelAndView index(WebRequestContext request)throws CrudException {
		request.setAttribute("pathURL", getPathURL());
		List<CIDOGrupo> grupos = cidoGrupoService.findAll();
		
		TreeView treeView = new TreeView();
		treeView.addHeader("CID-O");
		int indice = 1;
		int mae = 1;
		
		for (CIDOGrupo grupo : grupos) {
			grupo.setCategorias(cidoCategoriaService.getCategoriasByGrupos(grupo.getCatInic(), grupo.getCatFim()));
		}
		
		
		for (CIDOGrupo grupo : grupos) {
			treeView.addNode(indice, 0, grupo.getDescricao(), "javascript:getText("+grupo.getIdCIDOGrupo()+",1);");
			mae = indice;
			for (CIDOCategoria categoria : grupo.getCategorias()) {
				indice ++;
				treeView.addNode(indice, mae, categoria.getDescricao(),  "javascript:getText("+categoria.getIdCIDOCategoria()+",2);");
			}
			indice++;			
		}
		
		request.setAttribute("tree", treeView.getTreeView());
		
		return new ModelAndView("process/cido");
	}

	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">home</a> &raquo; Cl�nica &raquo; <a href=\"/ClinicalSys/clinic/process/CIDO\">CID-O</a></p>";
	}
	
}

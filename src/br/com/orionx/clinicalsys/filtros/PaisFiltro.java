package br.com.orionx.clinicalsys.filtros;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;

import br.com.orionx.clinicalsys.util.generics.FiltroListagem;

public class PaisFiltro extends FiltroListagem {
	
	private String nome;
	
	@MaxLength(100)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
}

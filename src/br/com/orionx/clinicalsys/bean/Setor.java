package br.com.orionx.clinicalsys.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

import br.com.orionx.clinicalsys.util.log.Log;

@Entity
@Table(name = "setor")
public class Setor implements Log{
	
	private Integer idSetor;
	private String nome;

	//Log
	private Integer idUsuarioAltera;
	private Timestamp dtAltera;

	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdSetor() {
		return idSetor;
	}
	@Required
	@MinLength(1)
	@MaxLength(30)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	//SET
	public void setIdSetor(Integer idSetor) {
		this.idSetor = idSetor;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	//LOG
	@Override
	public void setIdUsuarioAltera(Integer idUsuarioAltera) {
		this.idUsuarioAltera = idUsuarioAltera;
	}

	@Override
	public void setDtAltera(Timestamp dtAltera) {
		this.dtAltera = dtAltera;
	}

	@Override
	public Integer getIdUsuarioAltera() {
		return idUsuarioAltera;
	}

	@Override
	public Timestamp getDtAltera() {
		return dtAltera;
	}
}

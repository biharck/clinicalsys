package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name = "telefonetipo")
public class TelefoneTipo {
	
	private Integer idTelefoneTipo;
	private String tipo;

	public static final TelefoneTipo FIXO    = new TelefoneTipo(1);
	public static final TelefoneTipo CELULAR = new TelefoneTipo(2);
	public static final TelefoneTipo FAX     = new TelefoneTipo(3);
	public static final TelefoneTipo RECADO  = new TelefoneTipo(4);
	
	public TelefoneTipo(){}
	
	public TelefoneTipo(Integer idTelefoneTipo){
		this.idTelefoneTipo = idTelefoneTipo;
	}
	
		
	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdTelefoneTipo() {
		return idTelefoneTipo;
	}
	@Required
	@MaxLength(20)
	@DescriptionProperty
	public String getTipo() {
		return tipo;
	}
	
	//SET
	public void setIdTelefoneTipo(Integer idTelefoneTipo) {
		this.idTelefoneTipo = idTelefoneTipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
}

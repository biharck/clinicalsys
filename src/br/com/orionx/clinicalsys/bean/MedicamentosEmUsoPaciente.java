package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "medicamentosemusopaciente")
public class MedicamentosEmUsoPaciente {

	private Integer idMedicamentosEmUsoPaciente;
	private ClassificacaoMedicacao classificacaoMedicacao;
	private Medicamento medicamento;
	private String posologia;
	private HistoriaPregressa historiaPregressa;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getIdMedicamentosEmUsoPaciente() {
		return idMedicamentosEmUsoPaciente;
	}
	@Transient
	public ClassificacaoMedicacao getClassificacaoMedicacao() {
		return classificacaoMedicacao;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idMedicamento")
	public Medicamento getMedicamento() {
		return medicamento;
	}

	public String getPosologia() {
		return posologia;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idHistoriaPregressa")
	@ForeignKey(name="fk_histo_medica_uso")
	public HistoriaPregressa getHistoriaPregressa() {
		return historiaPregressa;
	}
	
	public void setIdMedicamentosEmUsoPaciente(Integer idMedicamentosEmUsoPaciente) {
		this.idMedicamentosEmUsoPaciente = idMedicamentosEmUsoPaciente;
	}
	public void setClassificacaoMedicacao(
			ClassificacaoMedicacao classificacaoMedicacao) {
		this.classificacaoMedicacao = classificacaoMedicacao;
	}
	public void setMedicamento(Medicamento medicamento) {
		this.medicamento = medicamento;
	}
	public void setPosologia(String posologia) {
		this.posologia = posologia;
	}
	public void setHistoriaPregressa(HistoriaPregressa historiaPregressa) {
		this.historiaPregressa = historiaPregressa;
	}
	
	
	
	
	
}

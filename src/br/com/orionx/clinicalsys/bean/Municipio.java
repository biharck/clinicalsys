package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name = "municipio")
public class Municipio {
	
	private Integer idMunicipio;
	private UnidadeFederativa unidadeFederativa;
	private String nome;
	private Pais pais;
	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdMunicipio() {
		return idMunicipio;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "idUf")
	@ForeignKey(name = "fk_municipio_uf")
	@Required
	public UnidadeFederativa getUnidadeFederativa() {
		return unidadeFederativa;
	}
	@Required
	@MinLength(2)
	@MaxLength(100)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}

	@Transient
	@Required
	public Pais getPais() {
		return pais;
	}
	
	//SET
	public void setIdMunicipio(Integer idMunicipio) {
		this.idMunicipio = idMunicipio;
	}
	public void setUnidadeFederativa(UnidadeFederativa unidadeFederativa) {
		this.unidadeFederativa = unidadeFederativa;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setPais(Pais pais) {
		this.pais = pais;
	}

}

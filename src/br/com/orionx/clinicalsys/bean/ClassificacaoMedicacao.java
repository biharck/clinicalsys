package br.com.orionx.clinicalsys.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

import br.com.orionx.clinicalsys.util.log.Log;

@Entity
@Table(name = "classificacaomedicacao")
public class ClassificacaoMedicacao implements Log{
	
	private Integer idClassificacaoMedicacao;
	private String nome;
	private List<Medicamento> medicamentos;
	
	
	public ClassificacaoMedicacao(){}
	
	public ClassificacaoMedicacao(Integer idClassificacaoMedicacao){
		this.idClassificacaoMedicacao = idClassificacaoMedicacao;	
	}
	
	//Log
	private Integer idUsuarioAltera;
	private Timestamp dtAltera;

	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdClassificacaoMedicacao() {
		return idClassificacaoMedicacao;
	}
	@Required
	@MinLength(1)
	@MaxLength(100)
	@Column(length=100)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	@OneToMany(mappedBy="classificacaoMedicacao")
	public List<Medicamento> getMedicamentos() {
		return medicamentos;
	}
	
	//SET
	public void setIdClassificacaoMedicacao(Integer idClassificacaoMedicacao) {
		this.idClassificacaoMedicacao = idClassificacaoMedicacao;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setMedicamentos(List<Medicamento> medicamentos) {
		this.medicamentos = medicamentos;
	}
	
	//LOG
	@Override
	public void setIdUsuarioAltera(Integer idUsuarioAltera) {
		this.idUsuarioAltera = idUsuarioAltera;
	}

	@Override
	public void setDtAltera(Timestamp dtAltera) {
		this.dtAltera = dtAltera;
	}

	@Override
	public Integer getIdUsuarioAltera() {
		return idUsuarioAltera;
	}

	@Override
	public Timestamp getDtAltera() {
		return dtAltera;
	}
}

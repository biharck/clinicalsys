package br.com.orionx.clinicalsys.dao;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.QueryBuilder;

import br.com.orionx.clinicalsys.bean.ViaAdministracao;
import br.com.orionx.clinicalsys.filtros.ViaAdministracaoFiltro;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class ViaAdministracaoDAO extends GenericDAO<ViaAdministracao>{

	
	@Override
	public void updateListagemQuery(QueryBuilder<ViaAdministracao> query,FiltroListagem _filtro) {
		ViaAdministracaoFiltro filtro = (ViaAdministracaoFiltro) _filtro;		
		query
			.whereLikeIgnoreAll("viaAdministracao.nome",filtro.getNome())
			.whereLikeIgnoreAll("viaAdministracao.sigla",filtro.getSigla());
		super.updateListagemQuery(query, filtro);
	}
}

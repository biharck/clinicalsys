<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro columns="6">
			<t:property name="codigo" renderAs="doubleline" id="codigo"/>		
			<t:property name="descricao" renderAs="doubleline"/>		
			<t:property name="CFPO" renderAs="doubleline"/>		
			<t:property name="ativoEnum" renderAs="doubleline"/>
			<t:property name="produtoOuServico" renderAs="doubleline" type="select-one" trueFalseNullLabels="Produto,Servi�o,"/>
			<t:property name="categoriaProdutos" renderAs="doubleline"/>	
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados>
			<t:property name="codigo" />
			<t:property name="CFPO" />
			<t:property name="genero" />
			<t:property name="ativo"/>
			<t:property name="produtoOuServico" trueFalseNullLabels="Produto,Servi�o,"/>
			<t:property name="categoriaProdutos"/>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
<script type="text/javascript">
$(document).ready(function(){
	$('#codigo').focus();
});
</script>

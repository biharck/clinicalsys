package br.com.orionx.clinicalsys.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.orionx.clinicalsys.bean.Alimentacao;
import br.com.orionx.clinicalsys.bean.HistoriaPregressa;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class AlimentacaoDAO extends GenericDAO<Alimentacao>{

	public List<Alimentacao> findByHistoriaPregressa(HistoriaPregressa hp){
		if(hp.getIdHistoriaPregressa() == null){
			return new ArrayList<Alimentacao>();
		}
		return query()
				.select("alimentacao")
				.from(Alimentacao.class)
				.leftOuterJoin("alimentacao.alimentacoesPaciente ap")
				.where("ap.historiaPregressa = ?", hp)
				.list();
	}
	
}

package br.com.orionx.clinicalsys.bean;

public class DadosBancarios {
	
	protected Integer cddadobancario;
	protected Banco banco;
	protected TipoConta tipoConta;
	protected String operacao;
	protected String agencia;
	protected String digitoVerificadoragencia;
	protected String conta;
	protected String digitoVerificadorconta;
	protected Colaborador colaborador;

}

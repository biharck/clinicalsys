package br.com.orionx.clinicalsys.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * http://pt.wikipedia.org/wiki/Anexo:Lista_de_c�digos_da_CID-10
 * sub n�veis em formato de �rvore
 * 
 * @author biharck
 * representam as categorias do cid10
 *
 */
@Entity
@Table(name = "cid10categoria")
public class CID10Categoria{
	
	private Integer idCid10Categoria;
	private String categoria;
	private String classificacao;
	private String descricao;
	private String descrabrev;
	private String refer;
	private String excluidos;
	
	
	public CID10Categoria() {}
	public CID10Categoria(Integer idCid10Categoria, String categoria, String classificacao, String descricao,
			String descrabrev, String refer, String excluidos) {
		this.idCid10Categoria 	= idCid10Categoria;
		this.categoria 			= categoria;
		this.classificacao 		= classificacao;
		this.descricao 			= descricao;
		this.descrabrev 		= descrabrev;
		this.refer 				= refer;
		this.excluidos 			= excluidos;
	}
	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdCid10Categoria() {
		return idCid10Categoria;
	}

	@Column(name="cat")
	public String getCategoria() {
		return categoria;
	}
	
	@Column(name="classif")
	public String getClassificacao() {
		return classificacao;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getDescrabrev() {
		return descrabrev;
	}

	public String getRefer() {
		return refer;
	}

	public String getExcluidos() {
		return excluidos;
	}

	//SET
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public void setIdCid10Categoria(Integer idCid10Categoria) {
		this.idCid10Categoria = idCid10Categoria;
	}

	public void setClassificacao(String classificacao) {
		this.classificacao = classificacao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setDescrabrev(String descrabrev) {
		this.descrabrev = descrabrev;
	}

	public void setRefer(String refer) {
		this.refer = refer;
	}

	public void setExcluidos(String excluidos) {
		this.excluidos = excluidos;
	}

}

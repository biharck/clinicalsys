package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name = "estadocivil")
public class EstadoCivil {
	
	private Integer idEstadoCivil;
	private String nome;
	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdEstadoCivil() {
		return idEstadoCivil;
	}
	@Required
	@MinLength(2)
	@MaxLength(15)
	@DescriptionProperty
	@DisplayName("Nome")
	public String getNome() {
		return nome;
	}

	//SET
	public void setIdEstadoCivil(Integer idEstadoCivil) {
		this.idEstadoCivil = idEstadoCivil;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

}

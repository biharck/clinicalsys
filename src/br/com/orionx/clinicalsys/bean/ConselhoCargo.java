package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "conselhocargo")
public class ConselhoCargo {
	
	private Integer idConselhoCargo;
	private Conselho conselho;
	private Cargo cargo;
	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdConselhoCargo() {
		return idConselhoCargo;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="idConselho")
	@ForeignKey(name = "fk_conscargo_conselho")
	public Conselho getConselho() {
		return conselho;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="idCargo")
	@ForeignKey(name = "fk_conscargo_cargo")
	public Cargo getCargo() {
		return cargo;
	}
	
	//SET
	public void setIdConselhoCargo(Integer idConselhoCargo) {
		this.idConselhoCargo = idConselhoCargo;
	}
	public void setConselho(Conselho conselho) {
		this.conselho = conselho;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

}

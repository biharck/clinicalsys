<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="cs" uri="clinicalsys"%>

<div class="onecolumn">
	<div class="header">
		<span>Agenda dos Colaboradores</span>
	</div>
	<br class="clear"/>
	<div class="content">
	<div id="ErrosSubmissao"></div>
		<t:tela>
			<n:panel>
				<n:panelGrid columns="2">
					<n:bean name="colaboradorAgenda" valueType="${br.com.orionx.clinicalsys.bean.ColaboradorAgenda}">
						<n:panel>
							<n:panelGrid columns="4" columnStyles="width:300px,width:200px">
								<n:group legend="Passo 1">
									<n:panelGrid columns="4">
										<t:property name="idColaboradorAgenda"/>
										<t:property name="tipoAgenda" label="tipo de agenda" renderAs="doubleline" colspan="4"/>
										<t:property name="colaborador" renderAs="doubleline" selectOnePath="/clinic/pag/Colaborador?onePath=true" selectOneWindowSize="980,630" style="width:170px;" colspan="4"/>
										
										<n:panel colspan="1"><br/> Primeiro Per�odo </n:panel>
										<t:property name="inicioPrimeiroPeriodo" id="inicioPrimeiroPeriodo" type="text" style="width:50px;" colspan="1" renderAs="doubleline" label=" " size="5" maxlength="5" onkeypress="return maskHora(this, event)" onchange="isCampoCompleto(this)"/>
										<n:panel colspan="1"><br/><center> �s</center> </n:panel>
										<t:property name="terminoPrimeiroPeriodo" id="terminoPrimeiroPeriodo" type="text" style="width:50px;" colspan="1" renderAs="doubleline" label=" " size="5" maxlength="5" onkeypress="return maskHora(this, event)" onchange="isCampoCompleto(this)"/>
										
										<n:panel colspan="1"><br/> Segundo Per�odo </n:panel>
										<t:property name="inicioSegundoPeriodo" id="inicioSegundoPeriodo" type="text" style="width:50px;" colspan="1" renderAs="doubleline" label=" " size="5" maxlength="5" onkeypress="return maskHora(this, event)" onchange="isCampoCompleto(this)"/>
										<n:panel colspan="1"><br/> <center> �s</center> </n:panel>
										<t:property name="terminoSegundoPeriodo" id="terminoSegundoPeriodo" type="text" style="width:50px;" colspan="1" renderAs="doubleline" label=" " size="5" maxlength="5" onkeypress="return maskHora(this, event)" onchange="isCampoCompleto(this)"/>
										
										<n:panel colspan="1"><br/> Terceiro Per�odo </n:panel>
										<t:property name="inicioTerceiroPeriodo" id="inicioTerceiroPeriodo" type="text" style="width:50px;" colspan="1" renderAs="doubleline" label=" " size="5" maxlength="5" onkeypress="return maskHora(this, event)" onchange="isCampoCompleto(this)"/>
										<n:panel colspan="1"><br/> <center> �s</center> </n:panel>
										<t:property name="terminoTerceiroPeriodo" id="terminoTerceiroPeriodo" type="text" style="width:50px;" colspan="1" renderAs="doubleline" label=" " size="5" maxlength="5" onkeypress="return maskHora(this, event)" onchange="isCampoCompleto(this)"/>
										
										<n:panel colspan="1"><br/> Quarto Per�odo </n:panel>
										<t:property name="inicioQuartoPeriodo" id="inicioQuartoPeriodo" type="text" style="width:50px;" colspan="1" renderAs="doubleline" label=" " size="5" maxlength="5" onkeypress="return maskHora(this, event)" onchange="isCampoCompleto(this)"/>
										<n:panel colspan="1"><br/> <center> �s</center> </n:panel>
										<t:property name="terminoQuartoPeriodo" id="terminoQuartoPeriodo" type="text" style="width:50px;" colspan="1" renderAs="doubleline" label=" " size="5" maxlength="5" onkeypress="return maskHora(this, event)" onchange="isCampoCompleto(this)"/>
										<n:panel>
											<c:if test="${empty iteracoes}">
												<center><input type="submit" onclick="validaCampos()" value="Gerar Tabela"></center>
											</c:if>
										</n:panel>
									</n:panelGrid>
								</n:group>
								<n:panelGrid columns="2" colspan="3" styleClass="horarioAgenda">
									<c:if test="${!empty iteracoes}">
										<n:group legend="Passo 2">
											<table id="tabelaHorarios">
												<tr>
													<td>
														
													</td>
													<td>
														<center>Domingo</center>											
													</td>
													<td>
														<center>Segunda</center>
													</td>
													<td>
														<center>Ter�a</center>
													</td>
													<td>
														<center>Quarta</center>
													</td>
													<td>
														<center>Quinta</center>
													</td>
													<td>
														<center>Sexta</center>
													</td>
													<td>
														<center>S�bado</center>
													</td>
												</tr>
												<tr>
													<td>
														Todos
													</td>
													<td>
														<center><input type='button' value='Marcar' onclick="checkAllChecks('0_',this)"/></center>						
													</td>
													<td>
														<center><input type='button' value='Marcar' onclick="checkAllChecks('1_',this)"/></center>
													</td>
													<td>
														<center><input type='button' value='Marcar' onclick="checkAllChecks('2_',this)"/></center>
													</td>
													<td>
														<center><input type='button' value='Marcar' onclick="checkAllChecks('3_',this)"/></center>
													</td>
													<td>
														<center><input type='button' value='Marcar' onclick="checkAllChecks('4_',this)"/></center>
													</td>
													<td>
														<center><input type='button' value='Marcar' onclick="checkAllChecks('5_',this)"/></center>
													</td>
													<td>
														<center><input type='button' value='Marcar' onclick="checkAllChecks('6_',this)"/></center>
													</td>
												</tr>
												
											</table>
											<br/>
											<center>
												<input type="submit" onclick="openDialogAguarde();form.ACAO.value ='generateAgenda';form.action = '/ClinicalSys/system/process/ColaboradorAgenda?saveConfigAgenda=true'; submitForm()" value="Gerar Agenda">
											</center>
										</n:group>
									</c:if>
								</n:panelGrid>
							</n:panelGrid>
						</n:panel>
					</n:bean>
				</n:panelGrid>
			</n:panel>
		</t:tela>
	</div>
</div>
<script type="text/javascript">
	
	var difPrimeiroPeriodo = '';
	var difSegundoPeriodo  = '';
	var difTerceiroPeriodo = '';
	var difQuartoPeriodo   = '';
	var tempoConsulta = '${tempoConsulta}';
	
	$(document).ready(function(){
		/**
		* Diferenca usada no for de linhas
		*/
		difPrimeiroPeriodo = '${difPrimeiroPeriodo}';
		difSegundoPeriodo  = '${difSegundoPeriodo}';
		difTerceiroPeriodo = '${difTerceiroPeriodo}';
		difQuartoPeriodo   = '${difQuartoPeriodo}';
		tempoConsulta      = '${tempoConsulta}';
		
		if(difPrimeiroPeriodo!='')
			generateAgenda();
	});
	
	function generateAgenda(){
		
		arrayIntervalos = new Array(difPrimeiroPeriodo,difSegundoPeriodo,difTerceiroPeriodo,difQuartoPeriodo);
		arrayHoras = new Array($('#inicioPrimeiroPeriodo').val(),$('#inicioSegundoPeriodo').val(),$('#inicioTerceiroPeriodo').val(),$('#inicioQuartoPeriodo').val());
		
		var html = "";
		var somadorHoras = 0;
		var iterator = 0;
		console.log(tempoConsulta);
		for ( var k = 0; k < arrayIntervalos.length; k++) {
			var horaInicio = ""+ arrayHoras[k].split(':')[0];
			var minutoInicio = ""+ arrayHoras[k].split(':')[1];
			var firstTime = true;
			for ( var i = 0; i < arrayIntervalos[k]; i++) {
				html += '<tr>';
					html += '<td>';
						if(firstTime){
							html += "<input type='hidden' name='hora_"+ iterator +"' value='"+ horaInicio+':'+minutoInicio +"'>";
							html += horaInicio+':'+minutoInicio; 
							firstTime = false;
						}else{
							html += somarHorasFormated(horaInicio,minutoInicio,tempoConsulta); 
							somadorHoras = "" + somarHorasFormated(horaInicio,minutoInicio,tempoConsulta);
							horaInicio   = "" + somadorHoras.split(':')[0];
							minutoInicio = "" + somadorHoras.split(':')[1];
							html += "<input type='hidden' name='hora_"+ iterator +"' value='"+ horaInicio+':'+minutoInicio +"'>";
						}
					html += '</td>';
					
				
				for ( var j = 0; j < 7; j++) {
					html += '<td>';
						html += "<center><input name='"+j+"_"+ iterator +"' type='checkbox'></center>";
					html += '</td>';
				}
				iterator ++;
				html += '</tr>';
			}
		}
		$('#tabelaHorarios').append(html);
	}
	
	function checkAllChecks(param,obj){
		if($(obj).val() == 'Marcar'){
			$(obj).val('Desmarcar');
			$("#tabelaHorarios input[type=checkbox][name*="+param+"]").each(function(){ 
				$(this).attr("checked",true); 
			});
		}else{
			$(obj).val('Marcar');
			$("#tabelaHorarios input[type=checkbox][name*="+param+"]").each(function(){ 
				$(this).attr("checked",false); 
			});
		}
	}
	
	function validaCampos(){
		
		if(!primeiroEhMaior($('#inicioPrimeiroPeriodo').val(), $('#terminoPrimeiroPeriodo').val())){
			return false;	
		}
		
		if($('#inicioSegundoPeriodo').val() != '' && $('#terminoSegundoPeriodo').val() != ''){
			if(!primeiroEhMaior($('#inicioSegundoPeriodo').val(), $('#terminoSegundoPeriodo').val())){
				return false;
			}
		}
		
		if($('#inicioTerceiroPeriodo').val() != '' && $('#terminoTerceiroPeriodo').val() != ''){
			if(!primeiroEhMaior($('#inicioTerceiroPeriodo').val(), $('#terminoTerceiroPeriodo').val())){
				return false;
			}
		}
		
		if($('#inicioQuartoPeriodo').val() != '' && $('#terminoQuartoPeriodo').val() != ''){
			if(!primeiroEhMaior($('#inicioQuartoPeriodo').val(), $('#terminoQuartoPeriodo').val())){
				return false;
			}
		}
		
		if(!verificaOrdem()){
			return false;
		}
		
		
		openDialogAguarde();
		form.ACAO.value ='generateAgenda';
		form.action = '/ClinicalSys/system/process/ColaboradorAgenda'; 
		submitForm();
	}
	
	function primeiroEhMaior(valor1, valor2){
	
		
		var horaPrimeiro = valor1.split(":")[0];
		var minutoPrimeiro = valor1.split(":")[1];

		var horaSegundo = valor2.split(":")[0];
		var minutoSegundo = valor2.split(":")[1];
		console.log(horaPrimeiro);
		console.log(horaSegundo);
		console.log(minutoPrimeiro);
		console.log(minutoSegundo);
		
		if(parseInt(horaPrimeiro,10) > parseInt(horaSegundo,10)){
			openDialogInformation('A hora de in�cio deve ser menor que a hora de t�rmino.');
			return false;
		}
		else if(parseInt(horaPrimeiro,10) == parseInt(horaSegundo,10)){
			if((parseInt(minutoPrimeiro,10) > parseInt(minutoSegundo,10)) || (parseInt(minutoPrimeiro,10) == parseInt(minutoSegundo,10)) ){
				openDialogInformation('A hora de in�cio deve ser menor que a hora de t�rmino.<br> Verifique os minutos.');
				return false;
			}
		}
		return true;
		
	}
	
	function verificaOrdem(){
		
		var terminoPrimeiroPeriodo = $('#terminoPrimeiroPeriodo').val();
		var inicioSegundoPeriodo = $('#inicioSegundoPeriodo').val();
		var terminoSegundoPeriodo = $('#terminoSegundoPeriodo').val();
		var inicioTerceiroPeriodo = $('#inicioTerceiroPeriodo').val();
		var terminoTerceiroPeriodo = $('#terminoTerceiroPeriodo').val();
		var inicioQuartoPeriodo = $('#inicioQuartoPeriodo').val();
		
		if(!primeiroEhMaior(terminoPrimeiroPeriodo,inicioSegundoPeriodo)){
			return false;
		}
		if(!primeiroEhMaior(terminoSegundoPeriodo,inicioTerceiroPeriodo)){
			return false;
		}
		if(!primeiroEhMaior(terminoTerceiroPeriodo,inicioQuartoPeriodo)){
			return false;
		}
		return true;
	}
	
	function isCampoCompleto(campo){
		if(campo.value.length != 5){
			campo.value = '';
			return true;
		}else return false;
		
			
	}
	
</script>
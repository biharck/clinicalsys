<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:listagem>
	<t:janelaFiltro>
		<t:tabelaFiltro>
			<t:property name="nome" renderAs="doubleline" style="width:300px"/>
			<t:property name="suaIdentificacao" renderAs="doubleline" style="width:150px"/>
			<t:property name="versaoXMLTISS" renderAs="doubleline"/>
			<t:property name="nomePlano" renderAs="doubleline" style="width:150px"/>
			<t:property name="ativoEnum" renderAs="doubleline"/>
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados>
			<t:property name="nomeFantasia"/>
			<t:property name="cnpj"/>
			<t:property name="registroANS"/>
			<t:property name="versaoXMLTISS"/>
			<t:property name="telefoneAutorizacao"/>
			<t:property name="email"/>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>

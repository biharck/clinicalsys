<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<div class="onecolumn">
	<div class="header">
		<span>Permiss�es do Sistema</span>
	</div>
	<br class="clear"/>
	<div class="content">
		<t:tela titulo="">
		    <t:janelaFiltro>
		        <t:tabelaFiltro showSubmit="false">
		            <t:property name="role" itens="${roles}" reloadOnChange="true" label="Papeis no sistema" onchange="openDialogAguarde()" renderAs="doubleline"/>
		        </t:tabelaFiltro>
		    </t:janelaFiltro>
		    <c:if test="${!empty filtro.groupAuthorizationMap}">
			    <div align="right">
			    	<n:submit action="salvar" validate="true" class="noboard" confirmationScript="${janelaEntradaTag.submitConfirmationScript}"><img src="${application}/images/shortcut/save.png" alt="Salvar" class="help" title="Salvar"/></n:submit>
			    </div>
		    </c:if>
		    <c:forEach items="${filtro.groupAuthorizationMap}" var="item">
		        <t:janelaResultados>
		        	<br><br>
		            <n:panelGrid columns="1" rowStyleClasses="filtro1" width="100%">
		                <div class="title-authorization" > <i>${item.key}</i></div>
		            </n:panelGrid>
		            <n:dataGrid headerStyleClass="" footerStyleClass="" styleClass="data" bodyStyleClasses=""  itens="${item.value}" width="100%" cellspacing="1" 
		            rowonclick="javascript:$csu.coloreLinha('tabelaPermissao',this)" 
					rowonmouseover="javascript:$csu.mouseonOverTabela('tabelaPermissao',this)" 
					rowonmouseout="javascript:$csu.mouseonOutTabela('tabelaPermissao',this)" 
					id="tabelaPermissao" varIndex="index">
		                <n:bean name="row" propertyPrefix="groupAuthorizationMap[${item.key}][${index}]" valueType="${authorizationProcessItemFilterClass}">
		
		                <n:column header="Descri��o">
		                    <t:property name="descricaoTela" mode="output"/>                
		                </n:column>
		                <n:column header="Tela">
		                    <t:property name="description" mode="output"/>                
		                    <t:property name="path" mode="input" type="hidden" write="false"/>                      
		                </n:column>
		                
		                <c:forEach items="${mapaGroupModule[item.key].authorizationItens}" var="authorizationItem">
		                    <n:column header="${authorizationItem.nome}" width="80px">
		                        <c:if test="${fn:length(authorizationItem.valores) == 2}">
		                            <%-- Possibilidade de ser true false --%>
		                            <n:property name="permissionMap[${authorizationItem.id}]">
		                                <n:input type="checkbox"/>                            
		                            </n:property>
		                        </c:if>
		                        <c:if test="${fn:length(authorizationItem.valores) != 2}">
		                            (N�o implementado ainda)
		                            <n:input itens="${authorizationItem.valores}"/>
		                        </c:if>                        
		                    </n:column>
		                </c:forEach>                    
		                </n:bean>
		            </n:dataGrid>
		            
		        </t:janelaResultados>
		    </c:forEach>
		    <c:if test="${!empty filtro.role}">
		        <t:janelaResultados>
		        <div align="right">
		        	<n:submit action="salvar" validate="true" class="noboard" confirmationScript="${janelaEntradaTag.submitConfirmationScript}"><img src="${application}/images/shortcut/save.png" alt="Salvar" class="help" title="Salvar"/></n:submit>
		        </div>
		        </t:janelaResultados>
		    </c:if>
		</t:tela>
	</div>
</div>


package br.com.orionx.clinicalsys.util.generics;

import java.sql.Timestamp;

import br.com.orionx.clinicalsys.util.ClinicalSysUtil;
import br.com.orionx.clinicalsys.util.log.Inclusao;
import br.com.orionx.clinicalsys.util.log.Log;

/**
 * Classe que deve ser extendida pelas classes que necessitam fazer acesso ao banco de dados
 * 
 * @author biharck
 *
 * @param <BEAN>
 */
public class GenericDAO<BEAN> extends org.nextframework.persistence.GenericDAO<BEAN>{

	@Override
	public void saveOrUpdate(BEAN bean) {
		if (bean instanceof Log){
			((Log) bean).setDtAltera(new Timestamp(System.currentTimeMillis()));
			((Log) bean).setIdUsuarioAltera(ClinicalSysUtil.getUsuarioLogado().getIdUsuario());
		}
		if (bean instanceof Inclusao){
			((Inclusao) bean).setDtInclusao((new Timestamp(System.currentTimeMillis())));
			((Inclusao) bean).setIdUsuarioInclusao(ClinicalSysUtil.getUsuarioLogado().getIdUsuario());
		}
		super.saveOrUpdate(bean);
	}	
	
}
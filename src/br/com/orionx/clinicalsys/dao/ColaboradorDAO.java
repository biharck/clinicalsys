package br.com.orionx.clinicalsys.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.QueryBuilder;
import org.nextframework.persistence.SaveOrUpdateStrategy;

import br.com.orionx.clinicalsys.bean.Colaborador;
import br.com.orionx.clinicalsys.bean.Usuario;
import br.com.orionx.clinicalsys.filtros.ColaboradorFiltro;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class ColaboradorDAO extends GenericDAO<Colaborador> {

	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("telefones");
		save.saveOrUpdateManaged("cargos");
		save.saveOrUpdateManaged("listaEspecializacaoColaboradors");
	}
	
	/**
	 * <p> M�todo que retorna uma colaborador espec�fico
	 * @param id {@link Integer}
	 * @return {@link Colaborador}
	 */
	public Colaborador getColaboradorEspecifico(Integer id){
		return query()
		.select("colaborador.idColaborador,pais.idPais, unidadeFederativa.idUF,municipio.idMunicipio,"+
		"foto.cdfile, foto.contenttype, foto.name, foto.size, foto.content," +
		"usuario.idUsuario,usuario.login,usuario.password," +
		"cargos.idColaboradorCargo, cargos.dataAdmissao, cargos.dataDesligamento, cargos.regimeTrabalho,"+
		"cargo.idCargo, cargo.nome, cargo.codigoCBO, cargo.ativo, cargo.descricao")
		.leftOuterJoin("colaborador.municipio municipio")
		.leftOuterJoin("colaborador.foto foto")
		.leftOuterJoin("colaborador.usuario usuario")
		.leftOuterJoin("municipio.unidadeFederativa unidadeFederativa")
		.leftOuterJoin("unidadeFederativa.pais pais")
		.leftOuterJoin("colaborador.cargos cargos")
		.leftOuterJoin("cargos.cargo cargo")
		.where("colaborador.idColaborador=?",id).unique();
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Colaborador> query,FiltroListagem _filtro) {
		ColaboradorFiltro filtro = (ColaboradorFiltro) _filtro;
		if(filtro.getAtivoEnum()!=null)
			if(filtro.getAtivoEnum().getSituacao()!=null && (filtro.getAtivoEnum().getSituacao() || !filtro.getAtivoEnum().getSituacao()))
				query.where("colaborador.ativo is "+filtro.getAtivoEnum().getSituacao());
		
		query
			.whereLikeIgnoreAll("colaborador.nome", filtro.getNome())
			.where("colaborador.cpf=?",filtro.getCpf())
			.where("sexo=?",filtro.getSexo())
			.where("usuario.login=?",filtro.getUsuario())
			.where("cargo=?",filtro.getCargo())
			.where("especializacao=?",filtro.getEspecializacao())
			.leftOuterJoin("colaborador.sexo sexo")
			.leftOuterJoin("colaborador.usuario usuario")
			.leftOuterJoin("colaborador.cargos cargos")
			.leftOuterJoin("cargos.cargo cargo")
			.leftOuterJoin("colaborador.listaEspecializacaoColaboradors listaEspecializacaoColaboradors")
			.leftOuterJoin("listaEspecializacaoColaboradors.especializacao especializacao");
		
		super.updateListagemQuery(query, filtro);
	}
	
	/**
	 * <p>M�todo respons�vel em retornar um colaborador a partir de seu usu�rio
	 * @param u {@link Usuario}
	 * @return {@link Colaborador}
	 */
	public Colaborador getColaboradorByUser(String login){
		return query()
				.select("colaborador.idColaborador, colaborador.ativo, colaborador.permissaoAgenda," +
						"foto.cdfile, foto.contenttype, foto.name, foto.size, foto.content")
				.leftOuterJoin("colaborador.usuario u")
				.leftOuterJoin("colaborador.foto foto")
				.where("u.login=?",login).unique();
	}

	@Override
	public List<Colaborador> findAll() {
		return query().where("colaborador.ativo is true").list();
	}
	
	@Override
	public List<Colaborador> findForCombo(String... extraFields) {
		extraFields[0] += "ativo";
		return super.findForCombo(extraFields);
	}
	
	
	public List<Colaborador> getColaboradoresReport(ColaboradorFiltro filtro){
		QueryBuilder<Colaborador> query = query();
		
		if(filtro.getAtivoEnum()!=null)
			if(filtro.getAtivoEnum().getSituacao()!=null && (filtro.getAtivoEnum().getSituacao() || !filtro.getAtivoEnum().getSituacao()))
				query.where("colaborador.ativo is "+filtro.getAtivoEnum().getSituacao());
		
		return query
				.select("colaborador.nome, sexo.nome, colaborador.idColaborador, colaborador.cpf")
				.whereLikeIgnoreAll("colaborador.nome", filtro.getNome())
				.where("colaborador.cpf=?",filtro.getCpf())
				.where("sexo=?",filtro.getSexo())
				.where("usuario.login=?",filtro.getUsuario())
				.where("cargo=?",filtro.getCargo())
				.where("especializacao=?",filtro.getEspecializacao())
				.leftOuterJoin("colaborador.sexo sexo")
				.leftOuterJoin("colaborador.usuario usuario")
				.leftOuterJoin("colaborador.cargos cargos")
				.leftOuterJoin("cargos.cargo cargo")
				.leftOuterJoin("colaborador.listaEspecializacaoColaboradors listaEspecializacaoColaboradors")
				.leftOuterJoin("listaEspecializacaoColaboradors.especializacao especializacao").list();
	}
	

}

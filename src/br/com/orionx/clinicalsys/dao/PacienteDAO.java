package br.com.orionx.clinicalsys.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.QueryBuilder;
import org.nextframework.persistence.SaveOrUpdateStrategy;

import br.com.orionx.clinicalsys.bean.Paciente;
import br.com.orionx.clinicalsys.filtros.PacienteFiltro;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class PacienteDAO extends GenericDAO<Paciente> {

	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("planos");
		save.saveOrUpdateManaged("listaTelefonePaciente");
	}
	
	@Override
	public void updateEntradaQuery(QueryBuilder<Paciente> query) {
		query.leftOuterJoinFetch("paciente.planos planos");
	}
	
	@Override
	public void updateListagemQuery(QueryBuilder<Paciente> query,FiltroListagem _filtro) {
		
		PacienteFiltro filtro = (PacienteFiltro) _filtro;
		if(filtro.getAtivoEnum()!=null)
			if(filtro.getAtivoEnum().getSituacao()!=null && (filtro.getAtivoEnum().getSituacao() || !filtro.getAtivoEnum().getSituacao()))
				query.where("paciente.ativo is "+filtro.getAtivoEnum().getSituacao());
		
		query
			.select("paciente.idPaciente, fotoPaciente.cdfile, fotoPaciente.contenttype, fotoPaciente.name, fotoPaciente.size, fotoPaciente.content," +
					"sexo.nome, paciente.cpf, paciente.email,paciente.nome,paciente.dtnascimento, paciente.ativo")
			.leftOuterJoin("paciente.sexo sexo")
			.leftOuterJoin("paciente.planos planos")
			.leftOuterJoin("planos.plano plano")
			.leftOuterJoin("plano.operadora operadora")
			.leftOuterJoin("paciente.estadoCivil estadoCivil")
			.leftOuterJoin("paciente.escolaridade escolaridade")
			.leftOuterJoin("paciente.municipio municipio")
			.leftOuterJoin("municipio.unidadeFederativa unidadeFederativa")
			.leftOuterJoin("unidadeFederativa.pais pais")
			.leftOuterJoin("paciente.fotoPaciente fotoPaciente")
			.whereLikeIgnoreAll("paciente.nome", filtro.getNome())
			.where("paciente.idPaciente=?",filtro.getRegistro())
			.where("sexo=?",filtro.getSexo())
			.where("paciente.cpf=?",filtro.getCpf())
			.where("plano=?",filtro.getPlano())
			.where("operadora=?",filtro.getOperadora())
		;
		super.updateListagemQuery(query, filtro);
	}
	
	@Override
	public List<Paciente> findAll() {
		return query().where("paciente.ativo is true").list();
	}
	
	@Override
	public List<Paciente> findForCombo(String... extraFields) {
		extraFields[0] += "ativo";
		return super.findForCombo(extraFields);
	}
	
	
	public List<Paciente> getPacienteAutoComplete(String nome){
		return query().select("paciente").where("paciente.nome like '"+nome+"%'").list();
	}
	
	public Paciente getPacienteByCPF(String cpf){
		return query().select("paciente").where("paciente.cpf ='"+cpf+"'").unique();
	}
	
	public Paciente getPacienteProntuario(Paciente paciente){
		return query()
				.select("paciente.idPaciente, paciente.nome,paciente.mae,paciente.dtnascimento, paciente.sexo," +
						"foto.cdfile, foto.contenttype, foto.name, foto.size, foto.content, paciente.profissao," +
						"paciente.naturalidade,tl.descricao,paciente.logradouro, paciente.bairro, paciente.numero, paciente.complemento, paciente.mae," +
						"municipio.nome, unidadeFederativa.sigla,operadora.razaoSocial, plano.nome,estadoCivil.nome, sexo.idSexo")		
				.leftOuterJoin("paciente.fotoPaciente foto")
				.leftOuterJoin("paciente.tipoLogradouro tl")
				.leftOuterJoin("paciente.sexo sexo")
				.leftOuterJoin("paciente.planos planos")
				.leftOuterJoin("planos.plano plano")
				.leftOuterJoin("plano.operadora operadora")
				.leftOuterJoin("paciente.estadoCivil estadoCivil")
				.leftOuterJoin("paciente.escolaridade escolaridade")
				.leftOuterJoin("paciente.municipio municipio")
				.leftOuterJoin("municipio.unidadeFederativa unidadeFederativa")
				.leftOuterJoin("unidadeFederativa.pais pais")
				.where("paciente.idPaciente=?",paciente.getIdPaciente())
				.unique();
	}
	
	public List<Paciente> getPacientesReport(PacienteFiltro filtro){
		QueryBuilder<Paciente> query = query();

		if(filtro.getAtivoEnum()!=null)
			if(filtro.getAtivoEnum().getSituacao()!=null && (filtro.getAtivoEnum().getSituacao() || !filtro.getAtivoEnum().getSituacao()))
				query.where("paciente.ativo is "+filtro.getAtivoEnum().getSituacao());
		
		return query
						.select("paciente.idPaciente, fotoPaciente.cdfile, fotoPaciente.contenttype, fotoPaciente.name, fotoPaciente.size, fotoPaciente.content," +
								"sexo.nome, paciente.cpf, paciente.email,paciente.nome,paciente.dtnascimento, paciente.ativo")
						.leftOuterJoin("paciente.sexo sexo")
						.leftOuterJoin("paciente.planos planos")
						.leftOuterJoin("planos.plano plano")
						.leftOuterJoin("plano.operadora operadora")
						.leftOuterJoin("paciente.estadoCivil estadoCivil")
						.leftOuterJoin("paciente.escolaridade escolaridade")
						.leftOuterJoin("paciente.municipio municipio")
						.leftOuterJoin("municipio.unidadeFederativa unidadeFederativa")
						.leftOuterJoin("unidadeFederativa.pais pais")
						.leftOuterJoin("paciente.fotoPaciente fotoPaciente")
						.whereLikeIgnoreAll("paciente.nome", filtro.getNome())
						.where("paciente.idPaciente=?",filtro.getRegistro())
						.where("sexo=?",filtro.getSexo())
						.where("paciente.cpf=?",filtro.getCpf())
						.where("plano=?",filtro.getPlano())
						.where("operadora=?",filtro.getOperadora())
						.list();
	}

}

package br.com.orionx.clinicalsys.service;

import org.nextframework.core.standard.Next;

import br.com.orionx.clinicalsys.bean.Colaborador;
import br.com.orionx.clinicalsys.bean.Usuario;
import br.com.orionx.clinicalsys.dao.UsuarioDAO;
import br.com.orionx.clinicalsys.util.generics.GenericService;

public class UsuarioService extends GenericService<Usuario>{
	
	
	private UsuarioDAO usuarioDAO;
	
	public void setUsuarioDAO(UsuarioDAO usuarioDAO) {
		this.usuarioDAO = usuarioDAO;
	}
	
	/**
	 * @see br.com.orionx.clinicalsys.dao.UsuarioDAO#getUsuarioById
	 * @param code c�digo do usu�rio
	 * @return usuario
	 */
	public Usuario getUsuarioById(Integer code){
		return usuarioDAO.getUsuarioById(code);
	}
	
	/**
	 * <p> Retorna um usu�rio pelo seu ID do colaborador
	 * @param code id do usu�rio
	 * @return usuario
	 */
	public Usuario getUsuarioByColaborador(Colaborador c){
		return usuarioDAO.getUsuarioByColaborador(c);
	}
	
	/**
	 * singleton
	 */
	private static UsuarioService instance;
	
	public static UsuarioService getInstance(){
		if(instance==null){
			instance= Next.getObject(UsuarioService.class);
		}		
		return instance;
		
	}
	
	public void updateSenha(Usuario user){
		usuarioDAO.updateSenha(user);
	}
	
}

package br.com.orionx.clinicalsys.filtros;

import org.nextframework.validation.annotation.MaxLength;

import br.com.orionx.clinicalsys.bean.UnidadeFederativa;
import br.com.orionx.clinicalsys.util.generics.FiltroListagem;

public class MunicipioFiltro extends FiltroListagem {
	
	protected String nome;
	protected UnidadeFederativa uf;
	
	//GET
	@MaxLength(100)
	public String getNome() {
		return nome;
	}
	
	public UnidadeFederativa getUf() {
		return uf;
	}
	
	//SET
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setUf(UnidadeFederativa uf) {
		this.uf = uf;
	}
}

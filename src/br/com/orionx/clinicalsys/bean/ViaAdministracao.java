package br.com.orionx.clinicalsys.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

import br.com.orionx.clinicalsys.util.log.Log;

@Entity
@Table(name = "viaadministracao")
public class ViaAdministracao implements Log{
	
	private Integer idViaAdministracao;
	private String nome;
	private String sigla;

	//Log
	private Integer idUsuarioAltera;
	private Timestamp dtAltera;

	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdViaAdministracao() {
		return idViaAdministracao;
	}
	@Required
	@MinLength(1)
	@MaxLength(30)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	@MinLength(1)
	@MaxLength(8)
	public String getSigla() {
		return sigla;
	}
	
	//SET
	public void setIdViaAdministracao(Integer idViaAdministracao) {
		this.idViaAdministracao = idViaAdministracao;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	
	//LOG
	@Override
	public void setIdUsuarioAltera(Integer idUsuarioAltera) {
		this.idUsuarioAltera = idUsuarioAltera;
	}

	@Override
	public void setDtAltera(Timestamp dtAltera) {
		this.dtAltera = dtAltera;
	}

	@Override
	public Integer getIdUsuarioAltera() {
		return idUsuarioAltera;
	}

	@Override
	public Timestamp getDtAltera() {
		return dtAltera;
	}
}

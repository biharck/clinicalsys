package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name="imunizacoesinfanciapaciente")
public class ImunizacoesInfanciaPaciente {

	private Integer idImunizacoesInfanciaPaciente;
	private ImunizacoesInfancia imunizacaoPaciente;
	private HistoriaPregressa historiaPregressa;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdImunizacoesInfanciaPaciente() {
		return idImunizacoesInfanciaPaciente;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idImunizacoesInfancia")
	@ForeignKey(name="fk_imunizacoes_pct")
	public ImunizacoesInfancia getImunizacaoPaciente() {
		return imunizacaoPaciente;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idHistoriaPregressa")
	@ForeignKey(name = "fk_pct_imunizacoes")
	public HistoriaPregressa getHistoriaPregressa() {
		return historiaPregressa;
	}
	public void setIdImunizacoesInfanciaPaciente(Integer idImunizacoesInfanciaPaciente) {
		this.idImunizacoesInfanciaPaciente = idImunizacoesInfanciaPaciente;
	}
	public void setImunizacaoPaciente(ImunizacoesInfancia imunizacaoPaciente) {
		this.imunizacaoPaciente = imunizacaoPaciente;
	}
	public void setHistoriaPregressa(HistoriaPregressa historiaPregressa) {
		this.historiaPregressa = historiaPregressa;
	}
	
	
}

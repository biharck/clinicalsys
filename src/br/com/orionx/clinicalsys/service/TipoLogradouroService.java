package br.com.orionx.clinicalsys.service;

import br.com.orionx.clinicalsys.bean.TipoLogradouro;
import br.com.orionx.clinicalsys.dao.TipoLogradouroDAO;
import br.com.orionx.clinicalsys.util.generics.GenericService;

public class TipoLogradouroService extends GenericService<TipoLogradouro> {

	private TipoLogradouroDAO tipoLogradouroDAO;
	
	public void setTipoLogradouroDAO(TipoLogradouroDAO tipoLogradouroDAO) {
		this.tipoLogradouroDAO = tipoLogradouroDAO;
	}
	
	/**
	 * <p>M�todo de refer�ncia ao DAO
	 * @param name {@link String}
	 * @return {@link TipoLogradouro}
	 */
	public TipoLogradouro getTipoByName(String name){
		return tipoLogradouroDAO.getTipoByName(name);
	}
}

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<t:property name="idRecomendacaoMedicamentosa" type="hidden" label="" write="false" colspan="2"/>
	<t:janelaEntrada>
		<t:tabelaEntrada columns="1">
			<t:property name="nome" renderAs="doubleline" style="width:500px" id="nome" rows="3"/>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>
<script type="text/javascript">
	$(document).ready(function(){
		$('#nome').focus();
	});
</script>
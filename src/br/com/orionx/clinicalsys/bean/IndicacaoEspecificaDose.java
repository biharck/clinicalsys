package br.com.orionx.clinicalsys.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name = "indicacaoespecificadose")
public class IndicacaoEspecificaDose {
	
	private Integer idIndicacaoEspecificaDose;
	private String indicacao;
	private String doseAdultos;
	private String doseCriancas;
	private String nivelSericoDisfRenalHepa;
	private String consideracoesRecomendacoes;
	private Medicamento medicamento;

	//GET
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getIdIndicacaoEspecificaDose() {
		return idIndicacaoEspecificaDose;
	}
	@MaxLength(50)
	@Column(length = 100)
	@Required
	public String getIndicacao() {
		return indicacao;
	}
	@MaxLength(4000)
	@Column(length=4000)
	public String getDoseAdultos() {
		return doseAdultos;
	}
	@MaxLength(4000)
	@Column(length=4000)
	public String getDoseCriancas() {
		return doseCriancas;
	}
	@MaxLength(400)
	@Column(length = 400)
	public String getNivelSericoDisfRenalHepa() {
		return nivelSericoDisfRenalHepa;
	}
	@MaxLength(1000)
	@Column(length = 1000)
	public String getConsideracoesRecomendacoes() {
		return consideracoesRecomendacoes;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idMedicamento")
	public Medicamento getMedicamento() {
		return medicamento;
	}
	
	//SET
	public void setIdIndicacaoEspecificaDose(Integer idIndicacaoEspecificaDose) {
		this.idIndicacaoEspecificaDose = idIndicacaoEspecificaDose;
	}
	public void setIndicacao(String indicacao) {
		this.indicacao = indicacao;
	}
	public void setDoseAdultos(String doseAdultos) {
		this.doseAdultos = doseAdultos;
	}
	public void setDoseCriancas(String doseCriancas) {
		this.doseCriancas = doseCriancas;
	}
	public void setNivelSericoDisfRenalHepa(String nivelSericoDisfRenalHepa) {
		this.nivelSericoDisfRenalHepa = nivelSericoDisfRenalHepa;
	}
	public void setConsideracoesRecomendacoes(String consideracoesRecomendacoes) {
		this.consideracoesRecomendacoes = consideracoesRecomendacoes;
	}
	public void setMedicamento(Medicamento medicamento) {
		this.medicamento = medicamento;
	}
}

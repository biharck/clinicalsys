package br.com.orionx.clinicalsys.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * http://pt.wikipedia.org/wiki/Anexo:Lista_de_c�digos_da_CID-10
 * sub n�veis em formato de �rvore
 * 
 * @author biharck
 * representam as subCategorias do cid10
 *
 */
@Entity
@Table(name = "cid10subcategoria")
public class CID10SubCategoria{
	
	private Integer idCid10SubCategoria;
	private String subCategoria;
	private String subClassificacao;
	private String restrSexo;
	private String causaObito;
	private String descricao;
	private String descrabrev;
	private String refer;
	private String excluidos;
	
	public CID10SubCategoria() {}
	public CID10SubCategoria(Integer idCid10SubCategoria, String subCategoria, String subClassificacao, String restrSexo, 
							 String causaObito, String descricao, String descrabrev,String refer, String excluidos) {
		
		this.idCid10SubCategoria = idCid10SubCategoria;
		this.subCategoria = subCategoria;
		this.subClassificacao = subClassificacao;
		this.restrSexo = restrSexo;
		this.causaObito = causaObito;
		this.descricao = descricao;
		this.descrabrev = descrabrev;
		this.refer = refer;
		this.excluidos = excluidos;
	}
	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdCid10SubCategoria() {
		return idCid10SubCategoria;
	}
	
	@Column(name="subCat")
	public String getSubCategoria() {
		return subCategoria;
	}

	@Column(name="classif")
	public String getSubClassificacao() {
		return subClassificacao;
	}

	public String getRestrSexo() {
		return restrSexo;
	}

	public String getCausaObito() {
		return causaObito;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getDescrabrev() {
		return descrabrev;
	}

	public String getRefer() {
		return refer;
	}

	public String getExcluidos() {
		return excluidos;
	}

	//SET
	public void setIdCid10SubCategoria(Integer idCid10SubCategoria) {
		this.idCid10SubCategoria = idCid10SubCategoria;
	}
	
	public void setSubCategoria(String subCategoria) {
		this.subCategoria = subCategoria;
	}

	public void setSubClassificacao(String subClassificacao) {
		this.subClassificacao = subClassificacao;
	}

	public void setRestrSexo(String restrSexo) {
		this.restrSexo = restrSexo;
	}

	public void setCausaObito(String causaObito) {
		this.causaObito = causaObito;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public void setDescrabrev(String descrabrev) {
		this.descrabrev = descrabrev;
	}

	public void setRefer(String refer) {
		this.refer = refer;
	}

	public void setExcluidos(String excluidos) {
		this.excluidos = excluidos;
	}
	
}

package br.com.orionx.clinicalsys.service;

import java.util.ArrayList;
import java.util.List;

import br.com.orionx.clinicalsys.bean.ConfiguracaoAtendimento;
import br.com.orionx.clinicalsys.bean.PrecoConsultaEspecialidade;
import br.com.orionx.clinicalsys.dao.ConfiguracaoAtendimentoDAO;
import br.com.orionx.clinicalsys.util.generics.GenericService;

public class ConfiguracaoAtendimentoService extends GenericService<ConfiguracaoAtendimento> {

	ConfiguracaoAtendimentoDAO configuracaoAtendimentoDAO;
	PrecoConsultaEspecialidadeService precoConsultaEspecialidadeService;
	
	public void setConfiguracaoAtendimentoDAO(ConfiguracaoAtendimentoDAO configuracaoAtendimentoDAO) {
		this.configuracaoAtendimentoDAO = configuracaoAtendimentoDAO;
	}
	public void setPrecoConsultaEspecialidadeService(PrecoConsultaEspecialidadeService precoConsultaEspecialidadeService) {
		this.precoConsultaEspecialidadeService = precoConsultaEspecialidadeService;
	}
	
	@Override
	public void saveOrUpdate(ConfiguracaoAtendimento bean) {
		List<PrecoConsultaEspecialidade> precosVelhos = null;
		List<PrecoConsultaEspecialidade> precosNovos = new ArrayList<PrecoConsultaEspecialidade>();
		
		if(bean!=null && bean.getIdConfiguracaoAtendimento()!=null)
			precosVelhos = precoConsultaEspecialidadeService.getPrecosByConfiguracao(bean);
		
		// Desativa os registros que o usu�rio escolheu apagar, eles n�o podem ser apagados pois possuem referencias no sistema
		//ent�o � desativado, onde n�o aparecer� na tela e os valores utilizados por ele anteiormente continuar�o armazenados
		if(precosVelhos!=null){
			if(bean.getPrecosConsultas()==null){
				for (PrecoConsultaEspecialidade precoConsultaEspecialidade : precosVelhos) {
					precoConsultaEspecialidade.setAtivo(false);
				}
				bean.setPrecosConsultas(precosVelhos);
			}else{
				for (PrecoConsultaEspecialidade pceVelho : precosVelhos) {
					if(!bean.getPrecosConsultas().contains(pceVelho)){
						pceVelho.setAtivo(false);
						bean.getPrecosConsultas().add(pceVelho);
					}
				}
				
			}
		}
		if(bean.getPrecosConsultas()!=null)
			for (PrecoConsultaEspecialidade pce : bean.getPrecosConsultas()) {
				if(pce.getIdPrecoConsultaEspecialidade()==null)
					pce.setAtivo(true);
				precosNovos.add(pce);
			}
		
		bean.setPrecosConsultas(precosNovos);
		super.saveOrUpdate(bean);	
	}
	
	/**
	 * <p>M�todo de refer�ncia ao DAO
	 * @return {@link ConfiguracaoAtendimento}
	 * @see br.com.orionx.clinicalsys.dao.ConfiguracaoAtendimentoDAO#getDadosConfiguracao
	 */
	public ConfiguracaoAtendimento getDadosConfiguracao() {
		return configuracaoAtendimentoDAO.getDadosConfiguracao();
	}
	
}

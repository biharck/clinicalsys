package br.com.orionx.clinicalsys.system.controller.index;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

@Controller(path={"/system","/gestao","/clinic"},authorizationModule=CrudAuthorizationModule.class)
public class IndexModulosController extends MultiActionController {

    @DefaultAction
    public ModelAndView doPage(WebRequestContext request){
    	request.getSession().setAttribute("pathURL", getPathURL());
        return new ModelAndView("index");
    }
    
    public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a></p>"; 
	}
    
    
    public void renovaSessao(WebRequestContext request){
    	System.out.println("Sess�o Renovada");
    	
    }
    
    
}

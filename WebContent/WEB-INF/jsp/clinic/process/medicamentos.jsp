<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="cs" uri="clinicalsys"%>

<!-- Begin three column window -->
<div class="onecolumn" style="padding-bottom:20px">
	<div class="header">
		<span>Medicamentos</span>
	</div>
	<br class="clear"/>
	<div class="content">
		<br class="clear"/>
		<!-- Begin pagination -->
		<div class="pagination">
			<a href="javascript:findMedicineByLetter('a',this);" id="a" >A</a>
			<a href="javascript:findMedicineByLetter('b',this);" id="b" >B</a>
			<a href="javascript:findMedicineByLetter('c',this);" id="c" >C</a>
			<a href="javascript:findMedicineByLetter('d',this);" id="d" >D</a>
			<a href="javascript:findMedicineByLetter('e',this);" id="e" >E</a>
			<a href="javascript:findMedicineByLetter('f',this);" id="f" >F</a>
			<a href="javascript:findMedicineByLetter('g',this);" id="g" >G</a>
			<a href="javascript:findMedicineByLetter('h',this);" id="h" >H</a>
			<a href="javascript:findMedicineByLetter('i',this);" id="i" >I</a>
			<a href="javascript:findMedicineByLetter('j',this);" id="j" >J</a>
			<a href="javascript:findMedicineByLetter('k',this);" id="k" >K</a>
			<a href="javascript:findMedicineByLetter('l',this);" id="l" >L</a>
			<a href="javascript:findMedicineByLetter('m',this);" id="m" >M</a>
			<a href="javascript:findMedicineByLetter('n',this);" id="n" >N</a>
			<a href="javascript:findMedicineByLetter('o',this);" id="o" >O</a>
			<a href="javascript:findMedicineByLetter('p',this);" id="p" >P</a>
			<a href="javascript:findMedicineByLetter('q',this);" id="q" >Q</a>
			<a href="javascript:findMedicineByLetter('r',this);" id="r" >R</a>
			<a href="javascript:findMedicineByLetter('s',this);" id="s" >S</a>
			<a href="javascript:findMedicineByLetter('t',this);" id="t" >T</a>
			<a href="javascript:findMedicineByLetter('u',this);" id="u" >U</a>
			<a href="javascript:findMedicineByLetter('v',this);" id="v" >V</a>
			<a href="javascript:findMedicineByLetter('w',this);" id="w" >W</a>
			<a href="javascript:findMedicineByLetter('x',this);" id="x" >X</a>
			<a href="javascript:findMedicineByLetter('y',this);" id="y" >Y</a>
			<a href="javascript:findMedicineByLetter('z',this);" id="z" >Z</a>
			<!-- <a href="javascript:openFindMedicamento();">Procurar</a> -->
		</div>
		<!-- End pagination -->
		<div id="threecolumn" class="threecolumn">
			<div class="threecolumn_each">
				<div class="header">
					<span>Relação de Medicamentos</span>
				</div>
				<br class="clear"/>
				<div class="content" id="listaMed">
					Selecione uma letra do alfabeto para filtrar sua pesquisa de medicamentos.
				</div>
			</div>
			<div class="threecolumn_each">
				<div class="header">
					<span id="nomeMed">Nome do medicamento</span>
				</div>
				<br class="clear"/>
				<div class="content">
				<div align="right" >
					<a rel="modalFrame" id="bula" href="http://www4.anvisa.gov.br/BularioEletronico/default.asp?txtPrincipioAtivo=">Buscar bula na Anvisa</a>
				</div>
				<h4><span id="classificacao">classificação do medicamento</span></h4><br>
					<div id="accordion">
						<h3><a href="#">Descrição</a></h3>
						<div>
							<p id="contentMed">
								
							</p>
						</div>
						<h3><a href="#">Interações Medicamentosas</a></h3>
						<div>
							<p>
							
							</p>
							
						</div>
						<h3><a href="#">Nomes Comerciais</a></h3>
						<div>
							<p id="nomeComercial">
							
							</p>
						</div>
						<h3><a href="#">Indicações Específicas e Doses</a></h3>
						<div id="ied">
							
						</div>
						<h3><a href="#">Efeitos Colaterais</a></h3>
						<div>
							<p id="efeitosColateraisCom">
							</p>
							<p id="efeitosColateraisMF">
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br class="clear"/>
</div>
<!-- End three column window -->
<style>
	.threecolumn_each{
		width: 45%;
	}
	
	h3{
		background-color: #fafafa;		
		-webkit-box-shadow: #CCC 0px 1px 2px;
		border: 1px solid #CDCDCD;
		border-radius: 4px 4px 4px 4px;
		
	}
	
</style>
<script>
	var icons = {
		header: "ui-icon-circle-arrow-e",
		headerSelected: "ui-icon-circle-arrow-s"
	};
	$(function() {
		$( "#accordion" ).accordion({
			fillSpace: true,
			navigation: true
		});
	});
	$(function() {
		$('a[rel=modalFrame]').click(function(e) {
			e.preventDefault();
			var $this = $(this);
			var horizontalPadding = 30;
			var verticalPadding = 30;
	        $('<iframe id="externalSite" class="externalSite" src="' + this.href + '" />').dialog({
	            title: ($this.attr('title')) ? $this.attr('title') : '',
	            autoOpen: true,
	            width: 980,
	            height: 630,
	            modal: true,
	            resizable: true,
				autoResize: true,
	            overlay: {
	                opacity: 0.5,
	                background: "black"
	            }
	        }).width(1000 - horizontalPadding).height(630 - verticalPadding);	        
		});
	});
</script>
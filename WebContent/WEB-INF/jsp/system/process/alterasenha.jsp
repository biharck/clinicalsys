<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"> 
<%@page import="br.com.orionx.clinicalsys.util.ClinicalSysUtil"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"> 
 
<!-- Website Title --> 
<title>ClinicalSys</title>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="cs" uri="clinicalsys"%>

<!-- Template stylesheet -->
<link id="screenColor" href="${application}/css/blue/screen.css" rel="stylesheet" type="text/css" media="all">
<link id="datepickerColor" href="${application}/css/blue/datepicker.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/tipsy.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/js/visualize/visualize.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/js/jwysiwyg/jquery.wysiwyg.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/js/fancybox/jquery.fancybox-1.3.0.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/tipsy.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/autocomplete.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/default.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/ui.datepicker.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/ui.theme.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/ui.dialog.css" rel="stylesheet" type="text/css" media="all">
<link href="${application}/css/dtree.css" rel="stylesheet" type="text/css" media="all">


<!--[if IE]>
	<link href="${application}/css/ie.css" rel="stylesheet" type="text/css" media="all">
	<script type="text/javascript" src="${application}/js/excanvas.js"></script>
<![endif]-->

<!-- Jquery and plugins -->
<script type="text/javascript" src="${application}/js/jquery.js"></script>
<script type="text/javascript" src="${application}/js/jquery-ui.js"></script>
<script type="text/javascript" src="${application}/js/jquery.img.preload.js"></script>
<script type="text/javascript" src="${application}/js/hint.js"></script>
<script type="text/javascript" src="${application}/js/visualize/jquery.visualize.js"></script>
<script type="text/javascript" src="${application}/js/jwysiwyg/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="${application}/js/fancybox/jquery.fancybox-1.3.0.js"></script>
<script type="text/javascript" src="${application}/js/jquery.tipsy.js"></script>
<script type="text/javascript" src="${application}/js/custom_blue.js"></script>
<script type="text/javascript" src="${application}/js/validate.js"></script>
<script type="text/javascript" src="${application}/js/functions.js"></script>
<script type="text/javascript" src="${application}/js/dtree.js"></script>
<script type="text/javascript" src="${application}/js/util.js"></script>
<script type="text/javascript" src="${application}/js/ajax.js"></script>
<script type="text/javascript" src="${application}/js/input.js"></script>
<script type="text/javascript" src="${application}/js/dynatable.js"></script>
<script type="text/javascript" src="${application}/js/clinicalSysUtil.js"></script>
<!-- lib do qtip -->
<script type="text/javascript" src="${application}/js/jquery-qtip-1.0.0-rc3140944/jquery.qtip-1.0.0-rc3.min.js"></script>


</head>
<body class="nobg">
<a name="topo"></a>
<div class="content_wrapper">

	<!-- Begin header -->
	<div id="header">
		<div id="logo">
			<img src="${application}/images/logo.png" alt="logo"/>
		</div>
		<c:if test="${param.fromInsertOne != 'true'}">
			<c:if test="${param.onePath != 'true'}">
				<div id="search">
					<form action="dashboard.html" id="search_form" name="search_form" method="get">
						<input type="text" id="CityLocal" id="CityLocal" name="q" title="Procurar" class="search noshadow"/>			
					</form>
				</div>
			</c:if>
		</c:if>
		<div id="account_info">
			<img src="${application}/images/icon_online.png" alt="Online" class="mid_align"/>
			Ol� <a href="">Dr. <%= ClinicalSysUtil.getUsuarioLogado().getLogin() %></a>
			<c:if test="${param.fromInsertOne != 'true'}">
				<c:if test="${param.onePath != 'true'}">
			 		(<a href="">1 nova menssagem</a>) | <a href="${application}/system/Logout">Logout</a>
			 	</c:if>
			</c:if>
			&nbsp;&nbsp;&nbsp;&nbsp;Sua sess�o expira em: <span id="cronometro_tempo"></span>
		</div>
	</div>
	<!-- End header -->
	
	<!-- Begin content -->
	<div id="content" style="margin-left:30px;">
		<div class="inner">
			<c:if test="${param.ACAO != 'criar'}">
				<c:if test="${param.fromInsertOne != 'true'}">
					<c:if test="${param.onePath != 'true'}">
						<i id="pathURL">${pathURL}</i>
					</c:if>
				</c:if>
			</c:if>
			
			<br class="clear"/>
			<div class="messageOuterDiv">
				<n:messages/> <%-- Imprime as mensagens do sistema --%>
			</div>
			<!-- conte�do da aba -->
			<div class="onecolumn">
				<t:tela>
					<div class="header">
						<span>Alterar Senha</span>
					</div>
					<br class="clear"/>
					<div class="content">
						<div id="ErrosSubmissao"></div>
						<div id="forcaSenha"></div>
						<div class="linkBar" align="right">
							<n:submit action="updateSenha" validate="true"  class="noboard" ><img src="${application}/images/shortcut/save.png" alt="Salvar" class="help" title="Salvar"/></n:submit>
						</div>
							<n:bean name="usuario" >
								<n:panel  >
									<n:panelGrid columns="2" columnStyles="width:400px,width:300px">
										<t:property name="password" colspan="2" renderAs="doubleline"   id="psw" onkeyUp="checa_seguranca(this)"/>
										<t:property name="confirmacaoSenha"  renderAs="doubleline" onchange="equalsPassword(this,document.getElementById('psw'));" type="password"/>
									</n:panelGrid>
								</n:panel>
							</n:bean>
						<br class="clear"/>
					</div>
				</t:tela>
			</div>
			<!-- End one column tab content window -->
			
			<!-- Begin footer -->
			<div id="footer">
				 Copyright&copy; by OrionX Technologies
			</div>
			<!-- End footer -->
			
		</div>
	</div><!-- End inner -->
</div>
<!-- End content -->

<div id="menuLateral">
	<ul id="shortcut">
		<li>
   		   <a rel="modalFrame" href="/ClinicalSys/clinic/pag/Paciente?ACAO=editar&onePath=true&idPaciente=${paciente.idPaciente}" title="Cadastro do Paciente">
		    <img src="${application}/images/shortcut/cadastro.png" alt="Cadastro do Paciente"/><br/>
		    <strong>Cadastro</strong>
		  </a>
		</li>
		<li>
   		   <a href="javascript:redirectHistoriaPregressa(${paciente.idPaciente})" title="Hist�ria Pregressa">
		    <img src="${application}/images/shortcut/historico.png" alt="Hist�ria Pregressa"/><br/>
		    <strong>Hist. Preg</strong>
		  </a>
		</li>
		<li>
   		   <a href="javascript:redirectAnamnese(${paciente.idPaciente})" title="Anamnese">
		    <img src="${application}/images/shortcut/anamnese.png" alt="Anamnese"/><br/>
		    <strong>Anamnese</strong>
		  </a>
		</li>
		<li>
   		    <a href="javascript:redirectPrescricao(${paciente.idPaciente})" title="Prescri��o">
		    <img src="${application}/images/shortcut/exameFisico.png" alt="Prescri��o"/><br/>
		    <strong>Prescri��o</strong>
		  </a>
		</li>
		<li>
   		   <a href="javascript:redirectExames(${paciente.idPaciente})" title="Exames">
		    <img src="${application}/images/shortcut/exames.png" alt="Exames"/><br/>
		    <strong>Exames</strong>
		  </a>
		</li>
		<li>
   		   <a href="javascript:redirectFormularios(${paciente.idPaciente})" title="Formul�rios">
		    <img src="${application}/images/shortcut/pdf.png" alt="Formul�rios"/><br/>
		    <strong>Formul�rios</strong>
		  </a>
		</li>
	</ul>
	<div id="headerMenuLateral">
		<div id="actionMenuLateral">&laquo;</div>
	</div>
</div>
<div id="actionMenuLateral-open">&raquo;</div>

<c:if test="${param.onePath != 'true'}">
	<div id="rodapeFixo">
	<div id="headerFooter"><div align="right"><div id="divButtonClose">Fechar</div></div></div>
		
		<!-- Begin shortcut menu -->
		<ul id="shortcut">
   			<li>
   			   <a href="#" onclick="openDialogAguarde();location.href='${application}/clinic/process/listaEspera'" title="Lista de Espera">
			    <img src="${application}/images/shortcut/atendimento.png" alt="Lista de Espera"/><br/>
			    <strong>Espera</strong>
			  </a>
			</li>
   			<li>
   			  <a href="#" onclick="openDialogAguarde();location.href='${application}/clinic/process/Calendar'" title="Agenda">
			    <img src="${application}/images/shortcut/agenda.png" alt="Agenda"/><br/>
			    <strong>Agenda</strong>
			  </a>
			</li>
			<li>
   			  <a href="#" onclick="openDialogAguarde();location.href='${application}/clinic/pag/Paciente'" id="shortcut_contacts" title="Cadastro de Pacientes">
			    <img src="${application}/images/shortcut/patient.png" alt="Cadastro de Pacientes"/><br/>
			    <strong>Paciente</strong>
			  </a>
			</li>
			<li>
   			  <a href="#" onclick="openDialogAguarde();location.href='${application}/clinic/process/Medicamentos'" id="shortcut_contacts" title="Medicamentos">
			    <img src="${application}/images/shortcut/bookmedicine.png" alt="Medicamentos"/><br/>
			    <strong>Drogas</strong>
			  </a>
			</li>
   			<li>
   			  <a href="modal_window.html" title="Relat�rios">
			    <img src="${application}/images/shortcut/report.png" alt="Relat�rios"/><br/>
			    <strong>Relat�rios</strong>
			  </a>
			</li>
   			<li>
   			  <a href="modal_window.html" title="Dashboard">
			    <img src="${application}/images/shortcut/stats.png" alt="Dashboard"/><br/>
			    <strong>Dashboard</strong>
			  </a>
			</li>
			<li>
   			  <a href="#" onclick="openMenuByModulo('estoque')" title="Administra��o de estoque e financeiro">
			    <img src="${application}/images/shortcut/stock.png" alt="Administra��o de estoque e financeiro"/><br/>
			    <strong>Adm</strong>
			  </a>
			</li>
			<li>
   			  <a href="#" onclick="openMenuByModulo('util')" title="Utilit�rios">
			    <img src="${application}/images/shortcut/util.png" alt="Utilit�rios"/><br/>
			    <strong>Utilit�rios</strong>
			  </a>
			</li>
			<li>
   			  <a href="#" onclick="openMenuByModulo('system')" title="Configura��es do sistema">
			    <img src="${application}/images/shortcut/setting.png" alt="Configura��es"/><br/>
			    <strong>Config</strong>
			  </a>
			</li>
 			</ul>
		<!-- End shortcut menu -->
	</div>
	<div id="openRodapeFixo">
		<div id="divButton">Abrir</div>
	</div>
</c:if>
<!-- ui-dialog -->
<!-- Include das menssagens de alerta -->
<jsp:include page="../../menssages/dialogInformation.jsp"></jsp:include>
<script type="text/javascript">
$(document).ready(function(){
	$('#show_menu').fadeIn();
	$('#wysiwyg').css('width', '97%');
	//setNotifications();
	startCountdown();
	$('#divButtonClose').click(function(){
		$('#rodapeFixo').slideUp();
		$('#openRodapeFixo').slideDown();
	});
	$('#openRodapeFixo').click(function(){
		$('#openRodapeFixo').slideUp();
		$('#rodapeFixo').slideDown();
	});
	$('#actionMenuLateral').click(function(){
		//$('#menuLateral').hide('slide', {direction: 'left'}, 600);
		$('#menuLateral').animate({width: 'hide'});
		$('#actionMenuLateral-open').animate({width: 'show'});
	});
	$('#actionMenuLateral-open').click(function(){
		$('#menuLateral').animate({width: 'show'});
		$('#actionMenuLateral-open').animate({width: 'hide'});
	});
	
});
	$(function() {
		$('a[rel=modalFrame]').click(function(e) {
			e.preventDefault();
			var $this = $(this);
			var horizontalPadding = 30;
			var verticalPadding = 30;
	        $('<iframe id="cadPaciente" class="externalSite" src="' + this.href + '" />').dialog({
	            title: ($this.attr('title')) ? $this.attr('title') : '',
	            autoOpen: true,
	            width: 980,
	            height: 630,
	            modal: true,
	            resizable: true,
				autoResize: true,
	            overlay: {
	                opacity: 0.5,
	                background: "black"
	            }
	        }).width(1000 - horizontalPadding).height(630 - verticalPadding);	        
		});
	});
	function closeIframe(){
	   var iframe = document.getElementById('cadPaciente');
	   iframe.parentNode.removeChild(iframe);
	   openDialogAguarde();
	   location.href = location.href;
	}
	function redirectHistoriaPregressa(param){
		openDialogAguarde();
		location.href= '/ClinicalSys/clinic/process/historiaPregressa?idPaciente='+param;
	}
	function redirectAnamnese(param){
		openDialogAguarde();
		location.href= '/ClinicalSys/clinic/process/Anamnese?idPaciente='+param;
	}
	function redirectPrescricao(param){
		openDialogAguarde();
		location.href= '/ClinicalSys/clinic/process/prescricao?idPaciente='+param;
	}
	function redirectFormularios(param){
		openDialogAguarde();
		location.href= '/ClinicalSys/clinic/process/formularios?idPaciente='+param;
	}
	function redirectExames(param){
		openDialogAguarde();
		location.href= '/ClinicalSys/clinic/process/exames?idPaciente='+param;
	}
	
</script>
</body>
</html>
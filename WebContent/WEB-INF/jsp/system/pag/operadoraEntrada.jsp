<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<t:property name="idOperadora" type="hidden" write="false"/>
	<t:janelaEntrada>
		<t:tabelaEntrada colspan="2" style="width:100%">
			<n:group legend="Dados Cadastrais" >
				<n:panelGrid columns="6">
					<t:property name="cnpj" renderAs="doubleline" id="cnpj" colspan="6" style="width:150px;"/>
					<t:property name="razaoSocial" renderAs="doubleline" colspan="6" style="width:500px;"/>
					<t:property name="nomeFantasia" renderAs="doubleline"colspan="6" style="width:500px;"/>
					<t:property name="registroANS" renderAs="doubleline"colspan="2" style="width:150px;"/>
					<t:property name="suaIdentificacao" renderAs="doubleline" colspan="2" style="width:150px;"/>
					<t:property name="versaoXMLTISS" renderAs="doubleline" colspan="2"/>
					<t:property name="tabelaTISS" renderAs="doubleline" colspan="6"/>
					<t:property name="telefone" renderAs="doubleline" colspan="3" style="width:100px;"/>
					<t:property name="telefoneAutorizacao" renderAs="doubleline" colspan="3" style="width:100px;"/>
					<t:property name="email" renderAs="doubleline" colspan="6" style="width:500px;"/>
					<t:property name="website" renderAs="doubleline" colspan="6" style="width:500px;"/>
					<t:property name="ativo" renderAs="doubleline" colspan="2"/>
				</n:panelGrid>
			</n:group>
			<br>
				<n:panel id="picture" style="text-align:center;">
					<n:group legend="Logomarca" >
						<c:if test="${showpicture}">
							<n:link url="/DOWNLOADFILE/${operadora.logomarca.cdfile}">
								<center><img src="${application}/DOWNLOADFILE/${operadora.logomarca.cdfile}" id="imagem" class="foto"/></center>
							</n:link>
						</c:if>
						<c:if test="${!showpicture}">
							<div class="image" align="center">
								<center><img src="${application}/images/imagenotfound.png" id="imagem" style="padding-top:13px;"/></center>
							</div>
						</c:if>
						<t:property name="logomarca" renderAs="doubleline" onblur="validaImagem();"/>
					</n:group>
					<br>
					<n:group legend="Planos desta Operadora" colspan="2">
						<n:panelGrid colspan="4">
							<t:detalhe name="planos">
								<t:property name="nome" style="width:400px;"/>
								<t:property name="ativo" trueFalseNullLabels="Ativo,Inativo,"/>
							</t:detalhe>
						</n:panelGrid>
					</n:group>
				</n:panel>
			<n:group legend="Endere�o" colspan="2">
				<n:panelGrid columns="6" colspan="2">
					<t:property name="cep" id="cep" renderAs="doubleline" colspan="1" onchange="ajaxBuscaCEP(this.value)" style="width:100px;"/>
					<n:panel colspan="1" id="modalLink">
						<label for="btn_busca_end">N�o sabe o CEP?</label><br>
						<a id="btn_busca_end"  href="javascript:openDialogBuscaCEP()"><img src="/ClinicalSys/images/shortcut/find-help.png"/> Clique aqui</a>
					</n:panel>
					<n:panel colspan="4"></n:panel>
					<t:property name="tipoLogradouro" renderAs="doubleline" colspan="2" id="tipoLogradouro"/>
					<t:property name="logradouro" renderAs="doubleline" colspan="3" style="width:400px;" id="logradouro"/>
					<t:property name="numero" renderAs="doubleline" colspan="2" id="numero"/>
					<t:property name="complemento" renderAs="doubleline" colspan="2" style="width:100px;"/>
					<t:property name="bairro" renderAs="doubleline" colspan="2" id="bairro" style="width:200px;"/>
					<n:comboReloadGroup useAjax="true">
						<t:property name="pais" renderAs="doubleline" colspan="2" onchange="openAguardeAjax()" id="pais"/>
						<t:property name="unidadeFederativa" renderAs="doubleline" colspan="2" onchange="openAguardeAjax()" id="uf"/>
						<t:property name="municipio" renderAs="doubleline" colspan="2" id="cidade"/>
					</n:comboReloadGroup>
				</n:panelGrid>
			</n:group>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>
<script type="text/javascript">
$(document).ready(function(){
	$('#cnpj').focus();
});
</script>

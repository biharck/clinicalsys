package br.com.orionx.clinicalsys.filtros;

import org.nextframework.validation.annotation.MaxLength;

import br.com.orionx.clinicalsys.util.generics.FiltroListagem;

public class ConselhoFiltro extends FiltroListagem{
	
	protected String nome;
	protected String codigoConselho;
	protected AtivoEnum ativoEnum;
	
	@MaxLength(50)
	public String getNome() {
		return nome;
	}
	public String getCodigoConselho() {
		return codigoConselho;
	}
	public AtivoEnum getAtivoEnum() {
		return ativoEnum;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCodigoConselho(String codigoConselho) {
		this.codigoConselho = codigoConselho;
	}
	public void setAtivoEnum(AtivoEnum ativoEnum) {
		this.ativoEnum = ativoEnum;
	}
}

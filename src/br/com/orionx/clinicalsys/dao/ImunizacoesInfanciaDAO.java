package br.com.orionx.clinicalsys.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.orionx.clinicalsys.bean.HistoriaPregressa;
import br.com.orionx.clinicalsys.bean.ImunizacoesInfancia;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class ImunizacoesInfanciaDAO extends GenericDAO<ImunizacoesInfancia> {

	public List<ImunizacoesInfancia> findByHistoriaPregressa(HistoriaPregressa hp){
		if(hp.getIdHistoriaPregressa() == null){
			return new ArrayList<ImunizacoesInfancia>();
		}
		return query()
				.select("imunizacoesInfancia")
				.from(ImunizacoesInfancia.class)
				.leftOuterJoin("imunizacoesInfancia.imunizacoesInfanciaPaciente iip")
				.where("iip.historiaPregressa = ?", hp)
				.list();
	}
	
	
	public List<ImunizacoesInfancia> getByMeses(Integer meses){
		return query()
			.select("imunizacoesInfancia")
			.where("imunizacoesInfancia.meses = ?", meses)
			.list();
	}
	
}

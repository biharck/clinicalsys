package br.com.orionx.clinicalsys.util.generics;

import java.lang.reflect.Method;

import javax.persistence.Id;

import org.nextframework.controller.Action;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.util.ReflectionCache;
import org.nextframework.util.ReflectionCacheFactory;
import org.nextframework.util.Util;
import org.springframework.dao.DataAccessException;
import org.springframework.web.servlet.ModelAndView;

import br.com.orionx.clinicalsys.util.PathURL;
import br.com.orionx.clinicalsys.util.exception.ClinicalSysException;

public abstract class CrudController<FILTRO extends br.com.orionx.clinicalsys.util.generics.FiltroListagem, FORMBEAN, BEAN> extends org.nextframework.controller.crud.CrudController<FiltroListagem, FORMBEAN, BEAN> implements PathURL{


	
	/**
	 * <p>Sobrescrito para que sempre a primeira vez a listagem de dados venha vazia, aumentando o tempo de resposta do sistema
	 * 
	 * @author biharck
	 */
	@Override
	protected boolean listagemVaziaPrimeiraVez() {
		return true;
	}
	
	@Override
	protected ModelAndView getListagemModelAndView(WebRequestContext request,FiltroListagem filtro) {
		return new ModelAndView("pag/"+getBeanName()+"Listagem");
	}
	
	
	@Override
	public ModelAndView doListagem(WebRequestContext request,FiltroListagem filtro) throws CrudException {
		String vazio = request.getParameter("vazio");
		if(vazio!=null && vazio.equals("true"))
			filtro.setNotFirstTime(false);
		request.getSession().setAttribute("pathURL", getPathURL());
		return super.doListagem(request, filtro);
	}
	
	protected ModelAndView getEntradaModelAndView(WebRequestContext request, FORMBEAN form) {
		if(Boolean.TRUE.equals(request.getAttribute(CONSULTAR))){
			return new ModelAndView("pag/"+getBeanName()+"Consulta");
		} else {
			return new ModelAndView("pag/"+getBeanName()+"Entrada");	
		}
	};
	
	protected void salvar(WebRequestContext request, BEAN bean) throws Exception {
		super.salvar(request, bean);
		request.addMessage("Registro salvo com sucesso." );
	};
	
	public ModelAndView saveAndNew(WebRequestContext request, FORMBEAN form) throws Exception {
		super.salvar(request, (BEAN) form);
		request.addMessage("Registro salvo com sucesso. Voc� j� pode criar um novo registro." );
		return sendRedirectToAction("criar");
	};
	
	@Override
	protected void excluir(WebRequestContext request, BEAN bean) throws Exception {
		String itens = request.getParameter("rd");
		if(itens != null && !itens.equals("")){
			ReflectionCache reflectionCache = ReflectionCacheFactory.getReflectionCache();
			Method[] methods = reflectionCache.getMethods(beanClass);
			Method methodId = null;
			for (Method method : methods) {
				if(reflectionCache.isAnnotationPresent(method, Id.class)){
					methodId = method;
					break;
				}
			}
			
			String propertyFromGetter = Util.beans.getPropertyFromGetter(methodId.getName());
			methodId = Util.beans.getSetterMethod(beanClass, propertyFromGetter);
			String[] codes = itens.split(",");
			for (String code : codes) {
				if(!"".equals(code) && code != null){
					BEAN obj = beanClass.newInstance();
					methodId.invoke(obj, Integer.parseInt(code));
					if(validateManyDelete(request, obj))
						try {
							genericService.delete(obj);
						} catch (DataAccessException da) {
//							throw new ClinicalSysException("Esta opera��o n�o pode ser realizada, pois este registro possui refer�ncias em outros registros do sistema.<br><script type=\"text/javascript\"> $(document).ready(function() { $('#dialog-help-delete').dialog({closeText: 'hide',closeOnEscape: false,resizable: false,autoOpen: true,width: 600,open: function(event, ui) { $('.ui-dialog-titlebar-close').hide();}	,modal: true});  });</script>");
							throw new ClinicalSysException("O(s) registro(s) n�o pode(m) ser exclu�do(s), j� possui(em) refer�ncias em outros registros do sistema.");
						}
				}
			}
		} else {
			try {
				super.excluir(request, bean);
			} catch (DataAccessException da) {
//				throw new ClinicalSysException("Esta opera��o n�o pode ser realizada, pois este registro possui refer�ncias em outros registros do sistema.<br><script type=\"text/javascript\"> $(document).ready(function() { $('#dialog-help-delete').dialog({closeText: 'hide',closeOnEscape: false,resizable: false,autoOpen: true,width: 600,open: function(event, ui) { $('.ui-dialog-titlebar-close').hide();},modal: true});  });</script>");
				throw new ClinicalSysException("O(s) registro(s) n�o pode(m) ser exclu�do(s), j� possui(em) refer�ncias em outros registros do sistema.");
			}	
		}
		if(!"true".equals(request.getParameter("fromInsertOne")))
			request.addMessage("Registro(s) exclu�do(s) com sucesso.");
	}
	
	public Boolean validateManyDelete(WebRequestContext request, BEAN bean) {
		return true;
	}
	
	@Action("defaultSimpleAjaxRequest")
	protected void defaultSimpleAjaxRequest(WebRequestContext request){
		
	}
}

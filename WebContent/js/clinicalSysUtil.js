function ClinicalSysUtil (){
	var selectedIndex = null;
} 

/**
 * Acessa o link que tem a classe activation
 */
ClinicalSysUtil.prototype.editarRegistro = function (obj){
	var acv = $(".consult",obj);
	if(acv.size() > 0)
		window.location = $(".consult",obj).attr("href");
}

/**
 * Colore a linha que est� o mouse no DG
 */
ClinicalSysUtil.prototype.coloreLinha = function (tabelaId,elementoSelecionado){
	var tabela = document.getElementById(tabelaId);
	var cellRows = tabela.rows;
	
	for(i = 0; i< cellRows.length ; i++){
		cellRows[i].style.backgroundColor = "";
	}
	
	elementoSelecionado.style.backgroundColor = "#ffff80";
	this.selectedIndex = elementoSelecionado.rowIndex;
}



/**
 * Controla o evento de entrada
 */
ClinicalSysUtil.prototype.mouseonOverTabela = function (tabelaId,elementoSelecionado){
	if(elementoSelecionado.rowIndex != this.selectedIndex)
		elementoSelecionado.style.backgroundColor = '#ffffb3';
	
	//elementoSelecionado.children[0].children[1].style.display =  "table";	
}
/**
 * Controla o evento de sa�da
 */
ClinicalSysUtil.prototype.mouseonOutTabela = function (tabelaId,elementoSelecionado){
	if(elementoSelecionado.rowIndex != this.selectedIndex)
		elementoSelecionado.style.backgroundColor = '';
	//elementoSelecionado.children[0].children[1].style.display =  "none";
}

ClinicalSysUtil.prototype.getChecksSelecionados = function(){
	var check = $("#allInputs").attr("checked");
	
	$("#tabelaResultados input[type=checkbox][name=radioselecionado]").each(function(){
		if(check) $(this).attr("checked",check);
		else $(this).removeAttr("checked");
	});
}

ClinicalSysUtil.prototype.getSelectedValues = function(){
	var selectedValues = "";
	
	
	$("#tabelaResultados input[type=checkbox][name=radioselecionado]").each(function(){
		if(this.checked) selectedValues += this.value+","; 
	});
	
	if(selectedValues != ""){
		selectedValues = selectedValues.substr(0,(selectedValues.length -1));
	}
	return selectedValues;
}

ClinicalSysUtil.prototype.validarChecksSelecionados = function(){
		openDialogInformation("Para realizar a exclus�o de um registro, voc� deve selecionar os registros que deseja excluir antes.");
		return false;
}

ClinicalSysUtil.prototype.clearForm = function(){
	
	$('.backGroundFiltro input[type=text]').each(function(){
		$(this).val('');
	});
	$('.backGroundFiltro input[type=date]').each(function(){
		$(this).val('');
	});
	$('.backGroundFiltro select').each(function(){
		$(this).val('<null>');
	});
}


var $csu = new ClinicalSysUtil();
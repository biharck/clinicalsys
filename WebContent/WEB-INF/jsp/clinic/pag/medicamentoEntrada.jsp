<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada >
	<t:property name="idMedicamento" type="hidden" write="false" label=""/>
	<t:janelaEntrada>
		<t:tabelaEntrada columns="1" >
			<n:panelGrid columns="2" >
				<t:property name="classificacaoMedicacao" id="classificacaoMedicacao" renderAs="doubleline" />
				<t:property name="principioAtivo"  renderAs="doubleline" style="width:400px" />
				<t:property name="descricaoDroga" renderAs="doubleline" rows="4" style="width:400px" />
				<t:property name="bula" renderAs="doubleline"/>
				<t:property name="nivelSericoDisfRenalHepa" renderAs="doubleline" rows="4" style="width:400px"/>
				<t:property name="consideracoesRecomendacoes" renderAs="doubleline" style="width:400px" rows="4"/>
				<t:property name="ativo" renderAs="doubleline" trueFalseNullLabels="Ativo, Inativo,"/>
			</n:panelGrid>
			
			<n:panelGrid columns="2" >
				<table class="data tablefull">
					<tr class="dataGridHeader">
						<th>Efeitos Colaterais</th>
					</tr>
				</table>
				<t:property name="efeitosColateraisComuns" rows="4" style="width:400px" renderAs="doubleline"/>
				<t:property name="efeitosColateraisMenosFrequente" rows="4" style="width:400px" renderAs="doubleline"/>
				<t:property name="contraIndicacoes" rows="4" style="width:400px" renderAs="doubleline"/>
				
			</n:panelGrid>
		</t:tabelaEntrada>
		<t:detalhe name="nomesComerciaisDroga" id="nomeComercialDroga">
				<n:column header="Nomes comerciais da droga">
					<n:panelGrid columns="3">
						<t:propertyConfig mode="input" renderAs="double" showLabel="true">
							<t:property name="nomeComercial" renderAs="doubleline"/>
							<t:property name="industriaFarmaceutica" insertPath="/system/pag/IndustriaFarmaceutica" renderAs="doubleline"/>
							<t:property name="generico" renderAs="doubleline"/>							
							<t:property name="apresentacao" renderAs="doubleline"  rows="3"/>
							<t:property name="dosagem" renderAs="doubleline" style="width:300px" rows="3"/>
							<t:property name="qtdDisponivelEmbalagem" renderAs="doubleline" style="width:300px" rows="3"/>
							<t:acao>
								<a onclick="javascript:copyData(${index})">
							          <img src="/ClinicalSys/images/copy.png" class="help" alt="Copiar" title="Copiar">         
							    </a>
							    &nbsp;&nbsp;|&nbsp;&nbsp;
							</t:acao>
						</t:propertyConfig>
					</n:panelGrid>
				</n:column>
			</t:detalhe>
			<t:detalhe name="indicacoesEspecificasEDoses"   >
				<n:column header="Indicações específicas e doses">
					<n:panelGrid columns="2" >
						<t:propertyConfig mode="input" renderAs="double" showLabel="true">
							<t:property name="indicacao" renderAs="doubleline" style="width:400px" colspan="2"/>
							<t:property name="doseAdultos" renderAs="doubleline" style="width:400px" rows="4"/>
							<t:property name="doseCriancas" renderAs="doubleline" style="width:400px" rows="4"/>
						</t:propertyConfig>
					</n:panelGrid>
				</n:column>
			</t:detalhe>
	</t:janelaEntrada>
</t:entrada>
<script type="text/javascript">
$(document).ready(function(){
	$('#classificacaoMedicacao').focus();
});


function copyData(idx){
	var sub = idx -1;
	var anteriorNome =  $(form["nomesComerciaisDroga["+sub+"].nomeComercial"]).val();
	var anteriorIndustria =  $(form["nomesComerciaisDroga["+sub+"].industriaFarmaceutica"]).val();
	var anteriorGenerico =  $(form["nomesComerciaisDroga["+sub+"].generico"]).attr("checked");
	$(form["nomesComerciaisDroga["+idx+"].nomeComercial"]).val(anteriorNome);
	$(form["nomesComerciaisDroga["+idx+"].industriaFarmaceutica"]).val(anteriorIndustria);
	if(anteriorGenerico)
		$(form["nomesComerciaisDroga["+idx+"].generico"]).click();
}

</script>
package br.com.orionx.clinicalsys.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "cidogrupo")
public class CIDOGrupo {

	private Integer idCIDOGrupo;
	private String catInic;
	private String catFim;
	private String descricao;
	private String refer;
	
	private List<CIDOCategoria> categorias;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdCIDOGrupo() {
		return idCIDOGrupo;
	}
	public String getCatInic() {
		return catInic;
	}
	public String getCatFim() {
		return catFim;
	}
	public String getDescricao() {
		return descricao;
	}
	public String getRefer() {
		return refer;
	}
	@Transient
	public List<CIDOCategoria> getCategorias() {
		return categorias;
	}
	
	public void setIdCIDOGrupo(Integer idCIDOGrupo) {
		this.idCIDOGrupo = idCIDOGrupo;
	}
	public void setCatInic(String catInic) {
		this.catInic = catInic;
	}
	public void setCatFim(String catFim) {
		this.catFim = catFim;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setRefer(String refer) {
		this.refer = refer;
	}
	public void setCategorias(List<CIDOCategoria> categorias) {
		this.categorias = categorias;
	}
	
}


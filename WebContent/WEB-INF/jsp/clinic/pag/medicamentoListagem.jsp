<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="cs" uri="clinicalsys"%>

<t:listagem showdelete="true">
	<t:janelaFiltro>
		<t:tabelaFiltro colspan="2" showReport="true">
			<t:property name="nomeComercial" renderAs="doubleline" style="width:400px" id="nomeComercial"/>
			<t:property name="principioAtivo" renderAs="doubleline" style="width:400px"/>
			<t:property name="descricaoDroga" renderAs="doubleline"  style="width:400px"/>
			<t:property name="ativoEnum" renderAs="doubleline"/>
		</t:tabelaFiltro>
	</t:janelaFiltro>
	<t:janelaResultados>
		<t:tabelaResultados >
			<n:column header="Princ�pio Ativo" order="medicamento.principioAtivo">
				${cs:trunc(medicamento.principioAtivo,50)}
		  	</n:column>
			<t:property name="classificacaoMedicacao"/>
			<n:column header="Breve Descri��o da Droga" order="medicamento.descricaoDroga">
				${cs:trunc(medicamento.descricaoDroga,50)}
		  	</n:column>
		  	<n:column header="Bula" order="medicamento.bula" >
			  	<c:if test="${!empty medicamento.bula.cdfile}">
			  		<a href="/ClinicalSys/DOWNLOADFILE/${medicamento.bula.cdfile}"><img src="/ClinicalSys/images/bula.png"/></a>	
			  	</c:if>
			  	<c:if test="${empty medicamento.bula.cdfile}">
			  		Bula n�o encontrada	
			  	</c:if>
		  	</n:column>
		  	<t:property name="ativo" trueFalseNullLabels="Ativo, Inativo,"/>
		  	<t:acao>
		  		<t:property name="bula" type="hidden"/>
		  	</t:acao>
		</t:tabelaResultados>
	</t:janelaResultados>
</t:listagem>
<script type="text/javascript">
	$(document).ready(function(){
		$('#nomeComercial').focus();
	});
</script>

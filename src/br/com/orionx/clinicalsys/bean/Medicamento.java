package br.com.orionx.clinicalsys.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;

import br.com.orionx.clinicalsys.util.log.Log;

@Entity
@Table(name = "medicamento")
public class Medicamento implements Log{
	
	private Integer idMedicamento;
	private ClassificacaoMedicacao classificacaoMedicacao;
	private String principioAtivo;
	private String descricaoDroga;
	private List<NomeComercialDroga> nomesComerciaisDroga;
	private List<IndicacaoEspecificaDose> indicacoesEspecificasEDoses;
	private String efeitosColateraisComuns;
	private String efeitosColateraisMenosFrequente;
	private String contraIndicacoes;
	private Arquivo bula;
	private Boolean ativo;
	private String nivelSericoDisfRenalHepa;
	private String consideracoesRecomendacoes;
	
	
	
	public Medicamento(){}
	public Medicamento(Integer idMedicamento){
		this.idMedicamento = idMedicamento;
	}
	
	//Log
	private Integer idUsuarioAltera;
	private Timestamp dtAltera;
	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdMedicamento() {
		return idMedicamento;
	}
	
	@MinLength(1)
	@MaxLength(100)
	@Column(unique=true,length=100)
	@DescriptionProperty
	public String getPrincipioAtivo() {
		return principioAtivo;
	}
	
	@MaxLength(1000)
	@Column(length=1000)
	public String getDescricaoDroga() {
		return descricaoDroga;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "idClassificacaoMedicacao")
	@ForeignKey(name = "fk_medica_classi")
	
	public ClassificacaoMedicacao getClassificacaoMedicacao() {
		return classificacaoMedicacao;
	}
	
	@MaxLength(4000)
	@Column(length=4000)
	public String getEfeitosColateraisComuns() {
		return efeitosColateraisComuns;
	}
	
	@MaxLength(4000)
	@Column(length=4000)
	public String getEfeitosColateraisMenosFrequente() {
		return efeitosColateraisMenosFrequente;
	}
	@MaxLength(4000)
	@Column(length = 4000)
	public String getContraIndicacoes() {
		return contraIndicacoes;
	}
	@ManyToOne
	public Arquivo getBula() {
		return bula;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}
	@OneToMany(mappedBy = "medicamento")
	public List<IndicacaoEspecificaDose> getIndicacoesEspecificasEDoses() {
		return indicacoesEspecificasEDoses;
	}
	@OneToMany(mappedBy = "medicamento")
	public List<NomeComercialDroga> getNomesComerciaisDroga() {
		return nomesComerciaisDroga;
	}
	@MaxLength(400)
	@Column(length = 400)
	public String getNivelSericoDisfRenalHepa() {
		return nivelSericoDisfRenalHepa;
	}
	@MaxLength(3000)
	@Column(length = 3000)
	public String getConsideracoesRecomendacoes() {
		return consideracoesRecomendacoes;
	}
	
	//SET
	public void setIdMedicamento(Integer idMedicamento) {
		this.idMedicamento = idMedicamento;
	}
	public void setPrincipioAtivo(String principioAtivo) {
		this.principioAtivo = principioAtivo;
	}
	public void setClassificacaoMedicacao(ClassificacaoMedicacao classificacaoMedicacao) {
		this.classificacaoMedicacao = classificacaoMedicacao;
	}
	public void setDescricaoDroga(String descricaoDroga) {
		this.descricaoDroga = descricaoDroga;
	}
	public void setEfeitosColateraisComuns(String efeitosColateraisComuns) {
		this.efeitosColateraisComuns = efeitosColateraisComuns;
	}
	public void setIndicacoesEspecificasEDoses(List<IndicacaoEspecificaDose> indicacoesEspecificasEDoses) {
		this.indicacoesEspecificasEDoses = indicacoesEspecificasEDoses;
	}
	public void setNomesComerciaisDroga(List<NomeComercialDroga> nomesComerciaisDroga) {
		this.nomesComerciaisDroga = nomesComerciaisDroga;
	}
	public void setContraIndicacoes(String contraIndicacoes) {
		this.contraIndicacoes = contraIndicacoes;
	}
	public void setEfeitosColateraisMenosFrequente(String efeitosColateraisMenosFrequente) {
		this.efeitosColateraisMenosFrequente = efeitosColateraisMenosFrequente;
	}
	public void setBula(Arquivo bula) {
		this.bula = bula;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public void setNivelSericoDisfRenalHepa(String nivelSericoDisfRenalHepa) {
		this.nivelSericoDisfRenalHepa = nivelSericoDisfRenalHepa;
	}
	public void setConsideracoesRecomendacoes(String consideracoesRecomendacoes) {
		this.consideracoesRecomendacoes = consideracoesRecomendacoes;
	}
	
	//LOG
	@Override
	public void setIdUsuarioAltera(Integer idUsuarioAltera) {
		this.idUsuarioAltera = idUsuarioAltera;
	}

	@Override
	public void setDtAltera(Timestamp dtAltera) {
		this.dtAltera = dtAltera;
	}

	@Override
	public Integer getIdUsuarioAltera() {
		return idUsuarioAltera;
	}

	@Override
	public Timestamp getDtAltera() {
		return dtAltera;
	}
	
}

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="combo" uri="combo"%>
<%@ taglib prefix="t" uri="template"%>

<div class="onecolumn">
	<div class="header">
		<span>Listagem de ${listagemTag.titulo}</span>
	</div>
	<br class="clear"/>
	<div class="content">
		<t:tela titulo="" validateForm="false">
				<input type="hidden" name="notFirstTime" value="true"/>
				<div align="right" class="backGroundFiltro">
					<c:if test="${listagemTag.showNewLink || !empty listagemTag.linkArea}">
						${listagemTag.invokeLinkArea}
						<c:if test="${listagemTag.showNewLink}">						
							<n:link action="criar" onclick="openDialogAguarde()" ><img src="${application}/images/shortcut/add.png" alt="Novo" class="help" title="Novo"/></n:link>
							<c:if test="${listagemTag.dynamicAttributesMap['showdelete']}">
								<n:link url="#"  onclick="javascript:deleteSelectedItens();" ><img src="${application}/images/shortcut/trash.png" alt="Apagar" class="help" title="Apagar"/></n:link>
							</c:if>
						</c:if>						
					</c:if>	
				</div>
			
				<div>
					<n:doBody />
				</div>
		
		</t:tela>
	</div>
</div>

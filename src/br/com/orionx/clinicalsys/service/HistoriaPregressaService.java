package br.com.orionx.clinicalsys.service;

import br.com.orionx.clinicalsys.bean.HistoriaPregressa;
import br.com.orionx.clinicalsys.bean.Paciente;
import br.com.orionx.clinicalsys.dao.HistoriaPregressaDAO;
import br.com.orionx.clinicalsys.util.generics.GenericService;

public class HistoriaPregressaService extends GenericService<HistoriaPregressa>{

	private HistoriaPregressaDAO historiaPregressaDAO;
	
	public void setHistoriaPregressaDAO(HistoriaPregressaDAO historiaPregressaDAO) {
		this.historiaPregressaDAO = historiaPregressaDAO;
	}
	
	public HistoriaPregressa getByPaciente(Paciente p){
		return historiaPregressaDAO.getByPaciente(p);
	}
	
}

package br.com.orionx.clinicalsys.util;

public enum EnumMenu {
	
	OPEN_LI("<li>"),
	CLOSE_LI("</li>"),
	OPEN_A("<a href='#1'>"),
	CLOSE_A("</a>"),
	OPEN_IM("<img src'ClinicalSys/images/#2' alt='#3' />"),
	OPEN_UL("<ul>"),
	CLOSE_UL("</ul>");
	
	private String html;
	
	private EnumMenu(String html){
		this.html = html;
	}
	
	public String getHtml() {
		return html;
	}
	
	@Override
	public String toString() {
		return getHtml().toUpperCase();
	}

}

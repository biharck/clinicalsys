package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name = "tipologradouro")
public class TipoLogradouro {
	
	private Integer idTipoLogradouro;
	private String codigo;
	private String descricao;
	
	//GET
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getIdTipoLogradouro() {
		return idTipoLogradouro;
	}
	@Required
	@MinLength(1)
	@MaxLength(20)
	public String getCodigo() {
		return codigo;
	}
	@DescriptionProperty
	public String getDescricao() {
		return descricao;
	}
	
	//SET
	public void setIdTipoLogradouro(Integer idTipoLogradouro) {
		this.idTipoLogradouro = idTipoLogradouro;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}

package br.com.orionx.clinicalsys.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.validation.annotation.Required;


/**
 * representa os dias da semana e horários indisponíveis
 * @author biharck
 *
 */
@Table(name = "indispagendacolaborador")
@Entity
public class IndispAgendaColaborador {
	
	private Integer idIndispAgendaColaborador;
	private Integer diaSemana;
	private String hora;
	private ColaboradorAgenda colaboradorAgenda;
	
	public IndispAgendaColaborador(){}
	public IndispAgendaColaborador(Integer diaSemana, String hora, ColaboradorAgenda colaboradorAgenda){
		this.diaSemana = diaSemana;
		this.hora = hora;
		this.colaboradorAgenda = colaboradorAgenda;
	}
	
	//GET
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getIdIndispAgendaColaborador() {
		return idIndispAgendaColaborador;
	}
	@Required
	public Integer getDiaSemana() {
		return diaSemana;
	}
	@Required
	@Column(length=5)
	public String getHora() {
		return hora;
	}
	@JoinColumn(name = "idColaboradorAgenda")
	@ManyToOne(fetch = FetchType.LAZY)
	@ForeignKey(name = "fk_indispagendacol")
	public ColaboradorAgenda getColaboradorAgenda() {
		return colaboradorAgenda;
	}
	
	//SET
	public void setIdIndispAgendaColaborador(Integer idIndispAgendaColaborador) {
		this.idIndispAgendaColaborador = idIndispAgendaColaborador;
	}
	public void setDiaSemana(Integer diaSemana) {
		this.diaSemana = diaSemana;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public void setColaboradorAgenda(ColaboradorAgenda colaboradorAgenda) {
		this.colaboradorAgenda = colaboradorAgenda;
	}
	
	
	
}

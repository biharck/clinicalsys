package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name = "uf")
public class UnidadeFederativa {
	
	private Integer idUF;
	private Pais pais;
	private String nome;
	private String sigla;
	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdUF() {
		return idUF;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="idPais")
	@ForeignKey(name = "fk_uf_pais")
	@Required
	public Pais getPais() {
		return pais;
	}
	@Required
	@MinLength(2)
	@MaxLength(100)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	@Required
	@MinLength(2)
	@MaxLength(2)
	public String getSigla() {
		return sigla;
	}
	
	//SET
	public void setIdUF(Integer idUF) {
		this.idUF = idUF;
	}
	public void setPais(Pais pais) {
		this.pais = pais;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	
	

}

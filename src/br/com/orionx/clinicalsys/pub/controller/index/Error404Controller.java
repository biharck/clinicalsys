package br.com.orionx.clinicalsys.pub.controller.index;

import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.web.servlet.ModelAndView;

@Controller(path="/pub/404")
public class Error404Controller extends MultiActionController {

	
    @DefaultAction
    public ModelAndView doPage(WebRequestContext request){
        return new ModelAndView("erro404");
    }
}
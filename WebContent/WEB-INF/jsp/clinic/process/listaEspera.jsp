<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<style type="text/css">
.portlet {
	margin: 5px 5px 5px 5px;
}
</style>

<script>
	function atualizaAgendamento(idAtendimento,idStatus){
		openDialogAguarde();
		
		$.getJSON('/ClinicalSys/clinic/process/listaEspera?ACAO=updateListaEspera',{'idAtendimento':idAtendimento,'idStatus':idStatus},
		  	function(data){
	  			closeDialogAguarde();
	  			openDialogInformation(data.msg);
			});
	} 
	$(function() {
		$( ".column" ).sortable({
			connectWith: ".column",
			stop: function(event, ui){
				console.log('stop');
				console.log($(ui.item).attr('id'));
				atualizaAgendamento($(ui.item).attr('id'),2);
				//console.log(event);
				//console.log(ui);
			},
			update: function(event, ui) {
			
				console.log('update');
				//console.log(ui.item);
				//console.log($(ui.item).attr('id'));
				//console.log(ui.sender);
				//console.log($(ui.item));
				
			
				var items = $( ".column" ).sortable( "option", "items" );
				//console.log(items);
				
				separaCoresDivs();
			}
			
		});
		
 
		$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
			.find( ".portlet-header" )
				.addClass( "ui-widget-header ui-corner-all" )
				.prepend( "<span class='ui-icon ui-icon-plusthick'></span>")
				.end()
			.find( ".portlet-content" );
 
		$( ".portlet-header .ui-icon" ).click(function() {
			$( this ).toggleClass( "ui-icon-plusthick" ).toggleClass( "ui-icon-minusthick" );
			$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
		});
 
		$( ".column" ).disableSelection();
	});

	$(document).ready(function(){
		separaCoresDivs();
	});
	$(function() {
		$('a[rel=modalFrame]').click(function(e) {
			e.preventDefault();
			var $this = $(this);
			var horizontalPadding = 30;
			var verticalPadding = 30;
	        $('<iframe id="externalSite" class="externalSite" src="' + this.href + '" />').dialog({
	            title: ($this.attr('title')) ? $this.attr('title') : '',
	            autoOpen: true,
	            width: 980,
	            height: 700,
	            modal: true,
	            resizable: true,
				autoResize: true,
	            overlay: {
	                opacity: 0.5,
	                background: "black"
	            }
	        }).width(1000 - horizontalPadding).height(630 - verticalPadding);	        
		});
	});
	function closeIframe(){
	   var iframe = document.getElementById('externalSite');
	   iframe.parentNode.removeChild(iframe);
	   openDialogAguarde();
	   location.href = location.href;
	}
	function separaCoresDivs(){
		$('#aguardando_atendimento .ui-widget-header').css('background-color','#353C42');
		$('#nao_chegou .ui-widget-header').css('background-color','#856b1c');
		$('#desistencia .ui-widget-header').css('background-color','brown');
	}
	
</script>
<style type="text/css">
.ui-widget-header{
	background-image: none;
}
#aguardando_atendimento{
	border: 1px solid #AAA;
	background-color: #353C42;
	color: #222;
	border-radius: 4px; 
}

#nao_chegou{
	border: 1px solid #AAA;
	background-color: #353C42;
	color: #222;
	border-radius: 4px;
}

#desistencia{
	border: 1px solid #AAA;
	background-color: #353C42;
	color: #222;
	border-radius: 4px;
}

</style>
</head> 
<body> 
<div id="dialog">
</div>
<div class="demo" id="principal">
	<br /> 
	<br /> 
 	
	<div class="column" id="aguardando_atendimento">
		<div align="center" style="background-color: #353C42;color:#fff;font-weight: bold;font-family: inherit;">Aguardando atendimento</div>
	 	<c:forEach items="${aguardandoAtendimento}" var="agendamentoConsulta">
			<div class="portlet" id="${agendamentoConsulta.idAgendamentoConsulta}">
				<div class="portlet-header">${agendamentoConsulta.paciente.nome} | ${agendamentoConsulta.hora}</div> 
				<div class="portlet-content" style="display:none;">
					<div>
						<div class="leftcolumn">
							<c:if test="${empty agendamentoConsulta.paciente.fotoPaciente.cdfile}">
								<img src="${pageContext.request.contextPath}/images/peopleNotFound.png" id="imagem" style="padding-top:13px;" title="Fulano de tal"/>
							</c:if>
							<c:if test="${!empty agendamentoConsulta.paciente.fotoPaciente.cdfile}">
								<img src="${pageContext.request.contextPath}/DOWNLOADFILE/${agendamentoConsulta.paciente.fotoPaciente.cdfile}" id="imagem" class="foto"/><br />
							</c:if>
						</div>
						<div class="rightcolumn">
							<h3><b><a rel="modalFrame" href="/ClinicalSys/clinic/pag/Paciente?ACAO=editar&idPaciente=${agendamentoConsulta.paciente.idPaciente}&onePath=true">&nbsp;${agendamentoConsulta.paciente.nome}</a></b></h3>
							<div class="dotted-class"></div>
							<p>&nbsp; ${agendamentoConsulta.nascimento}</p>
							<p>&nbsp; ${agendamentoConsulta.paciente.nome}</p>
							<c:if test="${empty agendamentoConsulta.colaborador.foto.cdfile}">
								<img src="${pageContext.request.contextPath}/images/peopleNotFound.png" id="imagem_listagem" style="padding-top:13px;" title="Fulano de tal"/>&nbsp;Dr. Fulano de tal<br />
							</c:if>
							<c:if test="${!empty agendamentoConsulta.colaborador.foto.cdfile}">
								<img src="${pageContext.request.contextPath}/DOWNLOADFILE/${agendamentoConsulta.colaborador.foto.cdfile}" id="imagem_listagem" class="foto"/>&nbsp;Dr. Fulano de tal<br />
							</c:if>
							<p>&nbsp;<img src="${agendamentoConsulta.situacaoURL}">${agendamentoConsulta.situacaoDESC }</p>
						</div>
					</div>
					<div class="clear"></div>
					<div class="bottom_collumn">
						<h3>Plano de Sa�de</h3>
						<p>&nbsp; ${agendamentoConsulta.plano.nome}</p>
						<p>&nbsp; ${agendamentoConsulta.plano.operadora.nomeFantasia}</p>
						<br />
						<c:if test="${empty agendamentoConsulta.paciente.rg }">
							<h3>Aten��o</h3>
							<p><b><font style="color:red">CADASTRO INCOMPLETO</font></b></p><br />
							<a rel="modalFrame" href="/ClinicalSys/clinic/pag/Paciente?ACAO=editar&idPaciente=${agendamentoConsulta.paciente.idPaciente}&onePath=true">
								<b>
									<font style="color:red">Clique aqui</font>
								</b>
							</a> para completar o cadastro<br />
						</c:if>
						<h3>Motivo da �ltima consulta</h3>
						<p> paciente com dor de kbe�a NA PARTE FONTAL COM DIAGNOSTICO PRECORCE no dia 10/10/2000 com o m�dico JOSE</p><br />
						<div class="dotted-class"></div>
						<br />
						<h3>Queixa Atual</h3>
						<p> ${agendamentoConsulta.observacoes}</p>
					</div>
				</div> 
			</div>
	 	</c:forEach>
	</div> 
	 
	<div class="column" id="nao_chegou">
		<div align="center" style="background-color: #856B1C;color:#fff;font-weight: bold;font-family: inherit;">N�o Chegou</div>  
	 	<c:forEach items="${naoChegou}" var="agendamentoConsulta">
			<div class="portlet" id="${agendamentoConsulta.idAgendamentoConsulta}"> 
				<div class="portlet-header">${agendamentoConsulta.paciente.nome} | ${agendamentoConsulta.hora}</div> 
				<div class="portlet-content" style="display:none;">
					<div>
						<div class="leftcolumn">
							<c:if test="${empty agendamentoConsulta.paciente.fotoPaciente.cdfile}">
								<img src="${pageContext.request.contextPath}/images/peopleNotFound.png" id="imagem" style="padding-top:13px;" title="Fulano de tal"/>
							</c:if>
							<c:if test="${!empty agendamentoConsulta.paciente.fotoPaciente.cdfile}">
								<img src="${pageContext.request.contextPath}/DOWNLOADFILE/${agendamentoConsulta.paciente.fotoPaciente.cdfile}" id="imagem" class="foto"/><br />
							</c:if>
						</div>
						<div class="rightcolumn">
							<h3><b><a rel="modalFrame" href="/ClinicalSys/clinic/pag/Paciente?ACAO=editar&idPaciente=${agendamentoConsulta.paciente.idPaciente}&onePath=true">&nbsp;${agendamentoConsulta.paciente.nome}</a></b></h3>
							<div class="dotted-class"></div>
							<p>&nbsp; ${agendamentoConsulta.nascimento}</p>
							<p>&nbsp; ${agendamentoConsulta.paciente.nome}</p>
							<c:if test="${empty agendamentoConsulta.colaborador.foto.cdfile}">
								<img src="${pageContext.request.contextPath}/images/peopleNotFound.png" id="imagem_listagem" style="padding-top:13px;" title="Fulano de tal"/>&nbsp;Dr. Fulano de tal<br />
							</c:if>
							<c:if test="${!empty agendamentoConsulta.colaborador.foto.cdfile}">
								<img src="${pageContext.request.contextPath}/DOWNLOADFILE/${agendamentoConsulta.colaborador.foto.cdfile}" id="imagem_listagem" class="foto"/>&nbsp;Dr. Fulano de tal<br />
							</c:if>
							<p>&nbsp;<img src="${agendamentoConsulta.situacaoURL}">${agendamentoConsulta.situacaoDESC }</p>
						</div>
					</div>
					<div class="clear"></div>
					<div class="bottom_collumn">
						<h3>Plano de Sa�de</h3>
						<p>&nbsp; ${agendamentoConsulta.plano.nome}</p>
						<p>&nbsp; ${agendamentoConsulta.plano.operadora.nomeFantasia}</p>
						<br />
						<c:if test="${empty agendamentoConsulta.paciente.rg }">
							<h3>Aten��o</h3>
							<p><b><font style="color:red">CADASTRO INCOMPLETO</font></b></p><br />
							<a rel="modalFrame" href="/ClinicalSys/clinic/pag/Paciente?ACAO=editar&idPaciente=${agendamentoConsulta.paciente.idPaciente}&onePath=true">
								<b>
									<font style="color:red">Clique aqui</font>
								</b>
							</a> para completar o cadastro<br />
						</c:if>
						<h3>Motivo da �ltima consulta</h3>
						<p> paciente com dor de kbe�a NA PARTE FONTAL COM DIAGNOSTICO PRECORCE no dia 10/10/2000 com o m�dico JOSE</p><br />
						<div class="dotted-class"></div>
						<br />
						<h3>Queixa Atual</h3>
						<p> ${agendamentoConsulta.observacoes}</p>
					</div>
				</div> 
			</div>
		</c:forEach>
	</div> 
	 
	<div class="column" id="desistencia">
		<div align="center" style="background-color: brown;color:#fff;font-weight: bold;font-family: inherit;">Desist�ncias</div> 
	 	<c:forEach items="${desistencia}" var="agendamentoConsulta" >
			<div class="portlet" id="${agendamentoConsulta.idAgendamentoConsulta}">
				<div class="portlet-header">${agendamentoConsulta.paciente.nome} | ${agendamentoConsulta.hora}</div> 
				<div class="portlet-content" style="display:none;">
					<div>
						<div class="leftcolumn">
							<c:if test="${empty agendamentoConsulta.paciente.fotoPaciente.cdfile}">
								<img src="${pageContext.request.contextPath}/images/peopleNotFound.png" id="imagem" style="padding-top:13px;" title="Fulano de tal"/>
							</c:if>
							<c:if test="${!empty agendamentoConsulta.paciente.fotoPaciente.cdfile}">
								<img src="${pageContext.request.contextPath}/DOWNLOADFILE/${agendamentoConsulta.paciente.fotoPaciente.cdfile}" id="imagem" class="foto"/><br />
							</c:if>
						</div>
						<div class="rightcolumn">
							<h3><b><a rel="modalFrame" href="/ClinicalSys/clinic/pag/Paciente?ACAO=editar&idPaciente=${agendamentoConsulta.paciente.idPaciente}&onePath=true">&nbsp;${agendamentoConsulta.paciente.nome}</a></b></h3>
							<div class="dotted-class"></div>
							<p>&nbsp; ${agendamentoConsulta.nascimento}</p>
							<p>&nbsp; ${agendamentoConsulta.paciente.nome}</p>
							<c:if test="${empty agendamentoConsulta.colaborador.foto.cdfile}">
								<img src="${pageContext.request.contextPath}/images/peopleNotFound.png" id="imagem_listagem" style="padding-top:13px;" title="Fulano de tal"/>&nbsp;Dr. Fulano de tal<br />
							</c:if>
							<c:if test="${!empty agendamentoConsulta.colaborador.foto.cdfile}">
								<img src="${pageContext.request.contextPath}/DOWNLOADFILE/${agendamentoConsulta.colaborador.foto.cdfile}" id="imagem_listagem" class="foto"/>&nbsp;Dr. Fulano de tal<br />
							</c:if>
							<p>&nbsp;<img src="${agendamentoConsulta.situacaoURL}">${agendamentoConsulta.situacaoDESC }</p>
						</div>
					</div>
					<div class="clear"></div>
					<div class="bottom_collumn">
						<h3>Plano de Sa�de</h3>
						<p>&nbsp; ${agendamentoConsulta.plano.nome}</p>
						<p>&nbsp; ${agendamentoConsulta.plano.operadora.nomeFantasia}</p>
						<br />
						<c:if test="${empty agendamentoConsulta.paciente.rg }">
							<h3>Aten��o</h3>
							<p><b><font style="color:red">CADASTRO INCOMPLETO</font></b></p><br />
							<a rel="modalFrame" href="/ClinicalSys/clinic/pag/Paciente?ACAO=editar&idPaciente=${agendamentoConsulta.paciente.idPaciente}&onePath=true">
								<b>
									<font style="color:red">Clique aqui</font>
								</b>
							</a> para completar o cadastro<br />
						</c:if>
						<h3>Motivo da �ltima consulta</h3>
						<p> paciente com dor de kbe�a NA PARTE FONTAL COM DIAGNOSTICO PRECORCE no dia 10/10/2000 com o m�dico JOSE</p><br />
						<div class="dotted-class"></div>
						<br />
						<h3>Queixa Atual</h3>
						<p> ${agendamentoConsulta.observacoes}</p>
					</div>
				</div> 
			</div>
		</c:forEach>
	</div> 
 
</div><!-- End demo --> 
 
<br class="clear"/>
package br.com.orionx.clinicalsys.filtros;

import org.nextframework.validation.annotation.MaxLength;

import br.com.orionx.clinicalsys.bean.CategoriaProdutos;
import br.com.orionx.clinicalsys.util.generics.FiltroListagem;

public class ProdutoServicoFiltro extends FiltroListagem {

	private String codigo;//60
	private String descricao;//120
	private Integer CFPO;//4
	private AtivoEnum ativoEnum;
	private Boolean produtoOuServico;
	private CategoriaProdutos categoriaProdutos;
	
	//GET
	@MaxLength(6)
	public String getCodigo() {
		return codigo;
	}
	@MaxLength(50)
	public String getDescricao() {
		return descricao;
	}
	@MaxLength(4)
	public Integer getCFPO() {
		return CFPO;
	}
	public AtivoEnum getAtivoEnum() {
		return ativoEnum;
	}
	public Boolean getProdutoOuServico() {
		return produtoOuServico;
	}
	public CategoriaProdutos getCategoriaProdutos() {
		return categoriaProdutos;
	}
	
	//SET
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setCFPO(Integer cFPO) {
		CFPO = cFPO;
	}
	public void setAtivoEnum(AtivoEnum ativoEnum) {
		this.ativoEnum = ativoEnum;
	}
	public void setProdutoOuServico(Boolean produtoOuServico) {
		this.produtoOuServico = produtoOuServico;
	}
	public void setCategoriaProdutos(CategoriaProdutos categoriaProdutos) {
		this.categoriaProdutos = categoriaProdutos;
	}
}

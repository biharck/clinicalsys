package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name= "doencasdainfaciapaciente")
public class DoencasDaInfanciaPaciente {
	
	private Integer idDoencasDaInfanciaPaciente;
	private DoencasDaInfancia doencasDaInfancia;
	private HistoriaPregressa historiaPregressa;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getIdDoencasDaInfanciaPaciente() {
		return idDoencasDaInfanciaPaciente;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idDoencasDaInfancia")
	@ForeignKey(name = "fk_demais_histpregres")
	public DoencasDaInfancia getDoencasDaInfancia() {
		return doencasDaInfancia;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idHistoriaPregressa")
	@ForeignKey(name = "fk_pct_historiapreg")
	public HistoriaPregressa getHistoriaPregressa() {
		return historiaPregressa;
	}
	
	public void setIdDoencasDaInfanciaPaciente(
			Integer idDoencasDaInfanciaPaciente) {
		this.idDoencasDaInfanciaPaciente = idDoencasDaInfanciaPaciente;
	}
	public void setDoencasDaInfancia(DoencasDaInfancia doencasDaInfancia) {
		this.doencasDaInfancia = doencasDaInfancia;
	}
	public void setHistoriaPregressa(HistoriaPregressa historiaPregressa) {
		this.historiaPregressa = historiaPregressa;
	}

}

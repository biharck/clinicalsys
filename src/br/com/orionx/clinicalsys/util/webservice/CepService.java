package br.com.orionx.clinicalsys.util.webservice;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Properties;

import javax.persistence.Id;

import org.nextframework.util.ReflectionCache;
import org.nextframework.util.ReflectionCacheFactory;
import org.nextframework.util.Util;

import br.com.orionx.clinicalsys.bean.Medicamento;

import com.thoughtworks.xstream.XStream;
/**
 * 
 * 
 * @author user
 * chave 1a8wmri2C8yR4a3fnXuHyOBJ6v4vnq/
 *
 */
public class CepService {
	private static String key= "1a8wmri2C8yR4a3fnXuHyOBJ6v4vnq/";

	public static WebServiceCEP findAddress(String cep) {
		
		// String da url		
		String urlString = "http://www.buscarcep.com.br/index.php";

		// Parametros a serem enviados
		Properties parameters = new Properties();
		parameters.setProperty("cep", cep);
		parameters.setProperty("formato", "xml");
		
		 

		// Iterador
		Iterator i = parameters.keySet().iterator();
		// Contador
		int counter = 0;

		// Enquanto ainda existir parametros
		while (i.hasNext()) {

			// Nome
			String name = (String) i.next();
			// Valor
			String value = parameters.getProperty(name);

			// Adiciona com um conector (? ou &)
			// Separa a url por ?, e as vari�veis com &
			urlString += (++counter == 1 ? "?" : "&") + name + "=" + value;
			
		}
		WebServiceCEP serviceCEP = null;

		try {
			// Objeto URL
			URL url = new URL(urlString+"&chave="+key);

			// Objeto HttpURLConnection
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// M�todo
			connection.setRequestProperty("Request-Method", "GET");

			// V�ariavel do resultado
			connection.setDoInput(true);
			connection.setDoOutput(false);

			
			
			// Faz a conex�o
			connection.connect();

			
			// Abre a conex�o
			BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(),Charset.forName("UTF-8") ));

			// L� a conex�o
			StringBuffer newData = new StringBuffer();
			String s = "";
			while (null != ((s = br.readLine()))) {
				newData.append(s);
			}
			br.close();

            String comHtml = new String(newData);
            serviceCEP = getResultado(comHtml);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return serviceCEP;

	}
	/**
	 * @param serviceCEP
	 * @return
	 */
	public static WebServiceCEP findCEP(WebServiceCEP serviceCEP) {
		XStream xstream = new XStream();
		WebServiceCEP webservicecep = new WebServiceCEP();

		xstream.alias("webservicecep", WebServiceCEP.class);
		xstream.alias("retorno", Retorno.class);
		xstream.aliasField("tipo_logradouro", Retorno.class, "tipoLogradouro");
		xstream.aliasField("resultado_txt", Retorno.class, "resultadoTxt");
		xstream.aliasField("limite_buscas", Retorno.class, "limiteBuscas");
		xstream.aliasField("ibge_uf", Retorno.class, "ibgeUf");
		xstream.aliasField("ibge_municipio", Retorno.class, "ibgeMunicipio");
		xstream.aliasField("ibge_municipio_verificador", Retorno.class, "ibgeMunicipioVerificador");
		
//		webservicecep.setQuantidade("1");
//		Retorno retorno = new Retorno("","MG","Belo Horizonte","Castelo","Rua","Castelo Lamego","2010-07-09 03:00:47","1","sucesso","10","31","310620","3106200");
//		webservicecep.setRetorno(retorno);
		
		//webservicecep = findCEP(webservicecep);

		// String da url		
		String urlString = "http://www.buscarcep.com.br/index.php?tipo_logradouro="+serviceCEP.getRetorno().getTipoLogradouro().replace(" ", "%20")+
		"&logradouro="+serviceCEP.getRetorno().getLogradouro().replace(" ", "%20")+"&cidade="+serviceCEP.getRetorno().getCidade().replace(" ", "%20")+
		"&bairro="+serviceCEP.getRetorno().getBairro().replace(" ", "%20")+"&formato=xml&chave="+key;

		try {
			// Objeto URL
			URL url = new URL(urlString);

			// Objeto HttpURLConnection
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// M�todo
			connection.setRequestProperty("Request-Method", "GET");

			// V�ariavel do resultado
			connection.setDoInput(true);
			connection.setDoOutput(false);

			// Faz a conex�o
			connection.connect();

			// Abre a conex�o
			BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(),Charset.forName("UTF-8") ));

			// L� a conex�o
			StringBuffer newData = new StringBuffer();
			String s = "";
			while (null != ((s = br.readLine()))) {
				newData.append(s);
			}
			br.close();

            String comHtml = new String(newData);
            serviceCEP = getResultado(comHtml);
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		return serviceCEP;

	}
	
	public static WebServiceCEP getResultado(String xml){
		XStream xstream = new XStream();
		
		xstream.alias("webservicecep", WebServiceCEP.class);
		xstream.alias("retorno", Retorno.class);
		xstream.aliasField("tipo_logradouro", Retorno.class, "tipoLogradouro");
		xstream.aliasField("resultado_txt", Retorno.class, "resultadoTxt");
		xstream.aliasField("limite_buscas", Retorno.class, "limiteBuscas");
		xstream.aliasField("ibge_uf", Retorno.class, "ibgeUf");
		xstream.aliasField("ibge_municipio", Retorno.class, "ibgeMunicipio");
		xstream.aliasField("ibge_municipio_verificador", Retorno.class, "ibgeMunicipioVerificador");
		return (WebServiceCEP) xstream.fromXML(xml);
	}
	
	public static void main(String[] args) {
		ReflectionCache reflectionCache = ReflectionCacheFactory.getReflectionCache();
		Method[] methods = reflectionCache.getMethods(Medicamento.class);
		Method methodId = null;
		for (Method method : methods) {
			if(reflectionCache.isAnnotationPresent(method, Id.class)){
				methodId = method;
				break;
			}
		}
		String propertyFromGetter = Util.beans.getPropertyFromGetter(methodId.getName());
		System.out.println(propertyFromGetter);
		
	}

}

package br.com.orionx.clinicalsys.dao;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

import br.com.orionx.clinicalsys.bean.AgendamentoConsulta;
import br.com.orionx.clinicalsys.bean.Colaborador;
import br.com.orionx.clinicalsys.bean.ColaboradorAgenda;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class AgendamentoConsultaDAO extends GenericDAO<AgendamentoConsulta>{
	
	/**
	 * <p>M�todo que busca todas as consultas agendadas
	 * @return {@link List} {@link AgendamentoConsulta}
	 * @author biharck
	 */
	public List<AgendamentoConsulta> getConsultasAgendadas(){
		return query().select("agendamentoConsulta.idAgendamentoConsulta,agendamentoConsulta.paciente,agendamentoConsulta.colaborador," +
				"agendamentoConsulta.plano,agendamentoConsulta.dia,agendamentoConsulta.hora,agendamentoConsulta.horaFim,agendamentoConsulta.observacoes," +
				"agendamentoConsulta.situacao,agendamentoConsulta.horaChegada,agendamentoConsulta.horaAtendimento,agendamentoConsulta.retorno," +
				"agendamentoConsulta.remarcacao,agendamentoConsulta.idUsuarioAltera,agendamentoConsulta.dtAltera,agendamentoConsulta.idUsuarioInclusao," +
				"agendamentoConsulta.dtInclusao,agendamentoConsulta.allDay")
				.list();
	}

	/**
	 * <p>M�todo que busca todas as consultas agendadas
	 * @return {@link List} {@link AgendamentoConsulta}
	 * @param Colaborador c {@link Colaborador}
	 * @author biharck
	 */
	public List<AgendamentoConsulta> getConsultasAgendadasByColaborador(Colaborador c){
		return query()
		.select("agendamentoConsulta.idAgendamentoConsulta,agendamentoConsulta.paciente," +
				"agendamentoConsulta.plano,agendamentoConsulta.dia,agendamentoConsulta.hora,agendamentoConsulta.horaFim,agendamentoConsulta.observacoes," +
				"agendamentoConsulta.situacao,agendamentoConsulta.horaChegada,agendamentoConsulta.horaAtendimento,agendamentoConsulta.retorno," +
				"agendamentoConsulta.remarcacao,agendamentoConsulta.idUsuarioAltera,agendamentoConsulta.dtAltera,agendamentoConsulta.idUsuarioInclusao," +
		"agendamentoConsulta.dtInclusao,agendamentoConsulta.allDay")
		.leftOuterJoin("agendamentoConsulta.colaborador c")
		.where("c.idColaborador=?",c.getIdColaborador())
		.list();
	}
	
	/**
	 * <p>M�todo que busca todas as consultas agendadas
	 * @return {@link List} {@link AgendamentoConsulta}
	 * @param Colaborador c {@link Colaborador}
	 * @author biharck
	 */
	public List<AgendamentoConsulta> getConsultasAgendadasByColaboradorAndDate(Colaborador c,Date date){
		return query()
		.select("agendamentoConsulta.idAgendamentoConsulta,agendamentoConsulta.paciente,agendamentoConsulta.colaborador," +
				"agendamentoConsulta.plano,agendamentoConsulta.dia,agendamentoConsulta.hora,agendamentoConsulta.horaFim,agendamentoConsulta.observacoes," +
				"agendamentoConsulta.situacao,agendamentoConsulta.horaChegada,agendamentoConsulta.horaAtendimento,agendamentoConsulta.retorno," +
				"agendamentoConsulta.remarcacao,agendamentoConsulta.idUsuarioAltera,agendamentoConsulta.dtAltera,agendamentoConsulta.idUsuarioInclusao," +
		"agendamentoConsulta.dtInclusao,agendamentoConsulta.allDay")
		.where("agendamentoConsulta.colaborador=?",c)
		.where("agendamentoConsulta.dia=?",date)
		.where("agendamentoConsulta.situacao <> 5")//diferente de cancelado...
		.list();
	}
	
	/**
	 * <p>M�todo que busca uma determinada consulta pelo seu id
	 * @return @link AgendamentoConsulta}
	 * @author biharck
	 */
	public AgendamentoConsulta getConsultaAgendadaById(Integer id){
		return query().select("agendamentoConsulta.idAgendamentoConsulta," +
				"plano.idPlano, plano.nome ,agendamentoConsulta.dia,agendamentoConsulta.hora,agendamentoConsulta.horaFim,agendamentoConsulta.observacoes," +
				"agendamentoConsulta.situacao,agendamentoConsulta.horaChegada,agendamentoConsulta.horaAtendimento,agendamentoConsulta.retorno," +
				"agendamentoConsulta.remarcacao,agendamentoConsulta.idUsuarioAltera,agendamentoConsulta.dtAltera,agendamentoConsulta.idUsuarioInclusao," +
				"agendamentoConsulta.dtInclusao,operadora.idOperadora, operadora.nomeFantasia,agendamentoConsulta.allDay," +
				"colaborador.idColaborador, paciente.idPaciente, paciente.nome")
				.leftOuterJoin("agendamentoConsulta.plano plano")
				.leftOuterJoin("plano.operadora operadora")
				.leftOuterJoin("agendamentoConsulta.paciente paciente")
				.leftOuterJoin("agendamentoConsulta.colaborador colaborador")
				.where("agendamentoConsulta.idAgendamentoConsulta=?",id)
				.unique();
	}
	
	/**
	 * <p>M�todo respons�vel em recuperar algum agendamento casa exista para uma determinada data e hora de determinado colaborador
	 * @param date {@link Date} data da consulta
	 * @param ca {@link ColaboradorAgenda}
	 * @param hora {@link Time} hora da consulta
	 * @return {@link AgendamentoConsulta}
	 */
	public AgendamentoConsulta getAgendamentoByHorarioAndDia(Date date, ColaboradorAgenda ca, Time hora){
		return query().select("agendamentoConsulta")
					.where("agendamentoConsulta.dia=?",date)
					.where("agendamentoConsulta.colaborador=?",ca.getColaborador())
					.where("agendamentoConsulta.hora=?",hora)
					.where("agendamentoConsulta.situacao <> 5")
					.where("agendamentoConsulta.situacao <> 6")
					.unique();
	}
	
	
	/**
	 * <p>M�todo que busca todas as consultas agendadas
	 * @return {@link List} {@link AgendamentoConsulta}
	 * @author biharck
	 */
	public List<AgendamentoConsulta> getConsultasAgendadasDiaCorrente(){
		return query()
				.select("agendamentoConsulta.idAgendamentoConsulta,paciente.idPaciente, paciente.nome,paciente.dtnascimento,paciente.rg," +
				"agendamentoConsulta.dia,agendamentoConsulta.hora,agendamentoConsulta.horaFim,agendamentoConsulta.observacoes," +
				"agendamentoConsulta.situacao,agendamentoConsulta.horaChegada,agendamentoConsulta.horaAtendimento,agendamentoConsulta.retorno," +
				"agendamentoConsulta.remarcacao,agendamentoConsulta.idUsuarioAltera,agendamentoConsulta.dtAltera,agendamentoConsulta.idUsuarioInclusao," +
				"agendamentoConsulta.dtInclusao,agendamentoConsulta.allDay," +
				"fotoPaciente.cdfile, fotoPaciente.contenttype, fotoPaciente.name, fotoPaciente.size, fotoPaciente.content," +
				"telefones.idTelefonePaciente, telefones.telefone, telefones.telefoneTipo, plano.idPlano, plano.nome, operadora.idOperadora, operadora.nomeFantasia")
				.leftOuterJoin("agendamentoConsulta.paciente paciente")
				.leftOuterJoin("paciente.fotoPaciente fotoPaciente")
				.leftOuterJoin("paciente.listaTelefonePaciente telefones")
				.leftOuterJoin("agendamentoConsulta.plano plano")
				.leftOuterJoin("plano.operadora operadora")
				.leftOuterJoin("agendamentoConsulta.colaborador colaborador")
				.where("agendamentoConsulta.dia=?",new Date(System.currentTimeMillis()))
				.orderBy("agendamentoConsulta.hora")
				.list();
	}
	
	/**
	 * <p>M�todo que busca todas as consultas agendadas que n�o passaram ainda
	 * @return {@link List} {@link AgendamentoConsulta}
	 * @param Colaborador c {@link Colaborador}
	 * @author biharck
	 */
	public List<AgendamentoConsulta> getConsultasAgendadasByColaboradorSuperiores(Colaborador c){
		return query()
		.select("agendamentoConsulta.idAgendamentoConsulta,agendamentoConsulta.paciente,agendamentoConsulta.colaborador," +
				"agendamentoConsulta.plano,agendamentoConsulta.dia,agendamentoConsulta.hora,agendamentoConsulta.horaFim,agendamentoConsulta.observacoes," +
				"agendamentoConsulta.situacao,agendamentoConsulta.horaChegada,agendamentoConsulta.horaAtendimento,agendamentoConsulta.retorno," +
				"agendamentoConsulta.remarcacao,agendamentoConsulta.idUsuarioAltera,agendamentoConsulta.dtAltera,agendamentoConsulta.idUsuarioInclusao," +
		"agendamentoConsulta.dtInclusao,agendamentoConsulta.allDay")
		.where("agendamentoConsulta.colaborador=?",c)
		.where("agendamentoConsulta.dia >= ?", new Date(System.currentTimeMillis()))
		.list();
	}
	
}

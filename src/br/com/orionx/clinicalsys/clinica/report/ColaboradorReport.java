package br.com.orionx.clinicalsys.clinica.report;

import java.awt.Image;
import java.io.IOException;

import org.nextframework.authorization.report.ReportAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.report.IReport;
import org.nextframework.report.Report;
import org.nextframework.util.NextImageResolver;

import br.com.orionx.clinicalsys.filtros.ColaboradorFiltro;
import br.com.orionx.clinicalsys.service.ColaboradorService;
import br.com.orionx.clinicalsys.util.ClinicalSysReport;

@Controller(path = "/clinic/report/colaboradores", authorizationModule = ReportAuthorizationModule.class)
public class ColaboradorReport extends ClinicalSysReport<ColaboradorFiltro>{

	private NextImageResolver nextImageResolver;
	private ColaboradorService colaboradorService;
	
	public void setNextImageResolver(NextImageResolver nextImageResolver) {
		this.nextImageResolver = nextImageResolver;
	}
	public void setColaboradorService(ColaboradorService colaboradorService) {
		this.colaboradorService = colaboradorService;
	}
	
	@Override
	public IReport createReportClinicalSys(WebRequestContext request, ColaboradorFiltro filtro) throws Exception {
		
		Image logoClinical = null;
		Image logoEmpresa = null;
		try {
			logoClinical = nextImageResolver.getImage("/images/logo.png");
			logoEmpresa = nextImageResolver.getImage("/images/logo.png");
		} catch (IOException e) {
			e.printStackTrace();
		}
		Report report = new Report("colaborador");
		report.addParameter("logoClinical", logoClinical);
		report.addParameter("logoEmpresa", logoEmpresa);
		report.setDataSource(colaboradorService.getColaboradoresReport(filtro));
		report.addParameter("titulo",getTitulo());
		return report;
	}

	@Override
	public String getTitulo() {
		return "Relatório de Colaboradores";
	}

	
}

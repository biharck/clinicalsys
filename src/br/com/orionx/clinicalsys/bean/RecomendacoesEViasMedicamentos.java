package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "receviasmedicamentos")
public class RecomendacoesEViasMedicamentos {

	private Integer idRecomendacoesEViasMedicamentos;
	private RecomendacaoMedicamentosa recomendacaoMedicamentosa;
	private ViaAdministracao viaAdministracao;
	private Medicamento medicamento;
	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdRecomendacoesEViasMedicamentos() {
		return idRecomendacoesEViasMedicamentos;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "idUsuarioAltera")
	public RecomendacaoMedicamentosa getRecomendacaoMedicamentosa() {
		return recomendacaoMedicamentosa;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "idViaAdministracao")
	public ViaAdministracao getViaAdministracao() {
		return viaAdministracao;
	}
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "idMedicamento")
	public Medicamento getMedicamento() {
		return medicamento;
	}
	
	
	//SET
	public void setIdRecomendacoesEViasMedicamentos(Integer idRecomendacoesEViasMedicamentos) {
		this.idRecomendacoesEViasMedicamentos = idRecomendacoesEViasMedicamentos;
	}
	public void setRecomendacaoMedicamentosa(RecomendacaoMedicamentosa recomendacaoMedicamentosa) {
		this.recomendacaoMedicamentosa = recomendacaoMedicamentosa;
	}
	public void setViaAdministracao(ViaAdministracao viaAdministracao) {
		this.viaAdministracao = viaAdministracao;
	}
	public void setMedicamento(Medicamento medicamento) {
		this.medicamento = medicamento;
	}
	
	
}

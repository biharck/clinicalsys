<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@ taglib prefix="cs" uri="clinicalsys"%>

<t:entrada >
	<t:property name="idCargo" type="hidden" write="false" label=""/>
	<t:janelaEntrada>
		<t:tabelaEntrada >
			<t:property name="nome" id="nome" renderAs="doubleline" colspan="2"/>
			<t:property name="codigoCBO" renderAs="doubleline" colspan="2"/>
			<t:property name="descricao" style="width:500px;" renderAs="doubleline" colspan="2" cols="5"/>
			<n:group legend="Papeis *" colspan="2">
				<n:panel propertyRenderAsDouble="true">
					<cs:checklist name="papeis" renderas="double" itens="${listaPapel}" label=" " /><span id="textoFieldSet"></span>
				</n:panel>
			</n:group>
			<t:property name="ativo" renderAs="doubleline" colspan="2"/>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>
<script type="text/javascript">
	function validaPapeis(){
		var checado = false;
		$('input[name*=papeis]').each(function(){
			if(this.checked){
				 checado = true;
				 return;
			} 
		});
		if(!checado){
			$('form[name=form] fieldset').css('background','#B81900 url(/ClinicalSys/images/requiredicon.png) repeat 50% 50%').css('border','1px solid #CC0000');
			$('#textoFieldSet').html('&nbsp;&raquo;&nbsp;� necess�rio vincular papeis para este cargo.').css('color','#B81900');
			closeDialogAguarde();
			return false;
		}else{
			$('form[name=form] fieldset').css('background','').css('border','');
			$('#textoFieldSet').html('');
			return true;
		}
		return true;
	}
	function validateRequiredOptional(){
		return validaPapeis();
	}
	$(document).ready(function(){
		$('#nome').focus();
	});
</script>
package br.com.orionx.clinicalsys.bean;

public enum EnumMeioUrbano {

	RURAL(1,"Zona Rural"),
	CASA(2,"Casa/Condomínio"),
	AGLOMERADO(3,"Aglomerado");
	
	private Integer id;
	private String descricao;

	private EnumMeioUrbano(Integer id, String descricao){
		this.id = id;
		this.descricao = descricao;
	}
	
	public Integer getId() {
		return id;
	}
	public String getDescricao() {
		return descricao;
	}
	
	@Override
	public String toString() {
		return getDescricao();
	}

}

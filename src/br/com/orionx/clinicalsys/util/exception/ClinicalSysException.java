package br.com.orionx.clinicalsys.util.exception;

public class ClinicalSysException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ClinicalSysException() {
		super();
	}

	public ClinicalSysException(String message, Throwable cause) {
		super(message, cause);
	}

	public ClinicalSysException(String message) {
		super(message);
	}

	public ClinicalSysException(Throwable cause) {
		super(cause);
	}

}

/*
 * Next Framework http://www.nextframework.org
 * Copyright (C) 2009 the original author or authors.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * You may obtain a copy of the license at
 * 
 *     http://www.gnu.org/copyleft/lesser.html
 * 
 */
package org.nextframework.view;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.nextframework.core.web.NextWeb;
import org.nextframework.util.Util;
import org.nextframework.view.DataGridTag.Status;

/**
 * @author rogelgarcia
 * @since 27/01/2006
 * @version 1.1
 */
public class ColumnTag extends BaseTag {

	public static final String REGISTERING_DATAGRID = "REGISTERING_DATAGRID";

	protected String header;
	
	protected String order;

	protected DataGridTag dataGrid;
	
	protected boolean hasBodyTag = false;

	public void setHasBodyTag(boolean hasBodyTag) {
		this.hasBodyTag = hasBodyTag;
	}

	@Override
	protected void doComponent() throws Exception {
		if (dataGrid == null) {
			dataGrid = findParent2(DataGridTag.class, true);
		}
		// pega o corpo atual da tag (dependendo o status do datagrid apenas uma
		// parte HEADER, BODY OU FOOTER
		// ser� renderizado)
		Map<String, Object> mapHeader = new HashMap<String, Object>();
		Map<String, Object> mapBody = new HashMap<String, Object>();
		Set<String> keySet = getDynamicAttributesMap().keySet();
		for (String string : keySet) {
			if(string.startsWith("body")){
				mapBody.put(string.substring("body".length()), getDynamicAttributesMap().get(string));
			} else if(string.startsWith("header")) {
				mapHeader.put(string.substring("header".length()), getDynamicAttributesMap().get(string));
			} else {
				mapBody.put(string, getDynamicAttributesMap().get(string));
				mapHeader.put(string, getDynamicAttributesMap().get(string));
			}
		}
		if(dataGrid.getCurrentStatus() == Status.REGISTER){
			pushAttribute(REGISTERING_DATAGRID, true);
			//temos que falar que estamos registrando um dataGrid para determinados c�digos do corpo do column n�o ser executados. Por exemplo valida��o
		}
		String tagBody = null;
		if(!(dataGrid.getCurrentStatus() == Status.HEADER && header != null)){// SE ESTIVER NA ETAPA DE HEADER.. MAS J� TIVER HEADER.. NAO INVOCAR O CORPO
			tagBody = getBody();
		}
		
		
		if(dataGrid.getCurrentStatus() == Status.REGISTER){
			popAttribute(REGISTERING_DATAGRID);
		}
		switch (dataGrid.getCurrentStatus()) {
		case HEADER:
			if (Util.strings.isEmpty(tagBody) && header == null) {
				getOut().print("<th"+getDynamicAttributesToString(mapHeader)+"> </th>");
			} else if (header == null) {
				getOut().print("<th"+getDynamicAttributesToString(mapHeader)+"> </th>");
			} else {
				if(order == null){
					getOut().print("<th"+getDynamicAttributesToString(mapHeader)+">" + header + "</th>");	
				} else{
					getOut().print("<th"+getDynamicAttributesToString(mapHeader)+">" + "<a class=\"order\" href=\""+getRequest().getContextPath()+NextWeb.getRequestContext().getRequestQuery()+"?orderBy="+order+"\">"+header+"</a>" + "</th>");
				}
				
			}
			break;
		case BODY:
			if (!hasBodyTag) {
				getOut().print("<td"+getDynamicAttributesToString(mapBody)+">");
				getOut().print(tagBody == null || tagBody.trim().equals("")? "&nbsp;" : tagBody.trim());
				getOut().print("</td>");
			} else {
				//Adicionado, porque o tagBody pode vir com <td ...> ... </td> e precisa validar se o conte�do entre as tags � em branco.
				//modificado por pedrogoncalves em 17/04/2007
				
				//C�digo removido, o uso de express�o regular estava deixando o datagrid lento, foi alterado para fazer semelhante no arquivo BodyTag.java
				//modificado por pedrogoncalves em 20/04/2007
				
//				Pattern pattern = Pattern.compile("<td (.*?)>(.*)</td>",Pattern.DOTALL);
//				Matcher matcher = pattern.matcher(tagBody.trim());
//				if (matcher.find()) {
//					String tdBody = matcher.group(2);
//					getOut().print(tdBody == null || tdBody.trim().equals("")? "<td "+matcher.group(1)+" >&nbsp;</td>" : tagBody);
//				} else {
//					getOut().print(tagBody);
//				}
				getOut().print(tagBody);
			}
			break;
		case DYNALINE:
			PanelRenderedBlock block = new PanelRenderedBlock();
			block.setBody(tagBody);				
			dataGrid.add(block);
			break;
		case FOOTER:
			if (Util.strings.isEmpty(tagBody)) {
				getOut().print("<td"+getDynamicAttributesToString()+"> </td>");
			} else {
				getOut().print(tagBody);
			}
			break;
		case REGISTER:
			// registrar
			// if (getJspBody()!=null) {
			// PrintWriter writer = new PrintWriter(new
			// ByteArrayOutputStream());
			// getJspBody().invoke(writer);
			// }
			if (Util.strings.isNotEmpty(header)) {
				dataGrid.setRenderHeader(true);
			}
			dataGrid.setHasColumns(true);			
			break;
		}
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

}

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>
<%@page import="br.com.orionx.clinicalsys.util.ClinicalSysUtil"%>

<div id="dialog-confirm" style="display:none;" title="Ol� <%= ClinicalSysUtil.getUsuarioLogado().getLogin() %>">
	<p>Voc� est� tentando voltar sem salvar um registro, deixe-me ajudar.</p><br>
	
	
	<div class="p_dialog p_dialog_success" onclick="$('#dialog-confirm').dialog('destroy');$('.noboard').trigger('click');">
		<p style="background-color: #DFD">Salvar</p>
		<p class="p_dialog_sub p_dialog_sub_success">Salva o registro que voc� estava criando ou editando neste momento.</p>
	</div><br>
	<div class="p_dialog p_dialog_warning" onclick="location.href = window.location.href.substr(0,window.location.href.indexOf('?'))">
		<p style="background-color: #FFC;">Continuar</p>
		<p class="p_dialog_sub p_dialog_sub_warning">Cancela o que estava fazendo e retorna para a listagem dos dados sem salvar.</p>
	</div><br>	
	<div class="p_dialog p_dialog_error" onclick="$('#dialog-confirm').dialog('destroy');">
		<p style="background-color: #FCC;">Cancelar</p>
		<p class="p_dialog_sub p_dialog_sub_error">Retorna para a tela de cria��o/edi��o de dados antes de voc� ter clicado em voltar.</p>
	</div><br>
</div>
<div id="dialog-information" style="display:none;" title="Ol� <%= ClinicalSysUtil.getUsuarioLogado().getLogin() %>">
	
</div>

<div id="dialog-help-delete" style="display:none;" title="Ol� <%= ClinicalSysUtil.getUsuarioLogado().getLogin() %>">
	<p>Voc� est� tentando excluir registro do sistema, aten��o pois esta a��o pode ser irrevers�vel.</p><br>
	
	
	<div class="p_dialog p_dialog_warning" onclick="$('#dialog-help-delete').dialog('destroy');">
		<p style="background-color: #FFC">Cancelar</p>
		<p class="p_dialog_sub p_dialog_sub_warning">Igonora este processo e n�o apaga o registro.</p>
	</div><br>
	<div class="p_dialog p_dialog_error" onclick="document.location = '?ACAO=excluir&rd='+$csu.getSelectedValues();">
		<p style="background-color: #FCC;">Apagar</p>
		<p class="p_dialog_sub p_dialog_sub_error">Continua o processo e apaga o registro.</p>
	</div><br>
</div>

<div id="dialog-aguarde" style="display:none;" title="Ol� <%= ClinicalSysUtil.getUsuarioLogado().getLogin() %>">
	<center><img src="/ClinicalSys/images/loading.gif" /></center><br>
	<center><p><h3>Aguarde um instante...</h3></p></center>
		
</div>

<div id="dialog-busca-endereco" style="display:none;" title="Ol� <%= ClinicalSysUtil.getUsuarioLogado().getLogin() %>">
<center><p><h3>Insira aqui os dados para buscar o CEP.</h3></p></center>

	<label for="uf">Unidade Federativa&nbsp;<BR></label>
	<select name="unidadeFederativaAjax" id="uf" onchange="comboReloadAjaxModal(this.value);"  >
		<option value='<null>' selected='selected' ></option>
		<option value="12">Acre</option>
		<option value="27">Alagoas</option>
		<option value="16">Amap�</option>
		<option value="13">Amazonas</option>
		<option value="29">Bahia</option>
		<option value="23">Cear�</option>
		<option value="53">Distrito Federal</option>
		<option value="32">Esp�rito Santo</option>
		<option value="52">Goi�s</option>
		<option value="21">Maranh�o</option>
		<option value="51">Mato Grosso</option>
		<option value="50">Mato Grosso do Sul</option>
		<option value="31">Minas Gerais</option>
		<option value="15">Par�</option>
		<option value="25">Para�ba</option>
		<option value="41">Paran�</option>
		<option value="26">Pernambuco</option>
		<option value="22">Piau�</option>
		<option value="33">Rio de Janeiro</option>
		<option value="24">Rio Grande do Norte</option>
		<option value="43">Rio Grande do Sul</option>
		<option value="11">Rond�nia</option>
		<option value="14">Roraima</option>
		<option value="42">Santa Catarina</option>
		<option value="35">S�o Paulo</option>
		<option value="28">Sergipe</option>
		<option value="17">Tocantins</option>
	</select>
	<span class='requiredMark'>*</span>
	
	<br><label for="cidade">Munic�pio&nbsp;<BR></label>
	<select name="municipioAjax" id="cidade"  ></select>
	<span class='requiredMark'>*</span>	

	<BR/><label for="tipoLogradouroAjax">Tipo de Logradouro&nbsp;<BR></label>
	<select name="tipoLogradouroAjax" id="tipoLogradouroAjax" onchange=""  >
		<option value='<null>' selected='selected' ></option>
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=167]' >10a Rua </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=122]' >10a Travessa</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=168]' >11a Rua </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=123]' >11a Travessa </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=169]' >12a Rua </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=124]' >12a Travessa </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=125]' >13a Travessa </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=126]' >14a Travessa </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=127]' >15a Travessa </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=128]' >16a Travessa </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=139]' >1a Paralela </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=158]' >1a Rua </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=144]' >1a Subida </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=113]' >1a Travessa</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=150]' >1a Vila </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=129]' >1o Alto</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=134]' >1o Beco </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=155]' >1o Parque </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=140]' >2a Paralela </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=159]' >2a Rua </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=145]' >2a Subida </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=114]' >2a Travessa </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=151]' >2a Vila </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=130]' >2o Alto </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=135]' >2o Beco </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=156]' >2o Parque </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=141]' >3a Paralela </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=160]' >3a Rua </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=146]' >3a Subida </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=115]' >3a Travessa </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=152]' >3a Vila </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=131]' >3o Alto </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=136]' >3o Beco </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=157]' >3o Parque </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=142]' >4a Paralela </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=161]' >4a Rua </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=147]' >4a Subida </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=116]' >4a Travessa </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=153]' >4a Vila </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=132]' >4o Alto </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=137]' >4o Beco </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=143]' >5a Paralela </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=162]' >5a Rua </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=148]' >5a Subida </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=117]' >5a Travessa </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=154]' >5a Vila </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=133]' >5o Alto </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=138]' >5o Beco </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=163]' >6a Rua </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=149]' >6a Subida </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=118]' >6a Travessa </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=164]' >7a Rua</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=119]' >7a Travessa </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=165]' >8a Rua </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=120]' >8a Travessa </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=166]' >9a Rua </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=121]' >9a Travessa </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=180]' >Acampamento</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=1]' >Acesso</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=2]' >Adro</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=107]' >Aeroporto </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=3]' >Alameda</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=4]' >Alto</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=80]' >Art�ria</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=5]' >Atalho</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=6]' >Avenida</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=85]' >�?rea</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=13]' >Baixa</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=83]' >Bal�o</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=7]' >Balneario</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=9]' >Beco</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=8]' >Belvedere</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=10]' >Bloco</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=11]' >Bosque</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=12]' >Boulevard</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=102]' >Buraco </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=14]' >Cais</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=177]' >Cal�ada </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=15]' >Caminho</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=20]' >Campo </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=101]' >Canal</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=94]' >Ch�cara </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=16]' >Chapad�o</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=92]' >Circular </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=18]' >Col�nia </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=109]' >Complexo Vi�rio</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=98]' >Condom�nio </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=17]' >Conjunto </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=19]' >Corredor </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=21]' >C�rrego </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=91]' >Descida </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=22]' >Desvio </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=23]' >Distrito </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=81]' >Elevada </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=179]' >Entrada Particular </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=24]' >Escada </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=87]' >Esplanada </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=26]' >Esta��o </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=170]' >Estacionamento </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=27]' >Est�dio </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=104]' >Est�ncia </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=25]' >Estrada </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=28]' >Favela </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=29]' >Fazenda </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=32]' >Feira </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=30]' >Ferrovia </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=31]' >Fonte </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=33]' >Forte </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=34]' >Galeria </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=35]' >Granja </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=99]' >Habitacional </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=36]' >Ilha </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=37]' >Jardim </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=86]' >Jardinete </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=38]' >Ladeira</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=105]' >Lago </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=40]' >Lagoa </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=39]' >Largo </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=41]' >Loteamento </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=90]' >Marina </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=103]' >M�dulo </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=43]' >Monte </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=42]' >Morro </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=106]' >N�cleo </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=48]' >Parada </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=84]' >Paradouro </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=44]' >Paralela </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=51]' >Parque </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=53]' >Passagem </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=108]' >Passagem Subterr�nea </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=52]' >Passarela </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=45]' >Passeio </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=46]' >P�tio </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=96]' >Ponta</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=54]' >Ponte </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=82]' >Porto </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=47]' >Pra�a </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=110]' >Pra�a de Esportes </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=49]' >Praia </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=50]' >Prolongamento </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=55]' >Quadra </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=56]' >Quinta </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=88]' >Quintas </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=58]' >Ramal </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=95]' >Rampa </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=59]' >Recanto </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=100]' >Residencial </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=61]' >Reta </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=60]' >Retiro </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=63]' >Retorno </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=175]' >Rodo Anel</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=62]' >Rodovia </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=112]' >Rotat�ria </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=89]' >Rotula </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=57]'  >Rua </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=172]' >Rua de Pedestre </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=65]' >Servid�o </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=66]' >Setor </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=64]' >S�tio</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=67]' >Subida </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=69]' >Terminal </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=71]' >Travessa </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=176]' >Travessa Particular </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=78]' >Trecho </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=70]' >Trevo </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=68]' >Trincheira </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=173]' >T�nel </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=93]' >Unidade </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=171]' >Vala</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=76]' >Vale </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=174]' >Variante </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=79]' >Vereda </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=72]' >Via </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=178]' >Via de Acesso </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=97]' >Via de pedestre </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=111]' >Via Elevada </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=181]' >Via Expressa</option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=73]' >Viaduto </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=75]' >Viela </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=74]' >Vila </option> 
		<option value='br.com.orionx.clinicalsys.bean.TipoLogradouro[idTipoLogradouro=77]' >Zigue-zague </option> 
	</select>
	<span class='requiredMark'>*</span>
				

	<BR/><label for="logradouroAjax">Logradouro&nbsp; (N�o use acentua��o, nem n�mero da resid�ncia)<BR></label>
	<input type="text" name="logradouroAjax" id="logradouroAjax" onchange="" value=""   maxlength='200' style='width:400px;' size='50'/>
	<span class='requiredMark'>*</span>
	
	<BR><label for="bairroAjax">Bairro&nbsp; (N�o use acentua��o)<BR></label>
	<input type="text" name="bairroAjax" id="bairroAjax" onchange="" value=""   maxlength='50' style='width:200px;' size='50'/>
	<span class='requiredMark'>*</span>
</div>

<div id="dialog-busca-medicamento" style="display:none;" title="Ol� <%= ClinicalSysUtil.getUsuarioLogado().getLogin() %>">
	<form name="busca-med">
		<BR><label for="principioAtivo">Princ�pio Ativo&nbsp;<BR></label>
		<input type="text" name="principioAtivo" id="principioAtivo" onchange="" value=""   maxlength='50' style='width:200px;' size='50'/>
		<BR><label for="nomeComercial">Nome Comercial&nbsp;<BR></label>
		<input type="text" name="nomeComercial" id="nomeComercial" onchange="" value=""   maxlength='50' style='width:200px;' size='50'/>
	</form>
</div>

<div id="dialog-sessao" style="display:none;" title="Ol� <%= ClinicalSysUtil.getUsuarioLogado().getLogin() %>">
	<p>Sua sess�o ir� expirar em menos de 2 minutos.Deixe-me lhe ajudar:</p><br/>
	<div class="p_dialog p_dialog_success" onclick="renovaSessao()">
		<p style="background-color: #DFD">Renovar Sess�o</p>
		<p class="p_dialog_sub p_dialog_sub_success">Renova sua sess�o por mais 30 minutos.</p>
	</div><br>
	<div class="p_dialog p_dialog_warning" onclick="closeDialogSessao();">
		<p style="background-color: #FFC;">Continuar</p>
		<p class="p_dialog_sub p_dialog_sub_warning">Cancela o processo de renovar sua sess�o e ela ir� expirar em menos de 2 minutos.</p>
	</div><br>		
</div>

<div id="dialog-sessao-expirada" style="display:none;" title="Ol� <%= ClinicalSysUtil.getUsuarioLogado().getLogin() %>">
	<p>Sua sess�o expirou.<br>Deixe-me lhe ajudar:</p><br/>
	<div class="p_dialog p_dialog_success" onclick="location.href='/ClinicalSys/system'">
		<p style="background-color: #DFD">Logar</p>
		<p class="p_dialog_sub p_dialog_sub_success">Se sua sess�o expirou, voc� precisa logar novamente no ClinicalSys.</p>
	</div><br>
</div>

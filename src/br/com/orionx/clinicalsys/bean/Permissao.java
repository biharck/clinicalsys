package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.nextframework.authorization.impl.AbstractPermission;
import org.nextframework.bean.annotation.DisplayName;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

/**
 * <p>
 * Representa a permiss�o para determinado papel em determinada tela
 * </p>
 * 
 * @author biharck
 */
@Entity
@Table(name = "permissao")
public class Permissao extends AbstractPermission {

	Integer idPermissao;
	Papel role;
	String permissionString;
	String path;
	String descricaoTela;

	/*
	 * GET
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdPermissao() {
		return idPermissao;
	}
	
	@MaxLength(255)
	@Required
	@MinLength(5)
	public String getPath() {
		return path;
	}

	// API
	public String getPermissionString() {
		return permissionString;
	}
	

	// API
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "role_id")
	@ForeignKey(name = "fk_perissao_papel")
	public Papel getRole() {
		return role;
	}

	@DisplayName("Descri��o da Tela")
	public String getDescricaoTela() {
		return descricaoTela;
	}
	
	
	/*
	 * SET
	 */
	public void setIdPermissao(Integer idPermissao) {
		this.idPermissao = idPermissao;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public void setPermissionString(String permissionString) {
		this.permissionString = permissionString;
	}

	public void setRole(Papel role) {
		this.role = role;
	}
	public void setDescricaoTela(String descricaoTela) {
		this.descricaoTela = descricaoTela;
	}
}
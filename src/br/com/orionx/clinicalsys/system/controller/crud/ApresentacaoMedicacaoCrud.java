package br.com.orionx.clinicalsys.system.controller.crud;

import org.nextframework.authorization.crud.CrudAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.crud.CrudException;
import org.nextframework.core.web.WebRequestContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.servlet.ModelAndView;

import br.com.orionx.clinicalsys.bean.ApresentacaoMedicacao;
import br.com.orionx.clinicalsys.filtros.ApresentacaoMedicacaoFiltro;
import br.com.orionx.clinicalsys.util.DatabaseError;
import br.com.orionx.clinicalsys.util.exception.ClinicalSysException;
import br.com.orionx.clinicalsys.util.generics.CrudController;

@Controller(path="/system/pag/ApresentacaoMedicacao",authorizationModule=CrudAuthorizationModule.class)
public class ApresentacaoMedicacaoCrud extends CrudController<ApresentacaoMedicacaoFiltro, ApresentacaoMedicacao, ApresentacaoMedicacao>{

	@Override
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Administra��o &raquo; <a href=\"/ClinicalSys/system/pag/ApresentacaoMedicacao\">Apresentac�es de Medica��es</a></p>";
	}
	
	@Override
	public ModelAndView doSalvar(WebRequestContext request, ApresentacaoMedicacao form)throws CrudException {
		return saveUnique(request, form,true);
	}
	
	@Override
	public ModelAndView saveAndNew(WebRequestContext request, ApresentacaoMedicacao form)throws Exception {
		return saveUnique(request, form,false);
	}
	
	public ModelAndView saveUnique(WebRequestContext request, ApresentacaoMedicacao form, boolean retornaListagem){
		try {
			salvar(request, form);
		} catch (DataIntegrityViolationException e) {
			if (DatabaseError.isKeyPresent(e, "idx_nome_apresmed")) throw new ClinicalSysException("J� existe uma apresenta��o de medica��o com este nome cadastrado no sistema.");
			return sendRedirectToAction("entrada");
		}
		catch (Exception e) {
			return sendRedirectToAction("entrada");
		}
		if(retornaListagem)return sendRedirectToAction("listagem");
		else return sendRedirectToAction("entrada");
	}

}

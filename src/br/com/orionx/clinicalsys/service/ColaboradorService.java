package br.com.orionx.clinicalsys.service;

import java.util.List;

import br.com.orionx.clinicalsys.bean.Colaborador;
import br.com.orionx.clinicalsys.bean.Papel;
import br.com.orionx.clinicalsys.bean.Usuario;
import br.com.orionx.clinicalsys.dao.ColaboradorDAO;
import br.com.orionx.clinicalsys.filtros.ColaboradorFiltro;
import br.com.orionx.clinicalsys.util.generics.GenericService;

public class ColaboradorService extends GenericService<Colaborador> {

	private ColaboradorDAO colaboradorDAO;
	
	public void setColaboradorDAO(ColaboradorDAO colaboradorDAO) {
		this.colaboradorDAO = colaboradorDAO;
	}
	
	@Override
	public void saveOrUpdate(Colaborador bean) {
		if(bean.getIdColaborador()!=null){
			List<Papel> usuarioPapelTemp = bean.getUsuario().getPapeis();
			bean.setUsuario(UsuarioService.getInstance().getUsuarioByColaborador(bean));
			bean.getUsuario().setPapeis(usuarioPapelTemp);
		}
			UsuarioService.getInstance().saveOrUpdate(bean.getUsuario());
		super.saveOrUpdate(bean);
	}
	
	/**
	 * <p> M�todo de refer�ncia ao dao
	 * @param id {@link Integer}
	 * @return {@link Colaborador}
	 * @see br.com.orionx.clinicalsys.dao.ColaboradorDAO#getColaboradorEspecifico
	 */
	public Colaborador getColaboradorEspecifico(Integer id){
		return colaboradorDAO.getColaboradorEspecifico(id);
	}
	
	/**
	 * <p> M�todo de refer�ncia ao dao
	 * @param u {@link Usuario}
	 * @return {@link Colaborador}
	 * @see br.com.orionx.clinicalsys.dao.ColaboradorDAO.getColaboradorByUser
	 */
	public Colaborador getColaboradorByUser(String login){
		return colaboradorDAO.getColaboradorByUser(login);
	}
	
	public List<Colaborador> getColaboradoresReport(ColaboradorFiltro filtro){
		return colaboradorDAO.getColaboradoresReport(filtro);
	}
}

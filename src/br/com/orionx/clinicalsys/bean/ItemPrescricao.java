package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="itemprescricao")
public class ItemPrescricao {

	private Integer idItemPrescricao;
	private ClassificacaoMedicacao classificacaoMedicacao;//transient
	private Medicamento medicamento;
	private String dose;
	private String outrasRecomendacoes;
	private RecomendacaoMedicamentosa recomendacaoMedicamentosa;
	
	private Prescricao prescricao;
	
	@Transient
	public ClassificacaoMedicacao getClassificacaoMedicacao() {
		return classificacaoMedicacao;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdItemPrescricao() {
		return idItemPrescricao;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idMedicamento")
	public Medicamento getMedicamento() {
		return medicamento;
	}
	public String getDose() {
		return dose;
	}
	public String getOutrasRecomendacoes() {
		return outrasRecomendacoes;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idRecomendacaoMedicamentosa")
	public RecomendacaoMedicamentosa getRecomendacaoMedicamentosa() {
		return recomendacaoMedicamentosa;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idPrescricao")
	public Prescricao getPrescricao() {
		return prescricao;
	}
	public void setClassificacaoMedicacao(
			ClassificacaoMedicacao classificacaoMedicacao) {
		this.classificacaoMedicacao = classificacaoMedicacao;
	}

	public void setMedicamento(Medicamento medicamento) {
		this.medicamento = medicamento;
	}

	public void setDose(String dose) {
		this.dose = dose;
	}
	public void setOutrasRecomendacoes(String outrasRecomendacoes) {
		this.outrasRecomendacoes = outrasRecomendacoes;
	}

	public void setRecomendacaoMedicamentosa(RecomendacaoMedicamentosa recomendacaoMedicamentosa) {
		this.recomendacaoMedicamentosa = recomendacaoMedicamentosa;
	}
	public void setIdItemPrescricao(Integer idItemPrescricao) {
		this.idItemPrescricao = idItemPrescricao;
	}
	public void setPrescricao(Prescricao prescricao) {
		this.prescricao = prescricao;
	}
	
}

package br.com.orionx.clinicalsys.bean;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

import br.com.orionx.clinicalsys.util.log.Log;

@Entity
@Table(name = "cargo")
public class Cargo implements Log{
	
	private Integer idCargo;
	private String nome;
	private String codigoCBO;
	private String descricao;
	private boolean ativo;
	
	private List<Papel> papeis;
	private List<CargoPapel> listaCargosPapeis;
	
	//Log
	private Integer idUsuarioAltera;
	private Timestamp dtAltera;
	
	
	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdCargo() {
		return idCargo;
	}
	@Required
	@MaxLength(50)
	@MinLength(2)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	@Required
	@MaxLength(5)
	@MinLength(5)
	public String getCodigoCBO() {
		return codigoCBO;
	}
	@Required
	@MaxLength(100)
	@MinLength(5)
	public String getDescricao() {
		return descricao;
	}
	public boolean isAtivo() {
		return ativo;
	}
	
	@Required
	@Transient
	public List<Papel> getPapeis() {
		return papeis;
	}
	
	@OneToMany(mappedBy = "cargo")
	public List<CargoPapel> getListaCargosPapeis() {
		return listaCargosPapeis;
	}
	
	//SET
	public void setIdCargo(Integer idCargo) {
		this.idCargo = idCargo;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setCodigoCBO(String codigoCBO) {
		this.codigoCBO = codigoCBO;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	public void setPapeis(List<Papel> papeis) {
		this.papeis = papeis;
	}
	public void setListaCargosPapeis(List<CargoPapel> listaCargosPapeis) {
		this.listaCargosPapeis = listaCargosPapeis;
	}
	
	
	//LOG
	@Override
	public void setIdUsuarioAltera(Integer idUsuarioAltera) {
		this.idUsuarioAltera = idUsuarioAltera;
	}

	@Override
	public void setDtAltera(Timestamp dtAltera) {
		this.dtAltera = dtAltera;
	}

	@Override
	public Integer getIdUsuarioAltera() {
		return idUsuarioAltera;
	}

	@Override
	public Timestamp getDtAltera() {
		return dtAltera;
	}

}

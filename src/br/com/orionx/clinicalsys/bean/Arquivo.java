package br.com.orionx.clinicalsys.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.types.File;

/**
 * Bean que representar� um arquivo.
 * Ter� uma tabela no banco que mapeia as propriedades dessa classe.
 * O NEXT detectar� essa classe automaticamente para realizar algumas configura��es.
 * IMPORTANTE: S� PODER� EXISTIR UMA CLASSE QUE IMPLEMENTA File PARA QUE ESSAS CONFIGURA��ES 
 * SEJAM REALIZADAS!
 */
@Entity
@Table(name="arquivo")
public class Arquivo implements File {
	
	Long cdfile;
	String contenttype; //tipo do arquivo (ser� populado pelo neo automaticamente
	String name;//nome do arquivo
	Long size;//tamanho do arquivo
	
	byte[] content;//conte�do do arquivo
	

	//cdfile � o c�digo do arquivo no banco
	//o primary key � o campo id (que estamos retornando)
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	public Long getCdfile() {
		return cdfile;
	}

	//salvaremos o conte�do do arquivo em disco, por isso
	//o content (que � o conteudo em bytes do arquivo) � transiente
	public byte[] getContent() {
		return content;
	}

	public String getContenttype() {
		return contenttype;
	}
	
	@Transient
	public String getTipoconteudo() {
		return getContenttype();
	}

	@DescriptionProperty
	public String getName() {
		return name;
	}
	public Long getSize() {
		return size;
	}


	public void setCdfile(Long cdfile) {
		this.cdfile = cdfile;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public void setContenttype(String contenttype) {
		this.contenttype = contenttype;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSize(Long size) {
		this.size = size;
	}

}
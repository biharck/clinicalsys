package br.com.orionx.clinicalsys.dao;

import br.com.orionx.clinicalsys.bean.Cargo;
import br.com.orionx.clinicalsys.bean.CargoPapel;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class CargoPapelDAO extends GenericDAO<CargoPapel>{

	/**
	 * <p> Fun�ao para apagar o todos os registros vinculados em cargopapel a partir de um cargo
	 * @param cargo {@link Cargo}
	 */
	public void deleteByCargo(Cargo cargo){
		getJdbcTemplate().execute(" delete from cargopapel where idcargopapel = "+cargo.getIdCargo());
	}
}

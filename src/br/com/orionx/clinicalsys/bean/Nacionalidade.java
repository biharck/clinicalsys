package br.com.orionx.clinicalsys.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

@Entity
@Table(name  = "nacionalidade")
public class Nacionalidade {
	
	private Integer idNacionalidade;
	private String nome;

	//GET
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getIdNacionalidade() {
		return idNacionalidade;
	}
	@MinLength(2)
	@MaxLength(50)
	@Required
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	
	
	//SET
	public void setIdNacionalidade(Integer idNacionalidade) {
		this.idNacionalidade = idNacionalidade;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}

package br.com.orionx.clinicalsys.filtros;

import br.com.orionx.clinicalsys.bean.TabelaTISS;
import br.com.orionx.clinicalsys.bean.VersaoXMLTISS;
import br.com.orionx.clinicalsys.util.generics.FiltroListagem;

public class OperadoraFiltro extends FiltroListagem{

	protected String nome;
	protected String suaIdentificacao;
	protected VersaoXMLTISS versaoXMLTISS;
	protected TabelaTISS tabelaTISS;
	protected String nomePlano;
	protected AtivoEnum ativoEnum;
	
	//GET
	public String getNome() {
		return nome;
	}
	public String getSuaIdentificacao() {
		return suaIdentificacao;
	}
	public VersaoXMLTISS getVersaoXMLTISS() {
		return versaoXMLTISS;
	}
	public TabelaTISS getTabelaTISS() {
		return tabelaTISS;
	}
	public String getNomePlano() {
		return nomePlano;
	}
	public AtivoEnum getAtivoEnum() {
		return ativoEnum;
	}
	
	//SET
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setSuaIdentificacao(String suaIdentificacao) {
		this.suaIdentificacao = suaIdentificacao;
	}
	public void setVersaoXMLTISS(VersaoXMLTISS versaoXMLTISS) {
		this.versaoXMLTISS = versaoXMLTISS;
	}
	public void setNomePlano(String nomePlano) {
		this.nomePlano = nomePlano;
	}
	public void setTabelaTISS(TabelaTISS tabelaTISS) {
		this.tabelaTISS = tabelaTISS;
	}
	public void setAtivoEnum(AtivoEnum ativoEnum) {
		this.ativoEnum = ativoEnum;
	}
}

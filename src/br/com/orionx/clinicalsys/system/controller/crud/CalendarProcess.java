package br.com.orionx.clinicalsys.system.controller.crud;

import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.nextframework.authorization.process.ProcessAuthorizationModule;
import org.nextframework.controller.Controller;
import org.nextframework.controller.DefaultAction;
import org.nextframework.controller.MultiActionController;
import org.nextframework.core.web.NextWeb;
import org.nextframework.core.web.WebRequestContext;
import org.nextframework.types.Cpf;
import org.nextframework.types.Telefone;
import org.nextframework.view.DownloadFileServlet;
import org.springframework.web.servlet.ModelAndView;

import br.com.orionx.clinicalsys.bean.AgendamentoConsulta;
import br.com.orionx.clinicalsys.bean.Colaborador;
import br.com.orionx.clinicalsys.bean.ColaboradorAgenda;
import br.com.orionx.clinicalsys.bean.ConfiguracaoAtendimento;
import br.com.orionx.clinicalsys.bean.IndispAgendaColaborador;
import br.com.orionx.clinicalsys.bean.Operadora;
import br.com.orionx.clinicalsys.bean.Paciente;
import br.com.orionx.clinicalsys.bean.Plano;
import br.com.orionx.clinicalsys.bean.TelefonePaciente;
import br.com.orionx.clinicalsys.bean.TelefoneTipo;
import br.com.orionx.clinicalsys.dao.ArquivoDAO;
import br.com.orionx.clinicalsys.dao.ColaboradorDAO;
import br.com.orionx.clinicalsys.service.AgendamentoConsultaService;
import br.com.orionx.clinicalsys.service.ColaboradorAgendaService;
import br.com.orionx.clinicalsys.service.ConfiguracaoAtendimentoService;
import br.com.orionx.clinicalsys.service.IndispAgendaColaboradorService;
import br.com.orionx.clinicalsys.service.OperadoraService;
import br.com.orionx.clinicalsys.service.PacienteService;
import br.com.orionx.clinicalsys.service.PlanoService;
import br.com.orionx.clinicalsys.util.ClinicalSysUtil;
import br.com.orionx.clinicalsys.util.EnumSituacoesAgendamento;

@Controller(path="/clinic/process/Calendar",authorizationModule=ProcessAuthorizationModule.class)
public class CalendarProcess extends MultiActionController{
	
	private ColaboradorAgendaService colaboradorAgendaService;
	private ArquivoDAO arquivoDAO;
	private IndispAgendaColaboradorService indispAgendaColaboradorService;
	private ConfiguracaoAtendimentoService configuracaoAtendimentoService;
	private OperadoraService operadoraService;
	private PlanoService planoService;
	private PacienteService pacienteService;
	private AgendamentoConsultaService agendamentoConsultaService;
	private ColaboradorDAO colaboradorDAO;
	
	public void setColaboradorAgendaService(ColaboradorAgendaService colaboradorAgendaService) {
		this.colaboradorAgendaService = colaboradorAgendaService;
	}
	public void setArquivoDAO(ArquivoDAO arquivoDAO) {
		this.arquivoDAO = arquivoDAO;
	}
	public void setIndispAgendaColaboradorService(IndispAgendaColaboradorService indispAgendaColaboradorService) {
		this.indispAgendaColaboradorService = indispAgendaColaboradorService;
	}
	public void setConfiguracaoAtendimentoService(ConfiguracaoAtendimentoService configuracaoAtendimentoService) {
		this.configuracaoAtendimentoService = configuracaoAtendimentoService;
	}
	public void setOperadoraService(OperadoraService operadoraService) {
		this.operadoraService = operadoraService;
	}
	public void setPlanoService(PlanoService planoService) {
		this.planoService = planoService;
	}
	public void setPacienteService(PacienteService pacienteService) {
		this.pacienteService = pacienteService;
	}
	public void setAgendamentoConsultaService(AgendamentoConsultaService agendamentoConsultaService) {
		this.agendamentoConsultaService = agendamentoConsultaService;
	}
	public void setColaboradorDAO(ColaboradorDAO colaboradorDAO) {
		this.colaboradorDAO = colaboradorDAO;
	}

	@SuppressWarnings("unused")
	@DefaultAction
	public ModelAndView index(WebRequestContext request,ColaboradorAgenda colaboradorAgenda){
		request.setAttribute("pathURL", getPathURL());
		ColaboradorAgenda ca = new ColaboradorAgenda();
		
		/**
		 * Testa para ver se o usu�rio pode ver todas as agendas
		 */
		Colaborador c = colaboradorDAO.getColaboradorByUser(NextWeb.getUser().getLogin());
		if(c==null){
			request.addError("N�o Existem agendas para este colaborador!");
			return new ModelAndView("process/choiceACalendar","lista",new ArrayList<ColaboradorAgenda>());
		}else if(!c.getPermissaoAgenda()){
			ca = colaboradorAgendaService.getByColaborador(c);
			if(ca==null){
				request.addError("N�o Existem agendas para este colaborador!");
				return new ModelAndView("process/choiceACalendar","lista",new ArrayList<ColaboradorAgenda>());
			}
			
		}else{
			List<ColaboradorAgenda> agendas = colaboradorAgendaService.getAgendaColaboradores();
			if(agendas!=null && !agendas.isEmpty() && getParameter("shv")==null){
				for (ColaboradorAgenda caIteracao : agendas) {
					if(caIteracao.getColaborador().getFoto()!=null){
						arquivoDAO.loadAsImage(caIteracao.getColaborador().getFoto());
						DownloadFileServlet.addCdfile(request.getSession(), caIteracao.getColaborador().getFoto().getCdfile());
					}
				}
				return new ModelAndView("process/choiceACalendar","lista",agendas);
			}
		}
		//fim teste
		
		if(ca == null)
			ca = new ColaboradorAgenda();
		
		if(ca.getIdColaboradorAgenda()==null)
			ca.setIdColaboradorAgenda(Integer.parseInt(getParameter("shv")));
		ca = colaboradorAgendaService.loadForEntrada(ca);
		List<IndispAgendaColaborador> indisponibilidades = indispAgendaColaboradorService.getIndisponibilidades(ca);
		
		List<String> fullDays    = new ArrayList<String>();
		List<String> partialDays = new ArrayList<String>();
		
		long ipp = ca.getInicioPrimeiroPeriodo()!=null? ca.getInicioPrimeiroPeriodo().getTime(): 0l;
		long tpp = ca.getTerminoPrimeiroPeriodo()!=null? ca.getTerminoPrimeiroPeriodo().getTime(): 0l;
		
		long isp = ca.getInicioSegundoPeriodo()!=null? ca.getInicioSegundoPeriodo().getTime(): 0l;
		long tsp = ca.getTerminoSegundoPeriodo()!=null? ca.getTerminoSegundoPeriodo().getTime(): 0l;
		
		long itp = ca.getInicioTerceiroPeriodo()!=null? ca.getInicioTerceiroPeriodo().getTime(): 0l;
		long ttp = ca.getTerminoTerceiroPeriodo()!=null? ca.getTerminoTerceiroPeriodo().getTime(): 0l;
		
		long iqp = ca.getInicioQuartoPeriodo()!=null? ca.getInicioQuartoPeriodo().getTime(): 0l;
		long tqp = ca.getTerminoQuartoPeriodo()!=null? ca.getTerminoQuartoPeriodo().getTime(): 0l;
		
		ConfiguracaoAtendimento configAtendimento = configuracaoAtendimentoService.getDadosConfiguracao();
		
		//buscar do banco de dados
		float tempoConsulta = configAtendimento.getTempoConsulta();
		request.setAttribute("slotMinutes", tempoConsulta);
		float resultado = 0F;
		//tempo em minutos
		
		/**
		 * se o resto do per�odo for superior a 80% do intervalo, � realizado um tipo de encaixe no hor�rio
		 */
		long first  =  (tpp - ipp) / 60000;
		resultado = new Float(first)/tempoConsulta;
		if(resultado % 2 >= 0.8)
			first += 60;
		
		long second =  (tsp - isp) / 60000;
		resultado = new Float(second)/tempoConsulta;
		if(resultado % 2 >= 0.8)
			second += 60;
		
		long third  =  (ttp - itp) / 60000;
		resultado = new Float(third)/tempoConsulta;
		if(resultado % 2 >= 0.8)
			third += 60;
		
		long fourth =  (tqp - iqp) / 60000;
		resultado = new Float(fourth)/tempoConsulta;
		if(resultado % 2 >= 0.8)
			fourth += 60;
		
		int total = new Integer(new Long(first/60).intValue()) + new Integer(new Long(second/60).intValue())+new Integer(new Long(third/60).intValue())+new Integer(new Long(fourth/60).intValue());
		
		int sun, mon, tue, wed, thu, fri, sat;
		sun = mon = tue = wed = thu = fri = sat = 0;
		for (IndispAgendaColaborador i : indisponibilidades) {
			switch(i.getDiaSemana()){
				case 0: sun ++;break;
				case 1: mon ++;break;
				case 2: tue ++;break;
				case 3: wed ++;break;
				case 4: thu ++;break;
				case 5: fri ++;break;
				case 6: sat ++;break;
			}
		}
		
		if(sun > 0 && sun < total)
			partialDays.add("sun");
		else if(sun == total)
			fullDays.add("sun");
		
		if(mon > 0 && mon < total)
			partialDays.add("mon");
		else if(mon == total)
			fullDays.add("mon");
		
		if(tue > 0 && tue < total)
			partialDays.add("tue");
		else if(tue == total)
			fullDays.add("tue");
		
		if(wed > 0 && wed < total)
			partialDays.add("wed");
		else if(wed == total)
			fullDays.add("wed");
		
		if(thu > 0 && thu < total)
			partialDays.add("thu");
		else if(thu == total)
			fullDays.add("thu");
		
		if(fri > 0 && fri < total)
			partialDays.add("fri");
		else if(fri == total)
			fullDays.add("fri");

		if(sat > 0 && sat < total)
			partialDays.add("sat");
		else if(sat == total)
			fullDays.add("sat");
		
		
		request.setAttribute("fullDays", ClinicalSysUtil.returnArrayConcat(fullDays.toArray(), ','));
//		request.setAttribute("partialDays", partialDays.toArray());
		request.setAttribute("maxHourDay", getPrimeiroEUltimoHorario(ca)[1]);
		request.setAttribute("minHourDay", getPrimeiroEUltimoHorario(ca)[0]);
		List<Operadora> operadoras = operadoraService.findAll();
		List<Plano> planos = planoService.findAll(); 
		
		request.setAttribute("operadoras", operadoras == null ? new ArrayList<Operadora>() : operadoras);
		request.setAttribute("planos", planos == null ? new ArrayList<Plano>() : planos);
		request.setAttribute("situacoes", EnumSituacoesAgendamento.values());
		
		/**
		 * 
		 * 
		 * PEGAR SOMENTE OS MAIS RECENTES, SUGEST�O, OS 3 MESES PRA FRENTE E PRA TRAS?????
		 * 
		 * 
		 */
		Colaborador temp = colaboradorAgendaService.getColaboradorAgendaAndColaborador(ca).getColaborador();
		
		request.setAttribute("datas", ClinicalSysUtil.formataAgendaConsulta(agendamentoConsultaService.getConsultasAgendadasByColaborador(temp)));
		return new ModelAndView("direct:process/calendar","colaboradorAgenda",ca);
	}
	
	public String getPathURL() {
		return "<p><a href=\"/ClinicalSys/system\">Home</a> &raquo; Cl�nica &raquo; <a href=\"/ClinicalSys/system/process/Calendar\">Calend�rio/Agenda</a></p>";
	}
	
	
	public void saveAgenda(WebRequestContext request, ColaboradorAgenda ca) throws IOException{
		JSONObject jsonObj = new JSONObject();
		Calendar dia = Calendar.getInstance();
		Calendar fim = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm");
		
		try {
			AgendamentoConsulta ac = new AgendamentoConsulta();
			
			Integer horaInteira;
			Integer minutoInteiro;
			Time hora=null,horaFim=null;
			if (getParameter("startHour")!=null && !getParameter("startHour").isEmpty() && !getParameter("startHour").equals("null")&& !getParameter("startHour").equals("<null>")) {
				horaInteira = Integer.parseInt(getParameter("startHour").split(":")[0]);
				minutoInteiro = Integer.parseInt(getParameter("startHour").split(":")[1]);
				dia.set(Integer.parseInt(getParameter("startDate").substring(6,10)),Integer.parseInt(getParameter("startDate").substring(3,	5)) - 1,Integer.parseInt(getParameter("startDate").substring(0,2)), horaInteira, minutoInteiro, 0);
				hora = new Time(dia.getTime().getTime());
				ac.setHora(hora);
			}
			
			if(getParameter("endHour")!=null && !getParameter("endHour").isEmpty() && !getParameter("endHour").equals("null")){
				horaInteira = Integer.parseInt(getParameter("endHour").split(":")[0]);
				minutoInteiro = Integer.parseInt(getParameter("endHour").split(":")[1]);
				fim.set(Integer.parseInt(getParameter("startDate").substring(6, 10)), Integer.parseInt(getParameter("startDate").substring(3, 5))-1, Integer.parseInt(getParameter("startDate").substring(0, 2)), horaInteira, minutoInteiro,0);
				horaFim = new Time(fim.getTime().getTime());
				ac.setHoraFim(horaFim);
			}
			
			String obs = getParameter("observacoes");
			byte[] latin1 = new String(obs.getBytes(), "UTF-8").getBytes("ISO-8859-1");
			obs = new String(latin1);
			
			
			if(getParameter("ID")!=null && !getParameter("ID").equals("")){
				ac.setIdAgendamentoConsulta(Integer.parseInt(getParameter("ID")));
				ac = agendamentoConsultaService.getConsultaAgendadaById(ac.getIdAgendamentoConsulta());
				if(getParameter("situacao")!=null && !getParameter("situacao").equals("<null>") && !getParameter("situacao").isEmpty())
					ac.setSituacao(EnumSituacoesAgendamento.getEnumByValue(getParameter("situacao")).getIdSituacao());
				dia.set(Integer.parseInt(getParameter("startDate").substring(6,10)),Integer.parseInt(getParameter("startDate").substring(3,	5)) - 1,Integer.parseInt(getParameter("startDate").substring(0,2)));
				ac.setObservacoes(obs);
				ac.setDia(new Date(dia.getTimeInMillis()));
				if(hora!=null)
					ac.setHora(hora);
				if(horaFim!=null)
					ac.setHoraFim(horaFim);
				
				//verificar disponibilidade da nova data caso tenha sito alterado
				if(agendamentoConsultaService.indisponivel(ac.getDia(), ca, ac.getHora(), ac)){
					jsonObj.put("erro", true);
					jsonObj.put("msg", "J� existe uma consulta marcada para o dia "+dateFormat.format(ac.getDia())+" �s "+hourFormat.format(ac.getHora()) +" horas.");
					jsonObj.put("revert", true);
					request.getServletResponse().setContentType("application/json");
					request.getServletResponse().setCharacterEncoding("UTF-8");
					request.getServletResponse().getWriter().println( jsonObj );
					return;
				}
				//verificar se o horario a ser movido � um horario que o colaborador ir� trabalhar
				//IMPORTANTE - DEVE SER FEITO ANTES DA DISPONIBILIDADE FEITO ACIMA
				if(agendamentoConsultaService.colaboradorTrabalhaNesteHorario(new ColaboradorAgenda(Integer.parseInt(getParameter("shv"))),ac)){
					jsonObj.put("erro", true);
					jsonObj.put("msg", "O colaborador "+ac.getColaborador().getNome() +" n�o antende no dia "+dateFormat.format(ac.getDia()) + " �s "+hourFormat.format(ac.getHora()) +" horas.");
					jsonObj.put("revert", true);
					request.getServletResponse().setContentType("application/json");
					request.getServletResponse().setCharacterEncoding("UTF-8");
					request.getServletResponse().getWriter().println( jsonObj );
					return;
				}
				
					
			}else{
				String obrigatoriedade = verificaObrigatoriedade(request);
				if(obrigatoriedade !=null){
					try {
						jsonObj.put("erro", true);
						jsonObj.put("msg", obrigatoriedade);
						request.getServletResponse().setContentType("application/json");
						request.getServletResponse().setCharacterEncoding("UTF-8");
						request.getServletResponse().getWriter().println( jsonObj );
						return;
					} catch (JSONException e) {
						e.printStackTrace();
					}				
				}	
				ac.setSituacao(1);
				Plano p = new Plano(ClinicalSysUtil.returnIdWithParameters(getParameter("plano")));
				Date diaConsulta = new Date(dia.getTimeInMillis());
				ac.setDia(diaConsulta);
				ca  = new ColaboradorAgenda(Integer.parseInt(getParameter("shv")));
				ca = colaboradorAgendaService.loadForEntrada(ca);
				Paciente paciente = new Paciente();
				if(getParameter("autocomplete_id")==null || getParameter("autocomplete_id").isEmpty()){
					paciente.setIdPaciente(null);
					paciente.setNome(getParameter("paciente"));
					//setar cpf, data de nascimento e telefones
					Cpf cpf = new Cpf(getParameter("cpfNovo"));
					paciente.setCpf(cpf);
					Telefone fixo = new Telefone(getParameter("telFixoNovo"));
					List<TelefonePaciente> telefonesPaciente = new ArrayList<TelefonePaciente>();
					telefonesPaciente.add(new TelefonePaciente(paciente,fixo,TelefoneTipo.FIXO));

					if(getParameter("celularNovo")!=null && !getParameter("celularNovo").isEmpty()){
						Telefone celular = new Telefone(getParameter("celularNovo"));
						telefonesPaciente.add(new TelefonePaciente(paciente,celular,TelefoneTipo.CELULAR));
					}
					if(getParameter("emailNovo")!=null && !getParameter("emailNovo").isEmpty() && !getParameter("emailNovo").equals("null"))
						paciente.setEmail(getParameter("emailNovo"));

					paciente.setDtnascimento(ClinicalSysUtil.stringToDate(getParameter("dtNascimentoNovo")));
					paciente.setListaTelefonePaciente(telefonesPaciente);
					pacienteService.saveOrUpdate(paciente);
				}
				else 
					paciente.setIdPaciente(Integer.parseInt(getParameter("autocomplete_id")));
				
				ac.setObservacoes(obs);
				ac.setPaciente(paciente);
				ac.setPlano(p);
				ac.setColaborador(ca.getColaborador());
				ac.setPaciente(paciente);
			}
			
			ac.setAllDay(getParameter("allDay")==null?false:Boolean.valueOf(getParameter("allDay")));
			if(ac.getHoraFim()!=null)
				ac.setAllDay(false);
			
			agendamentoConsultaService.saveOrUpdate(ac);
			
			jsonObj.put("erro", false);
			request.getServletResponse().setContentType("application/json");
			request.getServletResponse().setCharacterEncoding("ISO-8859-1");
			request.getServletResponse().getWriter().println( jsonObj );
			
		} catch (Exception e) {
			try {
				jsonObj.put("erro", true);				
				request.getServletResponse().setContentType("application/json");
				request.getServletResponse().setCharacterEncoding("UTF-8");
				request.getServletResponse().getWriter().println( jsonObj );
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			catch (IOException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
	}
	
	public static String verificaObrigatoriedade(WebRequestContext request){
		
		if(request.getParameter("plano")==null  || request.getParameter("plano").isEmpty()){
			return "Voc� deve selecionar um Plano.";
		}
		if(request.getParameter("startDate")==null  || request.getParameter("startDate").isEmpty()){
			return "Voc� deve selecionar a data da consulta.";
		}
		if(request.getParameter("startHour")==null  || request.getParameter("startHour").isEmpty()){
			return "Voc� deve selecionar a hora da consulta.";
		}
		if(request.getParameter("paciente")==null  || request.getParameter("paciente").isEmpty()){
			return "Voc� deve selecionar um Paciente.";
		}
		return null;
		
	}
	
	/**
	 * <p>Verifica a exist�ncia do paciente, e caso n�o exista insere um novo paciente
	 */
	public Paciente verificaExistenciaPaciente(String id,String nome){
		if(id!=null)
			return new Paciente(Integer.parseInt(id));
		
		Paciente pProvisorio = new Paciente();
		pProvisorio.setNome(nome);
		pacienteService.saveOrUpdate(pProvisorio);
		return pProvisorio;
	}
	
	/**
	 * <p>M�todo respos�vel em realizar um update nos eventos da agenda
	 */
	public void updateAgenda(WebRequestContext resquest){
		Integer id = Integer.parseInt(getParameter("ID"));
		
		
		AgendamentoConsulta ac = new AgendamentoConsulta();
		ac.setIdAgendamentoConsulta(id);
	}
	
	
	public void getConsulta(WebRequestContext request) throws IOException{
		Integer id = Integer.parseInt(getParameter("ID"));
		AgendamentoConsulta ac = new AgendamentoConsulta(id);
		ac = agendamentoConsultaService.getConsultaAgendadaById(ac.getIdAgendamentoConsulta());
		JSONObject jsonObj = new JSONObject();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm");
		try {
			jsonObj.put("erro", false);
			jsonObj.put("ID", ac.getIdAgendamentoConsulta());
			jsonObj.put("colaborador", ac.getColaborador().getNome());
			jsonObj.put("dia", dateFormat.format(ac.getDia()));
			jsonObj.put("horaInicio", hourFormat.format(ac.getHora()));
			jsonObj.put("horaFim", hourFormat.format(ac.getHoraFim()));
			jsonObj.put("horaAtendimento", ac.getHoraAtendimento());
			jsonObj.put("horaChegada", ac.getHoraChegada());
			jsonObj.put("observacoes", ac.getObservacoes());
			jsonObj.put("plano", ac.getPlano().getNome());
			jsonObj.put("operadora", ac.getPlano().getOperadora().getNomeFantasia());
			jsonObj.put("situacao", EnumSituacoesAgendamento.getEnumById(ac.getSituacao()).getValueCombo());
			jsonObj.put("paciente", ac.getPaciente().getNome());
			
			request.getServletResponse().setContentType("application/json");
			request.getServletResponse().setCharacterEncoding("UTF-8");
			request.getServletResponse().getWriter().println( jsonObj );
			return;
		} catch (JSONException e) {
			try {
				jsonObj.put("erro", true);				
				request.getServletResponse().setContentType("application/json");
				request.getServletResponse().setCharacterEncoding("UTF-8");
				request.getServletResponse().getWriter().println( jsonObj );
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			catch (IOException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}				
	}
	
	
	public void preLoadComboHora(WebRequestContext request){
		
		ColaboradorAgenda ca = new ColaboradorAgenda();
		ca.setIdColaboradorAgenda(Integer.parseInt(getParameter("shv")));
		ca = colaboradorAgendaService.loadForEntrada(ca);
		
		Date dataDaHora = ClinicalSysUtil.stringToDate(getParameter("dia"));
		Calendar calendarioDataDaHora = Calendar.getInstance();
		calendarioDataDaHora.setTimeInMillis(dataDaHora.getTime());
		
		long ipp = ca.getInicioPrimeiroPeriodo()!=null? ca.getInicioPrimeiroPeriodo().getTime(): 0l;
		long tpp = ca.getTerminoPrimeiroPeriodo()!=null? ca.getTerminoPrimeiroPeriodo().getTime(): 0l;
		
		long isp = ca.getInicioSegundoPeriodo()!=null? ca.getInicioSegundoPeriodo().getTime(): 0l;
		long tsp = ca.getTerminoSegundoPeriodo()!=null? ca.getTerminoSegundoPeriodo().getTime(): 0l;
		
		long itp = ca.getInicioTerceiroPeriodo()!=null? ca.getInicioTerceiroPeriodo().getTime(): 0l;
		long ttp = ca.getTerminoTerceiroPeriodo()!=null? ca.getTerminoTerceiroPeriodo().getTime(): 0l;
		
		long iqp = ca.getInicioQuartoPeriodo()!=null? ca.getInicioQuartoPeriodo().getTime(): 0l;
		long tqp = ca.getTerminoQuartoPeriodo()!=null? ca.getTerminoQuartoPeriodo().getTime(): 0l;
		
		ConfiguracaoAtendimento configAtendimento = configuracaoAtendimentoService.getDadosConfiguracao();
		
		SimpleDateFormat formataHora = new SimpleDateFormat("HH:mm");
		
		//buscar do banco de dados
		float tempoConsulta = configAtendimento.getTempoConsulta();
		float resultado = 0F;
		//tempo em minutos
		
		/**
		 * se o resto do per�odo for superior a 80% do intervalo, � realizado um tipo de encaixe no hor�rio
		 */
		long first  =  (tpp - ipp) / 60000;
		resultado = new Float(first)/tempoConsulta;
		if(resultado % 2 >= 0.8)
			first += 60;
		
		long second =  (tsp - isp) / 60000;
		resultado = new Float(second)/tempoConsulta;
		if(resultado % 2 >= 0.8)
			second += 60;
		
		long third  =  (ttp - itp) / 60000;
		resultado = new Float(third)/tempoConsulta;
		if(resultado % 2 >= 0.8)
			third += 60;
		
		long fourth =  (tqp - iqp) / 60000;
		resultado = new Float(fourth)/tempoConsulta;
		if(resultado % 2 >= 0.8)
			fourth += 60;
		
		int[] arrayIntervalos = {new Integer(new Long(first/60).intValue()),
								new Integer(new Long(second/60).intValue()),
								new Integer(new Long(third/60).intValue()),
								new Integer(new Long(fourth/60).intValue())};
		Time[] arrayHoras = {ca.getInicioPrimeiroPeriodo(),ca.getInicioSegundoPeriodo(),ca.getInicioTerceiroPeriodo(),ca.getInicioQuartoPeriodo()};
		
		List<IndispAgendaColaborador> indisponibilidades = new ArrayList<IndispAgendaColaborador>();
		indisponibilidades = indispAgendaColaboradorService.getIndisponibilidades(ca);
		List<String> horariosSair = new ArrayList<String>();
		if(indisponibilidades!=null){
			for (IndispAgendaColaborador dia : indisponibilidades) {
				if(dia.getDiaSemana().equals(calendarioDataDaHora.get(calendarioDataDaHora.DAY_OF_WEEK)-1))
					horariosSair.add(dia.getHora());
			}
		}
		
		/**
		 * tirar tb horarios que j� tenham consultas agendadas
		 * 
		 */
		List<AgendamentoConsulta> agendamentos = agendamentoConsultaService.getConsultasAgendadasByColaboradorAndDate(ca.getColaborador(),dataDaHora);
		for (AgendamentoConsulta agendamentoConsulta : agendamentos) {
			horariosSair.add(formataHora.format(agendamentoConsulta.getHora()));
		}
		List<Time> horarios = new ArrayList<Time>();
		Time timeTmp = null;
		
		boolean lineColor = true;
		for (int j = 0; j < arrayIntervalos.length; j++) {
			boolean firstTime = true;
			for (int i = 0; i < arrayIntervalos[j]; i++) {
				if(firstTime){
					timeTmp = arrayHoras[j];
					firstTime = false;
				}else{
					if(timeTmp==null)continue;
					timeTmp = ClinicalSysUtil.addTime(timeTmp, (float) tempoConsulta);
				}
				lineColor = !lineColor;
				if(!horariosSair.contains(formataHora.format(timeTmp)))
					horarios.add(timeTmp);
			}
		}
		
		try {
			request.getServletResponse().setContentType("application/json");
			request.getServletResponse().setCharacterEncoding("ISO-8859-1");
			request.getServletResponse().getWriter().println(formatCombo(horarios,tempoConsulta));
			
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * <p>Fun��o respons�vel em montar toda estrutura de um combo para povoar o html
	 * @return {@link JSONArray} formatada para enviar via JSON
	 * @throws JSONException 
	 */
	public JSONArray formatCombo(List<Time> horarios,float tempoConsulta) throws JSONException{
		if(horarios!=null && !horarios.isEmpty()){
			SimpleDateFormat format = new SimpleDateFormat("HH:mm");
			JSONArray jsonArray = new JSONArray();
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("jscript", "$('select[name=startHour]').empty();");		
			jsonArray.put(jsonObject);
			jsonObject = new JSONObject();
			jsonObject.put("jscript","$('select[name=startHour]').append('<option value=\"<null>\"></option>');");
			jsonArray.put(jsonObject);
			for (Time hora : horarios) {
				jsonObject = new JSONObject();
				jsonObject.put("jscript", "$('select[name=startHour]').append('<option value=\""+format.format(hora)+"\">"+format.format(hora)+"</option>');");
				jsonArray.put(jsonObject);
			}
			return jsonArray;
		}
		return null;
		
	}
	
	public Time[] getPrimeiroEUltimoHorario(ColaboradorAgenda ca){
		
		long ipp = ca.getInicioPrimeiroPeriodo()!=null? ca.getInicioPrimeiroPeriodo().getTime(): 0l;
		long tpp = ca.getTerminoPrimeiroPeriodo()!=null? ca.getTerminoPrimeiroPeriodo().getTime(): 0l;
		
		long isp = ca.getInicioSegundoPeriodo()!=null? ca.getInicioSegundoPeriodo().getTime(): 0l;
		long tsp = ca.getTerminoSegundoPeriodo()!=null? ca.getTerminoSegundoPeriodo().getTime(): 0l;
		
		long itp = ca.getInicioTerceiroPeriodo()!=null? ca.getInicioTerceiroPeriodo().getTime(): 0l;
		long ttp = ca.getTerminoTerceiroPeriodo()!=null? ca.getTerminoTerceiroPeriodo().getTime(): 0l;
		
		long iqp = ca.getInicioQuartoPeriodo()!=null? ca.getInicioQuartoPeriodo().getTime(): 0l;
		long tqp = ca.getTerminoQuartoPeriodo()!=null? ca.getTerminoQuartoPeriodo().getTime(): 0l;
		
		ConfiguracaoAtendimento configAtendimento = configuracaoAtendimentoService.getDadosConfiguracao();
		
		//buscar do banco de dados
		float tempoConsulta = configAtendimento.getTempoConsulta();
		float resultado = 0F;
		//tempo em minutos
		
		/**
		 * se o resto do per�odo for superior a 80% do intervalo, � realizado um tipo de encaixe no hor�rio
		 */
		long first  =  (tpp - ipp) / 60000;
		resultado = new Float(first)/tempoConsulta;
		if(resultado % 2 >= 0.8)
			first += 60;
		
		long second =  (tsp - isp) / 60000;
		resultado = new Float(second)/tempoConsulta;
		if(resultado % 2 >= 0.8)
			second += 60;
		
		long third  =  (ttp - itp) / 60000;
		resultado = new Float(third)/tempoConsulta;
		if(resultado % 2 >= 0.8)
			third += 60;
		
		long fourth =  (tqp - iqp) / 60000;
		resultado = new Float(fourth)/tempoConsulta;
		if(resultado % 2 >= 0.8)
			fourth += 60;
		
		int[] arrayIntervalos = {new Integer(new Long(first/60).intValue()),
								new Integer(new Long(second/60).intValue()),
								new Integer(new Long(third/60).intValue()),
								new Integer(new Long(fourth/60).intValue())};
		Time[] arrayHoras = {ca.getInicioPrimeiroPeriodo(),ca.getInicioSegundoPeriodo(),ca.getInicioTerceiroPeriodo(),ca.getInicioQuartoPeriodo()};
		
		List<IndispAgendaColaborador> indisponibilidades = new ArrayList<IndispAgendaColaborador>();
		indisponibilidades = indispAgendaColaboradorService.getIndisponibilidades(ca);
		Set<Integer> diasIndisponiveisDaSemana = null;
		if(indisponibilidades!=null){
			diasIndisponiveisDaSemana = new HashSet<Integer>();
			for (IndispAgendaColaborador dia : indisponibilidades) {
				diasIndisponiveisDaSemana.add(dia.getDiaSemana());
			}
		}
		
		List<Time> horarios = new ArrayList<Time>();
		Time timeTmp = null;
		boolean lineColor = true;
		for (int j = 0; j < arrayIntervalos.length; j++) {
			boolean firstTime = true;
			for (int i = 0; i < arrayIntervalos[j]; i++) {
				if(firstTime){
					timeTmp = arrayHoras[j];
					firstTime = false;
				}else{
					if(timeTmp==null)continue;
					timeTmp = ClinicalSysUtil.addTime(timeTmp, (float) tempoConsulta);
				}
				lineColor = !lineColor;
				horarios.add(timeTmp);
			}
		}
		Time [] horariosRetorno = {horarios.get(0),horarios.get(horarios.size()-1)};
		return horariosRetorno;
	}
}

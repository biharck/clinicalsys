package br.com.orionx.clinicalsys.bean;

import java.sql.Time;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.nextframework.validation.annotation.Required;

import br.com.orionx.clinicalsys.util.log.Log;

@Entity
@Table(name = "colaboradoragenda")
public class ColaboradorAgenda implements Log{
	
	private Integer idColaboradorAgenda;
	private TipoAgenda tipoAgenda;
	private Colaborador colaborador;
	private Time inicioPrimeiroPeriodo;
	private Time terminoPrimeiroPeriodo;
	private Time inicioSegundoPeriodo;
	private Time terminoSegundoPeriodo;
	private Time inicioTerceiroPeriodo;
	private Time terminoTerceiroPeriodo;
	private Time inicioQuartoPeriodo;
	private Time terminoQuartoPeriodo;
	private boolean ativo;
	private Time tempoAtendimento;
	
	//Log
	private Integer idUsuarioAltera;
	private Timestamp dtAltera;


	public ColaboradorAgenda(){}
	public ColaboradorAgenda(Integer idColaboradorAgenda){
		this.idColaboradorAgenda  = idColaboradorAgenda;
	}
	
	//GET
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getIdColaboradorAgenda() {
		return idColaboradorAgenda;
	}

	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idTipoAgenda")
	public TipoAgenda getTipoAgenda() {
		return tipoAgenda;
	}

	@Required
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idColaborador")
	public Colaborador getColaborador() {
		return colaborador;
	}

	@Required
	public Time getInicioPrimeiroPeriodo() {
		return inicioPrimeiroPeriodo;
	}

	@Required
	public Time getTerminoPrimeiroPeriodo() {
		return terminoPrimeiroPeriodo;
	}

	public Time getInicioSegundoPeriodo() {
		return inicioSegundoPeriodo;
	}

	public Time getTerminoSegundoPeriodo() {
		return terminoSegundoPeriodo;
	}

	public Time getInicioTerceiroPeriodo() {
		return inicioTerceiroPeriodo;
	}

	public Time getTerminoTerceiroPeriodo() {
		return terminoTerceiroPeriodo;
	}

	public Time getInicioQuartoPeriodo() {
		return inicioQuartoPeriodo;
	}

	public Time getTerminoQuartoPeriodo() {
		return terminoQuartoPeriodo;
	}

	@Required
	public boolean isAtivo() {
		return ativo;
	}

	@Required
	public Time getTempoAtendimento() {
		return tempoAtendimento;
	}

	//SET
	public void setIdColaboradorAgenda(Integer idColaboradorAgenda) {
		this.idColaboradorAgenda = idColaboradorAgenda;
	}

	public void setTipoAgenda(TipoAgenda tipoAgenda) {
		this.tipoAgenda = tipoAgenda;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	public void setInicioPrimeiroPeriodo(Time inicioPrimeiroPeriodo) {
		this.inicioPrimeiroPeriodo = inicioPrimeiroPeriodo;
	}

	public void setTerminoPrimeiroPeriodo(Time terminoPrimeiroPeriodo) {
		this.terminoPrimeiroPeriodo = terminoPrimeiroPeriodo;
	}

	public void setInicioSegundoPeriodo(Time inicioSegundoPeriodo) {
		this.inicioSegundoPeriodo = inicioSegundoPeriodo;
	}

	public void setTerminoSegundoPeriodo(Time terminoSegundoPeriodo) {
		this.terminoSegundoPeriodo = terminoSegundoPeriodo;
	}

	public void setInicioTerceiroPeriodo(Time inicioTerceiroPeriodo) {
		this.inicioTerceiroPeriodo = inicioTerceiroPeriodo;
	}

	public void setTerminoTerceiroPeriodo(Time terminoTerceiroPeriodo) {
		this.terminoTerceiroPeriodo = terminoTerceiroPeriodo;
	}

	public void setInicioQuartoPeriodo(Time inicioQuartoPeriodo) {
		this.inicioQuartoPeriodo = inicioQuartoPeriodo;
	}

	public void setTerminoQuartoPeriodo(Time terminoQuartoPeriodo) {
		this.terminoQuartoPeriodo = terminoQuartoPeriodo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public void setTempoAtendimento(Time tempoAtendimento) {
		this.tempoAtendimento = tempoAtendimento;
	}
	
	//LOG
	@Override
	public void setIdUsuarioAltera(Integer idUsuarioAltera) {
		this.idUsuarioAltera = idUsuarioAltera;
	}

	@Override
	public void setDtAltera(Timestamp dtAltera) {
		this.dtAltera = dtAltera;
	}

	@Override
	public Integer getIdUsuarioAltera() {
		return idUsuarioAltera;
	}

	@Override
	public Timestamp getDtAltera() {
		return dtAltera;
	}
	
}

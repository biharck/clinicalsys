package br.com.orionx.clinicalsys.dao;

import org.nextframework.persistence.DefaultOrderBy;

import br.com.orionx.clinicalsys.bean.TipoLogradouro;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

@DefaultOrderBy("tipoLogradouro.descricao")
public class TipoLogradouroDAO extends GenericDAO<TipoLogradouro> {

	
	/**
	 * <p>m�todo usado pelo webservice que retona o nome do tipo de logradouro e
	 * busca o id para montar o combo.
	 * @return {@link TipoLogradouro}
	 */
	public TipoLogradouro getTipoByName(String name){
		return query().select("tipoLogradouro").where(" UPPER(tipoLogradouro.descricao)=?",name.toUpperCase()).unique();
	}
}

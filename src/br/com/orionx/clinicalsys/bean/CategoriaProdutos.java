package br.com.orionx.clinicalsys.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.nextframework.bean.annotation.DescriptionProperty;
import org.nextframework.validation.annotation.MaxLength;
import org.nextframework.validation.annotation.MinLength;
import org.nextframework.validation.annotation.Required;

import br.com.orionx.clinicalsys.util.log.Log;

@Entity
@Table(name = "categoriaprodutos")
public class CategoriaProdutos implements Log{
	
	private Integer idCategoriaProdutos;
	private String nome;
	private Boolean ativo;

	//Log
	private Integer idUsuarioAltera;
	private Timestamp dtAltera;

	//GET
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdCategoriaProdutos() {
		return idCategoriaProdutos;
	}
	@Required
	@MinLength(1)
	@MaxLength(30)
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	@Required
	public Boolean getAtivo() {
		return ativo;
	}
	
	//SET
	public void setIdCategoriaProdutos(Integer idCategoriaProdutos) {
		this.idCategoriaProdutos = idCategoriaProdutos;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	//LOG
	@Override
	public void setIdUsuarioAltera(Integer idUsuarioAltera) {
		this.idUsuarioAltera = idUsuarioAltera;
	}

	@Override
	public void setDtAltera(Timestamp dtAltera) {
		this.dtAltera = dtAltera;
	}

	@Override
	public Integer getIdUsuarioAltera() {
		return idUsuarioAltera;
	}

	@Override
	public Timestamp getDtAltera() {
		return dtAltera;
	}
}

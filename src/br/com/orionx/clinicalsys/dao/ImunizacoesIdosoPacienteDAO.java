package br.com.orionx.clinicalsys.dao;

import org.nextframework.persistence.SaveOrUpdateStrategy;

import br.com.orionx.clinicalsys.bean.ImunizacoesIdosoPaciente;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class ImunizacoesIdosoPacienteDAO extends GenericDAO<ImunizacoesIdosoPaciente>{
	
	@Override
	public void updateSaveOrUpdate(SaveOrUpdateStrategy save) {
		save.saveOrUpdateManaged("influenzas");
	}
	
	

}

package br.com.orionx.clinicalsys.dao;

import java.util.List;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.QueryBuilder;

import br.com.orionx.clinicalsys.bean.Sala;
import br.com.orionx.clinicalsys.filtros.SalaFiltro;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class SalaDAO extends GenericDAO<Sala>{

	
	@Override
	public void updateListagemQuery(QueryBuilder<Sala> query,FiltroListagem _filtro) {
		SalaFiltro filtro = (SalaFiltro) _filtro;
		query.leftOuterJoin("sala.setor setor");
		if(filtro.getAtivoEnum()!=null)
			if(filtro.getAtivoEnum().getSituacao()!=null && (filtro.getAtivoEnum().getSituacao() || !filtro.getAtivoEnum().getSituacao()))
				query.where("sala.ativo is "+filtro.getAtivoEnum().getSituacao());
		query.whereLikeIgnoreAll("sala.nome",filtro.getNome());
		query.where("setor=?",filtro.getSetor());
		
		super.updateListagemQuery(query, filtro);
	}
	
	@Override
	public List<Sala> findAll() {
		return query().where("sala.ativo is true").list();
	}
	
	@Override
	public List<Sala> findForCombo(String... extraFields) {
		extraFields[0] += "ativo";
		return super.findForCombo(extraFields);
	}

}

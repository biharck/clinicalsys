package br.com.orionx.clinicalsys.filtros;

import java.sql.Date;

import br.com.orionx.clinicalsys.util.generics.FiltroListagem;

public class FeriadoFiltro extends FiltroListagem{

	private String nome;
	private Date dataInicio;
	private Date dataFim;
	
	public String getNome() {
		return nome;
	}
	public Date getDataInicio() {
		return dataInicio;
	}
	public Date getDataFim() {
		return dataFim;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	
	
}

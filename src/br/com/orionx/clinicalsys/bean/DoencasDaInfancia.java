package br.com.orionx.clinicalsys.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.nextframework.bean.annotation.DescriptionProperty;

@Entity
@Table(name= "doencasdainfacia")
public class DoencasDaInfancia {
	
	private Integer idDoencasDaInfancia;
	private String nome;
	private List<DoencasDaInfanciaPaciente> doencasDaInfanciaPacientes;
	private boolean checked;
	
	public DoencasDaInfancia(){}

	public DoencasDaInfancia(Integer idDoencasDaInfancia){
		this.idDoencasDaInfancia = idDoencasDaInfancia;
	}
	
	@Transient
	public boolean isChecked() {
		return checked;
	}
	
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getIdDoencasDaInfancia() {
		return idDoencasDaInfancia;
	}
	@DescriptionProperty
	public String getNome() {
		return nome;
	}
	@OneToMany(mappedBy = "doencasDaInfancia")
	public List<DoencasDaInfanciaPaciente> getDoencasDaInfanciaPacientes() {
		return doencasDaInfanciaPacientes;
	}
	public void setIdDoencasDaInfancia(Integer idDoencasDaInfancia) {
		this.idDoencasDaInfancia = idDoencasDaInfancia;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setDoencasDaInfanciaPacientes(List<DoencasDaInfanciaPaciente> doencasDaInfanciaPacientes) {
		this.doencasDaInfanciaPacientes = doencasDaInfanciaPacientes;
	}
	
	
	@Override
	public boolean equals(Object o) {
		if(o instanceof DoencasDaInfancia){
			DoencasDaInfancia de = (DoencasDaInfancia) o;
			return this.getIdDoencasDaInfancia().equals(de.getIdDoencasDaInfancia());
		}
		return false;
	}

}

package br.com.orionx.clinicalsys.service;

import java.util.List;

import br.com.orionx.clinicalsys.bean.Paciente;
import br.com.orionx.clinicalsys.bean.Prescricao;
import br.com.orionx.clinicalsys.dao.PrescricaoDAO;
import br.com.orionx.clinicalsys.util.generics.GenericService;

public class PrescricaoService extends GenericService<Prescricao> {

	private PrescricaoDAO prescricaoDAO;
	public void setPrescricaoDAO(PrescricaoDAO prescricaoDAO) {
		this.prescricaoDAO = prescricaoDAO;
	}
	
	public List<Prescricao> getPrescricoesByPaciente(Paciente p){
		return prescricaoDAO.getPrescricoesByPaciente(p);
	}
	
	public Prescricao getPrescricaoById(Integer idPrescricao){
		return prescricaoDAO.getPrescricaoById(idPrescricao);
	}
}

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="n" uri="next"%>
<%@ taglib prefix="t" uri="template"%>

<t:entrada>
	<t:property name="idPapel" type="hidden" write="false"/>
	<t:janelaEntrada>
		<t:tabelaEntrada columns="1" >
			<t:property name="name" renderAs="doubleline" id="nome"/>
			<t:property name="description" renderAs="doubleline" rows="5" style="width:300px"/>
		</t:tabelaEntrada>
	</t:janelaEntrada>
</t:entrada>
<script type="text/javascript">
$(document).ready(function(){
	$('#nome').focus();
});
</script>
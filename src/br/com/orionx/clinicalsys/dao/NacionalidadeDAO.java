package br.com.orionx.clinicalsys.dao;

import org.nextframework.controller.crud.FiltroListagem;
import org.nextframework.persistence.QueryBuilder;

import br.com.orionx.clinicalsys.bean.Nacionalidade;
import br.com.orionx.clinicalsys.filtros.NacionalidadeFiltro;
import br.com.orionx.clinicalsys.util.generics.GenericDAO;

public class NacionalidadeDAO extends GenericDAO<Nacionalidade>{

	@Override
	public void updateListagemQuery(QueryBuilder<Nacionalidade> query,FiltroListagem _filtro) {
		NacionalidadeFiltro filtro = (NacionalidadeFiltro) _filtro;
		query.whereLikeIgnoreAll("nacionalidade.nome",filtro.getNome());
		super.updateListagemQuery(query, _filtro);
	}
}
